<?php $__env->startSection('content'); ?>
<?php //echo "<pre>"; print_r($userinfo); ?>
<main id="main">
   <!-- ======= Blog Section ======= -->
   <section id="blog" class="blog mt-5a" style="padding-bottom: 0px;">
      <div class="">
         <div class="row m-0 p-0">
            <div class="container emp-profile">
               <form method="POST" action="<?php echo e(route('user_update')); ?>" enctype="multipart/form-data">
                  <?php echo csrf_field(); ?>
                  <input type="hidden" value="<?php echo e($userinfo->id); ?>" name="id">
                  <div class="row row-botm">
                     <div class="col-md-2" style="padding-left: 0px;">
                        <label class=newbtn>
                          <?php if($userinfo->profile_pic ==''): ?>         
                                <img id="blah" src="<?php echo e(url('/public/uploads/profile_img')); ?>/default.png" alt=""/  style="width: 200px;">      
                          <?php else: ?>
                                <img id="blah" src="<?php echo e(url('/public/uploads/profile_img')); ?>/<?php echo e($userinfo->profile_pic); ?>" style="width: 200px;" >     
                          <?php endif; ?>
                          <input id="pic" class='pis' onchange="editreadURL(this);" type="file" name="user_image">
                          <input type="hidden" name="oldimage" value="<?php echo e($userinfo->profile_pic); ?>">
                        </label>
                     </div>
                     <div class="col-md-6">
                        <div class="profile-head">
                           <h5>
                              <?php echo e($userinfo->first_name); ?>

                           </h5>
                           <h6>
                              Web Developer and Designer
                           </h6>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-6">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                           <div class="row">
                              <div class="col-md-5">
                                 <label>First Name</label>
                              </div>
                              <div class="col-md-6">
                                 <div class=" login-filed">
                                    <div class="form-group">
                                       <input id="first_name" type="text" class="form-control" name="first_name" required autocomplete="first_name" placeholder="first_name" value="<?php echo e($userinfo->first_name); ?>">
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-5">
                                 <label>Last Name</label>
                              </div>
                              <div class="col-md-6">
                                 <div class=" login-filed">
                                    <div class="form-group">
                                       <input id="last_name" type="text" class="form-control" name="last_name" required autocomplete="last_name" placeholder="Last name" value="<?php echo e($userinfo->last_name); ?>">
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-5">
                                 <label>Phone</label>
                              </div>
                              <div class="col-md-6">
                                 <div class=" login-filed">
                                    <div class="form-group">
                                       <input id="contact_number" type="text" class="form-control" name="contact_number" required autocomplete="contact_number" placeholder="contact_number" value="<?php echo e($userinfo->contact_number); ?>">
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-5">
                                 <label>I am polyamorous etc and looking?</label>
                              </div>
                              <div class="col-md-6">
                                 <div class="form-group">
                               <select class="form-control" id="exampleFormControlSelect1" name="looking">
                                 <option value="man" <?php if ($userinfo->i_am_looking == "man") echo "selected='selected'";?>>I am looking for a man</option>
                                 <option value="woman" <?php if ($userinfo->i_am_looking == "woman") echo "selected='selected'";?>>woman </option>
                                 <option value="regardless" <?php if ($userinfo->i_am_looking == "regardless") echo "selected='selected'";?>>Regardless Age between(24-35) </option>
                                 <option value="single" <?php if ($userinfo->i_am_looking == "single") echo "selected='selected'";?>>Marital status single </option>
                                 <option value="divorced" <?php if ($userinfo->i_am_looking == "divorced") echo "selected='selected'";?>>separated or divorced </option>
                                 <option value="widowed" <?php if ($userinfo->i_am_looking == "widowed") echo "selected='selected'";?>>widowed </option>
                                 <option value="none children" <?php if ($userinfo->i_am_looking == "none children") echo "selected='selected'";?>>same I am Children none</option>
                                 <option value="no matter" <?php if ($userinfo->i_am_looking == "no matter") echo "selected='selected'";?>>no matter I have</option>
                                 <option value=" yes children">Desire for children must be there yes </option>
                                 <option value="no want" <?php if ($userinfo->i_am_looking == "no want") echo "selected='selected'";?>>no I want</option>
                               </select>
                             </div>
                              </div>
                           </div>
                        </div>
                          <div class="row mt-5">
                           <div class="col-md-12">
                            <h5> External characteristics</h5>
                            <p>my partner should be </p>
                           </div>
                           <div class="col-md-5">
                              <label>Skin type</label>
                           </div>
                           <div class="col-md-6">
                               <div class="form-group">
                               <select class="form-control" id="exampleFormControlSelect1" name="skin_type">
                                 <option value="Central European" <?php if ($userinfo->skin_type == "Central European") echo "selected='selected'";?>> Central European </option>
                                 <option value="Southern European" <?php if ($userinfo->skin_type == "Southern European") echo "selected='selected'";?>>Southern European</option>
                                 <option value="Asian" <?php if ($userinfo->skin_type == "Asian") echo "selected='selected'";?>>Asian </option>
                                 <option value="Black"  <?php if ($userinfo->skin_type == "Black") echo "selected='selected'";?>>Black</option>
                                 <option value="Mixed Type" <?php if ($userinfo->skin_type == "Mixed Type") echo "selected='selected'";?>>Mixed Type  </option>
                               </select>
                             </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-5">
                              <label>Height between</label>
                           </div>
                           <div class="col-md-6">
                            <div class="form-group">
                               <select class="form-control" id="exampleFormControlSelect1" name="height">
                                 <option value="25-30" <?php if ($userinfo->height == "25-30") echo "selected='selected'";?>> 25-30</option>
                                 <option value="30-40" <?php if ($userinfo->height == "30-40") echo "selected='selected'";?>>30-40 </option>
                               </select>
                             </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-5">
                              <label>Figure</label>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                               <select class="form-control" id="exampleFormControlSelect1" name="figure">
                                 <option value="slim" <?php if ($userinfo->figure == "slim") echo "selected='selected'";?>> slim </option>
                                 <option value="muscular" <?php if ($userinfo->figure == "muscular") echo "selected='selected'";?>> muscular  </option>
                                 <option value="athletic" <?php if ($userinfo->figure == "athletic") echo "selected='selected'";?>> athletic </option>
                                 <option value="normal" <?php if ($userinfo->figure == "normal") echo "selected='selected'";?>> normal</option>
                                 <option value="potbellied"<?php if ($userinfo->figure == "potbellied") echo "selected='selected'";?>> potbellied</option>
                                 <option value="chubby" <?php if ($userinfo->figure == "chubby") echo "selected='selected'";?>> chubby </option>   
                               </select>
                             </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-5">
                              <label>Hair color </label>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                               <select class="form-control" id="exampleFormControlSelect1" name="hair_color">
                                 <option value="light" <?php if ($userinfo->hair_color == "light") echo "selected='selected'";?>>light </option>
                                 <option value="dark" <?php if ($userinfo->hair_color == "dark") echo "selected='selected'";?>>dark</option>
                                 <option value="red" <?php if ($userinfo->hair_color == "red") echo "selected='selected'";?>> red  </option>
                                 <option value="indifferent" <?php if ($userinfo->hair_color == "indifferent") echo "selected='selected'";?>> indifferent </option>
                               </select>
                             </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-5">
                              <label>Hair length bald </label>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                               <select class="form-control" id="exampleFormControlSelect1" name="hair_length">
                                 <option value="short" <?php if ($userinfo->hair_length == "short") echo "selected='selected'";?>>short</option>
                                 <option value="long" <?php if ($userinfo->hair_length == "long") echo "selected='selected'";?>>long</option>
                                 <option value="indifferent" <?php if ($userinfo->hair_length == "indifferent") echo "selected='selected'";?>> indifferent I have</option>
                               </select>
                             </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-5">
                              <label>I have </label>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                               <select class="form-control" id="exampleFormControlSelect1" name="beard">
                                 <option value="Facial" <?php if ($userinfo->beard == "Facial") echo "selected='selected'";?>>Facial hair without   </option>
                                 <option value="beard" <?php if ($userinfo->beard == "beard") echo "selected='selected'";?>> beard  </option>
                                 <option value="full beard" <?php if ($userinfo->beard == "full beard") echo "selected='selected'";?>>  full beard</option>
                                 <option value="moustache" <?php if ($userinfo->beard == "moustache") echo "selected='selected'";?>>  moustache </option>
                               </select>
                             </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-5">
                              <label> I have  </label>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                               <select class="form-control" id="exampleFormControlSelect1" name="tattoos">
                                 <option value="tattoos without" <?php if ($userinfo->tattoos == "tattoos without") echo "selected='selected'";?>> tattoos without  </option>
                                 <option value="not visible" <?php if ($userinfo->tattoos == "not visible") echo "selected='selected'";?>>  not visible  </option>
                                 <option value="absolutely" <?php if ($userinfo->tattoos == "absolutely") echo "selected='selected'";?>> absolutely</option>
                               </select>
                             </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-5">
                              <label> indifferent I have </label>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                               <select class="form-control" id="exampleFormControlSelect1" name="piercings">
                                 <option value="Piercings without" <?php if ($userinfo->piercings == "Piercings without") echo "selected='selected'";?>> Piercings without  </option>
                                 <option value="Pierced ears without" <?php if ($userinfo->piercings == "Pierced ears without") echo "selected='selected'";?>>Pierced ears without   </option>
                                 <option value="not on the face" <?php if ($userinfo->piercings == "not on the face") echo "selected='selected'";?>> not on the face  </option>
                                 <option value="absolutely" <?php if ($userinfo->piercings == "absolutely") echo "selected='selected'";?>>absolutely  </option>
                               </select>
                             </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6">
                      
                        <div class="row">
                           <div class="col-md-5">
                              <label>indifferent I have </label>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                               <select class="form-control" id="exampleFormControlSelect1">
                                 <option> Cosmetic surgery without  </option>
                                 <option> not important  </option>                                 
                               </select>
                             </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-5">
                              <label>not important I have </label>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                               <select class="form-control" id="exampleFormControlSelect1">
                                 <option> Character traits  </option>
                                 <option> preferences </option>
                                </select>
                             </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-5">
                              <label>Hobbies </label>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                               <select class="form-control" id="exampleFormControlSelect1" name="hobbies">
                                 <option value="same" <?php if ($userinfo->hobbies == "same") echo "selected='selected'";?>>should be the same   </option>
                                 <option value="hobbies" <?php if ($userinfo->hobbies == "hobbies") echo "selected='selected'";?>>no matter My hobbies  </option>
                                 <option value="pets" <?php if ($userinfo->hobbies == "pets") echo "selected='selected'";?>>Pets must have  </option>
                               </select>
                             </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-5">
                              <label>Pets must have   </label>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                               <select class="form-control" id="exampleFormControlSelect1" name="pets">
                                 <option value="like" <?php if ($userinfo->pets == "like") echo "selected='selected'";?>>like   </option>
                                 <option value="same" <?php if ($userinfo->pets == "same") echo "selected='selected'";?>> same  </option>
                                </select>
                             </div>
                           </div>
                        </div>
                          <div class="row">
                           <div class="col-md-5">
                              <label> Travel  </label>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                               <select class="form-control" id="exampleFormControlSelect1" name="travel">
                                 <option value="city trip" <?php if ($userinfo->travel == "city trip") echo "selected='selected'";?>> Travel must like to travel city trip   </option>
                                 <option value="other" <?php if ($userinfo->travel == "other") echo "selected='selected'";?>> other continents   </option>
                                 <option value="beach vacations" <?php if ($userinfo->travel == "beach vacations") echo "selected='selected'";?>> beach vacations I do hiking vacations  </option>
                                 <option value="sports vacations" <?php if ($userinfo->travel == "sports vacations") echo "selected='selected'";?>> sports vacations   </option>
                                 <option value="cultural vacations"<?php if ($userinfo->travel == "cultural vacations") echo "selected='selected'";?>>  cultural vacations   </option>
                                 <option value="camping vacations" <?php if ($userinfo->travel == "camping vacations") echo "selected='selected'";?>> camping vacations   </option>
                                 <option value="tent vacations" <?php if ($userinfo->travel == "tent vacations") echo "selected='selected'";?>> tent vacations</option>
                                
                                </select>
                             </div>
                           </div>
                        </div>
                          <div class="row">
                           <div class="col-md-5">
                              <label>Occupation   </label>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                               <select class="form-control" id="exampleFormControlSelect1" name="occupation">
                                 <option value="normal" <?php if ($userinfo->occupation == "normal") echo "selected='selected'";?>> must have normal working hours   </option>
                                 <option value="irregularly" <?php if ($userinfo->occupation == "irregularly") echo "selected='selected'";?>>  work irregularly  </option>
                                </select>
                             </div>
                           </div>
                        </div>
                          <div class="row">
                           <div class="col-md-5">
                              <label> food   </label>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                               <select class="form-control" id="exampleFormControlSelect1" name="food">
                                 <option value="vegetarian" <?php if ($userinfo->food == "vegetarian") echo "selected='selected'";?>>  I work food vegetarian  </option>
                                 <option value="non-vegetarian" <?php if ($userinfo->food == "non-vegetarian") echo "selected='selected'";?>>  non-vegetarian </option>
                                </select>
                             </div>
                           </div>
                        </div>
                          <div class="row">
                           <div class="col-md-5">
                              <label> don't care I am religion any </label>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                               <select class="form-control" id="exampleFormControlSelect1" name="religion">
                                 <option value="Christian"<?php if ($userinfo->religion == "Christian") echo "selected='selected'";?>> Christian   </option>
                                 <option value="Muslim"  <?php if ($userinfo->religion == "Muslim") echo "selected='selected'";?>> Muslim   </option>
                                 <option value="etc"  <?php if ($userinfo->religion == "etc") echo "selected='selected'";?>> etc</option>
                                </select>
                             </div>
                           </div>
                        </div>
                          <div class="row">
                           <div class="col-md-5">
                              <label>  Outing  </label>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                               <select class="form-control" id="exampleFormControlSelect1" name="outing">
                                 <option value="no_matter" <?php if ($userinfo->outing == "no_matter") echo "selected='selected'";?>> Outing doesn't matter   </option>
                                 <option value="partygoer" <?php if ($userinfo->outing == "partygoer") echo "selected='selected'";?>>  partygoer  </option>
                                 <option value="at_home" <?php if ($userinfo->outing == "at_home") echo "selected='selected'";?>> at home </option>
                                 <option value="friends" <?php if ($userinfo->outing == "friends") echo "selected='selected'";?>> friends </option>
                                </select>
                             </div>
                           </div>
                        </div>
                          <div class="row">
                           <div class="col-md-5">
                              <label>I amtype macho    </label>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                               <select class="form-control" id="exampleFormControlSelect1" name="macho">
                                 <option value="romantic" <?php if ($userinfo->macho == "romantic") echo "selected='selected'";?>>romantic</option>
                                 <option value="introvert" <?php if ($userinfo->macho == "introvert") echo "selected='selected'";?>> introvert   </option>
                                 <option value="extrovert" <?php if ($userinfo->macho == "extrovert") echo "selected='selected'";?>> extrovert </option>
                                </select>
                             </div>
                           </div>
                        </div>
                          <div class="row">
                           <div class="col-md-5">
                              <label>  I am smoker  </label>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                               <select class="form-control" id="exampleFormControlSelect1" name="smoker">
                                 <option value="smoker" <?php if ($userinfo->smoker == "smoker") echo "selected='selected'";?>> smoker   </option>
                                 <option value="non-smoker" <?php if ($userinfo->smoker == "non-smoker") echo "selected='selected'";?>> non-smoker   </option>
                                 <option value="whatever" <?php if ($userinfo->smoker == "whatever") echo "selected='selected'";?>> whatever </option>
                                </select>
                             </div>
                           </div>
                        </div>
                          <div class="row">
                           <div class="col-md-5">
                              <label> I am Place of residence  </label>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                               <select class="form-control" id="exampleFormControlSelect1" name="residence">
                                 <option value="city" <?php if ($userinfo->residence == "city") echo "selected='selected'";?>>  City  </option>
                                 <option value="country" <?php if ($userinfo->residence == "country") echo "selected='selected'";?>> Country   </option>
                                 <option value="other" <?php if ($userinfo->residence == "other") echo "selected='selected'";?>> For the right partner I also move (e.g.) from Bern to Zurich For the right partner I also move from the country to the city</option>
                                </select>
                             </div>
                           </div>
                        </div>
                          <div class="row">
                           <div class="col-md-5">
                              <label> Drinking   </label>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                               <select class="form-control" id="exampleFormControlSelect1" name="drinking">
                                 <option value="beer" <?php if ($userinfo->drinking == "beer") echo "selected='selected'";?>>beer</option>
                                 <option value="wine" <?php if ($userinfo->drinking == "wine") echo "selected='selected'";?>>wine</option>
                                 <option value="no_alcohol" <?php if ($userinfo->drinking == "no_alcohol") echo "selected='selected'";?>> no alcohol </option>
                                </select>
                             </div>
                           </div>
                        </div>
                          <div class="row">
                           <div class="col-md-5">
                              <label> Sex   </label>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                               <select class="form-control" id="exampleFormControlSelect1"
                                name="sex">
                                 <option value="several times a week" <?php if ($userinfo->sex == "several times a week") echo "selected='selected'";?>>several times a week    </option>
                                 <option value="several times a month" <?php if ($userinfo->sex == "several times a month") echo "selected='selected'";?>> several times a month   </option>
                                 <option value="no matter" <?php if ($userinfo->sex == "no matter") echo "selected='selected'";?>> no matter </option> 
                                </select>
                             </div>
                           </div>
                        </div> 
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-12">
                        <center>  <input type="submit" name="submit" class="neslat-update" value="Update"></center>
                     </div>
                  </div>
               </form>
            </div>
         </div>
         <!-- End blog entries list -->
      </div>
      </div>
   </section>
   <!-- End Blog Section -->
</main>


<!-- End #main -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home3/phpserver2/public_html/4twoo/resources/views/user/edit_user_profile.blade.php ENDPATH**/ ?>