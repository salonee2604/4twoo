<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>4Twoo</title>
  <meta content="" name="description">
  <meta content="" name="keywords">
  <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>" />
  <!-- Favicons -->
  <link href="<?php echo e(url('/public/assets/img/logo vector.png')); ?>" rel="icon">
  <link href="<?php echo e(url('/public/assets/img/logo vector.png')); ?>" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="<?php echo e(url('/public/assets/vendor/bootstrap/css/bootstrap.min.css')); ?>" rel="stylesheet">
  <link href="<?php echo e(url('/public/assets/vendor/icofont/icofont.min.css')); ?>" rel="stylesheet">
  <link href="<?php echo e(url('/public/assets/vendor/boxicons/css/boxicons.min.css')); ?>" rel="stylesheet">
  <link href="<?php echo e(url('/public/assets/vendor/animate.css/animate.min.css')); ?>" rel="stylesheet">
  <link href="<?php echo e(url('/public/assets/vendor/remixicon/remixicon.css')); ?>" rel="stylesheet">
  <link href="<?php echo e(url('/public/assets/vendor/owl.carousel/assets/owl.carousel.min.css')); ?>" rel="stylesheet">
  <link href="<?php echo e(url('/public/assets/vendor/venobox/venobox.css')); ?>" rel="stylesheet">
  <link href="<?php echo e(url('/public/assets/vendor/aos/aos.css')); ?>" rel="stylesheet">

  <link href="<?php echo e(url('/public/assets/css/style.css')); ?>" rel="stylesheet">


</head>

<body>

  <!-- ======= Top Bar ======= -->
  <div id="topbar" class="d-none d-lg-flex align-items-center fixed-top ">
    <div class="container d-flex align-items-center">
         <div class="top-oscal">
       <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
       <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
       <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
      </div>
      <div class="contact-info ml-auto">
        <ul>
          <?php if(!@Auth::user()->id): ?>
          <li><a href="<?php echo e(url('/login')); ?>">ANMELDEN <img src="<?php echo e(url('/public/assets/img/user-ac.png')); ?>"></a> </li>
          <li><a href="<?php echo e(url('/register')); ?>">REGISTRIEREN<img src="<?php echo e(url('/public/assets/img/usv.png')); ?>"></a> </li>
          <?php endif; ?>
          <?php if(@Auth::user()->id): ?>
          <li><a href="<?php echo e(url('user/dashboard')); ?>">Dashboard</a>  </li>
          <li><a href="<?php echo e(url('user/logout')); ?>">Ausloggen</a>  </li>
          <?php endif; ?>
        </ul>
      </div>
   
    </div>
  </div>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top ">
    <div class="container d-flex align-items-center">

    
       <a href="<?php echo e(url('/')); ?>" class="logo mr-auto scrollto"><img src="<?php echo e(url('/public/assets/img/logo vector.png')); ?>" alt="" class="img-fluid"></a>

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li class="active"><a href="#header">Tischreservation</a></li>
          <li><a href="#about">über 4two</a></li>
        
         <!--  <li class="drop-down"><a href="">Drop Down</a>
            <ul>
              <li><a href="#">Drop Down 1</a></li>
          
              <li><a href="#">Drop Down 2</a></li>
              <li><a href="#">Drop Down 3</a></li>
              <li><a href="#">Drop Down 4</a></li>
            </ul>
          </li> -->
          <li><a href="#contact">Veranstaltungen</a></li>

        </ul>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->

 <?php echo $__env->yieldContent('content'); ?>

  <!-- ======= Footer ======= -->
  <footer id="footer">

 
    <div class="footer-top">
      <div class="container">
        <div class="row">
          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Unsere Services</h4>
            <ul>
              <li><a href="#">Reservierung</a></li>
              <li><a href="#">Onlinereservierung</a></li>
              <li><a href="#">Wetter</a></li>
              <li><a href="#">Passende</a></li>
              <li><a href="#">Verkehr</a></li>
              <li><a href="#">Tickets</a></li>
            </ul>
          </div> 
          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Über uns</h4>
            <ul>
              <li><a href="<?php echo e(url('/contact')); ?>">Kontakt</a></li>
              <li><a href="<?php echo e(url('/impressum')); ?>">Impressum</a></li>
              <li><a href="<?php echo e(url('/privacy')); ?>">Datenschutz</a></li>
              <li><a href="<?php echo e(url('/career')); ?>">Karriere</a></li>
              <li><a href="<?php echo e(url('/faq')); ?>">FAQ</a></li>
            </ul>
          </div> 
          <div class="col-lg-4 col-md-6 footer-contact">
            <h4>Anmeldung für Newsletter</h4>
              <div class="" id="newsmsg" style="color: green;font-size: 20px;"></div>
              <form>
                  <div class="form-group newslate">
                    <input type="text" class="form-control" name="subject" id="subscribe_email" placeholder="Enter email" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject">
                    <span id="newserror" style="color: rgb(236 27 35);"></span>
                    <div class="validate"></div>
                  </div>

                  <a href="#" class="neslat" id="send_newsletter"> senden &nbsp;<img src="<?php echo e(url('/public/assets/img/send-cv.png')); ?>"></a>
              </form>
          </div>

          <div class="col-lg-3 col-md-6 footer-info">
            <h3>FOLGE UNS</h3>
            <div class="social-links mt-3">
              <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
              <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
              <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
            </div>
             <p>Laden Sie unsere App herunter auf </p>

              <div class="app-box-footer">
                  <a href="#"><img src="<?php echo e(url('/public/assets/img/go-ply.png')); ?>"></a>
                  <a href="#"><img src="<?php echo e(url('/public/assets/img/andor.png')); ?>"></a>
              </div>
          </div>
          <div class="col-md-12 bg-btn">
         <center><a href="<?php echo e(url('/')); ?>"><img src="<?php echo e(url('/public/assets/img/logo vector.png')); ?>" style="width: 100px;"></a></center>
        </div>
      </div>
      </div>
    </div>

    <div class="container">
      <div class="row">
        <div class="col-md-6">
      <div class="copyright">
        &copy; Copyright 2021  by<strong><span> 4TWO</span></strong>. All Rights Reserved
      </div></div>
       <div class="col-md-6">
      <div class="credits">
   
       <a href="#"><img src="<?php echo e(url('/public/assets/img/payment.png')); ?>"></a>
      </div>
    </div>
    </div>
  </footer><!-- End Footer -->


  <!-- Vendor JS Files -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
  <!-- <script src="<?php echo e(url('/public/assets/vendor/jquery/jquery.min.js')); ?>"></script> -->
  <script src="<?php echo e(url('/public/assets/vendor/jquery.easing/jquery.easing.min.js')); ?>"></script>
  <script src="<?php echo e(url('/public/assets/vendor/owl.carousel/owl.carousel.min.js')); ?>"></script>
  <script src="<?php echo e(url('/public/assets/vendor/venobox/venobox.min.js')); ?>"></script>
  <script src="<?php echo e(url('/public/assets/vendor/isotope-layout/isotope.pkgd.min.js')); ?>"></script>
  <script src="<?php echo e(url('/public/assets/vendor/aos/aos.js')); ?>"></script>
  <script src="<?php echo e(url('/public/assets/js/main.js')); ?>"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/additional-methods.min.js"></script>

  <script type="text/javascript">
  var site_url = "<?php echo url('/'); ?>";
   $("#UserForm").validate({
      
    rules: {
        name: {
                required: true,
            },
        last_name: {
            required: true,
        },
        username: {
            required: true,
        },
        email: {
                required: true,
                email: true,
                 
            }, 
        dob : {
          required: true,
        },

        address : {
          required: true,
        },

        contact_number: {
          required: true,
          minlength:10,
          maxlength:10
        },
        password: {
           required: true,

         },
       password_confirmation: {
           required: true,
           equalTo:'#password',
         },
       gender: {
         required: true,
       },        
    },
    messages: {
      name: {
        required: "First name is required",
      },
      last_name: {
        required: "Last name is required",
      },
      username: {
        required: "User name is required",
      },
      email: {
          required: "Email id is required",
          email: "Please enter valid email",
        },
      dob: {
          required: "Date of birth is required",
        },

      address: {
          required: "Address is required",
        },

      contact_number: {
          required: "Phone number is required",
        },

      password : {
          required : "Password is required",
        },
      password_confirmation : {
          required : "Password is required",
          equalTo : "Confirm password does not match",
        },
      gender : {
          required : "Gender is required",
        },  
         
    },
    submitHandler: function(form) {
     $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
      
      $.ajax({
        url: site_url+'/user_save' ,
        type: "POST",
        data: $('#UserForm').serialize(),
        success: function( response ) {
            $('#std_btn').html('Submit');
            if(response.status==false){
              $('#msg').html('<p style="text-align:center;color:red">'+response.msg+'</p>');
            }else{
              $('#msg').html('<p style="text-align:center;color:green">'+response.msg+'</p>');
            }
          
            if(response.status==true){
              document.getElementById("UserForm").reset();
              setTimeout(function(){
              $('#msg').hide();
                window.location.href = site_url+'/question_answer/'+user_id;
              },5000);
          }
          
        }
      });
    }
  })

  $("#UserLoginForm").validate({
      
    rules: {
        
        email: {
                required: true,
                email: true,
                 
            }, 
        password: {
           required: true,

         },
           
    },
    messages: {
     
      email: {
          required: "Email id is required",
          email: "Please enter valid email",
        },
      
      password : {
          required : "Password is required",
        },
    },
    // submitHandler: function(form) {
    // }
  })

  jQuery(document).ready(function(){  
    jQuery("#send_newsletter").click(function(event){
        event.preventDefault();
       var subscribe_email= jQuery('#subscribe_email').val();
 
       if (subscribe_email != null && subscribe_email != ''){
 
         if(IsEmail(subscribe_email)==false){
          $('#newserror').html('Please enter valid email address').fadeIn('slow');
          $('#newserror').delay(5000).fadeOut('slow');
           return false;
         }
 
          jQuery.ajaxSetup({ headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') } });
         jQuery.ajax({
          url:"newsletter",
          type:"POST",
          data:{'subscribe_email':subscribe_email,'_token' :'<?php echo csrf_token(); ?>' },
          success:function(response){
             $('#subscribe_email').val(''); 
             $('#newsmsg').html(response.message).fadeIn('slow');
             $('#newsmsg').delay(5000).fadeOut('slow');
          },
       });
       }else{
         $('#newserror').html('Please enter email address').fadeIn('slow');
         $('#newserror').delay(5000).fadeOut('slow');
       }
       
    });
  });
         
  function IsEmail(email) {
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if(!regex.test(email)) {
      return false;
    }else{
      return true;
    }
  }

  $('#change_pass').validate({
    rules: {
       old_password: {
        required: true,
      },
      password: {
        required: true,
        
      },
      confirm_password:{
        required : true,
        equalTo : '#pwd',
      }
    },
    messages :{
      old_password : {
        required : 'Old password is required',
      },
      password : {
        required : 'Password is required',
      },
      confirm_password :{
        required : 'Confirm that the password is required',
        equalTo : 'Confirm that the password should match the password'
      }
    },
    submitHandler:function(form){
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      $.ajax({
        url: site_url+'/update_password_action' ,
        type: "POST",
        data: $('#change_pass').serialize(),
        success: function( response ) {
            $('#std_btn').html('Submit');
            if(response.success==false){
              $('#msg').html('<p style="text-align:center;color:red">'+response.message+'</p>');
            }else{
              $('#msg').html('<p style="text-align:center;color:green">'+response.message+'</p>');
            }
          
            if(response.status==true){
              document.getElementById("change_pass").reset();
              setTimeout(function(){
              $('#msg').hide();
                window.location.reload(true);
              },5000);
          }
        }
      });
    }
  });

  $('.newbtn').bind("click" , function () {
      $('#pic').click();
  });
 
  function editreadURL(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();
          reader.onload = function (e) {
              $('#blah')
              .attr('src', e.target.result);
          };
          reader.readAsDataURL(input.files[0]);
      }
  }

  $('.profile-img').bind("click" , function () {
      $('#user_pic').click();
  });
 
  function readURL(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();
          reader.onload = function (e) {
              $('#user_blah')
              .attr('src', e.target.result);
          };
          reader.readAsDataURL(input.files[0]);
      }
  }
  
  $(document).ready(function(){
    $(document).on('change', '#user_pic', function(){
      var name = document.getElementById("user_pic").files[0].name;
      var form_data = new FormData();
      form_data.append("user_pic", document.getElementById('user_pic').files[0]);
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
      $.ajax({
        url:site_url+'/UploadProfilePicture',
        method:"POST",
        //headers: {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
        data: form_data,
        contentType: false,
        cache: false,
        processData: false,
        beforeSend:function(){
         $('#uploaded_image').html("<label class='text-success'>Image Uploading...</label>");
        },   
        success:function(data)
        {
         $('#uploaded_image').html('<p style="text-align:center;color:green">'+data.msg+'</p>');
            setTimeout(function(){
               $('#msg').hide();
             },5000);
        }

      });
    });
  });
  
  $('#profile_creation').on('submit', function(event){
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
      
      $.ajax({
        //url: "<?php echo e(url('/user_update')); ?>",
        url:"<?php echo e(route('user_update')); ?>",
        type: "POST",
        //headers: {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
        data: $('#profile_creation').serialize(),
        contentType: false,
        cache: false,
        processData: false,
        success: function( response ) {
            if(response.status==false){
              $('#msg').html('<p style="text-align:center;color:red">'+response.msg+'</p>');
            }else{
              $('#msg').html('<p style="text-align:center;color:green">'+response.msg+'</p>');

            } 
            if(response.status==true){ 
              setTimeout(function(){
              $('#msg').hide();
                window.location.href = site_url;
              },5000);
          }
        }
      });
  });

 

  $(document).ready(function(){
     $(".send_request").click(function(evt){
       evt.preventDefault();
       var to_id =  $(this).attr("data-id");
       jQuery.ajax({
          url:"send_request",
          type:"POST",
          data:{'to_id':to_id,'_token' :'<?php echo csrf_token(); ?>' },
          success:function(response){
            if(response.success==false){
              $('#reqmsg').html('<p style="text-align:center;color:red">'+response.message+'</p>');
            }else{
              $('#reqmsg').html('<p style="text-align:center;color:green">'+response.message+'</p>');
              //$('#send_request_'+to_id)
            }
          },
       });
         
     });
   });
</script>

</body>

</html><?php /**PATH /home3/phpserver2/public_html/4twoo/resources/views/layouts/app.blade.php ENDPATH**/ ?>