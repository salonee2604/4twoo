<?php $__env->startSection('content'); ?>
<style type="text/css">
    .mt-5a{


        margin-top: 90px;
    }

</style>
<main id="main">


    <!-- ======= Blog Section ======= -->
    <section id="blog" class="blog mt-5a" style="padding-bottom: 0px;">
      <div class="">

        <div class="row m-0 p-0">


<div class="col-lg-6 entries lg welcome-text">
    <h2>
Welcome,<br> Reset password to continue
</h2>
<center><img src="<?php echo e(url('/')); ?>/public/assets/img/logo vector.png" style="width: 150px;position: absolute;z-index: 99;top: 37px;left: 38%;right: 0;"></center>
</div>
          <div class="col-lg-6 entries lg">

            <article class="entry entry-single">
                <div id="msg"></div>

              <div class="reply-form">
                <h4>Reset password to continue..</h4>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                <div class="card">

                    <div class="card-header"><?php echo e(__('Reset Password')); ?></div>

                    <div class="card-body">

                        <?php if(session('status')): ?>

                            <div class="alert alert-success" role="alert">

                                <?php echo e(session('status')); ?>


                            </div>

                        <?php endif; ?>

                        <form method="POST" action="<?php echo e(route('password.email')); ?>">

                            <?php echo csrf_field(); ?>



                            <div class="form-group row">

                                <label for="email" class="col-md-4 col-form-label text-md-right"><?php echo e(__('E-Mail Address')); ?></label>



                                <div class="col-md-6">

                                    <input id="email" type="email" class="form-control <?php if ($errors->has('email')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('email'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" name="email" value="<?php echo e(old('email')); ?>" required autocomplete="email" autofocus>



                                    <?php if ($errors->has('email')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('email'); ?>

                                        <span class="invalid-feedback" role="alert">

                                            <strong><?php echo e($message); ?></strong>

                                        </span>

                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>

                                </div>

                            </div>



                            <div class="form-group row mb-0">

                                <div class="col-md-8 offset-md-4">

                                    <button type="submit" class="btn btn-primary">

                                        <?php echo e(__('Send Password Reset Link')); ?>


                                    </button>

                                </div>

                            </div>

                        </form>

                    </div>

                </div>

              </div>

            </div><!-- End blog comments -->


          </div><!-- End blog entries list -->

        </div>

      </div>
    </section><!-- End Blog Section -->

  </main><!-- End #main -->

  <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home3/phpserver2/public_html/4twoo/resources/views/auth/passwords/email.blade.php ENDPATH**/ ?>