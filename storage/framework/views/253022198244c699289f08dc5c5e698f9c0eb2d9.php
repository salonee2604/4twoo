<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		<!-- Sidebar user panel -->
		<div class="user-panel">
			<div class="pull-left image">
				<img src="<?php echo e(url('/')); ?>/resources/assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
			</div>
			<div class="pull-left info">
				<p>Admin</p> <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
			</div>
		</div>
		<!-- sidebar menu: : style can be found in sidebar.less -->
		<?php //echo Auth::user()->role_id; ?>
		<?php $menuList=App\Helpers\Helper::getMenu(); ?>
		<ul class="sidebar-menu" data-widget="tree">
			<li class="">
				<a href="<?php echo e(url('/admin/dashboard')); ?>">	<i class="fa fa-dashboard"></i>  <span>Dashboard</span>
				</a>
			</li>
			<li class="treeview">
				<a href="#">	<i class="fa fa-users"></i>  <span>Users</span>
					<span class="pull-right-container">

				  <i class="fa fa-angle-left pull-right"></i>

				</span>
				</a>
				<ul class="treeview-menu">
					<li class="active"><a href="<?php echo e(url('admin/user_list')); ?>"><i class="fa fa-circle-o"></i>Users list</a>
					</li>
					<li class="active"><a href="<?php echo e(url('admin/add_user')); ?>"><i class="fa fa-circle-o"></i>Add User</a>
					</li>
				</ul>
			</li>

			<li class="treeview">
				<a href="#"><i class="fa fa-heart"></i>  <span>Success Stories</span>
					<span class="pull-right-container">

				  <i class="fa fa-angle-left pull-right"></i>

				</span>
				</a>
				<ul class="treeview-menu">
					<li class="active"><a href="<?php echo e(url('admin/story_list')); ?>"><i class="fa fa-circle-o"></i>Stories list</a>
					</li>
					<li class="active"><a href="<?php echo e(url('admin/add_story')); ?>"><i class="fa fa-circle-o"></i>Add Story</a>
					</li>
				</ul>
			</li>

			<li class="treeview">
				<a href="#"><i class="fa fa-quote-left"></i><span>Testimonials</span>
					<span class="pull-right-container">

				  <i class="fa fa-angle-left pull-right"></i>

				</span>
				</a>
				<ul class="treeview-menu">
					<li class="active"><a href="<?php echo e(url('admin/testimonial_list')); ?>"><i class="fa fa-circle-o"></i>Testimonials list</a>
					</li>
					<li class="active"><a href="<?php echo e(url('admin/add_testimonial')); ?>"><i class="fa fa-circle-o"></i>Add Testimonial</a>
					</li>
				</ul>
			</li>

			<li class="treeview">
				<a href="#"><i class="fa fa-question"></i><span>Questionnaire</span>
					<span class="pull-right-container">

				  	<i class="fa fa-angle-left pull-right"></i>

					</span>
				</a>
				<ul class="treeview-menu">
					<li class="active"><a href="<?php echo e(url('admin/question_list')); ?>"><i class="fa fa-circle-o"></i>Question list</a>
					</li>
					<li class="active"><a href="<?php echo e(url('admin/add_question')); ?>"><i class="fa fa-circle-o"></i>Add Question</a>
					</li>
				</ul>
			</li>
			<li class="treeview">
				<a href="#"><i class="fa fa-group"></i><span>Table Management</span>
					<span class="pull-right-container">

				  	<i class="fa fa-angle-left pull-right"></i>

					</span>
				</a>
				<ul class="treeview-menu">
					<li class="active"><a href="<?php echo e(url('admin/TableBooking')); ?>"><i class="fa fa-circle-o"></i>Table Type</a>
					</li>
					<li class="active"><a href="<?php echo e(url('admin/tableList')); ?>"><i class="fa fa-circle-o"></i>Table Management</a>
					</li>
					<li class="active" class="treeview"><a href="#"><i class="fa fa-circle-o"></i>Time Slot Management</a>
						<ul class="treeview-menu">
							<li class="active"><a href="<?php echo e(url('admin/timeSlotList/monday')); ?>"><i class="fa fa-circle-o"></i>Monday</a></li>
							<li class="active"><a href="<?php echo e(url('admin/timeSlotList')); ?>"><i class="fa fa-circle-o"></i>Tuesday</a></li>
							<li class="active"><a href="<?php echo e(url('admin/timeSlotList')); ?>"><i class="fa fa-circle-o"></i>Wednesday</a></li>
							<li class="active"><a href="<?php echo e(url('admin/timeSlotList')); ?>"><i class="fa fa-circle-o"></i>Thursday</a> </li>
							<li class="active"><a href="<?php echo e(url('admin/timeSlotList')); ?>"><i class="fa fa-circle-o"></i>Friday</a></li>
							<li class="active"><a href="<?php echo e(url('admin/timeSlotList')); ?>"><i class="fa fa-circle-o"></i>Saturday</a></li>
							<li class="active"><a href="<?php echo e(url('admin/timeSlotList')); ?>"><i class="fa fa-circle-o"></i>Sunday</a></li>
						</ul>
					</li>
					<li class="active"><a href="<?php echo e(url('admin/booktable_list')); ?>"><i class="fa fa-circle-o"></i>Table Booking Management</a>
					</li>
					<li class="active"><a href="#"><i class="fa fa-circle-o"></i>Table Status</a>
					</li>
					<li class="active"><a href="#"><i class="fa fa-circle-o"></i>Table Enquiry</a>
					</li>
				</ul>
			</li>


			<?php /*if(Auth::check() && Auth::user()->role_id ==4) { if(!empty($menuList)) { foreach($menuList as $key=>$menuListVal){ $checkmenuPermission = App\Helpers\Helper::checkPermission($menuListVal->id,Auth::user()->id); if(empty($menuListVal->is_child)) { if($checkmenuPermission) { ?>
			<li><a href="{{url('/'.$menuListVal->url)}}"><i class="fa fa-dashboard"></i> <span>{{$menuListVal->menu_name}}</span></a>
			</li>
			<?php } }else { if($checkmenuPermission) { ?>
			<li class="treeview">
				<a href="#">	<i class="fa fa-users"></i>  <span>{{$menuListVal->menu_name}}</span>
					<span class="pull-right-container">

								  <i class="fa fa-angle-left pull-right"></i>

								</span>
				</a>
				<ul class="treeview-menu">
					<?php foreach($menuListVal->is_child as $key=>$childMenuVal){ $checkchildPermission = App\Helpers\Helper::checkPermission($childMenuVal->id,Auth::user()->id); if($checkchildPermission) { ?>
					<li class="active"><a href="{{url('/'.$childMenuVal->url)}}"><i class="fa fa-circle-o"></i>{{$childMenuVal->menu_name}}</a>
					</li>
					<?php } } ?>
				</ul>
			</li>
			<?php } } } } ?>
			<?php }else{ ?>
			<?php if(!empty($menuList)) { foreach($menuList as $key=>$menuListVal){ ?>
			<?php if(empty($menuListVal->is_child)) { ?>
			<li><a href="{{url('/'.$menuListVal->url)}}"><i class="fa fa-dashboard"></i> <span>{{$menuListVal->menu_name}}</span></a>
			</li>
			<?php } else {?>
			<li class="treeview">
				<a href="#">	<i class="fa fa-users"></i>  <span>{{$menuListVal->menu_name}}</span>
					<span class="pull-right-container">

								  <i class="fa fa-angle-left pull-right"></i>

								</span>
				</a>
				<ul class="treeview-menu">
					<?php foreach($menuListVal->is_child as $key=>$childMenuVal){ ?>
					<li class="active"><a href="{{url('/'.$childMenuVal->url)}}"><i class="fa fa-circle-o"></i>{{$childMenuVal->menu_name}}</a>
					</li>
					<?php } ?>
				</ul>
			</li>
			<?php } ?>
			<?php } } ?>
			<?php } */?>
		</ul>
	</section>
	<!-- /.sidebar -->
</aside><?php /**PATH /home3/phpserver2/public_html/4twoo/resources/views/admin/layout/sidebar.blade.php ENDPATH**/ ?>