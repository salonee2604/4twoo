<?php $__env->startSection('content'); ?>

<?php //echo "<pre>"; print_r($userinfo); ?>

  <main id="main">
 
    <!-- ======= Blog Section ======= -->
    <section id="blog" class="blog mt-5a" style="padding-bottom: 0px;">
      <div class="">
        
        <div class="row m-0 p-0">
            <div class="container emp-profile">
                <div id="uploaded_image"></div>
                <form method="post">
                    <?php echo csrf_field(); ?>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="profile-img">
                                <?php if($userinfo->profile_pic ==''): ?>         
                                      <img id="user_blah" src="<?php echo e(url('/public/uploads/profile_img')); ?>/default.png" alt=""/>      
                                <?php else: ?>
                                      <img id="user_blah" src="<?php echo e(url('/public/uploads/profile_img')); ?>/<?php echo e($userinfo->profile_pic); ?>" alt=""/>      
                                <?php endif; ?>
                                <div class="file btn btn-lg btn-primary">
                                    Change Photo
                                    <input id="user_pic" class='user_pic' type="file" name="user_pic" onchange="readURL(this);" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="profile-head">
                                        <h5>
                                            <?php echo e($userinfo->first_name); ?>

                                        </h5>
                                        <h6>
                                            Web Developer and Designer
                                        </h6>
                                      
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">About</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Timeline</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <a href="<?php echo e(url('/edituser/dashboard/')); ?>" class="profile-edit-btn" name="btnAddMore">Edit Profile</a>

                            <a href="<?php echo e(url('/changepassword')); ?>" class="profile-edit-btn" name="btnAddMore">ChangePaasword</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="profile-work">
                                <p>WORK LINK</p>
                                <a href="">Website Link</a><br/>
                                <a href="">Bootsnipp Profile</a><br/>
                                <a href="">Bootply Profile</a>
                                <p>SKILLS</p>
                                <a href="">Web Designer</a><br/>
                                <a href="">Web Developer</a><br/>
                                <a href="">WordPress</a><br/>
                                <a href="">WooCommerce</a><br/>
                                <a href="">PHP, .Net</a><br/>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="tab-content profile-tab" id="myTabContent">
                                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>User Id</label>
                                                </div>
                                                <div class="col-md-6">
                                                    <p><?php echo e($userinfo->id); ?> </p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>Name</label>
                                                </div>
                                                <div class="col-md-6">
                                                    <p><?php echo e($userinfo->first_name); ?> <?php echo e($userinfo->last_name); ?></p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>Email</label>
                                                </div>
                                                <div class="col-md-6">
                                                    <p><?php echo e($userinfo->email); ?></p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>Phone</label>
                                                </div>
                                                <div class="col-md-6">
                                                    <p><?php echo e($userinfo->contact_number); ?></p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>Profession</label>
                                                </div>
                                                <div class="col-md-6">
                                                    <p>Web Developer and Designer</p>
                                                </div>
                                            </div>
                                </div>
                                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>Experience</label>
                                                </div>
                                                <div class="col-md-6">
                                                    <p>Expert</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>Hourly Rate</label>
                                                </div>
                                                <div class="col-md-6">
                                                    <p>10$/hr</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>Total Projects</label>
                                                </div>
                                                <div class="col-md-6">
                                                    <p>230</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>English Level</label>
                                                </div>
                                                <div class="col-md-6">
                                                    <p>Expert</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>Availability</label>
                                                </div>
                                                <div class="col-md-6">
                                                    <p>6 months</p>
                                                </div>
                                            </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label>Your Bio</label><br/>
                                            <p>Your detail description</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>           
            </div>
        </div><!-- End blog entries list -->

        </div> 
    </section><!-- End Blog Section -->

  </main><!-- End #main -->

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home3/phpserver2/public_html/4twoo/resources/views/user/user.blade.php ENDPATH**/ ?>