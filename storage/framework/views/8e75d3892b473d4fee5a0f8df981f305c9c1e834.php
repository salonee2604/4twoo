<?php $__env->startSection('content'); ?>
<style type="text/css">
    .mt-5a{


        margin-top: 90px;
    }

</style>
<main id="main">


    <!-- ======= Blog Section ======= -->
    <section id="blog" class="blog mt-5a" style="padding-bottom: 0px;">
      <div class="">

        <div class="row m-0 p-0">


<div class="col-lg-6 entries lg welcome-text">
    <h2>
Welcome,<br> Sign in to continue
</h2>
<center><img src="<?php echo e(url('/')); ?>/public/assets/img/logo vector.png" style="width: 150px;position: absolute;z-index: 99;top: 37px;left: 38%;right: 0;"></center>
</div>
          <div class="col-lg-6 entries lg">

            <article class="entry entry-single">
                <div id="msg"></div>

              <div class="reply-form">
                <h4>Sign up to continue..</h4>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
            
                 <form method="POST" id="UserForm" name="userForm" action="javascript:void(0)">

                        <?php echo csrf_field(); ?>

                        <div class="row login-filed">

                            <div class="col-md-6 form-group">

                                <input id="name" type="text" class="form-control <?php if ($errors->has('name')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('name'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" name="name" autofocus placeholder="First Name*">

                                <?php if ($errors->has('name')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('name'); ?>

                                    <span class="invalid-feedback" role="alert">

                                        <strong><?php echo e($message); ?></strong>

                                    </span>

                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>

                            </div>

                       

                       

                            <div class="col-md-6 form-group">

                                <input id="last_name" type="text" class="form-control <?php if ($errors->has('last_name')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('last_name'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" name="last_name" required autofocus placeholder="Last Name*">

                                <?php if ($errors->has('last_name')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('last_name'); ?>

                                    <span class="invalid-feedback" role="alert">

                                        <strong><?php echo e($message); ?></strong>

                                    </span>

                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>

                            </div>

                        </div>

                        <div class="row login-filed">

                            <div class="col-md-6 form-group">

                                <input id="email" type="email" class="form-control <?php if ($errors->has('email')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('email'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" name="email" value="<?php echo e(old('email')); ?>" required autocomplete="email" placeholder="E-Mail Address*">

                                <?php if ($errors->has('email')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('email'); ?>

                                    <span class="invalid-feedback" role="alert">

                                        <strong><?php echo e($message); ?></strong>

                                    </span>

                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>

                            </div>

                       

                            <div class="col-md-6 form-group">
                                <input id="dob" type="date" class="form-control" name="dob" required autocomplete="dob" placeholder="Date Of birth*" data-date="" data-date-format="yyyy-mm-dd">
                            </div>

                          <?php if ($errors->has('dob')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('dob'); ?>

                                <span class="invalid-feedback" role="alert">

                                    <strong><?php echo e($message); ?></strong>

                                </span>

                            <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>

                        </div>

                        <div class="row login-filed">

                            <div class="col-md-12 form-group">
                                <input id="address" type="text" class="form-control" name="address" required autocomplete="address" placeholder="Address*">
                            </div>

                            <?php if ($errors->has('address')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('address'); ?>

                                <span class="invalid-feedback" role="alert">

                                    <strong><?php echo e($message); ?></strong>

                                </span>

                            <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>

                        </div>

                        <div class="row login-filed">

                            <div class="col-md-12 form-group">
                                <input id="contact_number" type="text" class="form-control" name="contact_number" required autocomplete="contact_number" placeholder="Phone number*">
                            </div>

                          <?php if ($errors->has('contact_number')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('contact_number'); ?>

                                <span class="invalid-feedback" role="alert">

                                    <strong><?php echo e($message); ?></strong>

                                </span>

                            <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>

                        </div>

                        <div class="row login-filed">

                            <div class="col-md-6 form-group">

                                <input id="password" type="password" class="form-control <?php if ($errors->has('password')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('password'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" name="password" required autocomplete="new-password" placeholder="Password*">

                                <?php if ($errors->has('password')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('password'); ?>

                                    <span class="invalid-feedback" role="alert">

                                        <strong><?php echo e($message); ?></strong>

                                    </span>

                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>

                            </div>

                       

                            <div class="col-md-6 form-group">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Confirm Password*">
                            </div>

                        </div>

                        <div class="row login-filed">
                            <div class="col-md-12 form-group">
                                <select name="gender" id="gender" class="form-control">
                                  <option value="">Select Gender*</option>
                                  <option value="male">Male</option>
                                  <option value="female">Female</option>
                                  <option value="other">Other</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row mb-0">

                            <div class="col-md-6">

                                <button type="submit" class="btn btn-primary">

                                    <?php echo e(__('Register')); ?>


                                </button>

                            </div>

                        </div>

                    </form>

              </div>

            </div><!-- End blog comments -->


          </div><!-- End blog entries list -->

        </div>

      </div>
    </section><!-- End Blog Section -->

  </main><!-- End #main -->

  
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home3/phpserver2/public_html/4twoo/resources/views/auth/register.blade.php ENDPATH**/ ?>