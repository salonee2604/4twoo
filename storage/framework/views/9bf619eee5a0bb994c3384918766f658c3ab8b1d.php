<?php $__env->startSection('content'); ?>

<!-- <section class="sec_inner-regid">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
          <div class="heading-sec main-headings">
          		<h1><span>Change</span><strong>password</strong></h1>
          </div>
          <div class="cnsmr-profile">
          <form id="change_pass" method="POST">
            <input type="hidden" name="user_id" value="<?php echo e($user_id); ?>">
            <div class="form-group">
                <label for="email">Old Password:</label>
                <input type="password" class="form-control" name="old_password" id="old_password">
            </div>
    			  <div class="form-group">
    			    <label for="email">New Password:</label>
    			    <input type="password" class="form-control" name="password" id="pwd" oonpaste="return false;" ondrop="return false;">
    			  </div>
    			  <div class="form-group">
    			    <label for="pwd">Confirm Password:</label>
    			    <input type="password" class="form-control" name="confirm_password" id="con_pwd" onCopy="return false">
    			  </div>
  			    <button type="submit" class="btn btn-primary waves-effect">Submit</button>
			   </form>
   		  </div>
   	  </div>
    </div> 
  </div>      	
</section> -->
  <main id="main">
    <!-- ======= Blog Section ======= -->
    <section id="blog" class="blog mt-5a" style="padding-bottom: 0px;">
      <div class="">
        <div class="row m-0 p-0">
        <div class="container emp-profile">
           <center><h3 class="mb-5">Change Paasword</h3></center>
            <div id="msg"></div>
            <form id="change_pass" method="post">
              <?php echo csrf_field(); ?>
              <input type="hidden" name="user_id" value="<?php echo e($user_id); ?>">
                <div class="row align-items-center justify-content-center">
                <div class="col-md-6">
                  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                      <div class="row">
                          <div class="col-md-5">
                              <label>Old password</label>
                          </div>
                          <div class="col-md-7">
                              <div class=" login-filed">
                        <div class="form-group">
                            <input id="old_password" type="password" class="form-control" name="old_password"  placeholder="Old password">
                        </div>

                          </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-md-5">
                              <label>New password</label>
                          </div>
                          <div class="col-md-7">
                               <div class=" login-filed">
                        <div class="form-group">
                            <input type="password" class="form-control" name="password" id="pwd" oonpaste="return false;" ondrop="return false;" placeholder="New password">
                        </div>

                          </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-md-5">
                              <label>Confirm password</label>
                          </div>
                          <div class="col-md-7">
                             <div class=" login-filed">
                        <div class="form-group">
                            <input type="password" class="form-control" name="confirm_password" id="con_pwd" onCopy="return false" placeholder="Confirm password">
                        </div>

                          </div>
                          </div>
                      </div>
                  </div>
                </div>
                </div>
                  <div class="row">
                    <div class="col-md-12">
                    <center> <button type="submit" class="neslat-update mt-4">Update</button></center>  
                    </div>
                  </div>
            </form>           
        </div>
        </div><!-- End blog entries list -->
        </div>
      </div>
    </section><!-- End Blog Section -->
  </main><!-- End #main -->
<script type="text/javascript">
   /*$(document).ready(function() {
     $('#pwd').bind("cut copy paste", function(event) {
       event.preventDefault();
     });
   })/*/
   //myElement.addEventListener('paste', e => e.preventDefault());
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home3/phpserver2/public_html/4twoo/resources/views/user/changepassword.blade.php ENDPATH**/ ?>