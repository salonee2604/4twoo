<?php $__env->startSection('title', 'Edit Testimonial'); ?>

<?php $__env->startSection('current_page_css'); ?>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Edit testimonial
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">Testimonial</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <?php if($message = Session::get('message')): ?>
       <div class="alert alert-success alert-block">  
        <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong><?php echo e($message); ?></strong>
      </div>
      <?php endif; ?>

      <?php if($message = Session::get('error')): ?>
      <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong><?php echo e($message); ?></strong>
      </div>
      <?php endif; ?>

      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-xs-12">
          <!-- SELECT2 EXAMPLE -->
        <div class="box box-default">
          <div class="box-header pull-right">
                <a href="<?php echo e(url('admin/testimonial_list')); ?>" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Back </a>
          </div>
          <!-- /.box-header -->
          <form action="<?php echo e(url('/admin/update_testimonial')); ?>" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="_token" id="csrf-token" value="<?php echo e(csrf_token()); ?>" />
            <div class="box-body">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Title</label>
                    <input type="text" class="form-control" name="title" value="<?php echo e((!empty($testimonial_info->title) ? $testimonial_info->title : '')); ?>" placeholder="Enter Title">
                  </div>
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Content</label>
                    <textarea class="form-control" name="content" placeholder="Enter Content"><?php echo e((!empty($testimonial_info->content) ? $testimonial_info->content : '')); ?></textarea>
                  </div>
  
                  <!-- /.form-group --> 
                  <div class="form-group">
                    <label>Image</label>
                    <img src="<?php echo e(url('/public/assets/img/'.$testimonial_info->image)); ?>" style="width:100px;height:100px;" id="image_change">
                    <input name="image" type="file" accept="image/*" onchange="document.getElementById('image_change').src = window.URL.createObjectURL(this.files[0])">
                  </div>
                  <!-- /.form-group -->

                  <input type="hidden" class="form-control" name="testimonial_id" value="<?php echo e((!empty($testimonial_info->id) ? $testimonial_info->id : '')); ?>">
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
              </div>
            </div>
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            <!-- /.row -->
          </form>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('current_page_js'); ?>
<script type="text/javascript">
 $(function() {
    $('.multiselect-ui').multiselect({
        includeSelectAllOption: true
    });
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home3/phpserver2/public_html/4twoo/resources/views/admin/testimonial/edit_testimonial.blade.php ENDPATH**/ ?>