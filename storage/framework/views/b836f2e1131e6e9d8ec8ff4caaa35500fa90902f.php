<?php $__env->startSection('title', 'Add Question'); ?>



<?php $__env->startSection('current_page_css'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

 <!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">

    <!-- Content Header (Page header) -->

    <section class="content-header">

      <h1>Edit Time Slot</h1>

      <ol class="breadcrumb">

        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

        <li class="active">Edit Time Slot</li>

      </ol>

    </section>

    <!-- Main content -->

    <section class="content">

      <?php if($message = Session::get('message')): ?>

       <div class="alert alert-success alert-block">  

        <button type="button" class="close" data-dismiss="alert">×</button> 

        <strong><?php echo e($message); ?></strong>

      </div>

      <?php endif; ?>

    

     <!-- Small boxes (Stat box) -->

      <div class="row">

        <div class="col-xs-12">

          <!-- SELECT2 EXAMPLE -->

        <div class="box box-default">

          <div class="box-header">

             <div class="box-tools pull-right">

                <a href="<?php echo e(url('admin/timeSlotList')); ?>" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Back </a>

            </div>

          </div>

          <!-- /.box-header -->

          <form action="<?php echo e(url('/admin/update_timeslot')); ?>" id="studentForm" method="POST" enctype="multipart/form-data">

            <input type="hidden" name="quns_type" value="1">
            <input type="hidden" name="_token" id="csrf-token" value="<?php echo e(csrf_token()); ?>" />

            <div class="box-body">

              <div class="row">

                <div class="col-md-6">
                  
                            
                          <div class="form-group">
                            <label for="table_number">Start Time</label><span class="error">*</span>
                             <div class="input-group"><div class="input-group-addon">
                                          <i class="fa fa-clock-o"></i>
                             </div>
                             <input type="text"  class="time form-control" id="start_time"  name="start_time" placeholder="Start Time"value="<?php echo e($Tableslot->start_time); ?>">
                              <input type="hidden"  class="form-control" id="time_slot_id" placeholder="Table Number" name="time_slot_id" value="<?php echo e((!empty($Tableslot->time_slot_id) ? $Tableslot->time_slot_id : '')); ?>" min="1">
                            <?php if($errors->has('start_time')): ?>
                            <span class="help-block <?php echo e($errors->has('start_time') ? ' has-error' : ''); ?>">
                            <strong><?php echo e($errors->first('start_time')); ?></strong>
                            </span>
                            <?php endif; ?>
                          </div>   
                          <div class="form-group">
                             <label for="menu_compare_price">End Time</label>
                                            <div class="input-group"><div class="input-group-addon">
                                          <i class="fa fa-clock-o"></i>
                                        </div>
                                            <input type="text" class="time form-control" id="end_time" name="end_time" placeholder="End Time" value="<?php echo e($Tableslot->end_time); ?>">
                             <?php if($errors->has('table_seet_capacity')): ?>
                            <span class="help-block <?php echo e($errors->has('table_seet_capacity') ? ' has-error' : ''); ?>">
                            <strong><?php echo e($errors->first('table_seet_capacity')); ?></strong>
                            </span>
                            <?php endif; ?>
                          </div>   
                          <div class="form-group">
                            <label for="status">Status</label>
                               <input type="radio"  name="status"  id="status" value="1" checked> Active
                              <input type="radio" name="status" id="status" value="0"> Inactive
                           </div> 
                  

                  
                  
                  
                </div>

                <!-- /.col -->

              </div>

            </div>
            </div>
          </div>
            <div class="box-footer">

              <button type="submit" class="btn btn-primary">Submit</button>

            </div>

            <!-- /.row -->

          </form>

          </div>

          <!-- /.box-body -->

        </div>

        <!-- /.box -->

      </div>

      <!-- /.row -->



    </section>

    <!-- /.content -->

  </div>

  <!-- /.content-wrapper -->

<?php $__env->stopSection(); ?>



<?php $__env->startSection('current_page_js'); ?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-datetimepicker/2.7.1/css/bootstrap-material-datetimepicker.min.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-datetimepicker/2.7.1/js/bootstrap-material-datetimepicker.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/locale/ja.js"></script>
<script src="https://openlayers.org/api/2.10/OpenLayers.js" type="text/javascript">
</script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css" rel="stylesheet"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.4/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
<script>
   $('#end_time_input').prop('disabled', true);
   $('#start_time').datetimepicker({
   format: 'HH:mm',
   useCurrent: false,
   });
   
   $('#end_time').datetimepicker({
   format: 'HH:mm',
   useCurrent: false,
   });
   
   $("#start_time").on("dp.change", function (e) {
   $('#end_time_input').prop('disabled', false);
   if( e.date ){
     $('#end_time').data("DateTimePicker").date(e.date.add(1, 'h'));
   }
   
   $('#end_time').data("DateTimePicker").minDate(e.date);
   });
</script>
<!--  <script type="text/javascript">
    $(document).ready(function()
    {

      $('.time').bootstrapMaterialDatePicker
      ({
        date: false,
        shortTime: false,
        format: 'HH:mm'
      });
      $.material.init()
    });
    </script>
 -->
<?php $__env->stopSection(); ?>


<?php echo $__env->make('admin.layout.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home3/phpserver2/public_html/4twoo/resources/views/admin/tablebooking/edit_timeslot.blade.php ENDPATH**/ ?>