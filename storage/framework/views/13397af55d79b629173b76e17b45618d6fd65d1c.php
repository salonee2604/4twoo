<?php $__env->startSection('title', 'Add Question'); ?>



<?php $__env->startSection('current_page_css'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

 <!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">

    <!-- Content Header (Page header) -->

    <section class="content-header">

      <h1>Add Table Reservation</h1>

      <ol class="breadcrumb">

        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

        <li class="active">Add Table Reservation</li>

      </ol>

    </section>

    <!-- Main content -->

    <section class="content">

      <?php if($message = Session::get('message')): ?>

       <div class="alert alert-success alert-block">  

        <button type="button" class="close" data-dismiss="alert">×</button> 

        <strong><?php echo e($message); ?></strong>

      </div>

      <?php endif; ?>

    

     <!-- Small boxes (Stat box) -->

      <div class="row">

        <div class="col-xs-12">

          <!-- SELECT2 EXAMPLE -->

        <div class="box box-default">

          <div class="box-header">

             <div class="box-tools pull-right">

                <a href="<?php echo e(url('admin/booktable_list')); ?>" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Back </a>

            </div>

          </div>

          <!-- /.box-header -->

             <div class="box-body">
                 <div class="row">
                <!-- left column -->
                  <div class="col-md-12">

                     <form class="forms-sample" id="add_table_booking" name="add_table_booking" enctype='multipart/form-data' method="post" action="<?php echo e(url('admin/table_booking_post')); ?>">

                     <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">

                   <div class="row">
                    <div id="msg"></div>
                      <!-- left column -->
                        <div class="col-md-6">
                          
                          <div class="form-group">
                            <label for="seat_capacity">Customer Type</label><span class="error">*</span>
                            <select class="form-control" id="customer_type" name="">
                              <option value="">--Select Customer Type--</option>
                              <option value="1">Existing User</option>
                              <option value="2">New User</option>

                            </select>
 <!--<input type="radio"  name="customer_type"  id="exist_customer" value="1"> Existing Customer
                              <input type="radio" name="customer_type" id="new_customer" value="2"> New Customer -->   
                              </div>   

                               <div class="form-group" style="display:none;" id="fetch_username">
                            <label for="section_name">Customer Name</label><span class="error">*</span>
                             <select class="form-control"  id="exist_username" name="exist_username">
                              <option value="">--Select User Name--</option>
                              <?php $__currentLoopData = $userlist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k => $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($v->id); ?>"><?php echo e($v->username); ?></option>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                          </div>

                         

                           <div class="form-group" style="display:none;" id="userid">
                            <label for="table_number">Customer Name</label><span class="error">*</span>
                            <input type="text" class="form-control" id="user_name" placeholder="User Name" name="fullname" autocomplete="off">
                          </div>

                           <div class="form-group" style="display:none;" id="email_address">
                            <label for="table_number">Email</label><span class="error">*</span>
                            <input type="text" class="form-control" id="email" placeholder="Email" name="email" autocomplete="off">
                          </div>

                          <div class="form-group" style="display:none;" id="contact">
                            <label for="table_number">Mobile No.</label><span class="error">*</span>
                            <input type="text" class="form-control" id="mobile_no" placeholder="Mobile No." name="mobile" autocomplete="off" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" minlength="8"  maxlength="12">
                          </div>
                          <div class="form-group">
                            <label for="table_number">Booking Date</label><span class="error">*</span>
                            <input type="text" class="form-control datepicker" id="reservation_date" placeholder="Booking Date" name="reservation_date" autocomplete="off">
                            
                          </div>   

                           <div class="form-group">
                            <label for="section_name">Time Slot</label><span class="error">*</span>
                             <select class="form-control hasDatepicker valid" id="time_slot_id" name="time_slot_id">
                              <option value="">--Select Time Slot--</option>
                            </select>
                          </div>   

                             

                             <div class="form-group">
                            <label for="seat_capacity">Adults</label><span class="error">*</span>
                            <input type="number"  class="form-control" id="adult" placeholder="Adults" name="adult" value="" autocomplete="off" min="1" step="1"/>
                          </div>   


                             <div class="form-group">
                            <label for="seat_capacity">Children</label><span class="error">*</span>
                            <input type="number"  class="form-control" id="children" placeholder="Children" name="children" value="" autocomplete="off" min="0" step="1"/>
                          </div>   
                       
                       <div class="form-group">
                            <label for="seat_capacity">Message</label><span class="error">*</span>
                            <textarea class="form-control" id="message" placeholder="Message" name="message" maxlength="200"></textarea>
                          </div>   

                      <div class="margin-top">        
                         <button type="submit" class="btn btn-primary mr-2" id="reservation_btn">Add Reservation</button> 

                          </div>
                        </div>
                         <!--  <button class="btn btn-light">Cancel</button> -->
                      </div>
                   
                      </form>
                      </div>
                  </div>
               
                
              </div>

          </div>

          <!-- /.box-body -->

        </div>

        <!-- /.box -->

      </div>

      <!-- /.row -->



    </section>

    <!-- /.content -->

  </div>

  <!-- /.content-wrapper -->

<?php $__env->stopSection(); ?>



<?php $__env->startSection('current_page_js'); ?>

<script type="text/javascript">

 

</script>
<script type="text/javascript">
$(function () {
             $("#customer_type").change(function () {
          
            if ($(this).val() == "1") {

                $("#fetch_username").show();
            } else {
                $("#fetch_username").hide();
            }
            
             if ($(this).val() == "2") {
                $("#userid").show();
            } else {
                $("#userid").hide();
            }
             if ($(this).val() == "2") {
                $("#email_address").show();
            } else {
                $("#email_address").hide();
            }
             if ($(this).val() == "2") {
                $("#contact").show();
            } else {
                $("#contact").hide();
            }
            
         });
    });
</script>
<script>
  $('#add_table_booking'). on("change","#reservation_date",function(){

      
        var rest_id = 1;
        var date = $(this).val();
        $.ajax({
          dataType: "html",
          method:"post", 
          url:"<?php echo e(url('/')); ?>/admin/ajax_reservation_timeslot",  
          data:{rest_id:rest_id,date:date,"_token": "<?php echo e(csrf_token()); ?>"}, 
           success:function(response)
           {       
                alert("hiiiii");
                if(response)
                {
                  $('#time_slot_id').html(response);  

                }else{
                  $("#time_slot_id").html("<option> not available</option>");
                }
           }
              // }
        });
       });
   

</script>
<script>
  $('.datepicker').datepicker({
    format: 'yyyy-mm-dd',
    startDate: '-3d'
});
</script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('admin.layout.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home3/phpserver2/public_html/4twoo/resources/views/admin/tablebooking/add_table_booking.blade.php ENDPATH**/ ?>