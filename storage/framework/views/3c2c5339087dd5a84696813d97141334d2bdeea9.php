<?php $__env->startSection('title', 'Questions List'); ?>

<?php $__env->startSection('current_page_css'); ?>

<!-- DataTables -->

<link rel="stylesheet" href="<?php echo e(url('/')); ?>/resources/assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<link rel="stylesheet" href="<?php echo e(url('/')); ?>/resources/assets/css/bootstrap-toggle.min.css">

<style>

    .error{

        color: red;

    }

</style>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('current_page_js'); ?>

<!-- DataTables -->

<script src="<?php echo e(url('/')); ?>/resources/assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>

<script src="<?php echo e(url('/')); ?>/resources/assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script src="<?php echo e(url('/')); ?>/resources/assets/js/bootstrap-toggle.min.js"></script>

<script type="text/javascript">


$(function () {

  $('#question_list').DataTable({

    'paging'      : true,

    'lengthChange': false,

    'searching'   : true,

    'ordering'    : true,

    'info'        : true,

    'autoWidth'   : false

  })

})



jQuery(function() {

    var table = jQuery('#question_list').DataTable();



    jQuery("#btnExport").click(function(e) {

        table.page.len(-1).draw();

        window.open('data:application/vnd.ms-excel,' +

            encodeURIComponent(jQuery('#question_list').parent().html()));

        setTimeout(function() {

            table.page.len(10).draw();

        }, 1000)



    });

});

</script>

<script type="text/javascript">

 function delete_question(question_id){

  $.ajaxSetup({

    headers: {

      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

    }

  });

  $.ajax({

   type: 'POST',

   url: "<?php echo url('/admin/delete_question'); ?>",

   enctype: 'multipart/form-data',

   data:{question_id:question_id,'_token':'<?php echo csrf_token(); ?>'},

     beforeSend:function(){

       return confirm("Are you sure you want to delete this item?");

     },

     success: function(resultData) { 

       console.log(resultData);

       var obj = JSON.parse(resultData);

       if (obj.status == 'success') {

          $('#success_message').fadeIn().html(obj.msg);

          setTimeout(function() {

            $('#success_message').fadeOut("slow");

          }, 2000 );

          $("#row" + question_id).remove();

       }

     },

     error: function(errorData) {

      console.log(errorData);

      alert('Please refresh page and try again!');

    }

  });

} 

</script>

<script>

  $(document).on("change",'.toggle-class', function() {

    var status = $(this).prop('checked') == true ? 1 : 0; 

    var question_id = $(this).data('id'); 


    $.ajax({

      type: "GET",

      dataType: "json",

      url: "<?php echo url('/admin/change_question_status'); ?>",

      data: {'status': status, 'question_id': question_id},

      success: function(data){

        $('#success_message').fadeIn().html(data.success);

        setTimeout(function() {

          $('#success_message').fadeOut("slow");

        }, 2000 );

      },

      error: function(errorData) {

        console.log(errorData);

        alert('Please refresh page and try again!');

      }

    });

  })

</script>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

 <!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">

    <!-- Content Header (Page header) -->

    <section class="content-header">

      <h1>Table Type</h1>

      <ol class="breadcrumb">

        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

        <li class="active">Table Type</li>

      </ol>

    </section>

    <!-- Main content -->

    <section class="content">

      <p style="display: none;" id="success_message" class="alert alert-success"></p>

      <?php if($errors->any()): ?>

      <div class="alert alert-danger">

       <ul>

         <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

         <li><?php echo e($error); ?></li>

         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

       </ul>

     </div>

     <?php endif; ?>

      <?php if(Session::has('error_arr')): ?>

          <?php $error_arr = Session::get('error_arr'); ?>

          <div class="alert alert-info">

            <ul>

              <?php

              for($i=0; $i < count($error_arr); $i++){

                if(!empty($error_arr[$i])){

                  ?><li><?php echo e($error_arr[$i]); ?></li><?php

                }

              }

              ?>

            </ul>

          </div>

          <?php Session::forget('error_arr'); ?>

      <?php endif; ?>



     <?php if(Session::has('message')): ?>

     <p class="alert <?php echo e(Session::get('alert-class', 'alert-success')); ?>"><?php echo e(Session::get('message')); ?></p>

     <?php endif; ?>



     <?php if(Session::has('error')): ?>

     <p class="alert <?php echo e(Session::get('alert-class', 'alert-danger')); ?>"><?php echo e(Session::get('error')); ?></p>

     <?php endif; ?>

      <div class="row">

        <div class="col-xs-12">

          <div class="box">

            <div class="box-header">

             <div class ="row">

               <div class="col-md-6">

                <!-- <form action="<?php echo e(url('admin/StoryImportData')); ?>" class="form-horizontal" method="POST" enctype="multipart/form-data">

                  <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">

                  <div class="col-md-5">

                    <div class="form-group">

                    <input type="file" name="import_file" />

                    </div>

                  </div>

                  <button class="btn btn-primary">Submit</button>

                </form> -->

               </div>

                <div class="col-md-6">

                 <h3 class="box-title pull-right btn-toolbar"><a href="<?php echo e(url('admin/addSection')); ?>" class="btn btn-primary">Add Table Type</a></h3>
                </div>

             </div>

            </div>

            <!-- /.box-header -->

            <div class="box-body">

              <table id="question_list" class="table table-bordered table-striped">

                <thead>
                  <tr>
                    <th>S.No.</th>
                    <th>Type</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                   <?php $i=0; ?>
                <?php $__currentLoopData = $tablesections; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php $i++;?>
                <tr>
                  <td><?php echo e($i); ?></td>
                  <td><?php echo e($row->section_name); ?></td>
                  <td>

              <div class="link-del-view link_one">
                        <a href="<?php echo e(url('/admin/viewsection')); ?>/<?php echo e(base64_encode($row->section_id)); ?>">View</a> |
                        <a href="<?php echo e(url('admin/editsection', $row->section_id)); ?>" class="btn btn-outline-primary">Edit</a>|
                        <a href="javascript:void(0)" onclick="delete_section('<?php echo $row->section_id; ?>');">Delete</a>
                   
                         
                      </div>
                    </td>
                 
                  </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>

              </table>

            </div>

            <!-- /.box-body -->

          </div>

          <!-- /.box -->

        </div>

      </div>

        <!-- /.col -->

      <!-- /.row -->

    </section>

    <!-- /.content -->

  </div>
  <script type="text/javascript">

 function delete_section(section_id){

  
  $.ajaxSetup({

    headers: {

      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

    }

  });

  $.ajax({

   type: 'POST',

   url: "<?php echo url('/admin/delete_section'); ?>",

   enctype: 'multipart/form-data',

   data:{section_id:section_id,'_token':'<?php echo csrf_token(); ?>'},

     beforeSend:function(){

       return confirm("Are you sure you want to delete this Section?");

     },

     success: function(resultData) { 

       console.log(resultData);

       var obj = JSON.parse(resultData);

       if (obj.status == 'success') {

          $('#success_message').fadeIn().html(obj.msg);

          setTimeout(function() {

            $('#success_message').fadeOut("slow");

          }, 2000 );
          location.reload();
          // $("#row" + story_id).remove();


       }

     },

     error: function(errorData) {

      console.log(errorData);

      alert('Please refresh page and try again!');

    }

  });

} 

</script>

  <!-- /.content-wrapper -->

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home3/phpserver2/public_html/4twoo/resources/views/admin/listSection.blade.php ENDPATH**/ ?>