<footer class="main-footer">
	<div class="pull-right hidden-xs"></div>
	<?php $logoImg=App\Helpers\Helper::getLogoImg(); ?> <strong>Copyright &copy; <?php echo date('Y')?> <a href="#"><?php echo !empty($logoImg->title)? $logoImg->title:'4Twoo';?></a>.</strong> All rights reserved.</footer>
<!-- Control Sidebar --><?php /**PATH /home3/phpserver2/public_html/4twoo/resources/views/admin/layout/footer.blade.php ENDPATH**/ ?>