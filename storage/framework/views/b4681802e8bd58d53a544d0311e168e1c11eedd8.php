<?php $__env->startSection('title', 'Add Question'); ?>



<?php $__env->startSection('current_page_css'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

 <!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">

    <!-- Content Header (Page header) -->

    <section class="content-header">

      <h1>Add Table Management</h1>

      <ol class="breadcrumb">

        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

        <li class="active">Add Table</li>

      </ol>

    </section>

    <!-- Main content -->

    <section class="content">

      <?php if($message = Session::get('message')): ?>

       <div class="alert alert-success alert-block">  

        <button type="button" class="close" data-dismiss="alert">×</button> 

        <strong><?php echo e($message); ?></strong>

      </div>

      <?php endif; ?>

    

     <!-- Small boxes (Stat box) -->

      <div class="row">

        <div class="col-xs-12">

          <!-- SELECT2 EXAMPLE -->

        <div class="box box-default">

          <div class="box-header">

             <div class="box-tools pull-right">

                <a href="<?php echo e(url('admin/tableList')); ?>" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Back </a>

            </div>

          </div>

          <!-- /.box-header -->

          <form action="<?php echo e(url('/admin/submit_tablelist')); ?>" id="studentForm" method="POST" enctype="multipart/form-data">

            <input type="hidden" name="quns_type" value="1">
            <input type="hidden" name="_token" id="csrf-token" value="<?php echo e(csrf_token()); ?>" />

            <div class="box-body">

              <div class="row">

                <div class="col-md-6">
                            <div class="form-group">
                            <label for="section_name">Table Type</label><span class="error">*</span>
                             <select class="form-control" id="section_name" name="table_type">
                              <option value="">Select One</option>
                              <?php $__currentLoopData = $all_section_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                             <option value="<?php echo e((!empty($value->section_id) ? $value->section_id : '')); ?>" ><?php echo e($value->section_name); ?></option>
                             <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                            <?php if($errors->has('table_type')): ?>
                            <span class="help-block <?php echo e($errors->has('table_type') ? ' has-error' : ''); ?>">
                            <strong><?php echo e($errors->first('table_type')); ?></strong>
                            </span>
                            <?php endif; ?>

                          </div>   
                           <div class="form-group">
                            <label for="table_number">Table Number</label><span class="error">*</span>
                            <input type="number"  class="form-control" id="table_number" placeholder="Table Number" name="table_number" value="" min="1">
                            <?php if($errors->has('table_number')): ?>
                            <span class="help-block <?php echo e($errors->has('table_number') ? ' has-error' : ''); ?>">
                            <strong><?php echo e($errors->first('table_number')); ?></strong>
                            </span>
                            <?php endif; ?>
                          </div>   
                          <div class="form-group">
                            <label for="seat_capacity">Table Seating Capacity</label><span class="error">*</span>
                            <input type="number" class="form-control" id="table_seet_capacity" placeholder="Table Seating Capacity" name="table_seet_capacity" value="" min="1" max="20">
                             <?php if($errors->has('table_seet_capacity')): ?>
                            <span class="help-block <?php echo e($errors->has('table_seet_capacity') ? ' has-error' : ''); ?>">
                            <strong><?php echo e($errors->first('table_seet_capacity')); ?></strong>
                            </span>
                            <?php endif; ?>
                          </div>   
                          <div class="form-group">
                            <label for="status">Status</label>
                               <input type="radio"  name="status"  id="status" value="1" checked> Active
                              <input type="radio" name="status" id="status" value="0"> Inactive
                           </div> 
                  

                  
                  
                  
                </div>

                <!-- /.col -->

              </div>

            </div>

            <div class="box-footer">

              <button type="submit" class="btn btn-primary">Submit</button>

            </div>

            <!-- /.row -->

          </form>

          </div>

          <!-- /.box-body -->

        </div>

        <!-- /.box -->

      </div>

      <!-- /.row -->



    </section>

    <!-- /.content -->

  </div>

  <!-- /.content-wrapper -->

<?php $__env->stopSection(); ?>



<?php $__env->startSection('current_page_js'); ?>

<script type="text/javascript">

 

</script>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('admin.layout.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home3/phpserver2/public_html/4twoo/resources/views/admin/tablebooking/addTableList.blade.php ENDPATH**/ ?>