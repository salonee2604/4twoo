<?php $__env->startSection('title', 'Edit Question'); ?>

<?php $__env->startSection('current_page_css'); ?>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Edit question
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">Question</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <?php if($message = Session::get('message')): ?>
       <div class="alert alert-success alert-block">  
        <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong><?php echo e($message); ?></strong>
      </div>
      <?php endif; ?>

      <?php if($message = Session::get('error')): ?>
      <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong><?php echo e($message); ?></strong>
      </div>
      <?php endif; ?>

      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-xs-12">
          <!-- SELECT2 EXAMPLE -->
        <div class="box box-default">
          <div class="box-header pull-right">
                <a href="<?php echo e(url('admin/question_list')); ?>" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Back </a>
          </div>
          <!-- /.box-header -->

          <form action="<?php echo e(url('/admin/update_questiontruefalse')); ?>" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="_token" id="csrf-token" value="<?php echo e(csrf_token()); ?>" />
            <div class="box-body">
              <div class="row">
                <div class="col-md-6">
                  <input type="hidden" class="form-control" name="question_id" value="<?php echo e((!empty($question_info->id) ? $question_info->id : '')); ?>">
                  <div class="form-group">
                    <label>Question</label>
                    <input type="text" class="form-control" name="title" value="<?php echo e((!empty($question_info->question) ? $question_info->question : '')); ?>" placeholder="Enter Question">
                  </div>
                </div>
              </div>
            </div>
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            <!-- /.row -->
          </form>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('current_page_js'); ?>
<script type="text/javascript">
 $(function() {
    $('.multiselect-ui').multiselect({
        includeSelectAllOption: true
    });
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home3/phpserver2/public_html/4twoo/resources/views/admin/question/edit_question_truefalse.blade.php ENDPATH**/ ?>