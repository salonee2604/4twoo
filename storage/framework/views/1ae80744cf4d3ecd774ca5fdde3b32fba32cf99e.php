<!DOCTYPE html>

<html>

<head>

  <meta charset="utf-8">

  <meta http-equiv="X-UA-Compatible" content="IE=edge">

   <?php $logoImg = App\Helpers\Helper::getLogoImg();  ?>

  <link href="<?php echo e(url('/public/assets/img/logo vector.png')); ?>" rel="icon">
  <link href="<?php echo e(url('/public/assets/img/logo vector.png')); ?>" rel="apple-touch-icon">

    <title> <?php echo !empty($logoImg->title)? $logoImg->title:'4Twoo';?>| Log in</title>

  <!-- Tell the browser to be responsive to screen width -->

  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <!-- Bootstrap 3.3.7 -->

  <link rel="stylesheet" href="<?php echo e(url('/')); ?>/resources/assets/bower_components/bootstrap/dist/css/bootstrap.min.css">

  <!-- Font Awesome -->

  <link rel="stylesheet" href="<?php echo e(url('/')); ?>/resources/assets/bower_components/font-awesome/css/font-awesome.min.css">

  <!-- Ionicons -->

  <link rel="stylesheet" href="<?php echo e(url('/')); ?>/resources/assets/bower_components/Ionicons/css/ionicons.min.css">

  <!-- Theme style -->

  <link rel="stylesheet" href="<?php echo e(url('/')); ?>/resources/assets/dist/css/AdminLTE.min.css">

  <!-- iCheck -->

  <link rel="stylesheet" href="<?php echo e(url('/')); ?>/resources/assets/plugins/iCheck/square/blue.css">



  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

  <!--[if lt IE 9]>

  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>

  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

  <![endif]-->



  <!-- Google Font -->

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <style ="text/css">

   .background_img {

   width: 100%;

   height: 100vh;

   display: inline-block;
   
   background: url("../4twoo/public/assets/img/client.png") no-repeat;
 }

.background_img:before {

   background: #00000085;

   position: absolute;

   content: "";

   width: 100%;

   height: 100%;

   top: 0px;

   z-index: 0;

 }

 .login-box{

position: relative;

}
.login-btn .btn-primary {
    background-color: #c52a39;
    border-color: #c52a39;
}
.form-control:focus {
    border-color: #c52a39 !important;
    box-shadow: none;
}
.login-box-body .form-control-feedback, .register-box-body .form-control-feedback {
    color: #c52a39 !important;
}

.forget-pas {
    color: #c52a39;
    font-weight: 600;
}
.forget-pas:hover {
    color: #cc505c;
    font-weight: 600;
}
  </style>



  

</head>

<body class="hold-transition login-page">

<div class="background_img">

		<div class="login-box">

		  <div class="login-logo">

			  <!-- height="80px;" -->

			  <?php if(!empty($logo_info->homelogo_img)): ?>

				<img src="<?php echo e(url('/')); ?>/public/uploads/logo/<?php echo e($logo_info->homelogo_img); ?>"  width="100px;" alt="User Image">



			  <?php else: ?>

				<img src="<?php echo e(url('/')); ?>/public/assets/img/logo vector.png"  width="100px;" alt="User Image">

			  <?php endif; ?>

			  

		  </div>

		  <!-- /.login-logo -->

		  <div class="login-box-body">

			<p class="login-box-msg">Sign in to start your session</p>



			<form action="<?php echo e(route('login')); ?>" id="loginForm" method="post">

			  <input type="hidden" name="_token" id="csrf-token" value="<?php echo e(csrf_token()); ?>" />

			  <div class="form-group has-feedback">

				<input id="email" type="text" class="form-control<?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" name="email" value="<?php echo e(old('email')); ?>" required autofocus placeholder="Email address">

				<?php if ($errors->has('email')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('email'); ?>

					<span class="invalid-feedback" role="alert">

						<strong><?php echo e($message); ?></strong>

					</span>

				<?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>

				<span class="glyphicon glyphicon-envelope form-control-feedback"></span>

			  </div>

			  <label id="email-error" class="error" for="email"></label>

			  <div class="form-group has-feedback">

				<input id="password" type="password" class="form-control <?php if ($errors->has('password')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('password'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" name="password" required autocomplete="current-password" placeholder="Password">

				<?php if ($errors->has('password')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('password'); ?>

					<span class="invalid-feedback" role="alert">

						<strong><?php echo e($message); ?></strong>

					</span>

				<?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>

				<span class="glyphicon glyphicon-lock form-control-feedback"></span>

			  </div>

			  <label id="password-error" class="error" for="password"></label>

			  <div class="row">

				<div class="col-xs-8">

				  <div class="checkbox icheck">

					<input class="form-check-input" type="checkbox" name="remember" id="remember" <?php echo e(old('remember') ? 'checked' : ''); ?>>

					<label class="form-check-label" for="remember">

						<?php echo e(__('Remember Me')); ?>


					</label>

				  </div>

				</div>

				<!-- /.col -->

				<div class="col-xs-4 login-btn">

				  <button type="submit" name="btnLogin" class="btn btn-primary btn-block btn-flat"><?php echo e(__('Login')); ?></button>

				</div>

				<!-- /.col -->

			  </div>

			</form>
			<a href="<?php echo e(url('admin/forgot-password')); ?>" class="forget-pas"><?php echo e(__('Forgot Your Password?')); ?></a><br>

		  </div>

		  <!-- /.login-box-body -->

		</div>

</div>

<!-- /.login-box -->



<!-- jQuery 3 -->

<script src="<?php echo e(url('/')); ?>/resources/assets/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap 3.3.7 -->

<script src="<?php echo e(url('/')); ?>/resources/assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- iCheck -->

<script src="<?php echo e(url('/')); ?>/resources/assets/plugins/iCheck/icheck.min.js"></script>

<script>

  $(function () {

    $('input').iCheck({

      checkboxClass: 'icheckbox_square-blue',

      radioClass: 'iradio_square-blue',

      increaseArea: '20%' /* optional */

    });

  });

</script>

</body>

</html>

<?php /**PATH /home3/phpserver2/public_html/4twoo/resources/views/admin/admin_login.blade.php ENDPATH**/ ?>