<?php $__env->startSection('title', 'Add Testimonial'); ?>



<?php $__env->startSection('current_page_css'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

 <!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">

    <!-- Content Header (Page header) -->

    <section class="content-header">

      <h1>Add testimonial </h1>

      <ol class="breadcrumb">

        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

        <li class="active">testimonial</li>

      </ol>

    </section>

    <!-- Main content -->

    <section class="content">

      <?php if($message = Session::get('message')): ?>

       <div class="alert alert-success alert-block">  

        <button type="button" class="close" data-dismiss="alert">×</button> 

        <strong><?php echo e($message); ?></strong>

      </div>

      <?php endif; ?>

    

     <!-- Small boxes (Stat box) -->

      <div class="row">

        <div class="col-xs-12">

          <!-- SELECT2 EXAMPLE -->

        <div class="box box-default">

          <div class="box-header">

             <div class="box-tools pull-right">

                <a href="<?php echo e(url('admin/testimonial_list')); ?>" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Back </a>

            </div>

          </div>

          <!-- /.box-header -->

          <form action="<?php echo e(url('/admin/submit_testimonial')); ?>" id="studentForm" method="POST" enctype="multipart/form-data">

            <input type="hidden" name="_token" id="csrf-token" value="<?php echo e(csrf_token()); ?>" />

            <div class="box-body">

              <div class="row">

                <div class="col-md-6">

                  <div class="form-group">

                    <label>Title</label>

                    <input type="text" class="form-control" name="title" placeholder="Enter Title">

                    <?php if($errors->has('title')): ?>

                        <span class="help-block <?php echo e($errors->has('title') ? ' has-error' : ''); ?>">

                          <strong><?php echo e($errors->first('title')); ?></strong>

                        </span>

                    <?php endif; ?>

                  </div>
                  <!-- /.form-group -->
                  <div class="form-group">

                    <label>Content</label>
                    
                    <textarea class="form-control" name="content" placeholder="Enter testimonial Content"></textarea>
                    
                    <?php if($errors->has('content')): ?>
                        <span class="help-block <?php echo e($errors->has('content') ? ' has-error' : ''); ?>">
                    
                          <strong><?php echo e($errors->first('content')); ?></strong>
                    
                        </span>
                    
                    <?php endif; ?>
                  
                  </div>
                  <!-- /.form-group -->

                  <div class="form-group">

                    <label>testimonial Image</label>

                    <input type="file" class="form-control" name="image"/>

                      <?php if($errors->has('image')): ?>

                        <span class="help-block <?php echo e($errors->has('image') ? ' has-error' : ''); ?>">

                          <strong><?php echo e($errors->first('image')); ?></strong>

                        </span>

                    <?php endif; ?>

                  </div>

        

                </div>

                <!-- /.col -->

              </div>

            </div>

            <div class="box-footer">

              <button type="submit" class="btn btn-primary">Submit</button>

            </div>

            <!-- /.row -->

          </form>

          </div>

          <!-- /.box-body -->

        </div>

        <!-- /.box -->

      </div>

      <!-- /.row -->



    </section>

    <!-- /.content -->

  </div>

  <!-- /.content-wrapper -->

<?php $__env->stopSection(); ?>



<?php $__env->startSection('current_page_js'); ?>

<script type="text/javascript">

 

</script>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('admin.layout.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home3/phpserver2/public_html/4twoo/resources/views/admin/testimonial/add_testimonial.blade.php ENDPATH**/ ?>