<!DOCTYPE html>

<html>

<head>

  <meta charset="utf-8">

  <meta http-equiv="X-UA-Compatible" content="IE=edge">

   <?php $logoImg = App\Helpers\Helper::getLogoImg();  ?>

  <link href="<?php echo e(url('/public/assets/img/logo vector.png')); ?>" rel="icon">
  <link href="<?php echo e(url('/public/assets/img/logo vector.png')); ?>" rel="apple-touch-icon">

    <title> <?php echo !empty($logoImg->title)? $logoImg->title:'4Twoo';?>| Forget Password</title>

  <!-- Tell the browser to be responsive to screen width -->

  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <!-- Bootstrap 3.3.7 -->

  <link rel="stylesheet" href="<?php echo e(url('/')); ?>/resources/assets/bower_components/bootstrap/dist/css/bootstrap.min.css">

  <!-- Font Awesome -->

  <link rel="stylesheet" href="<?php echo e(url('/')); ?>/resources/assets/bower_components/font-awesome/css/font-awesome.min.css">

  <!-- Ionicons -->

  <link rel="stylesheet" href="<?php echo e(url('/')); ?>/resources/assets/bower_components/Ionicons/css/ionicons.min.css">

  <!-- Theme style -->

  <link rel="stylesheet" href="<?php echo e(url('/')); ?>/resources/assets/dist/css/AdminLTE.min.css">

  <!-- iCheck -->

  <link rel="stylesheet" href="<?php echo e(url('/')); ?>/resources/assets/plugins/iCheck/square/blue.css">



  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

  <!--[if lt IE 9]>

  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>

  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

  <![endif]-->



  <!-- Google Font -->

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <style ="text/css">

   .background_img {

   width: 100%;

   height: 100vh;

   display: inline-block;

   background: url("https://votivetech.in/4twoo/public/assets/img/client.png") no-repeat;
 }

.background_img:before {

   background: #00000085;
   position: absolute;
   content: "";
   width: 100%;
   height: 100%;
   top: 0px;
   z-index: 0;

 }

.login-box{

position: relative;

}
.form-control:focus {
    border-color: #bc202f;
    box-shadow: none;
}
.btn-primary {
    background-color: #b9222f;
    border-color: #b9222f;
    transition: 0.5s;
}
.btn-primary:hover, .btn-primary:active, .btn-primary.hover {
    background-color: #000;
    color: #b9222f;
}
.login-rest {
    text-align: center;
    font-size: 16px;
    font-weight: bold;
    margin-top: 10px;
}
.login-rest a {
 color: #b9222f;
  }
  </style>

  

</head>

<body class="hold-transition login-page">

<div class="background_img"  style="background-color: #f0f3f5;">

    <div class="login-box">

      <div class="login-logo">

        <!-- height="80px;" -->

        <?php if(!empty($logo_info->homelogo_img)): ?>

        <img src="<?php echo e(url('/')); ?>/public/uploads/logo/<?php echo e($logo_info->homelogo_img); ?>"  width="100px;" alt="User Image">



        <?php else: ?>

        <img src="<?php echo e(url('/')); ?>/public/assets/img/logo vector.png"  width="100px;" alt="User Image">

        <?php endif; ?>

        

      </div>

      <!-- /.login-logo -->

      <div class="login-box-body">

      <p class="login-box-msg">Enter the email address you used to register. We will send you an email to reset your password</p>


      <div id="msg"></div>
      <form method="POST" id="forgot_password_admin">
        
         <?php echo e(csrf_field()); ?>

        <div class="form-group has-feedback">

        <input type="email" id="admin_email" class="form-control" name="admin_email" placeholder="Email" required="" autofocus>

        <?php if ($errors->has('admin_email')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('admin_email'); ?>

          <span class="invalid-feedback" role="alert">

            <strong><?php echo e($message); ?></strong>

          </span>

        <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>

        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>

        </div>
        <div class="form-group has-feedback">

        <div class="row">

        <!-- /.col -->

        <div class="col-xs-12">

          <button type="submit" class="btn btn-primary btn-block btn-flat"><?php echo e(__('RESET MY PASSWORD')); ?></button>

        </div>

        <!-- /.col -->

        </div>

      </form>
     
     <div class="login-rest"> <a href="<?php echo e(url('admin/login')); ?>"><?php echo e(__('Login')); ?></a></div>

      </div>

      <!-- /.login-box-body -->

    </div>

</div>

<input type="hidden" name="baseurl" id="baseurl" value="<?php echo e(url('/')); ?>" />
<input type="hidden" name="siteurl" id="siteurl" value="<?php echo e(url('admin')); ?>" />

<!-- /.login-box -->



<!-- jQuery 3 -->

<script src="<?php echo e(url('/')); ?>/resources/assets/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap 3.3.7 -->

<script src="<?php echo e(url('/')); ?>/resources/assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- iCheck -->

<script src="<?php echo e(url('/')); ?>/resources/assets/plugins/iCheck/icheck.min.js"></script>

<!-- <script src="<?php echo e(url('public/adminassets/plugins/jquery/jquery.min.js')); ?>"></script> -->

<!-- Bootstrap Core Js -->
<!-- <script src="<?php echo e(url('public/adminassets/plugins/bootstrap/js/bootstrap.js')); ?>"></script> -->

<!-- Waves Effect Plugin Js -->
<!-- <script src="<?php echo e(url('public/adminassets/plugins/node-waves/waves.js')); ?>"></script> -->

<!-- Validation Plugin Js -->
<!-- <script src="<?php echo e(url('public/adminassets/plugins/jquery-validation/jquery.validate.js')); ?>"></script> -->

<!-- Custom Js -->
<!-- <script src="<?php echo e(url('public/adminassets/js/admin.js')); ?>"></script> -->


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>  
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/additional-methods.min.js"></script>

<!-- page scripts -->
<script src="<?php echo e(url('public/adminassets/js/form-validations.js')); ?>"></script>
<script>

  $(function () {

    $('input').iCheck({

      checkboxClass: 'icheckbox_square-blue',

      radioClass: 'iradio_square-blue',

      increaseArea: '20%' /* optional */

    });

  });

  $('#forgot_password_admin').validate({
  rules: {
    admin_email: {
      required: true,
      email:true
    }
  },
  messages :{
    admin_email : {
      required : 'Email ID is required',
      email : 'Please enter a valid email address'
    }
  },
  submitHandler:function(form){
    $(".btn").attr('disabled',true) ;
    $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
      
      $.ajax({
        url: "<?php echo e(route('admin/forgot-password-function')); ?>" ,
        type: "POST",
        data: $('#forgot_password_admin').serialize(),
        success: function( response ) {
            $('#std_btn').html('Submit');
            if(response.status==false){
              $('#msg').html('<p style="text-align:center;color:red">'+response.message+'</p>');
            }else{
              $('#msg').html('<p style="text-align:center;color:green">'+response.message+'</p>');
            }
          
            if(response.status==true){
              document.getElementById("forgot_password_admin").reset();
              setTimeout(function(){ 
              $('#msg').hide(); 
              },5000);
          }
          
        }
      });

  }
}); 
</script>

</body>

</html>

<?php /**PATH /home3/phpserver2/public_html/4twoo/resources/views/admin/forgot_password.blade.php ENDPATH**/ ?>