 <?php $__env->startSection('content'); ?> 
<main id="main">
  <section id="blog" class="blog mt-5a" style="padding-bottom: 0px; background-color: #f5f5f5;">
  	<div class="col-md-12 questinar">
        <h3>Match</h3> 
    </div>
    <div class="container">
      <div class="row m-0 p-0 ">
        <div class="col-md-10 text-left m-auto mt-3 mb-5 ">
          <div id="reqmsg"></div>
          <div class="row">
            <?php $__currentLoopData = $usersinfo; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
             	<div class="col-md-4 mb-5 mt-5 m-xp">
                <input type="hidden" name="to_id" value="<?php echo e($user->id); ?>">
               	<a href="<?php echo e(url('/viewmatchProfile')); ?>/<?php echo e(base64_encode($user->id)); ?>" class="">
               		<div class="match-box">
                    <div class="match-pic">
                      <?php if(!empty($user->profile_pic)): ?> 
                     	<img src="<?php echo e(url('/public/uploads/profile_img')); ?>/<?php echo e($user->profile_pic); ?>">
                      <?php else: ?>
                      <img src="https://votivetech.in/4twoo/public/assets/img/Layer 11.png">
                      <?php endif; ?>
                     	<div class="match-persent">
                     	80% similarities
                       </div>
                    </div>
                      <h3><?php echo e($user->first_name); ?> <?php echo e($user->last_name); ?></h3>
                      <p> <?php echo e($user->occupation); ?> </p>
                      <div class="cross-btn">
                      	<!-- <a href="#" class="cross-btn-ser"><i class='bx bx-x'></i></a> -->
                      	<a href="javascript:void(0)" class="cross-btn-ser send_request" data-id="<?php echo e($user->id); ?>" id="send_request_<?php echo e($user->id); ?>"><i class='bx bxs-heart' style="display: none;"></i><i class='bx bx-heart'></i></a>
                      </div>
               		</div>
               	</a>
             	</div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
          </div>
        </div>
      </div>
    </div>
  </section>
</main> 
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home3/phpserver2/public_html/4twoo/resources/views/user/matching_profiles.blade.php ENDPATH**/ ?>