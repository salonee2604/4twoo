<?php $__env->startSection('content'); ?>
<style type="text/css">
	.mt-5a{


		margin-top: 90px;
	}

</style>
  <main id="main">
    <section id="blog" class="blog mt-5a" style="padding-bottom: 0px;">
      <div class="col-md-12 suc-pride">
        <h3> Welcome to 4Twoo Pride.<br>
        This is where we celebrate  Success Stories.</h3>
      </div>
      <div class="container">
        <div class="row">
            <article class="entry entry-single p-3 w-100">
              <div class="row">
                <?php $__currentLoopData = $stories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $story): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <div class="col-md-3 entries lg">
                    <a href="<?php echo e(url('/success_story/')); ?>/<?php echo base64_encode($story->id); ?>">
                      <div class="suces-box  mt-4">
                        <div class="img-story">
                        <img src="<?php echo e(url('/public/assets/img/')); ?>/<?php echo e($story->image); ?>" alt="">
                        </div>
                        <h4><?php echo e($story->title); ?></h4>
                        
                      </div>
                    </a>
                  </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
           </div>
            </article>
          </div><!-- End blog entries list -->
        </div>
      </div>
    </section><!-- End Blog Section -->
  </main><!-- End #main -->
 <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home3/phpserver2/public_html/4twoo/resources/views/success_story.blade.php ENDPATH**/ ?>