
<?php $__env->startSection('title', 'Edit User'); ?>

<?php $__env->startSection('current_page_css'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('current_page_js'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Edit User
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">User</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <?php if($message = Session::get('message')): ?>
       <div class="alert alert-success alert-block">  
        <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong><?php echo e($message); ?></strong>
      </div>
      <?php endif; ?>

      <?php if($message = Session::get('error')): ?>
      <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong><?php echo e($message); ?></strong>
      </div>
      <?php endif; ?>

      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-xs-12">
          <!-- SELECT2 EXAMPLE -->
        <div class="box box-default">
          <div class="box-header pull-right">
                <a href="<?php echo e(url('admin/user_list')); ?>" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Back </a>
          </div>
          <!-- /.box-header -->
          <form action="<?php echo e(url('/admin/update_user')); ?>" id="studentForm" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="_token" id="csrf-token" value="<?php echo e(csrf_token()); ?>" />
            <div class="box-body">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Full Name</label>
                    <input type="text" class="form-control" name="name" value="<?php echo e((!empty($student_info->first_name) ? $student_info->first_name : '')); ?> <?php echo e((!empty($student_info->last_name) ? $student_info->last_name : '')); ?>" placeholder="Enter Full Name">
                  </div>
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Username</label>
                    <input type="text" class="form-control" name="username" value="<?php echo e((!empty($student_info->username) ? $student_info->username : '')); ?>" placeholder="Enter Username">
                  </div>

                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Phone</label>
                    <input type="text" class="form-control" name="contact_number" value="<?php echo e((!empty($student_info->contact_number) ? $student_info->contact_number : '')); ?>" placeholder="Enter Contact Number">
                  </div>
                  <div class="form-group">
                    <label>Password</label>
                    <input type="password" class="form-control" name="password" value="" placeholder="Enter password">
                  </div>

                  <!-- /.form-group --> 
                  <div class="form-group">
                    <label>Post Image</label>
                    <img src="<?php echo e(url('/public/uploads/profile_img/'.$student_info->profile_pic)); ?>" style="width:100px;height:100px;" id="image_change">
                    <input name="image" type="file" accept="image/*" onchange="document.getElementById('image_change').src = window.URL.createObjectURL(this.files[0])">
                  </div>
                  
                  <!-- /.form-group -->

                  <input type="hidden" class="form-control" name="user_id" value="<?php echo e((!empty($student_info->id) ? $student_info->id : '')); ?>">
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
              </div>
            </div>
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            <!-- /.row -->
          </form>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home3/phpserver2/public_html/4twoo/resources/views/admin/student/edit_user.blade.php ENDPATH**/ ?>