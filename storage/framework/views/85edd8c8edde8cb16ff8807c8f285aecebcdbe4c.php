<?php $__env->startSection('title', 'User List'); ?>

<?php $__env->startSection('current_page_css'); ?>

<!-- DataTables -->

<link rel="stylesheet" href="<?php echo e(url('/')); ?>/resources/assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<link rel="stylesheet" href="<?php echo e(url('/')); ?>/resources/assets/css/bootstrap-toggle.min.css">

<style>

    .error{

        color: red;

    }

</style>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('current_page_js'); ?>

<!-- DataTables -->

<script src="<?php echo e(url('/')); ?>/resources/assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>

<script src="<?php echo e(url('/')); ?>/resources/assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script src="<?php echo e(url('/')); ?>/resources/assets/js/bootstrap-toggle.min.js"></script>

<script type="text/javascript">

  $(function () {

	   var user_list =  $('#user_list').DataTable({

		processing: true,

		serverSide: true,

		"order": [[ 0, "desc" ]],

		   "ajax":{

				"url": "<?php echo e(url('/admin/getStudentList')); ?>",

				"dataType": "json",

				"type": "POST",

				"data":{ _token: "<?php echo e(csrf_token()); ?>"}

			   },

			   "drawCallback": function (settings) { 

				var response = settings.json;

				$('.toggle-class').bootstrapToggle();

			},

		 columns: [

			{ "data": "id", name: 'id',searchable: false},



      { "data": "profile_pic", name: 'profile_pic',searchable: false},

			//{ "data": "full_name" , name: 'full_name',"orderable": false},

			{ "data": "username" , name: 'username',searchable: false,"orderable": false},

			{ "data": "email" , name: 'email' ,"orderable": false,searchable: false}, 

      { "data": "contact_number" , name: 'phone' ,"orderable": false,searchable: false}, 

			{ "data": "status" , name: 'status' ,"orderable": false,searchable: false},

			{ "data": "created_at" , name: 'created_at',"orderable": false },

			{ "data": "action" , name: 'action',"orderable": false,searchable: false},

		],

		'createdRow': function( row, data, dataIndex ) {

			$(row).attr('id',"row"+data.id);

			$('td:eq(3)', row).attr( 'id',"toemail_"+data.id);

		},

	});

  })

</script>

<script type="text/javascript">

 function delete_student(user_id){

  $.ajaxSetup({

    headers: {

      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

    }

  });

  $.ajax({

   type: 'POST',

   url: "<?php echo url('/admin/delete_user'); ?>",

   enctype: 'multipart/form-data',

   data:{user_id:user_id,'_token':'<?php echo csrf_token(); ?>'},

     beforeSend:function(){

       return confirm("Are you sure you want to delete this item?");

     },

     success: function(resultData) { 

       console.log(resultData);

       var obj = JSON.parse(resultData);

       if (obj.status == 'success') {

          $('#success_message').fadeIn().html(obj.msg);

          setTimeout(function() {

            $('#success_message').fadeOut("slow");

          }, 2000 );

          $("#row" + user_id).remove();

       }

     },

     error: function(errorData) {

      console.log(errorData);

      alert('Please refresh page and try again!');

    }

  });

}



  function sendemail(student_id){

   $('#emailtosend').val($('#track_email_'+student_id).attr('data-email'));

   $('#studentID').val(student_id);

   $('#myModal').modal('show');

  }



$('#sendemailfrm').validate({ 

      rules: {

       email: {

        required: true,

        email:true

      },

      language: "required",

      

    },

    submitHandler: function(form) {

        $.ajax({

         type: "POST",

         dataType: "json",

         url: "<?php echo url('/admin/send-email'); ?>",

         data: $('#sendemailfrm').serialize(),

         success: function(data){

           $('#notify').html(data.message).css('color','green').fadeIn();

           setTimeout(function() {

             $('#notify').fadeOut("slow");

              $('#myModal').modal('hide');

           }, 2000 );

         },

         error: function(errorData) {

           console.log(errorData);

           

         }

       });

    }

  });

</script>

<script>

  $(document).on("change",'.toggle-class', function() {

    var status = $(this).prop('checked') == true ? 1 : 0; 

    var student_id = $(this).data('id'); 

    

    $.ajax({

      type: "GET",

      dataType: "json",

      url: "<?php echo url('/admin/change_student_status'); ?>",

      data: {'status': status, 'student_id': student_id},

      success: function(data){

        $('#success_message').fadeIn().html(data.success);

        setTimeout(function() {

          $('#success_message').fadeOut("slow");

        }, 2000 );

      },

      error: function(errorData) {

        console.log(errorData);

        alert('Please refresh page and try again!');

      }

    });

  })



</script>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

 <!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">

    <!-- Content Header (Page header) -->

    <section class="content-header">

      <h1>Users List</h1>

      <ol class="breadcrumb">

        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

        <li class="active">User</li>

      </ol>

    </section>

    <!-- Main content -->

    <section class="content">

      <p style="display: none;" id="success_message" class="alert alert-success"></p>

      <?php if($errors->any()): ?>

      <div class="alert alert-danger">

       <ul>

         <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

         <li><?php echo e($error); ?></li>

         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

       </ul>

     </div>

     <?php endif; ?>

      <?php if(Session::has('error_arr')): ?>

          <?php $error_arr = Session::get('error_arr'); ?>

          <div class="alert alert-info">

            <ul>

              <?php

              for($i=0; $i < count($error_arr); $i++){

                if(!empty($error_arr[$i])){

                  ?><li><?php echo e($error_arr[$i]); ?></li><?php

                }

              }

              ?>

            </ul>

          </div>

          <?php Session::forget('error_arr'); ?>

      <?php endif; ?>



     <?php if(Session::has('message')): ?>

     <p class="alert <?php echo e(Session::get('alert-class', 'alert-success')); ?>"><?php echo e(Session::get('message')); ?></p>

     <?php endif; ?>



     <?php if(Session::has('error')): ?>

     <p class="alert <?php echo e(Session::get('alert-class', 'alert-danger')); ?>"><?php echo e(Session::get('error')); ?></p>

     <?php endif; ?>

      <div class="row">

        <div class="col-xs-12">

          <div class="box">

            <div class="box-header">

			 <div class ="row">

			   <div class="col-md-6">

			     <form action="<?php echo e(url('admin/UserImportData')); ?>" class="form-horizontal" method="POST" enctype="multipart/form-data">

            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">

					<div class="col-md-5">

					  <div class="form-group">

						<input type="file" name="import_file" />

					  </div>

					</div>

            <button class="btn btn-primary">Submit</button>

        </form>

			   </div>

			    <div class="col-md-6">

				   <h3 class="box-title pull-right btn-toolbar"><a href="<?php echo e(url('admin/add_user')); ?>" class="btn btn-primary">Add User</a></h3>

                   <h3 class="box-title pull-right"><a class="btn btn-primary" href="<?php echo e(url('/admin/UserExportData')); ?>">Export User Data</a></h3>

			    </div>

			 </div>

	        </div>

            <!-- /.box-header -->

            <div class="box-body">

              <table id="user_list" class="table table-bordered table-striped">

                <thead>

                  <tr>

                    <th>S.No.</th>

                    <th>Image</th>

                    <th>Username</th>

                    <th>Email</th>

                    <th>Phone</th>

                    <th>Status</th>

                    <th>Create Date</th>

                    <th>Action</th>

                  </tr>

                </thead>

                <tbody>

                </tbody>

              </table>

            </div>

            <!-- /.box-body -->

          </div>

          <!-- /.box -->

        </div>

        </div>

        <!-- /.col -->

      <!-- /.row -->

    </section>

    <!-- /.content -->

  </div>

  <!-- /.content-wrapper -->

  <!-- Modal -->

<div id="myModal" class="modal fade" role="dialog">

  <div class="modal-dialog">

    <!-- Modal content-->

    <form id="sendemailfrm" name="sendemailfrm" method="post" role="form">

           <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">

           <input type="hidden" id="studentID" name="student_id" value="">

    <div class="modal-content">

      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal">&times;</button>

        <h4 class="modal-title">Send Email</h4>

      </div>

      <div class="modal-body">

                     <div class="controls">

                 <div class="row">

                      <div class="col-md-6">

                          <div class="form-group">

                              <label for="form_email">Email *</label>

                              <input id="emailtosend" type="email" name="email" class="form-control" placeholder="Please enter your email *" required="required" data-error="Valid email is required.">

                              <div class="help-block with-errors"></div>

                          </div>

                      </div>

                     <div class="col-md-6"></div>

                  </div>

                  <div class="row">

                      <div class="col-md-12">

                          <div class="form-group">

                              <label for="form_message">Language *</label>

                              <select class="form-control" name="language" id="language">

                                  <option value="">Select Language</option>

                                  <?php

                                  if (!empty($language_list)) {

                                      foreach ($language_list as $language) { ?>

                                          <option value="<?php echo $language->code; ?>" <?php if (!empty($format_info->language) && $format_info->language == $language->code) { ?> selected <?php } ?>><?php echo e($language->name); ?></option>

                                          <?php

                                      }

                                  }

                                  ?>

                              </select>

                              <div class="help-block with-errors"></div>

                          </div>

                      </div>

                      </div>

                  </div>

                <span id="notify"></span>

              </div>

          

      <div class="modal-footer">

             <input type="submit" class="btn btn-success btn-send" value="Send">

             <button type="button" class="btn btn-small btn-default" data-dismiss="modal">Close</button>

      </div></div>

       </form>

    </div>

 </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home3/phpserver2/public_html/4twoo/resources/views/admin/student/user_list.blade.php ENDPATH**/ ?>