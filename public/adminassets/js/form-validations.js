var admin_baseurl = $("#baseurl").val() ;
var admin_siteurl = $("#siteurl").val() ;

$(function(){

/*
* load preview image
*/
$(".inputFile").change(function(event){
  load_preview_image(this);
});


/*
* load multiple preview image
*/
$(".multipleFile").change(function(event){
  var total_file=document.getElementById("galleryImages").files.length;
  $("#multiple_image_preview").html('') ;
  for(var i=0;i<total_file;i++){
    $('#multiple_image_preview').append("<div class='multi-image-preview'><img width='100px' title='Gallery Image' src='"+URL.createObjectURL(event.target.files[i])+"'></div>");
  }
});


/*
* view password on click eye icon
*/
$("#toggle_password").click(function() {
  var icon_name = $("#password_icon_name").text();

  if(icon_name == 'visibility'){
    $("#password_icon_name").text("visibility_off");
  }else{
    $("#password_icon_name").text("visibility");  
  }

  var input = $($(this).attr("toggle"));

  if (input.attr("type") == "password") {
    input.attr("type", "text");
  } else {
    input.attr("type", "password");
  }
});

/*
* view confirm password on click eye icon
*/
$("#toggle_conform_password").click(function() {
  var icon_name = $("#confirm_password_icon_name").text();

  if(icon_name == 'visibility'){
    $("#confirm_password_icon_name").text("visibility_off");
  }else{
    $("#confirm_password_icon_name").text("visibility");  
  }

  var input = $($(this).attr("toggle"));

  if (input.attr("type") == "password") {
    input.attr("type", "text");
  } else {
    input.attr("type", "password");
  }
});


$.validator.addMethod("lettersonly", function(value, element) {
  return this.optional(element) || /^[a-z\.\s]+$/i.test(value);
}, "Fullname must contain only letters or dots"); 


$.validator.addMethod("onlyletter", function(value, element) {
  return this.optional(element) || /^[a-z\s]+$/i.test(value);
}, "Region name must contain only letters."); 

$.validator.addMethod("Lttr", function(value, element) {
  return this.optional(element) || /^[a-z\s]+$/i.test(value);
}, "City name must contain only letters."); 



$.validator.addMethod("passwordStrength", function(value, element) {
  if (this.optional(element)) {
    return true;
  } else if (!/[A-Z]/.test(value)) {
    return false;
  } else if (!/[a-z]/.test(value)) {
    return false;
  } else if (!/[0-9]/.test(value)) {
    return false;
  }else if (!/[~^#%$&=!\-@._><+}{[,\]()"':/\\;|\?]/.test(value)) {  
    return false;
  }
  return true;
}, "The password should be a combination of lowercase letters, uppercase letters, numbers and special characters");

/*
* admin login validation
*/
$('#adminLoginForm').validate({
  rules: {
    email: {
      required: true,
      email:true
    },
    password: {
      required: true
    }
  },
  messages :{
    email : {
      required : 'E-Mailadresse wird benötigt.',
      email : 'Bitte geben Sie eine gültige E-Mail-Adresse ein'
    },
    password :{
      required : 'Passwort wird benötigt',
    }
  },
  highlight: function (input) {
    $(input).parents('.form-line').addClass('error');
  },
  unhighlight: function (input) {
    $(input).parents('.form-line').removeClass('error');
  },
  errorPlacement: function (error, element) {
    $(element).parents('.input-group').append(error);
  },
  submitHandler:function(form){
    admin_login();
    return false;
  }
});

/*
* forgot password validation
*/
// $('#forgot_password_admin').validate({
//   rules: {
//     admin_email: {
//       required: true,
//       email:true
//     }
//   },
//   messages :{
//     admin_email : {
//       required : 'Email ID is required',
//       email : 'Please enter a valid email address'
//     }
//   },
//   submitHandler:function(form){
//     admin_forgot_passowrd();
//     return false;
//   }
// });

/*
* reset password validation
*/
// $('#reset_password').validate({
//   rules: {
//     password: {
//       required: true,
//       passwordStrength : true
//     },
//     confirm_password:{
//       required : true,
//       equalTo : '#password',
//     }
//   },
//   messages :{
//     password : {
//       required : 'Passwort wird benötigt',
//     },
//     confirm_password :{
//       required : 'Bestätigen Sie, dass das Passwort erforderlich ist',
//       equalTo : 'Bestätigen Sie, dass das Passwort mit dem Passwort übereinstimmen sollte'
//     }
//   },
//   highlight: function (input) {
//     $(input).parents('.form-line').addClass('error');
//   },
//   unhighlight: function (input) {
//     $(input).parents('.form-line').removeClass('error');
//   },
//   errorPlacement: function (error, element) {
//     $(element).parents('.input-group').append(error);
//   },
//   submitHandler:function(form){
//     admin_reset_password();
//     return false;
//   }
// });


/*
* category add validation
*/
$('#categoryForm').validate({
  rules: {
    category_name: {
      required: true,
      remote: {
        url: admin_baseurl+'/check_exits_value',
        type: "POST",
        headers:{'x-csrf-token':$('meta[name="csrf-token"]').attr('content')},
        data:{check_for:'category_name',check_value:function() { return $('input[name=category_name]').val(); },tablename:'category',where_name:'category_id',where_value:function() { return $('input[name=category_id]').val();},msg:'Der Kategoriename ist bereits vorhanden'}
      },
    },
    /*txtCategoryImage: {
      required: true
    },
    */
  },
  messages :{
    category_name : {
      required : 'Kategoriename ist erforderlich',
    },
    /*txtCategoryImage : {
      required : 'Category image is required',
    },
    */
  },
  highlight: function (input) {
    $(input).parents('.form-line').addClass('error');
  },
  unhighlight: function (input) {
    $(input).parents('.form-line').removeClass('error');
  },
  errorPlacement: function (error, element) {
    $(element).parents('.form-group').append(error);
  },
  submitHandler:function(form){
    category_add_edit();
    return false;
  }
});




/*
* category add validation
*/
$('#OfferForm').validate({
  rules: {
    offer_name: {
      required: true,
      remote: {
        url: admin_baseurl+'/check_exits_value',
        type: "POST",
        headers:{'x-csrf-token':$('meta[name="csrf-token"]').attr('content')},
        data:{check_for:'offer_name',check_value:function() { return $('input[name=offer_name]').val(); },tablename:'offers',where_name:'offer_id',where_value:function() { return $('input[name=offer_id]').val();},msg:'Angebotsname existiert bereits'}
      },
    },
    /*txtCategoryImage: {
      required: true
    },
    */
  },
  messages :{
    offer_name : {
      required : 'Angebotsname ist erforderlich',
    },
    /*txtCategoryImage : {
      required : 'Category image is required',
    },
    */
  },
  highlight: function (input) {
    $(input).parents('.form-line').addClass('error');
  },
  unhighlight: function (input) {
    $(input).parents('.form-line').removeClass('error');
  },
  errorPlacement: function (error, element) {
    $(element).parents('.form-group').append(error);
  },
  submitHandler:function(form){
    offer_add_edit();
    return false;
  }
});


/*
* region add validation
*/
$('#regionForm').validate({
  rules: {
    region_name: {
      required: true,
      // remote: {
      //   url: admin_baseurl+'/check_exits_value',
      //   type: "POST",
      //   headers:{'x-csrf-token':$('meta[name="csrf-token"]').attr('content')},
      //   data:{check_for:'name',check_value:function() { return $('input[name=region_name]').val(); },tablename:'admin_states',where_name:'id',where_value:function() { return $('input[name=id]').val();},msg:'Der Name der Region ist bereits vorhanden'}
      // },

    },
    
  },
  messages :{
    region_name : {
      required : 'Der Name der Region ist erforderlich',

    }, 
  },
  highlight: function (input) {
    $(input).parents('.form-line').addClass('error');
  },
  unhighlight: function (input) {
    $(input).parents('.form-line').removeClass('error');
  },
  errorPlacement: function (error, element) {
    $(element).parents('.form-group').append(error);
  },
  submitHandler:function(form){
    region_add_edit();
    return false;
  }
});


/*
* City add validation
*/
$('#cityForm').validate({
  rules: {
    city_name: {
      required: true,
      Lttr :true,
      remote: {
        url: admin_baseurl+'/check_exits_value',
        type: "POST",
        headers:{'x-csrf-token':$('meta[name="csrf-token"]').attr('content')},
        data:{check_for:'name',check_value:function() { return $('input[name=city_name]').val(); },tablename:'admin_cities',where_name:'id',where_value:function() { return $('input[name=id]').val();},msg:'Der Name der Stadt existiert bereits'}
      },

    },


    state:{
      required: true,
    },
    /*txtCategoryImage: {
      required: true
    },
    */
  },
  messages :{
    city_name : {
      required : 'Name der Stadt ist erforderlich',

    },

    state:{
      required: 'Der Name des Staates ist erforderlich',
    },
    /*txtCategoryImage : {
      required : 'Category image is required',
    },
    */
  },
  highlight: function (input) {
    $(input).parents('.form-line').addClass('error');
  },
  unhighlight: function (input) {
    $(input).parents('.form-line').removeClass('error');
  },
  errorPlacement: function (error, element) {
    $(element).parents('.form-group').append(error);
  },
  submitHandler:function(form){
    city_add_edit();
    return false;
  }
});



/*
* product category add validation
*/
$('#ProductcategoryForm').validate({
  rules: {
    category_name: {
      required: true,
      remote: {
        url: admin_baseurl+'/check_exits_value',
        type: "POST",
        headers:{'x-csrf-token':$('meta[name="csrf-token"]').attr('content')},
        data:{check_for:'category_name',check_value:function() { return $('input[name=category_name]').val(); },tablename:'category',where_name:'category_id',where_value:function() { return $('input[name=category_id]').val();},msg:'Der Kategoriename ist bereits vorhanden'}
      },
      // status:{
      //   required: true,
      // },
    },
    /*txtCategoryImage: {
      required: true
    },
    */
  },
  messages :{
    category_name : {
      required : 'Kategoriename ist erforderlich',
    },

    // status : {
    //   required : 'Status is required',
    // },
    /*txtCategoryImage : {
      required : 'Category image is required',
    },
    */
  },
  highlight: function (input) {
    $(input).parents('.form-line').addClass('error');
  },
  unhighlight: function (input) {
    $(input).parents('.form-line').removeClass('error');
  },
  errorPlacement: function (error, element) {
    $(element).parents('.form-group').append(error);
  },
  submitHandler:function(form){
    product_category_add_edit();
    return false;
  }
});


/*
* product category add validation
*/
$('#ProductForm').validate({
  rules: {
    product_name: {
      required: true,
      remote: {
        url: admin_baseurl+'/check_exits_value',
        type: "POST",
        headers:{'x-csrf-token':$('meta[name="csrf-token"]').attr('content')},
        data:{check_for:'product_name',check_value:function() { return $('input[name=product_name]').val(); },tablename:'products',where_name:'product_id',where_value:function() { return $('input[name=product_id]').val();},msg:'Produktname existiert bereits'}
      },

    },

    product_prize:{
     required: true,
   },

   product_size_unit2:{
    required: false,
  },

  product_size:{
   required: true,
 },
 category:{
   required: true,
 },
    /*txtCategoryImage: {
      required: true
    },
    */
  },
  messages :{
    product_name : {
      required : 'Produktname ist erforderlich',
    },

    product_prize:{
     required: 'Produktpreis ist erforderlich',
   },
   product_size:{
     required: 'Produktgröße ist erforderlich',
   },

   category:{
     required: 'Produktkategorie ist erforderlich',
   },
   

    // status : {
    //   required : 'Status is required',
    // },
    /*txtCategoryImage : {
      required : 'Category image is required',
    },
    */
  },
  highlight: function (input) {
    $(input).parents('.form-line').addClass('error');
  },
  unhighlight: function (input) {
    $(input).parents('.form-line').removeClass('error');
  },
  errorPlacement: function (error, element) {
    $(element).parents('.form-group').append(error);
  },
  submitHandler:function(form){
    product_add_edit();
    return false;
  }
});

/*
* category edit validation
*/
/*$('#categoryEditForm').validate({
  rules: {
    category_name: {
      required: true,
      remote: {
        url: admin_siteurl+'/dashboard/check_exits_value',
        type: "POST",
        data:{check_for:'category_name',check_value:function() { return $('input[name=category_name]').val(); },tablename:'category',where_name:'category_id',where_value:function() { return $('input[name=category_id]').val();},msg:'Category name already exits'}
      },
    }
  },
  messages :{
    category_name : {
      required : 'Category name is required',
    }
  },
  highlight: function (input) {
    $(input).parents('.form-line').addClass('error');
  },
  unhighlight: function (input) {
    $(input).parents('.form-line').removeClass('error');
  },
  errorPlacement: function (error, element) {
    $(element).parents('.form-group').append(error);
  },
  submitHandler:function(form){
    category_add_edit();
    return false;
  }
});*/

/*
* Subscription package add/edit validation
*/
$('#membershipPlanForm').validate({
  rules: {
    package_for :{
      required: true,
    },
    plan_name: {
      required: true,
     /* remote: {
        url: admin_baseurl+'/check_exits_value',
        type: "POST",
        headers:{'x-csrf-token':$('meta[name="csrf-token"]').attr('content')},
        data:{check_for:'plan_name',check_value:function() { return $('input[name=plan_name]').val(); },tablename:'subscription_plan',where_name:'plan_id',where_value:function() { return $('input[name=plan_id]').val();},msg:'Subscription package already exits'}
      },*/
    },
    plan_price: {
      required: true,
      number : true,
      min : 0
    }
  },
  messages :{
    package_for : {
      required : 'Bitte wählen Sie das Abonnementpaket für',
    },
    plan_name : {
      required : 'Der Name des Abonnementpakets ist erforderlich',
    },
    plan_price : {
      required : 'Der Preis für das Abonnementpaket ist erforderlich',
      number : 'Der Preis sollte eine numerische Zahl sein',
      min : 'Der Preis sollte größer oder gleich 0 sein',
    },
  },
  highlight: function (input) {
    $(input).parents('.form-line').addClass('error');
  },
  unhighlight: function (input) {
    $(input).parents('.form-line').removeClass('error');
  },
  errorPlacement: function (error, element) {
    $(element).parents('.form-group').append(error);
  },
  submitHandler:function(form){
    membership_plan_add_edit();
    return false;
  }
});


/*
* fetlancer add/edit validation
*/
/*$('#fetlancerForm').validate({
  rules: {
    fullname: {
      required: true,
      lettersonly:true
    },
    email: {
      required: true,
      email : true,
      remote: {
        url: admin_baseurl+'/check_exits_value',
        type: "POST",
        headers:{'x-csrf-token':$('meta[name="csrf-token"]').attr('content')},
        data:{check_for:'email',check_value:function() { return $('input[name=email]').val(); },tablename:'users',where_name:'user_id',where_value:function() { return $('input[name=user_id]').val();},msg:'Email already exits'}
      },
    },
    password: {
      required: true,
      passwordStrength : true
    },
    confirm_password: {
      required: true,
      equalTo : "#password"
    },
    mobile_no: {
      required: true,
      number : true
    },
    country_id: {
      required: true
    },
    region_id: {
      required: true
    },
    city_id: {
      required: true
    }
  },
  messages :{
    fullname : {
      required : 'Fullname is required',
    },
    email : {
      required : 'Email ID is required',
      email : 'Please enter a valid email address'
    },
    password : {
      required : 'Password is required'
    },
    confirm_password : {
      required : 'Confirm password is required',
      equalTo : 'Confirm password should be same as password'
    },
    mobile_no : {
      required : 'Mobile phone number is required',
      number : 'Mobile phone number must be numeric value',
    },
    country_id : {
      required : 'Please select any country'
    },
    region_id : {
      required : 'Please select any region'
    },
    city_id : {
      required : 'Please select any city'
    },
  },
  highlight: function (input) {
    $(input).parents('.form-line').addClass('error');
  },
  unhighlight: function (input) {
    $(input).parents('.form-line').removeClass('error');
  },
  errorPlacement: function (error, element) {
    $(element).parents('.form-group').append(error);
  },
  submitHandler:function(form){
    fetlancer_add_edit();
    return false;
  }
});
*/


/*
* member add/edit validation
*/
/*$('#memberForm').validate({
  rules: {
    fullname: {
      required: true,
      lettersonly:true
    },
    email: {
      required: true,
      email : true,
      remote: {
        url: admin_baseurl+'/check_exits_value',
        type: "POST",
        headers:{'x-csrf-token':$('meta[name="csrf-token"]').attr('content')},
        data:{check_for:'email',check_value:function() { return $('input[name=email]').val(); },tablename:'users',where_name:'user_id',where_value:function() { return $('input[name=user_id]').val();},msg:'Email already exits'}
      },
    },
    password: {
      required: true,
      passwordStrength : true
    },
    confirm_password: {
      required: true,
      equalTo : "#password"
    },
    mobile_no: {
      required: true,
      number : true
    },
    country_id: {
      required: true
    },
    region_id: {
      required: true
    },
    city_id: {
      required: true
    }
  },
  messages :{
    fullname : {
      required : 'Fullname is required',
    },
    email : {
      required : 'Email ID is required',
      email : 'Please enter a valid email address'
    },
    password : {
      required : 'Password is required'
    },
    confirm_password : {
      required : 'Confirm password is required',
      equalTo : 'Confirm password should be same as password'
    },
    mobile_no : {
      required : 'Mobile phone number is required',
      number : 'Mobile phone number must be numeric value',
    },
    country_id : {
      required : 'Please select any country'
    },
    region_id : {
      required : 'Please select any region'
    },
    city_id : {
      required : 'Please select any city'
    },
  },
  highlight: function (input) {
    $(input).parents('.form-line').addClass('error');
  },
  unhighlight: function (input) {
    $(input).parents('.form-line').removeClass('error');
  },
  errorPlacement: function (error, element) {
    $(element).parents('.form-group').append(error);
  },
  submitHandler:function(form){
    member_add_edit();
    return false;
  }
});*/

/*
* users add/edit validation
*/
$('#usersForm').validate({
  rules: {
    fullname: {
      required: true,
      lettersonly:true
    },
    email: {
      required: true,
      email : true,
      remote: {
        url: admin_baseurl+'/check_exits_value',
        type: "POST",
        headers:{'x-csrf-token':$('meta[name="csrf-token"]').attr('content')},
        data:{check_for:'email',check_value:function() { return $('input[name=email]').val(); },tablename:'users',where_name:'user_id',where_value:function() { return $('input[name=user_id]').val();},msg:'E-Mail existiert bereits'}
      },
    },
    password: {
      required: true,
      passwordStrength : true
    },
    confirm_password: {
      required: true,
      equalTo : "#password"
    },
    mobile_no: {
      required: true,
      number : true
    },
    country_id: {
      required: true
    },
    region_id: {
      required: true
    },
    city_id: {
      required: true
    },
    usertype_id: {
      required: true
    }
  },
  messages :{
    fullname : {
      required : 'Vollständiger Name ist erforderlich',
    },
    email : {
      required : 'E-Mail-ID ist erforderlich',
      email : 'Bitte geben Sie eine gültige E-Mail-Adresse ein'
    },
    password : {
      required : 'Passwort wird benötigt'
    },
    confirm_password : {
      required : 'Bestätigen Sie, dass das Passwort erforderlich ist',
      equalTo : 'Bestätigen Sie, dass das Passwort mit dem Passwort übereinstimmen sollte'
    },
    mobile_no : {
      required : 'Handynummer ist erforderlich',
      number : 'Die Mobiltelefonnummer muss ein numerischer Wert sein',
    },
    country_id : {
      required : 'Bitte wählen Sie ein Land aus'
    },
    region_id : {
      required : 'Bitte wählen Sie eine Region aus'
    },
    city_id : {
      required : 'Bitte wählen Sie eine Stadt aus'
    },
    usertype_id : {
      required : 'Bitte wählen Sie einen Usertyp'
    },
  },
  highlight: function (input) {
    $(input).parents('.form-line').addClass('error');
  },
  unhighlight: function (input) {
    $(input).parents('.form-line').removeClass('error');
  },
  errorPlacement: function (error, element) {
    $(element).parents('.form-group').append(error);
  },
  submitHandler:function(form){
    user_add_edit();
    return false;
  }
});

/*
* admin profile add/edit validation
*/
$('#profileForm').validate({
  rules: {
    fullname: {
      required: true,
      lettersonly :true
    }
  },
  messages :{
    fullname : {
      required : 'Vollständiger Name ist erforderlich',
    }
  },
  highlight: function (input) {
    $(input).parents('.form-line').addClass('error');
  },
  unhighlight: function (input) {
    $(input).parents('.form-line').removeClass('error');
  },
  errorPlacement: function (error, element) {
    $(element).parents('.form-group').append(error);
  },
  submitHandler:function(form){
    profile_update();
    return false;
  }
});


/*
* profile change password validation
*/
$('#changePasswordForm').validate({
  rules: {
    password: {
      required: true,
      passwordStrength : true
    },
    confirm_password:{
      required : true,
      equalTo : '#password',
    }
  },
  messages :{
    password : {
      required : 'Passwort wird benötigt',
    },
    confirm_password :{
      required : 'Bestätigen Sie, dass das Passwort erforderlich ist',
      equalTo : 'Bestätigen Sie, dass das Passwort mit dem Passwort übereinstimmen sollte'
    }
  },
  highlight: function (input) {
    $(input).parents('.form-line').addClass('error');
  },
  unhighlight: function (input) {
    $(input).parents('.form-line').removeClass('error');
  },
  errorPlacement: function (error, element) {
    $(element).parents('.form-group').append(error);
  },
  submitHandler:function(form){
    profile_change_password();
    return false;
  }
});


/*
* payment gateway setting validation
*/
$('#PaymentGatewayForm').validate({
  rules: {
    key1: {
      required: true
    },
    key2:{
      required : true
    },
    key3:{
      required : true
    }
  },
  messages :{
    key1 : {
      required : 'Dieses Feld wird benötigt',
    },
    key2 :{
      required : 'Dieses Feld wird benötigt',
    },
    key3 :{
      required : 'Dieses Feld wird benötigt',
    }
  },
  highlight: function (input) {
    $(input).parents('.form-line').addClass('error');
  },
  unhighlight: function (input) {
    $(input).parents('.form-line').removeClass('error');
  },
  errorPlacement: function (error, element) {
    $(element).parents('.form-group').append(error);
  },
  submitHandler:function(form){
    edit_gateway_setting();
    return false;
  }
});


/*
* pornstar add validation
*/
$('#pornstarAddForm').validate({
  ignore: "",
  rules: {
    fullname: {
      required: true,
      lettersonly :true,
      remote: {
        url: admin_siteurl+'/dashboard/check_exits_value',
        type: "POST",
        data:{check_for:'fullname',check_value:function() { return $('input[name=fullname]').val(); },tablename:'pornstar_personal_details',where_name:'pornstar_id',where_value:function() { return $('input[name=pornstar_id]').val();},msg:'Dieses Pornostar-Profil existiert bereits'}
      },
    },
    categories: {
      required: true
    },
    dob:{
      required: true
    },
    age: {
      required: true,
      number : true,
      min : 13
    },
    measurements:{
      required: true,
    },
    zodiac_sign:{
      required: true,
    },
    career_status:{
      required: true,
    },
    fb_page_link:{
      url: true,
    },
    twitter_page_link:{
      url: true,
    },
    instagram_page_link:{
      url: true,
    },
    txtFile:{
      required: true,
    }
  },
  messages :{
    fullname : {
      required : 'Vollständiger Name ist erforderlich',
    },
    categories : {
      required : 'Kategorie ist erforderlich',
    },
    dob:{
      required : 'Geburtsdatum ist erforderlich'
    },
    age : {
      required : 'Alter ist erforderlich',
      number : 'Das Alter sollte eine numerische Zahl sein',
      min : 'Das Alter sollte mindestens 13 Jahre betragen',
    },
    measurements:{
      required : 'Messungen sind erforderlich'
    },
    zodiac_sign:{
      required : 'Sternzeichen ist erforderlich'
    },
    career_status:{
      required : 'Karrierestatus ist erforderlich'
    },
    fb_page_link:{
      url : 'Bitte geben Sie eine gültige URL ein, z. https://www.facebook.com/deine_Seite/'
    },
    twitter_page_link:{
      url : 'Bitte geben Sie eine gültige URL ein, z. https://www.twitter.com/deine_Seite/'
    },
    instagram_page_link:{
      url : 'Bitte geben Sie eine gültige URL ein, z. https://www.instagram.com/deine_Seite/'
    },
    txtFile:{
      required : 'Bitte laden Sie ein Bild für ein Profilbild hoch'
    }
  },
  highlight: function (input) {
    $(input).parents('.form-line').addClass('error');
  },
  unhighlight: function (input) {
    $(input).parents('.form-line').removeClass('error');
  },
  errorPlacement: function (error, element) {
    $(element).parents('.form-group').append(error);
  },
  submitHandler:function(form){
    pornstar_add_edit();
    return false;
  }
});

/*
* pornstar edit validation
*/
$('#pornstarEditForm').validate({
  ignore: "",
  rules: {
    fullname: {
      required: true,
      lettersonly :true,
      remote: {
        url: admin_siteurl+'/dashboard/check_exits_value',
        type: "POST",
        data:{check_for:'fullname',check_value:function() { return $('input[name=fullname]').val(); },tablename:'pornstar_personal_details',where_name:'pornstar_id',where_value:function() { return $('input[name=pornstar_id]').val();},msg:'Dieses Pornostar-Profil existiert bereits'}
      },
    },
    categories : {
      required : true
    },
    dob:{
      required: true
    },
    age: {
      required: true,
      number : true,
      min : 13
    },
    measurements:{
      required: true,
    },
    zodiac_sign:{
      required: true,
    },
    career_status:{
      required: true,
    },
    fb_page_link:{
      url: true,
    },
    twitter_page_link:{
      url: true,
    },
    instagram_page_link:{
      url: true,
    },
  },
  messages :{
    fullname : {
      required : 'Vollständiger Name ist erforderlich',
    },
    categories : {
      required : 'Kategorie ist erforderlich',
    },
    dob:{
      required : 'Geburtsdatum ist erforderlich'
    },
    age : {
      required : 'Alter ist erforderlich',
      number : 'Das Alter sollte ein numerischer Wert sein',
      min : 'Das Alter sollte mindestens 13 Jahre betragen',
    },
    measurements:{
      required : 'Messungen sind erforderlich'
    },
    zodiac_sign:{
      required : 'Sternzeichen ist erforderlich'
    },
    career_status:{
      required : 'Karrierestatus ist erforderlich'
    },
    fb_page_link:{
      url : 'Bitte geben Sie eine gültige URL ein, z. https://www.facebook.com/deine_Seite/'
    },
    twitter_page_link:{
      url : 'Bitte geben Sie eine gültige URL ein, z. https://www.twitter.com/deine_Seite/'
    },
    instagram_page_link:{
      url : 'Bitte geben Sie eine gültige URL ein, z. https://www.instagram.com/deine_Seite/'
    },
  },
  highlight: function (input) {
    $(input).parents('.form-line').addClass('error');
  },
  unhighlight: function (input) {
    $(input).parents('.form-line').removeClass('error');
  },
  errorPlacement: function (error, element) {
    $(element).parents('.form-group').append(error);
  },
  submitHandler:function(form){
    pornstar_add_edit();
    return false;
  }
});


/*
* mail template add/edit validation
*/
$('#mailTemplateForm').validate({
  ignore:'',
  rules: {
    template_name: {
      required: true
    },
    subject: {
      required: true
    },
    description: {
      required: true
    }
  },
  messages :{
    template_name : {
      required : 'Vorlagenname ist erforderlich',
    },
    subject : {
      required : 'Betreff ist erforderlich',
    },
    description : {
      required : 'Beschreibung ist erforderlich'
    },
  },
  highlight: function (input) {
    $(input).parents('.form-line').addClass('error');
  },
  unhighlight: function (input) {
    $(input).parents('.form-line').removeClass('error');
  },
  errorPlacement: function (error, element) {
    $(element).parents('.form-group').append(error);
  },
  submitHandler:function(form){
    mail_template_add_edit();
    return false;
  }
});


/*
* add video validation
*/
$('#videoAddForm').validate({
  ignore: "",
  rules: {
    video_name: {
      required: true,
      remote: {
        url: admin_siteurl+'/dashboard/check_exits_value',
        type: "POST",
        data:{check_for:'video_name',check_value:function() { return $('input[name=video_name]').val(); },tablename:'videos',where_name:'video_id',where_value:function() { return $('input[name=video_id]').val();},msg:'Videoname existiert bereits'}
      },
    },
    video_time_length:{
      required: true
    },
    categories: {
      required: true
    },
    starring:{
      required: true,
    },
    txtVideo:{
      required: true,
    },
    txtBannerImage:{
      required: true,
    },
    txtSidebarImage:{
      required: true,
    },
    video_description:{
      required: true,
    },
    right_sidebar_description:{
      required: true,
    },
    fps:{
      required: true,
    },
    preview_video:{
      required: true,
    },
  },
  messages :{
    video_name : {
      required : 'Videoname ist erforderlich',
    },
    video_time_length:{
      required : 'Die Videozeit ist erforderlich, z. 43 min'
    },
    categories : {
      required : 'Kategorie ist erforderlich',
    },
    starring:{
      required : 'Darsteller sind erforderlich'
    }, 
    txtVideo:{
      required : 'Video ist erforderlich'
    },
    txtBannerImage:{
      required : 'Das obere Bannerbild ist erforderlich'
    },
    txtSidebarImage:{
      required : 'Das Bild in der rechten Seitenleiste ist erforderlich'
    },
    video_description:{
      required : 'Videobeschreibung ist erforderlich'
    },
    right_sidebar_description:{
      required : 'Eine Beschreibung der rechten Seitenleiste ist erforderlich'
    },
    fps:{
      required : 'FPS ist erforderlich'
    },
    preview_video:{
      required : 'Vorschau Video ist erforderlich'
    },
  },
  highlight: function (input) {
    $(input).parents('.form-line').addClass('error');
  },
  unhighlight: function (input) {
    $(input).parents('.form-line').removeClass('error');
  },
  errorPlacement: function (error, element) {
    $(element).parents('.form-group').append(error);
  },
  submitHandler:function(form){
    video_add_edit();
    return false;
  }
});

/*
* edit video validation
*/
$('#videoEditForm').validate({
  ignore: "",
  rules: {
    video_name: {
      required: true,
      remote: {
        url: admin_siteurl+'/dashboard/check_exits_value',
        type: "POST",
        data:{check_for:'video_name',check_value:function() { return $('input[name=video_name]').val(); },tablename:'videos',where_name:'video_id',where_value:function() { return $('input[name=video_id]').val();},msg:'Videoname existiert bereits'}
      },
    },
    video_time_length:{
      required: true
    },
    categories: {
      required: true
    },
    starring:{
      required: true,
    },
    video_description:{
      required: true,
    },
    right_sidebar_description:{
      required: true,
    },
    fps:{
      required: true,
    },
  },
  messages :{
    video_name : {
      required : 'Videoname ist erforderlich',
    },
    video_time_length:{
      required : 'Die Videozeit ist erforderlich, z. 43 min'
    },
    categories : {
      required : 'Kategorie ist erforderlich',
    },
    starring:{
      required : 'Darsteller sind erforderlich'
    }, 
    video_description:{
      required : 'Videobeschreibung ist erforderlich'
    },
    right_sidebar_description:{
      required : 'Eine Beschreibung der rechten Seitenleiste ist erforderlich'
    },
    fps:{
      required : 'FPS ist erforderlich'
    },
  },
  highlight: function (input) {
    $(input).parents('.form-line').addClass('error');
  },
  unhighlight: function (input) {
    $(input).parents('.form-line').removeClass('error');
  },
  errorPlacement: function (error, element) {
    $(element).parents('.form-group').append(error);
  },
  submitHandler:function(form){
    video_add_edit();
    return false;
  }
});

/*
* setting home page top slider add/edit validation
*/
$('#settingHomeTopSliderForm').validate({
  ignore:'',
  rules: {
    text1: {
      required: true
    },
    text2: {
      required: true
    }
  },
  messages :{
    text1 : {
      required : 'Kopfzeilentext 1 ist erforderlich',
    },
    text2 : {
      required : 'Kopfzeilentext 2 ist erforderlich',
    }
  },
  highlight: function (input) {
    $(input).parents('.form-line').addClass('error');
  },
  unhighlight: function (input) {
    $(input).parents('.form-line').removeClass('error');
  },
  errorPlacement: function (error, element) {
    $(element).parents('.form-group').append(error);
  },
  submitHandler:function(form){
    setting_page_add_edit();
    return false;
  }
});

/*
* setting coming soon slider add/edit validation
*/
$('#settingComingSoonSliderForm').validate({
  ignore:'',
  rules: {
    image1: {
      required: true
    },
  },
  messages :{
    image1 : {
      required : 'Schiebereglerbild erforderlich',
    },
  },
  highlight: function (input) {
    $(input).parents('.form-line').addClass('error');
  },
  unhighlight: function (input) {
    $(input).parents('.form-line').removeClass('error');
  },
  errorPlacement: function (error, element) {
    $(element).parents('.form-group').append(error);
  },
  submitHandler:function(form){
    setting_page_add_edit();
    return false;
  }
});



/*
* models gallery add validation
*/
$('#galleryForm').validate({
  ignore: "",
  rules: {
    "galleryImages[]":{
      required: true
    },
  },
  messages :{
    "galleryImages[]":{
      required : 'Bitte laden Sie ein Bild hoch'
    },
  },
  highlight: function (input) {
    $(input).parents('.form-line').addClass('error');
  },
  unhighlight: function (input) {
    $(input).parents('.form-line').removeClass('error');
  },
  errorPlacement: function (error, element) {
    $(element).parents('.form-group').append(error);
  },
  submitHandler:function(form){
    gallery_image_add();
    return false;
  }
});


/*
* videos gallery add validation
*/
$('#galleryVideoForm').validate({
  ignore: "",
  rules: {
    "galleryImages[]":{
      required: true
    },
  },
  messages :{
    "galleryImages[]":{
      required : 'Bitte laden Sie ein Bild hoch'
    },
  },
  highlight: function (input) {
    $(input).parents('.form-line').addClass('error');
  },
  unhighlight: function (input) {
    $(input).parents('.form-line').removeClass('error');
  },
  errorPlacement: function (error, element) {
    $(element).parents('.form-group').append(error);
  },
  submitHandler:function(form){
    video_gallery_image_add();
    return false;
  }
});


/*
* upload different format video for download and view validation
*/
$('#uploadVideoForm').validate({
  ignore: "",
  rules: {
    download_title_name: {
      required: true,
      /*remote: {
        url: admin_siteurl+'/dashboard/check_exits_value',
        type: "POST",
        data:{check_for:'download_title_name',check_value:function() { return $('input[name=download_title_name]').val(); },tablename:'videos_diff_format',where_name:'video_id',where_value:function() { return $('input[name=video_id]').val();},msg:'Title already exits'}
      },*/
    },
    device_name:{
      required: true
    },
    video_pixel: {
      required: true
    },
    fps:{
      required: true,
    },
    /*preview_video:{
      required: true,
    },*/
    trailer_video:{
      required: true,
    },
    txtVideo:{
      required: true,
    }
  },
  messages :{
    download_title_name : {
      required : 'Download-Titel ist erforderlich',
    },
    device_name:{
      required : 'Bitte wählen Sie ein Gerät aus'
    },
    video_pixel : {
      required : 'Videopixel ist erforderlich',
    },
    fps:{
      required : 'Fps is erforderlich'
    }, 
    /*preview_video:{
      required : 'Preview video is required'
    },*/
    trailer_video:{
      required : 'Trailer Video ist erforderlich'
    },
    txtVideo:{
      required : 'Vollständiges Video ist erforderlich'
    }
  },
  highlight: function (input) {
    $(input).parents('.form-line').addClass('error');
  },
  unhighlight: function (input) {
    $(input).parents('.form-line').removeClass('error');
  },
  errorPlacement: function (error, element) {
    $(element).parents('.form-group').append(error);
  },
  submitHandler:function(form){
    upload_diff_format_video();
    return false;
  }
});


/*
* html pages add/edit validation
*/
$('#pageForm').validate({
  ignore : [],
  rules: {
    footer_column_no:{
      required:true
    },
    page_name: {
      required: true,
      remote: {
        url: admin_siteurl+'/dashboard/check_exits_value',
        type: "POST",
        data:{check_for:'page_name',check_value:function() { return $('input[name=page_name]').val(); },tablename:'cms_pages',where_name:'page_id',where_value:function() { return $('input[name=page_id]').val();},msg:'Seitenname existiert bereits'}
      },
    },
    url:{
      url : true 
    },
    /*description:{
      required : true 
    },*/
  },
  messages :{
    footer_column_no : {
      required : 'Die Fußzeilenspaltennummer ist erforderlich',
    },
    page_name : {
      required : 'Seitenname ist erforderlich',
    },
    url:{
      url : 'Bitte geben Sie eine gültige URL ein' 
    },
   /* description : {
      required :'Description is required',
    },*/
  },
  highlight: function (input) {
    $(input).parents('.form-line').addClass('error');
  },
  unhighlight: function (input) {
    $(input).parents('.form-line').removeClass('error');
  },
  errorPlacement: function (error, element) {
    $(element).parents('.form-group').append(error);
  },
  submitHandler:function(form){
   html_page_add_edit();
   return false;
 }
});


/*
* footer section edit validation
*/
$('#footerSectionForm').validate({
  rules: {
    footer_column_no:{
      required:true
    },
    copyright_text: {
      required: true,
    },
    coulmn1_title: {
      required: true,
    },
    coulmn2_title: {
      required: true,
    },
    coulmn3_title: {
      required: true,
    },
    coulmn4_title: {
      required: true,
    },
  },
  messages :{
    footer_column_no : {
      required : 'Die Fußzeilenspaltennummer ist erforderlich',
    },
    copyright_text : {
      required : 'Copyright-Text ist erforderlich',
    },
    coulmn1_title : {
      required : 'Titel der Spalte 1 ist erforderlich',
    },
    coulmn2_title : {
      required : 'Titel der Spalte 2 ist erforderlich',
    },
    coulmn3_title : {
      required : 'Titel der Spalte 3 ist erforderlich',
    },
    coulmn4_title : {
      required : 'Titel der Spalte 4 ist erforderlich',
    },
  },
  highlight: function (input) {
    $(input).parents('.form-line').addClass('error');
  },
  unhighlight: function (input) {
    $(input).parents('.form-line').removeClass('error');
  },
  errorPlacement: function (error, element) {
    $(element).parents('.form-group').append(error);
  },
  submitHandler:function(form){
   footer_section_edit();
   return false;
 }
});


/*
* page's header section edit validation
*/
$('#headerSectionForm').validate({
  ignore : [],
  rules: {
    heading_title:{
      required:true
    },
  },
  messages :{
    heading_title : {
      required : 'Überschrift Titel ist erforderlich',
    },
  },
  highlight: function (input) {
    $(input).parents('.form-line').addClass('error');
  },
  unhighlight: function (input) {
    $(input).parents('.form-line').removeClass('error');
  },
  errorPlacement: function (error, element) {
    $(element).parents('.form-group').append(error);
  },
  submitHandler:function(form){
   header_section_add_edit();
   return false;
 }
});

});


function admin_login(){
  $.ajax({
    url: admin_siteurl+'/logincheck',
    type:'POST',
    headers: { 'x-csrf-token' : $('meta[name="csrf-token"]').attr('content') },
    data:$("#adminLoginForm").serialize(),
    dataType: 'JSON',
    success : function(response){
      if(response.success == true){
       $('#msg_alert').removeClass('bg-red');
       $('#msg_alert').html(response.message).addClass('alert bg-green').show().fadeOut(7000);
       $('#adminLoginForm')[0].reset(); 
       window.location= admin_siteurl+"/dashboard";
     } else{
       $('#msg_alert').removeClass('bg-green');
       $('#msg_alert').html(response.message).addClass('alert bg-red').show().fadeOut(5000);
     }

   }
 });
}

// function admin_forgot_passowrd(){
//  //$(".btn").attr('disabled',true) ;
//  $.ajax({
//   url: '/admin/forgot-password-function',
//   type:'POST',
//   //headers: { 'x-csrf-token' : $('meta[name="csrf-token"]').attr('content') },
//   //data:$("#forgot_password").serialize(),
//   data: {"_token": "{{ csrf_token() }}","id": 'pushjj'}
//   dataType: 'JSON',
//   success : function(response){
//     if(response.success == true){
//      $('#msg_alert').removeClass('bg-red');
//      $('#msg_alert').html(response.message).addClass('alert bg-green').show().fadeOut(9000);
//      $('#forgot_password')[0].reset(); 
//    } else{
//      $('#msg_alert').removeClass('bg-green');
//      $('#msg_alert').html(response.message).addClass('alert bg-red').show().fadeOut(5000);
//    }
//    $(".btn").attr('disabled',false) ;
//  }
// });
// }

// function admin_reset_password(){
//   $(".btn").attr('disabled',true) ;
//   $.ajax({
//     url: admin_siteurl+'/reset-password-function',
//     type:'POST',
//     headers: { 'x-csrf-token' : $('meta[name="csrf-token"]').attr('content') },
//     data:$("#reset_password").serialize(),
//     dataType: 'JSON',
//     success : function(response){
//       if(response.success == true){
//        $('#msg_alert').removeClass('bg-red');
//        $('#msg_alert').html(response.message).addClass('alert bg-green').show().fadeOut(9000);
//        $('#reset_password')[0].reset(); 
//        window.location= admin_siteurl+'/login';
//      } else{
//        $('#msg_alert').removeClass('bg-green');
//        $('#msg_alert').html(response.message).addClass('alert bg-red').show().fadeOut(5000);
//      }
//      $(".btn").attr('disabled',false) ;
//    }
//  });
// } 


function profile_change_password(){
 var formdata = new FormData($('#changePasswordForm')[0]) ;
 $(".btn").attr('disabled',true) ;
 $.ajax({
  url: admin_siteurl+'/profile/change_password',
  type:'POST',
  headers:{'x-csrf-token':$('meta[name="csrf-token"]').attr('content')},
  data: formdata,
  dataType: 'JSON',
  cache: false,
  contentType: false,
  processData: false,
  success : function(response){
    if(response.success == true){
     var colorName = 'alert-success';
     $('#changePasswordForm')[0].reset(); 

     setTimeout(function() {
      window.location.reload(true);
    }, 1500);

   } else{
    var colorName = 'alert-danger';
    $(".btn").attr('disabled',false) ;
  }
  var msg = response.message ;
  show_notify_alert_msg(colorName, msg);

}
});
}


function edit_gateway_setting(){
  var formdata = new FormData($('form')[0]) ;
  $(".btn").attr('disabled',true) ;
  $.ajax({
    url: admin_siteurl+'/payment_gateway_setting/edit_gateway_setting', 
    type:'post',
    headers: { 'x-csrf-token' : $('meta[name="csrf-token"]').attr('content') },
    data: formdata,
    dataType: 'JSON',
    cache: false,
    contentType: false,
    processData: false,
    success : function(response){
      if(response.success == true){
       var colorName = 'alert-success';
/*       if(response.action == 'add'){
        $('#PaymentGatewayForm')[0].reset(); 
      }
      */
      setTimeout(function() {
        window.location= admin_siteurl+"/payment_gateway_setting";
      }, 1500);

    } else{
      var colorName = 'alert-danger';
      $(".btn").attr('disabled',false) ;
    }
    var msg = response.message ;
    show_notify_alert_msg(colorName, msg);

  }
});
}




function mail_template_add_edit(){
 var formdata = new FormData($('form')[0]) ;
 $(".btn").attr('disabled',true) ;
 $.ajax({
  url: admin_siteurl+'/mail_template/add-edit-function',
  type:'POST',
  headers:{'x-csrf-token':$('meta[name="csrf-token"]').attr('content')},
  data: formdata,
  dataType: 'JSON',
  cache: false,
  contentType: false,
  processData: false,
  success : function(response){
    if(response.success == true){
     var colorName = 'alert-success';
     if(response.action == 'add'){
      $('#mailTemplateForm')[0].reset(); 
    }
    setTimeout(function() {
      window.location= admin_siteurl+"/mail_template";
    }, 1500);

  } else{
    var colorName = 'alert-danger';
    $(".btn").attr('disabled',false) ;
  }
  var msg = response.message ;
  show_notify_alert_msg(colorName, msg);
}
});
}



function setting_page_add_edit(){
  var setting_slug_name = $("#setting_slug_name").val() ;
  var formdata = new FormData($('form')[0]) ;
  $(".btn").attr('disabled',true) ;
  $.ajax({
    url: admin_siteurl+'/settings/add_edit_function',
    type:'POST',
    data: formdata,
    dataType: 'JSON',
    cache: false,
    contentType: false,
    processData: false,
    success : function(response){
      if(response.success == true){
       var colorName = 'alert-success';
       if(response.action == 'add'){
        if(setting_slug_name == 'coming-soon-slider'){
          $('#settingComingSoonSliderForm')[0].reset(); 
        }else{
          $('#settingHomeTopSliderForm')[0].reset(); 
        }
        
        $("#previewImg").html('') ;
      }

      setTimeout(function() {
        window.location= admin_siteurl+"/setting/"+setting_slug_name ;
      }, 1500);

    } else{
      var colorName = 'alert-danger';
      $(".btn").attr('disabled',false) ;
    }
    var msg = response.message ;
    show_notify_alert_msg(colorName, msg);

  }
});
}

/*
* preview image
*/
function load_preview_image(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('#previewImg').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

/*
* for notify alert message
*/
function show_notify_alert_msg(colorName, text) {
  if (colorName === null || colorName === '') { colorName = 'bg-black'; }
  if (text === null || text === '') { text = 'Oops...!'; }

  var allowDismiss = true;

  $.notify({
    message: text
  },
  {
    type: colorName,
    allow_dismiss: allowDismiss,
    newest_on_top: true,
    timer: 200,
    placement: {
      from: 'bottom',
      align: 'center'
    },
    template: '<div data-notify="container" class="bootstrap-notify-container alert alert-dismissible {0} ' + (allowDismiss ? "p-r-35" : "") + '" role="alert">' +
    '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
    '<span data-notify="icon"></span> ' +
    '<span data-notify="title">{1}</span> ' +
    '<span data-notify="message">{2}</span>' +
    '<div class="progress" data-notify="progressbar">' +
    '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
    '</div>' +
    '<a href="{3}" target="{4}" data-notify="url"></a>' +
    '</div>'
  });
}





