<?php



/*

|--------------------------------------------------------------------------

| Web Routes

|--------------------------------------------------------------------------

|

| Here is where you can register web routes for your application. These

| routes are loaded by the RouteServiceProvider within a group which

| contains the "web" middleware group. Now create something great!

|

*/



/*Route::get('/', function () {

    return view('auth/login');

});*/

    Auth::routes();




    Route::get('/clear', function () {

	$exitCode = Artisan::call('view:clear', []);   

		  return '<h1>cache cleared</h1>';

	});

	Route::get('/config-cache', function() {

		$exitCode = Artisan::call('config:cache');

		$exitCode = Artisan::call('config:clear');

		  return '<h1>config cache cleared</h1>';

  

	});

	Route::get('/view-clear', function() {

		$exitCode = Artisan::call('view:clear');

		return '<h1>View cache cleared</h1>';

	});



    Route::get('/', 'HomeController@index');

    Route::get('/home', 'HomeController@index');
    
    Route::post('/user_save', 'UserController@user_save');

    Route::post('/reservation_save', 'UserController@reservation_save');

    Route::get('/table_reservation', 'UserController@table_reservation');
    
    Route::get('/booking_list', 'UserController@booking_list');
    Route::get('/verification', 'UserController@verification');

    Route::get('/delete_profile','DashboardController@delete_profile');
     Route::get('/logout_profile','DashboardController@logout_profile');

    Route::get('admin', 'Admin\LoginController@index');

    Route::get('admin/login', 'Admin\LoginController@index');

    Route::post('admin/logincheck', 'Admin\LoginController@loginCheck');

    Route::get('admin/forgot-password', 'Admin\LoginController@forgot_password');

    Route::post('admin/forgot-password-function', ['uses' =>'Admin\LoginController@forgot_password_function', 'as' => 'admin/forgot-password-function']); 

    Route::get('admin/matchProfiles/{id}', 'Admin\StudentController@matchProfiles');

    Route::get('admin/reset-password/{id}', 'Admin\LoginController@reset_password');

    //Route::post('admin/reset-password-function', 'Admin\LoginController@reset_password_function');
        
    Route::post('admin/reset-password-function', ['uses' =>'Admin\LoginController@reset_password_function', 'as' => 'admin/reset-password-function']);
        
    Route::get('admin/logout', 'Admin\LoginController@logout');

    Route::get('lang/{locale}', 'LocalizationController@lang');

    Route::get('/instauser', 'Admin\StudentController@InstaUser');

    Route::get('admin/add_student', 'Admin\StudentController@add_student');

    Route::get('admin/add_user', 'Admin\StudentController@add_user');

    Route::post('admin/submit_student', 'Admin\StudentController@submit_student');

    Route::get('admin/edit_student/{id}', 'Admin\StudentController@edit_student');

    Route::get('admin/edit_user/{id}', 'Admin\StudentController@edit_user');

    Route::post('admin/update_student', 'Admin\StudentController@update_student');

    Route::post('admin/submit_user', 'Admin\StudentController@submit_user');

    Route::post('admin/update_user', 'Admin\StudentController@update_user');

    Route::get('admin/student_list', 'Admin\StudentController@student_list');
    Route::get('admin/view_quation_answer/{id}', 'Admin\StudentController@view_quation_answer');

    Route::post('admin/update_quation_answer_admin', 'Admin\StudentController@update_quation_answer_admin');

    Route::post('admin/getStudentList', 'Admin\StudentController@getStudentList');

    Route::get('admin/user_list', 'Admin\StudentController@user_list');

    Route::get('admin/change_student_status', 'Admin\StudentController@change_student_status');

    Route::post('admin/delete_student', 'Admin\StudentController@delete_student');

    Route::get('admin/change_student_password/{id}', 'Admin\StudentController@change_password');

    Route::post('admin/update_student_password', 'Admin\StudentController@update_password');

    Route::post('admin/StudentImportData', 'Admin\StudentController@StudentImportData');

    Route::get('admin/StudentExportData', 'Admin\StudentController@StudentExportData');

    Route::post('admin/UserImportData', 'Admin\StudentController@UserImportData');

    Route::get('admin/UserExportData', 'Admin\StudentController@UserExportData');

    Route::post('admin/delete_user', 'Admin\StudentController@delete_user');

    Route::get('admin/dashboard', 'Admin\DashboardController@index');

    Route::get('admin/logout', 'Admin\DashboardController@logout');

    Route::get('admin/update_logo', 'Admin\CMSController@update_logo');

    Route::post('admin/edit_logo', 'Admin\CMSController@edit_logo');

    Route::get('admin/change-background-admin', 'Admin\CMSController@ChangeBackgroundAdmin');

    Route::post('admin/save-change-background', 'Admin\CMSController@SaveChangeBackgroundAdmin');

    Route::get('admin/edit_video', 'Admin\CMSController@edit_video');

    Route::post('admin/update_video', 'Admin\CMSController@update_video');

    Route::post('admin/update_video_student', 'Admin\CMSController@update_video_student');

    Route::get('admin/manage-media-list', 'Admin\CMSController@manageMediaList');

    Route::get('admin/manage-media', 'Admin\CMSController@addManageMedia');

    Route::post('admin/save-manage-media/', 'Admin\CMSController@savemedia');

    Route::get('admin/update-manage-media/{id}', 'Admin\CMSController@addManageMedia');

    Route::get('admin/change_managemedia_status', 'Admin\CMSController@changeManageMediaStatus');

    Route::get('admin/notification_list', 'Admin\NotificationController@notification_list');

    Route::get('admin/email-system-list', 'Admin\CMSController@emailFormateList');

    Route::get('admin/addemailformate', 'Admin\CMSController@addEmailFormate');

    Route::get('admin/editemailformate/{id}', 'Admin\CMSController@addEmailFormate');

    Route::post('admin/savemailformate', 'Admin\CMSController@saveEmailFormate');

    Route::get('admin/deleteformat/{id}', 'Admin\CMSController@deleteformat');

    Route::get('admin/email-setting/', 'Admin\CMSController@emailsetting');

    Route::post('admin/save-email-setting/', 'Admin\CMSController@saveemailsetting');

    Route::get('admin/subuserlist', 'Admin\CMSController@subuserlist');

    Route::get('admin/addsubuser', 'Admin\CMSController@addsubuser');

    Route::get('admin/updateuser/{id}', 'Admin\CMSController@updateuser');

    Route::post('admin/savesubsuer', 'Admin\CMSController@savesubsuer');
  
    Route::get('admin/changeuserpassword/{id}', 'Admin\CMSController@changeuserpassword');

    Route::post('admin/updateuserpassword', 'Admin\CMSController@updateuserpassword');

    Route::get('admin/change_user_status', 'Admin\CMSController@changeUserStatus');

    Route::get('admin/deleteUser/{id}', 'Admin\CMSController@deleteUser');

	Route::get('admin/UpdatePermission/{id}', 'Admin\CMSController@UpdatePermission');

	Route::post('admin/saveupdatepermission', 'Admin\CMSController@saveupdatepermission');

	Route::get('admin/error-access-permission', 'Admin\DashboardController@errorAccessPermission');

	Route::get('admin/add_story', 'Admin\StoryController@add_story');

	Route::get('admin/story_list', 'Admin\StoryController@story_list');

	Route::get('admin/edit_story/{id}', 'Admin\StoryController@edit_story');

    Route::post('admin/update_story', 'Admin\StoryController@update_story');

    Route::post('admin/submit_story', 'Admin\StoryController@submit_story');

    Route::get('admin/change_story_status', 'Admin\StoryController@change_story_status');

    Route::post('admin/delete_story', 'Admin\StoryController@delete_story');

     Route::post('admin/delete_question', 'Admin\QuestionsController@delete_question');

    Route::get('admin/add_testimonial', 'Admin\TestimonialController@add_testimonial');

    Route::get('admin/testimonial_list', 'Admin\TestimonialController@testimonial_list');

    Route::get('admin/edit_testimonial/{id}', 'Admin\TestimonialController@edit_testimonial');

    Route::post('admin/update_testimonial', 'Admin\TestimonialController@update_testimonial');

    Route::post('admin/submit_testimonial', 'Admin\TestimonialController@submit_testimonial');

    Route::get('admin/change_testimonial_status', 'Admin\TestimonialController@change_testimonial_status');

    Route::post('admin/delete_testimonial', 'Admin\TestimonialController@delete_testimonial');

    Route::get('admin/question_list', 'Admin\QuestionsController@question_list');

    Route::get('admin/add_question', 'Admin\QuestionsController@add_question');

    Route::post('admin/submit_question','Admin\QuestionsController@submit_question');

    Route::get('admin/edit_question/{id}', 'Admin\QuestionsController@edit_question');

    Route::post('admin/update_question', 'Admin\QuestionsController@update_question');

    Route::get('admin/change_question_status', 'Admin\QuestionsController@change_question_status');


    Route::get('admin/addquestiontruefalse', 'Admin\QuestionsController@addquestiontruefalse');

    Route::post('admin/submit_questiontruefalse','Admin\QuestionsController@submit_questiontruefalse');

    Route::get('admin/edit_questiontruefalse/{id}', 'Admin\QuestionsController@edit_questiontruefalse');

    Route::post('admin/update_questiontruefalse', 'Admin\QuestionsController@update_questiontruefalse');

    Route::get('admin/addquestionInsertText', 'Admin\QuestionsController@addquestionInsertText');

    Route::post('admin/submit_questionInsertText','Admin\QuestionsController@submit_questionInsertText');

    Route::get('admin/edit_questionInsertText/{id}', 'Admin\QuestionsController@edit_questionInsertText');

    Route::post('admin/update_questionInsertText', 'Admin\QuestionsController@update_questionInsertText');
    Route::get('admin/TableBooking', 'Admin\TableBookingController@listOfSection');
    Route::get('admin/addSection','Admin\TableBookingController@addSection');
    Route::post('admin/submitsection','Admin\TableBookingController@submitsection');
    Route::get('admin/viewsection/{id}','Admin\TableBookingController@viewsection');
    Route::get('admin/editsection/{id}','Admin\TableBookingController@editsection');
    Route::post('admin/updateSection/','Admin\TableBookingController@updateSection');
    Route::post('admin/delete_section/','Admin\TableBookingController@delete_section');
    Route::get('admin/tableList', 'Admin\TableBookingController@tableList');
    Route::get('admin/addTableList', 'Admin\TableBookingController@addTableList');
    Route::post('admin/submit_tablelist', 'Admin\TableBookingController@submit_tablelist');
    Route::get('admin/view_table_list/{id}', 'Admin\TableBookingController@view_table_list');
    Route::get('admin/edittablelist/{id}', 'Admin\TableBookingController@edittablelist');
    Route::post('admin/update_tablelist', 'Admin\TableBookingController@update_tablelist');
    Route::post('admin/delete_table_list', 'Admin\TableBookingController@delete_table_list');
    Route::get('admin/timeSlotList/{id}', 'Admin\TableBookingController@timeSlotList');
    Route::get('admin/add_timeslot', 'Admin\TableBookingController@add_timeslot');
    Route::post('admin/addslot_post', 'Admin\TableBookingController@addslot_post');
    Route::get('admin/viewTimeSlot/{id}', 'Admin\TableBookingController@viewTimeSlot');
    Route::get('admin/editTimeSlot/{id}', 'Admin\TableBookingController@editTimeSlot');
    Route::post('admin/update_timeslot', 'Admin\TableBookingController@update_timeslot');
    Route::post('admin/delete_timeslot', 'Admin\TableBookingController@delete_timeslot');
    Route::get('admin/booktable_list', 'Admin\TableBookingController@booktable_list');
    Route::get('admin/add_table_booking', 'Admin\TableBookingController@add_table_booking');
    Route::post('admin/ajax_timeslot', 'Admin\TableBookingController@ajax_timeslot');
    Route::post('admin/table_booking_post', 'Admin\TableBookingController@table_booking_post');
    Route::post('admin/uniqueemail', 'Admin\TableBookingController@uniqueemail');
    Route::post('admin/uniquenumber', 'Admin\TableBookingController@uniquenumber');
    Route::get('admin/view_table_booking_list/{id}', 'Admin\TableBookingController@view_table_booking_list');
    Route::post('admin/assign_table', 'Admin\TableBookingController@assign_table');
    Route::get('admin/ajax_table_cancel', 'Admin\TableBookingController@ajax_table_cancel');
    Route::get('admin/table_filter_status', 'Admin\TableBookingController@table_filter_status');
    Route::get('admin/ajaxval', 'Admin\TableBookingController@ajaxval');
    Route::get('admin/ajax_booktbl_filter', 'Admin\TableBookingController@ajax_booktbl_filter');


    /******************************* End admin *********************************/

    Route::post('/newsletter', 'HomeController@newsletter');
    
    Route::get('/contact', 'HomeController@contact');
    
    Route::get('/impressum', 'HomeController@impressum');
    
    Route::get('/privacy', 'HomeController@privacy');
    
    Route::get('/career', 'HomeController@career');
    
    Route::get('/faq', 'HomeController@faq');

    Route::get('/success_story', 'HomeController@success_story');

    Route::get('/success_story/{id?}', 'HomeController@success_details');

    Route::get('/question_answer/{id?}', 'HomeController@question_answer');

    Route::post('/submit_user_answer', 'HomeController@submit_user_answer');

    Route::get('/editquestion_answer/{id?}', 'HomeController@editquestion_answer');

    Route::post('/update_quation_answer', 'HomeController@update_quation_answer');



	/******************************* Start User section ************************/

    Route::get('/user/dashboard/{name?}','DashboardController@index');

    Route::get('/edituser/dashboard/{name?}','DashboardController@editUserProfile');

    Route::get('/changepassword','DashboardController@changepassword');

    Route::any('update_password_action', 'DashboardController@update_password_action');

    Route::any('UploadProfilePicture','DashboardController@UploadProfilePicture');

    Route::any('/user_update', 'DashboardController@user_update');

    // Route::get('/matchProfiles', 'DashboardController@matchProfiles');

    Route::get('/viewmatchProfile/{id?}', 'DashboardController@viewmatchProfile');

    Route::post('/send_request', 'DashboardController@send_request');


    //Route::get('/user/dashboard/{name?}','DashboardController@index');

    //Route::post('user_update', ['uses' =>'DashboardController@user_update', 'as' => 'user_update']);

    // Route::post('user_update', [
    //     'as' => 'user_update',
    //     'uses' => 'DashboardController@user_update'
    // ]);

    Route::post('/user_update', 'DashboardController@user_update')->name('user_update');


    Route::get('/user/logout','DashboardController@logout');

    Event::listen('Illuminate\Database\Events\QueryExecuted', function ($query) {

      	//echo "</br>".$query->sql; 
    });