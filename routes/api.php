<?php

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get("getUserDetails",[ApiController::class,'getUserDetails']);
Route::post("Login",[ApiController::class,'Login']);
Route::post("register",[ApiController::class,'register']);
Route::post("forgot",[ApiController::class,'forgot']);
Route::post("changePassword",[ApiController::class,'changePassword']);
Route::post('delete', [ApiController::class,'delete']);






