USE [DrZHub]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoleClaims]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoleClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [nvarchar](450) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetRoleClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](450) NOT NULL,
	[Name] [nvarchar](256) NULL,
	[NormalizedName] [nvarchar](256) NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](450) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](450) NOT NULL,
	[ProviderKey] [nvarchar](450) NOT NULL,
	[ProviderDisplayName] [nvarchar](max) NULL,
	[UserId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](450) NOT NULL,
	[RoleId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](450) NOT NULL,
	[UserName] [nvarchar](256) NULL,
	[NormalizedUserName] [nvarchar](256) NULL,
	[Email] [nvarchar](256) NULL,
	[NormalizedEmail] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEnd] [datetimeoffset](7) NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[Discriminator] [nvarchar](max) NOT NULL,
	[FirstName] [nvarchar](max) NULL,
	[LastName] [nvarchar](max) NULL,
	[CreationDate] [datetime2](7) NULL,
	[TimeZone] [nvarchar](max) NULL,
	[AreaId] [int] NULL,
	[UserType] [int] NULL,
	[Degree] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[DiscountPricePerHour] [decimal](18, 2) NULL,
	[PricePerHour] [decimal](18, 2) NULL,
	[IsOnline] [bit] NULL,
	[ThumbPath] [nvarchar](max) NULL,
	[IsVerified] [bit] NULL,
	[Address] [nvarchar](max) NULL,
	[Age] [int] NULL,
	[City] [nvarchar](max) NULL,
	[Country] [nvarchar](max) NULL,
	[Gender] [int] NULL,
	[NationalIdFilePath] [nvarchar](max) NULL,
	[VisaFilePath] [nvarchar](max) NULL,
	[RegToken] [int] NULL,
	[DateOfBirth] [datetime2](7) NULL,
	[Nationality] [nvarchar](max) NULL,
	[IsBlocked] [bit] NULL,
	[BankName] [nvarchar](max) NULL,
	[BranchName] [nvarchar](max) NULL,
	[CurrencyId] [int] NULL,
	[IBAN] [nvarchar](max) NULL,
	[SwiftAccount] [nvarchar](max) NULL,
	[ImagePath] [nvarchar](max) NULL,
	[PaymentToken] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserTokens]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserTokens](
	[UserId] [nvarchar](450) NOT NULL,
	[LoginProvider] [nvarchar](450) NOT NULL,
	[Name] [nvarchar](450) NOT NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetUserTokens] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[LoginProvider] ASC,
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BecomeATutors]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BecomeATutors](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NULL,
	[MediaPath] [nvarchar](max) NULL,
	[MediaType] [int] NOT NULL,
	[Description] [nvarchar](max) NULL,
	[CallToActionTitle] [nvarchar](max) NULL,
	[CallToActionLink] [nvarchar](max) NULL,
	[CallToActionColor] [nvarchar](max) NULL,
	[CalltoActionBorderColor] [nvarchar](max) NULL,
	[CalltoActionBackgroundcolor] [nvarchar](max) NULL,
	[LanguageId] [int] NOT NULL,
	[BackgroundColor] [nvarchar](max) NULL,
 CONSTRAINT [PK_BecomeATutors] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ChatHeads]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChatHeads](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DateCreated] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_ChatHeads] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ChatHeadStatus]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChatHeadStatus](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](450) NULL,
	[ChatHeadId] [int] NOT NULL,
	[IsOpen] [bit] NOT NULL,
	[EnterIsSend] [bit] NULL,
 CONSTRAINT [PK_ChatHeadStatus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ChatMessages]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChatMessages](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ChatHeadId] [int] NOT NULL,
	[SenderId] [nvarchar](450) NULL,
	[ReceiverId] [nvarchar](450) NULL,
	[Message] [nvarchar](max) NULL,
	[FilePath] [nvarchar](max) NULL,
	[DateCreated] [datetime2](7) NOT NULL,
	[Read] [bit] NOT NULL,
 CONSTRAINT [PK_ChatMessages] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ChatSettings]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChatSettings](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LanguageId] [int] NOT NULL,
	[MeetInClassRoom] [nvarchar](max) NULL,
	[NewMessageHeaderTip] [nvarchar](max) NULL,
	[StudentSuggestedMessage] [nvarchar](max) NULL,
	[TeacherSuggestedMessage] [nvarchar](max) NULL,
	[EnterIsSendText] [nvarchar](max) NULL,
	[SendBtnText] [nvarchar](max) NULL,
	[MeetInClassToolTip] [nvarchar](max) NULL,
	[TypeHere] [nvarchar](max) NULL,
 CONSTRAINT [PK_ChatSettings] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Classes]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Classes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NULL,
 CONSTRAINT [PK_Classes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CourseLevels]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CourseLevels](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CourseId] [int] NOT NULL,
	[LevelId] [int] NOT NULL,
 CONSTRAINT [PK_CourseLevels] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Courses]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Courses](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NULL,
	[Icon] [nvarchar](max) NULL,
	[IconColor] [nvarchar](max) NULL,
	[OrderIndex] [int] NOT NULL,
	[LanguageId] [int] NOT NULL,
	[Question] [nvarchar](max) NULL,
	[IsAvailable] [bit] NULL,
	[SuggestedByUserId] [nvarchar](450) NULL,
 CONSTRAINT [PK_Courses] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CoursesFilterSettings]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CoursesFilterSettings](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LogoPath] [nvarchar](max) NULL,
	[Title] [nvarchar](max) NULL,
	[TutorsLinkText] [nvarchar](max) NULL,
	[ButtonsColor] [nvarchar](max) NULL,
	[LanguageId] [int] NOT NULL,
	[BackGroundColor] [nvarchar](max) NULL,
	[FooterText] [nvarchar](max) NULL,
	[HeaderBondcolor] [nvarchar](max) NULL,
	[TimeZoneText] [nvarchar](max) NULL,
	[TimingQuestion] [nvarchar](max) NULL,
 CONSTRAINT [PK_CoursesFilterSettings] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Currencies]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Currencies](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NULL,
	[Rate] [real] NOT NULL,
	[IsDefault] [bit] NOT NULL,
 CONSTRAINT [PK_Currencies] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EmailContent]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailContent](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EmailType] [int] NOT NULL,
	[Subject] [nvarchar](max) NULL,
	[Body] [nvarchar](max) NULL,
	[BCC] [nvarchar](max) NULL,
	[LanguageId] [int] NOT NULL,
 CONSTRAINT [PK_EmailContent] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FAQ]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FAQ](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Question] [nvarchar](max) NULL,
	[Answer] [nvarchar](max) NULL,
	[LanguageId] [int] NOT NULL,
	[QuestionShadowColor] [nvarchar](max) NULL,
	[SelectedQuestionColor] [nvarchar](max) NULL,
	[SelectedQuestionButtonColor] [nvarchar](max) NULL,
 CONSTRAINT [PK_FAQ] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FAQSettings]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FAQSettings](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NULL,
	[SubTitle] [nvarchar](max) NULL,
	[SearchBorderColor] [nvarchar](max) NULL,
	[SearchPlaceholder] [nvarchar](max) NULL,
	[SearchPlaceholderColor] [nvarchar](max) NULL,
	[Message] [nvarchar](max) NULL,
	[CallToActionTitle] [nvarchar](max) NULL,
	[CallToActionLink] [nvarchar](max) NULL,
	[CallToActionColor] [nvarchar](max) NULL,
	[CalltoActionBorderColor] [nvarchar](max) NULL,
	[CalltoActionBackgroundcolor] [nvarchar](max) NULL,
	[LanguageId] [int] NOT NULL,
 CONSTRAINT [PK_FAQSettings] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Footers]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Footers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LogoPath] [nvarchar](max) NULL,
	[Title] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[CalltoActionBorderColor] [nvarchar](max) NULL,
	[BackgroundColor] [nvarchar](max) NULL,
	[ForGroundColor] [nvarchar](max) NULL,
	[CopyRight] [nvarchar](max) NULL,
	[LanguageId] [int] NOT NULL,
	[CallToActionColor] [nvarchar](max) NULL,
	[CallToActionLink] [nvarchar](max) NULL,
	[CallToActionTitle] [nvarchar](max) NULL,
	[CalltoActionBackgroundcolor] [nvarchar](max) NULL,
	[ProductTitle] [nvarchar](max) NULL,
	[CompanyTitle] [nvarchar](max) NULL,
 CONSTRAINT [PK_Footers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HomeBannerContents]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HomeBannerContents](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NULL,
	[SubTitle] [nvarchar](max) NULL,
	[SearchPlaceHolder] [nvarchar](max) NULL,
	[SearchPlaceHolderColor] [nvarchar](max) NULL,
	[SearchButtonTitle] [nvarchar](max) NULL,
	[SearchColor] [nvarchar](max) NULL,
	[LanguageId] [int] NOT NULL,
	[BannerOverlayColor] [nvarchar](max) NULL,
	[VideoPath] [nvarchar](max) NULL,
	[VideoPlayIcon] [nvarchar](max) NULL,
	[VideoPlayIconColor] [nvarchar](max) NULL,
	[VideoPlayText] [nvarchar](max) NULL,
 CONSTRAINT [PK_HomeBannerContents] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HomeBanners]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HomeBanners](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MediaPath] [nvarchar](max) NULL,
	[MediaType] [int] NOT NULL,
	[OrderIndex] [int] NULL,
	[OverlayColor] [nvarchar](max) NULL,
 CONSTRAINT [PK_HomeBanners] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HomeHeaders]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HomeHeaders](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LogoPath] [nvarchar](max) NULL,
	[StickyLogoPath] [nvarchar](max) NULL,
	[BackgroundColor] [nvarchar](max) NULL,
	[ForGroundColor] [nvarchar](max) NULL,
	[StickyBackgroundColor] [nvarchar](max) NULL,
	[StickyForGroundColor] [nvarchar](max) NULL,
	[RegisterButtonColor] [nvarchar](max) NULL,
	[CoursesBackgroundcolor] [nvarchar](max) NULL,
	[SelectedMenuBtnColor] [nvarchar](max) NULL,
	[SelectedMenuFontColor] [nvarchar](max) NULL,
	[MobileLogoPath] [nvarchar](max) NULL,
	[MobileStickyLogoPath] [nvarchar](max) NULL,
	[EmailPassword] [nvarchar](max) NULL,
	[EmailPort] [int] NULL,
	[EmailSmtp] [nvarchar](max) NULL,
	[EmailUser] [nvarchar](max) NULL,
	[IsEmailSSL] [bit] NULL,
	[PaymentAccessKey] [nvarchar](max) NULL,
	[PaymentIsTest] [bit] NULL,
	[PaymentProfileId] [nvarchar](max) NULL,
	[PaymentSecurityKey] [nvarchar](max) NULL,
	[EmailName] [nvarchar](max) NULL,
	[WebsiteIconPath] [nvarchar](max) NULL,
	[WebsiteTitle] [nvarchar](max) NULL,
	[GoogleTagId] [nvarchar](max) NULL,
 CONSTRAINT [PK_HomeHeaders] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HomeSectionCategory]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HomeSectionCategory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SectionName] [nvarchar](max) NULL,
	[Title] [nvarchar](max) NULL,
	[SubTitle] [nvarchar](max) NULL,
	[BackGroundImagePath] [nvarchar](max) NULL,
	[VideoPath] [nvarchar](max) NULL,
	[BackGroundColor] [nvarchar](max) NULL,
	[SectionTemplate] [int] NOT NULL,
	[LanguageId] [int] NOT NULL,
	[CallToActionColor] [nvarchar](max) NULL,
	[CallToActionLink] [nvarchar](max) NULL,
	[CallToActionTitle] [nvarchar](max) NULL,
	[CalltoActionBackgroundcolor] [nvarchar](max) NULL,
	[CalltoActionBorderColor] [nvarchar](max) NULL,
	[HowItWorksVideoPath] [nvarchar](max) NULL,
	[HowItWorksVideoPlayIcon] [nvarchar](max) NULL,
	[HowItWorksVideoPlayIconColor] [nvarchar](max) NULL,
	[HowItWorksVideoPlayText] [nvarchar](max) NULL,
	[OrderIndex] [int] NULL,
 CONSTRAINT [PK_HomeSectionCategory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HomeSections]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HomeSections](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NULL,
	[Icon] [nvarchar](max) NULL,
	[IconColor] [nvarchar](max) NULL,
	[IconBackGroundColor] [nvarchar](max) NULL,
	[SubTitle] [nvarchar](max) NULL,
	[PersonImagePath] [nvarchar](max) NULL,
	[CommentIconPath] [nvarchar](max) NULL,
	[EmoticonPath] [nvarchar](max) NULL,
	[PersonTitle] [nvarchar](max) NULL,
	[HomeSectionCategoryId] [int] NOT NULL,
	[LanguageId] [int] NOT NULL,
 CONSTRAINT [PK_HomeSections] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[InvitedEmails]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InvitedEmails](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SessionId] [int] NOT NULL,
	[Email] [nvarchar](max) NULL,
 CONSTRAINT [PK_InvitedEmails] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Language]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Language](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[RouteCode] [nvarchar](max) NULL,
	[IconPath] [nvarchar](max) NULL,
	[IsDefault] [bit] NULL,
	[IsEnabled] [bit] NULL,
	[IsRTL] [bit] NULL,
 CONSTRAINT [PK_Language] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Level]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Level](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NULL,
	[ParentId] [int] NULL,
	[Question] [nvarchar](max) NULL,
	[OrderIndex] [int] NOT NULL,
 CONSTRAINT [PK_Level] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LoginPageSettings]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoginPageSettings](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[StudentLoginTitle] [nvarchar](max) NULL,
	[LoginAsStudentText] [nvarchar](max) NULL,
	[RegisterAsStudentText] [nvarchar](max) NULL,
	[TutorLoginTitle] [nvarchar](max) NULL,
	[LoginAsTutorText] [nvarchar](max) NULL,
	[RegisterAsTutorText] [nvarchar](max) NULL,
	[EmailFieldText] [nvarchar](max) NULL,
	[PasswordFieldText] [nvarchar](max) NULL,
	[ForgotPasswordLinkText] [nvarchar](max) NULL,
	[RememberMeText] [nvarchar](max) NULL,
	[PageBtnColor] [nvarchar](max) NULL,
	[LoginBtnText] [nvarchar](max) NULL,
	[EmailValidationText] [nvarchar](max) NULL,
	[PasswordValidationText] [nvarchar](max) NULL,
	[LoginFailureText] [nvarchar](max) NULL,
	[LanguageId] [int] NOT NULL,
	[ForgotPasswordEmailNotFound] [nvarchar](max) NULL,
	[ForgotPasswordEmailSent] [nvarchar](max) NULL,
	[OrText] [nvarchar](max) NULL,
	[PageBackGroundColor] [nvarchar](max) NULL,
	[StudentRegisterTitle] [nvarchar](max) NULL,
	[TermsAndConditionsMessage] [nvarchar](max) NULL,
	[UserRegistrationSuccessMessage] [nvarchar](max) NULL,
	[BlockedUserBtnBackgroundColor] [nvarchar](max) NULL,
	[BlockedUserBtnColor] [nvarchar](max) NULL,
	[BlockedUserBtnText] [nvarchar](max) NULL,
	[BlockedUserMessage] [nvarchar](max) NULL,
	[BlockedUserPageHeaderColor] [nvarchar](max) NULL,
	[BlockedUserPageLogoPath] [nvarchar](max) NULL,
	[ResetPasswordTitle] [nvarchar](max) NULL,
	[ConfirmAccountTitle] [nvarchar](max) NULL,
 CONSTRAINT [PK_LoginPageSettings] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Notifications]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Notifications](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Text] [nvarchar](max) NULL,
	[DateIn] [datetime2](7) NULL,
	[LargeImage] [nvarchar](max) NULL,
	[Link] [nvarchar](max) NULL,
	[SmallImage] [nvarchar](max) NULL,
 CONSTRAINT [PK_Notifications] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NotificationsContent]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NotificationsContent](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IconPath] [nvarchar](max) NULL,
	[Message] [nvarchar](max) NULL,
	[PopUpMessage] [nvarchar](max) NULL,
	[NotificationType] [int] NOT NULL,
	[ForStudent] [bit] NOT NULL,
	[ActionType] [int] NOT NULL,
	[Link] [nvarchar](max) NULL,
	[LanguageId] [int] NOT NULL,
	[ActionNotificationId] [int] NOT NULL,
 CONSTRAINT [PK_NotificationsContent] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NotificationUsers]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NotificationUsers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NotificationId] [int] NOT NULL,
	[ApplicationUserId] [nvarchar](450) NULL,
	[IsRead] [bit] NOT NULL,
 CONSTRAINT [PK_NotificationUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Payments]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Payments](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TeachingSessionId] [int] NOT NULL,
	[PaymentDate] [datetime2](7) NOT NULL,
	[TransactionNumber] [nvarchar](max) NULL,
 CONSTRAINT [PK_Payments] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Pricings]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Pricings](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[MediaPath] [nvarchar](max) NULL,
	[MediaType] [int] NOT NULL,
	[PricingTemplate] [int] NOT NULL,
	[BackGroundColor] [nvarchar](max) NULL,
	[LanguageId] [int] NOT NULL,
	[CallToActionColor] [nvarchar](max) NULL,
	[CallToActionLink] [nvarchar](max) NULL,
	[CallToActionTitle] [nvarchar](max) NULL,
	[CalltoActionBackgroundcolor] [nvarchar](max) NULL,
	[CalltoActionBorderColor] [nvarchar](max) NULL,
 CONSTRAINT [PK_Pricings] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PricingTips]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PricingTips](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Icon] [nvarchar](max) NULL,
	[Title] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[OrderIndex] [int] NOT NULL,
	[IconColor] [nvarchar](max) NULL,
	[IconBackgroundColor] [nvarchar](max) NULL,
	[PricingId] [int] NOT NULL,
	[LanguageId] [int] NOT NULL,
 CONSTRAINT [PK_PricingTips] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SessionBookings]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SessionBookings](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[StudentId] [nvarchar](450) NULL,
	[BookingStatus] [int] NOT NULL,
	[SessionId] [int] NOT NULL,
 CONSTRAINT [PK_SessionBookings] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SocialMedias]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SocialMedias](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Icon] [nvarchar](max) NULL,
	[Link] [nvarchar](max) NULL,
 CONSTRAINT [PK_SocialMedias] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SpeakLevel]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SpeakLevel](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NULL,
	[LanguageId] [int] NOT NULL,
 CONSTRAINT [PK_SpeakLevel] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SpokenLanguage]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SpokenLanguage](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NULL,
	[LanguageId] [int] NOT NULL,
 CONSTRAINT [PK_SpokenLanguage] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StaticPages]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StaticPages](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[LanguageId] [int] NOT NULL,
 CONSTRAINT [PK_StaticPages] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StudentDashboard]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StudentDashboard](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LanguageId] [int] NOT NULL,
	[LogoPath] [nvarchar](max) NULL,
	[MenuMyProfile] [nvarchar](max) NULL,
	[MenuMyLessons] [nvarchar](max) NULL,
	[MenuMyTutors] [nvarchar](max) NULL,
	[EmailTitle] [nvarchar](max) NULL,
	[DateOfBirthTitle] [nvarchar](max) NULL,
	[GenderTitle] [nvarchar](max) NULL,
	[NationalityTitle] [nvarchar](max) NULL,
	[CountryTitle] [nvarchar](max) NULL,
	[CitytTitle] [nvarchar](max) NULL,
	[AddressTitle] [nvarchar](max) NULL,
	[CompletedSessionsText] [nvarchar](max) NULL,
	[CompletedSessionsColor] [nvarchar](max) NULL,
	[CompletedSessionsIconPath] [nvarchar](max) NULL,
	[UpcomingLessonsText] [nvarchar](max) NULL,
	[FindATutorBtnText] [nvarchar](max) NULL,
	[FindATutorBtnBackgroundColor] [nvarchar](max) NULL,
	[FindATutorBtnTextColor] [nvarchar](max) NULL,
	[OnlineTooltip] [nvarchar](max) NULL,
	[OfflineTooltip] [nvarchar](max) NULL,
	[UpcomingLessonsBtnText] [nvarchar](max) NULL,
	[HistoryLessonsBtnText] [nvarchar](max) NULL,
	[TableSubjectTitle] [nvarchar](max) NULL,
	[TableDateAndTimeTitle] [nvarchar](max) NULL,
	[TablePriceTitle] [nvarchar](max) NULL,
	[TableDescriptionTitle] [nvarchar](max) NULL,
	[TableTutorTitle] [nvarchar](max) NULL,
	[TablePaymentStatusTitle] [nvarchar](max) NULL,
	[TableSessionStatusTitle] [nvarchar](max) NULL,
	[TableRecordingsTitle] [nvarchar](max) NULL,
	[TableMyRatingAndReviewTitle] [nvarchar](max) NULL,
	[NoUpcomingLessonsMsg] [nvarchar](max) NULL,
	[NoHistoryLessonsMsg] [nvarchar](max) NULL,
	[PendingPaymentText] [nvarchar](max) NULL,
	[PaidText] [nvarchar](max) NULL,
	[ViewBtnText] [nvarchar](max) NULL,
	[JoinBtnText] [nvarchar](max) NULL,
	[PayBtnText] [nvarchar](max) NULL,
	[CancelBtnText] [nvarchar](max) NULL,
	[CanceledText] [nvarchar](max) NULL,
	[CompletedText] [nvarchar](max) NULL,
	[PlayLessonBtnText] [nvarchar](max) NULL,
	[NoRecordingText] [nvarchar](max) NULL,
	[RateNowText] [nvarchar](max) NULL,
	[ReadMoreBtnText] [nvarchar](max) NULL,
	[ViewAllCoursesBtnText] [nvarchar](max) NULL,
	[MyRatingText] [nvarchar](max) NULL,
	[RatedAllYourLessonsText] [nvarchar](max) NULL,
	[RatedAllYourLessonsIcon] [nvarchar](max) NULL,
	[RatedAllYourLessonsColor] [nvarchar](max) NULL,
	[NoLessonsToRateText] [nvarchar](max) NULL,
	[NoLessonsToRateIcon] [nvarchar](max) NULL,
	[NoLessonsToRateColor] [nvarchar](max) NULL,
	[CompletedLessonsText] [nvarchar](max) NULL,
	[CompletedLessonsIconPath] [nvarchar](max) NULL,
	[CompletedLessonsColor] [nvarchar](max) NULL,
	[YouHaveUpcomingLessonsText] [nvarchar](max) NULL,
	[YouHaveUpcomingLessonsIconPath] [nvarchar](max) NULL,
	[YouHaveUpcomingLessonsColor] [nvarchar](max) NULL,
	[RatingPopUpTitle] [nvarchar](max) NULL,
	[ViewProfileLinkText] [nvarchar](max) NULL,
	[LessonDetailsText] [nvarchar](max) NULL,
	[DateAndTimeText] [nvarchar](max) NULL,
	[SubjectText] [nvarchar](max) NULL,
	[DescriptionText] [nvarchar](max) NULL,
	[LessonPriceText] [nvarchar](max) NULL,
	[RateYourLessonTitle] [nvarchar](max) NULL,
	[LeaveAReviewText] [nvarchar](max) NULL,
	[PopUpMyRatingTitle] [nvarchar](max) NULL,
	[PopUpMyReviewTitle] [nvarchar](max) NULL,
	[SubmitBtnText] [nvarchar](max) NULL,
	[PopUpPaidText] [nvarchar](max) NULL,
	[YouWillBeAbletoRateText] [nvarchar](max) NULL,
	[YouWillBeAbletoReviewText] [nvarchar](max) NULL,
	[BookAnotherSessionBtnText] [nvarchar](max) NULL,
	[SendTeacherMessageBtnText] [nvarchar](max) NULL,
	[JoinLessonNowBtnText] [nvarchar](max) NULL,
	[AreYouSureYouWantToCancelLessonTitle] [nvarchar](max) NULL,
	[YesBtnText] [nvarchar](max) NULL,
	[NoBtnText] [nvarchar](max) NULL,
	[LessonCompletedTitle] [nvarchar](max) NULL,
	[LessonCanceledTitle] [nvarchar](max) NULL,
	[TutorDetailsText] [nvarchar](max) NULL,
	[CongratulationsLessonCompletedText] [nvarchar](max) NULL,
	[NowText] [nvarchar](max) NULL,
	[UpcomingLessonPopUpTitle] [nvarchar](max) NULL,
	[UpcomingLessonsColor] [nvarchar](max) NULL,
	[UpcomingLessonsIconPath] [nvarchar](max) NULL,
	[YouHaveUnratedLessonsColor] [nvarchar](max) NULL,
	[YouHaveUnratedLessonsText] [nvarchar](max) NULL,
	[StartDateMustBeGreaterThanEndDate] [nvarchar](max) NULL,
	[NoFreeSlotsAvailable] [nvarchar](max) NULL,
	[ThisTeacherDosentTeachThisCourse] [nvarchar](max) NULL,
	[AnErrorOccured] [nvarchar](max) NULL,
	[IncorrectCode] [nvarchar](max) NULL,
	[PasswordsDoNotMatch] [nvarchar](max) NULL,
	[EmailUsed] [nvarchar](max) NULL,
	[EditPictureBtnText] [nvarchar](max) NULL,
	[ViewPhotoBtnText] [nvarchar](max) NULL,
	[UploadPhotoBtnText] [nvarchar](max) NULL,
	[RemovePhotoBtnText] [nvarchar](max) NULL,
	[Male] [nvarchar](max) NULL,
	[Female] [nvarchar](max) NULL,
	[EditPersonalDetailsBtnText] [nvarchar](max) NULL,
	[ViewAllBtnText] [nvarchar](max) NULL,
	[ActiveStudents] [nvarchar](max) NULL,
	[CompletedSessions] [nvarchar](max) NULL,
	[YouCurrentlyDontHaveAnyTutor] [nvarchar](max) NULL,
	[FindATutor] [nvarchar](max) NULL,
	[Ok] [nvarchar](max) NULL,
	[NoProfilePictureUploadedYet] [nvarchar](max) NULL,
	[UploadAFileToStartCropping] [nvarchar](max) NULL,
	[SavePhoto] [nvarchar](max) NULL,
	[ReViewValidationText] [nvarchar](max) NULL,
	[ThankYouForYourRating] [nvarchar](max) NULL,
	[EditBtnText] [nvarchar](max) NULL,
	[PersonalDetails] [nvarchar](max) NULL,
	[ProvideYourEmailAddress] [nvarchar](max) NULL,
	[EnterNewEmailAddress] [nvarchar](max) NULL,
	[ValidEmailMustBeProvided] [nvarchar](max) NULL,
	[SaveBtnText] [nvarchar](max) NULL,
	[VerifyYourNewEmail] [nvarchar](max) NULL,
	[EnterVerificationCode] [nvarchar](max) NULL,
	[ProvideYourNewPassword] [nvarchar](max) NULL,
	[CurrentPassword] [nvarchar](max) NULL,
	[NewPassword] [nvarchar](max) NULL,
	[ReEnterNewPassword] [nvarchar](max) NULL,
	[PersonalDetailsSuccessfullyChanged] [nvarchar](max) NULL,
	[ValidCodeMustBeProvided] [nvarchar](max) NULL,
	[CurrentPasswordValidation] [nvarchar](max) NULL,
	[NewPasswordValidation] [nvarchar](max) NULL,
	[ReNewPasswordValidation] [nvarchar](max) NULL,
	[PasswordSuccessfullyChanged] [nvarchar](max) NULL,
	[EmailValidation] [nvarchar](max) NULL,
	[EmailSuccessfullyChanged] [nvarchar](max) NULL,
	[UpcomingLessonOnlinePaymentText] [nvarchar](max) NULL,
	[ViewMoreBtnText] [nvarchar](max) NULL,
	[MissedText] [nvarchar](max) NULL,
	[RatingNotAvailable] [nvarchar](max) NULL,
	[ThisLessonWasCanceledPopUpText] [nvarchar](max) NULL,
	[ThisLessonWasMissedPopUpText] [nvarchar](max) NULL,
	[CloseBtnText] [nvarchar](max) NULL,
	[CompleteYourRegistration] [nvarchar](max) NULL,
 CONSTRAINT [PK_StudentDashboard] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TeacherRegistrationSettings]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TeacherRegistrationSettings](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LanguageId] [int] NOT NULL,
	[StepBackgroundcolor] [nvarchar](max) NULL,
	[StepFontcolor] [nvarchar](max) NULL,
	[StepOneText] [nvarchar](max) NULL,
	[StepTwoText] [nvarchar](max) NULL,
	[StepThreeText] [nvarchar](max) NULL,
	[StepFourText] [nvarchar](max) NULL,
	[StepFiveText] [nvarchar](max) NULL,
	[StepSixText] [nvarchar](max) NULL,
	[StepOneTitle] [nvarchar](max) NULL,
	[StepTwoTitle] [nvarchar](max) NULL,
	[StepThreeTitle] [nvarchar](max) NULL,
	[StepFourTitle] [nvarchar](max) NULL,
	[StepFiveTitle] [nvarchar](max) NULL,
	[StepSixTitle] [nvarchar](max) NULL,
	[ButtonBackgroundColor] [nvarchar](max) NULL,
	[ButtonFontColor] [nvarchar](max) NULL,
	[ButtonBorderColor] [nvarchar](max) NULL,
	[InputControlBorderColor] [nvarchar](max) NULL,
	[InputControlFontColor] [nvarchar](max) NULL,
	[FullNameText] [nvarchar](max) NULL,
	[DateOfBirthText] [nvarchar](max) NULL,
	[GenderText] [nvarchar](max) NULL,
	[CountryText] [nvarchar](max) NULL,
	[CityText] [nvarchar](max) NULL,
	[AddressText] [nvarchar](max) NULL,
	[EmailText] [nvarchar](max) NULL,
	[PasswordText] [nvarchar](max) NULL,
	[RetypePasswordText] [nvarchar](max) NULL,
	[NextBtnText] [nvarchar](max) NULL,
	[LoginPageTitle] [nvarchar](max) NULL,
	[ForgotPasswordTitle] [nvarchar](max) NULL,
	[ForgotPasswordButtonText] [nvarchar](max) NULL,
	[PageFooterText] [nvarchar](max) NULL,
	[PageBackgroundColor] [nvarchar](max) NULL,
	[PageHeaderStripColor] [nvarchar](max) NULL,
	[OTPPageTitle] [nvarchar](max) NULL,
	[OTPCodeFieldText] [nvarchar](max) NULL,
	[WrongOTPIcon] [nvarchar](max) NULL,
	[WrongOTPIconColor] [nvarchar](max) NULL,
	[WrongOTPText] [nvarchar](max) NULL,
	[WrongOTPButtonText] [nvarchar](max) NULL,
	[VerificationCodeFieldText] [nvarchar](max) NULL,
	[WrongVerificationCodeIcon] [nvarchar](max) NULL,
	[WrongVerificationCodeIconColor] [nvarchar](max) NULL,
	[WrongVerificationCodeText] [nvarchar](max) NULL,
	[WrongVerificationCodeButtonText] [nvarchar](max) NULL,
	[EmailAlreadyRegisteredText] [nvarchar](max) NULL,
	[CorrectVerificationCodeIcon] [nvarchar](max) NULL,
	[CorrectVerificationCodeIconColor] [nvarchar](max) NULL,
	[CorrectVerificationCodeText] [nvarchar](max) NULL,
	[CorrectVerificationCodeButtonText] [nvarchar](max) NULL,
	[SpokenLanguagePlaceholder] [nvarchar](max) NULL,
	[SpeakLevelPlaceholder] [nvarchar](max) NULL,
	[AddLanguageButtonText] [nvarchar](max) NULL,
	[AddLanguageButtonColor] [nvarchar](max) NULL,
	[InstitutionNamePlaceholder] [nvarchar](max) NULL,
	[EducationFromPlaceholder] [nvarchar](max) NULL,
	[EducationToPlaceholder] [nvarchar](max) NULL,
	[DegreePlaceholder] [nvarchar](max) NULL,
	[UploadDiplomaButtonText] [nvarchar](max) NULL,
	[UploadDiplomaButtonColor] [nvarchar](max) NULL,
	[InstitutionNameTooltip] [nvarchar](max) NULL,
	[AddDiplomaButtonColor] [nvarchar](max) NULL,
	[AddDiplomaButtonText] [nvarchar](max) NULL,
	[CourseSubjectFieldPlaceholder] [nvarchar](max) NULL,
	[CourseSubjectFieldBorderColor] [nvarchar](max) NULL,
	[SearchButtonBackgroundColor] [nvarchar](max) NULL,
	[SearchButtonTextColor] [nvarchar](max) NULL,
	[SearchButtonText] [nvarchar](max) NULL,
	[NotFindingButtonText] [nvarchar](max) NULL,
	[NotFindingButtonTextColor] [nvarchar](max) NULL,
	[NotFindingButtonTooltip] [nvarchar](max) NULL,
	[TypeCourseNameText] [nvarchar](max) NULL,
	[SelectTeachingLanguageText] [nvarchar](max) NULL,
	[NationalIdText] [nvarchar](max) NULL,
	[VisaText] [nvarchar](max) NULL,
	[TermsAndConditionsTitle] [nvarchar](max) NULL,
	[TermsAndConditionsText] [nvarchar](max) NULL,
	[AcceptTAndCText] [nvarchar](max) NULL,
	[AcceptTAndCTextColor] [nvarchar](max) NULL,
	[ThankyouLogoPath] [nvarchar](max) NULL,
	[ThankyouText] [nvarchar](max) NULL,
	[FinishButtonText] [nvarchar](max) NULL,
	[FinishButtonBackGroundColor] [nvarchar](max) NULL,
	[FinishButtonTextColor] [nvarchar](max) NULL,
	[FinishButtonBorderColor] [nvarchar](max) NULL,
	[AddressValidation] [nvarchar](max) NULL,
	[CityValidation] [nvarchar](max) NULL,
	[CountryValidation] [nvarchar](max) NULL,
	[DateOfBirthValidation] [nvarchar](max) NULL,
	[EmailValidation] [nvarchar](max) NULL,
	[FullNameValidation] [nvarchar](max) NULL,
	[GenderValidation] [nvarchar](max) NULL,
	[PasswordValidation] [nvarchar](max) NULL,
	[RetypePasswordValidation] [nvarchar](max) NULL,
	[EducationText] [nvarchar](max) NULL,
	[StillStudyingText] [nvarchar](max) NULL,
	[CurrencyFieldText] [nvarchar](max) NULL,
	[CurrencyFieldValidation] [nvarchar](max) NULL,
	[PriceFieldText] [nvarchar](max) NULL,
	[PriceFieldValidation] [nvarchar](max) NULL,
	[PriceIndicationText] [nvarchar](max) NULL,
	[RequiredInputText] [nvarchar](max) NULL,
	[ProfilePictureTips] [nvarchar](max) NULL,
	[ProfilePictureTitle] [nvarchar](max) NULL,
	[TypeCourseNameValidation] [nvarchar](max) NULL,
	[CoursesValidation] [nvarchar](max) NULL,
	[EducationLanguageValidation] [nvarchar](max) NULL,
	[EducationLevelValidation] [nvarchar](max) NULL,
	[LegalDocumentValidation] [nvarchar](max) NULL,
	[ProfilePictureValidation] [nvarchar](max) NULL,
	[TermsAndconditionsValidation] [nvarchar](max) NULL,
	[UserDetailsValidation] [nvarchar](max) NULL,
	[NationalityText] [nvarchar](max) NULL,
	[NationalityValidation] [nvarchar](max) NULL,
	[PhoneText] [nvarchar](max) NULL,
	[PhoneValidation] [nvarchar](max) NULL,
 CONSTRAINT [PK_TeacherRegistrationSettings] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TeacherReviews]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TeacherReviews](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[StudentId] [nvarchar](450) NULL,
	[Review] [nvarchar](max) NULL,
	[Rate] [real] NOT NULL,
	[SessionId] [int] NOT NULL,
	[DateIn] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_TeacherReviews] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TeachingSessions]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TeachingSessions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DateFrom] [datetime2](7) NOT NULL,
	[DateTo] [datetime2](7) NOT NULL,
	[StartNow] [bit] NOT NULL,
	[CourseId] [int] NOT NULL,
	[HasCustomPrice] [bit] NOT NULL,
	[Price] [decimal](18, 2) NULL,
	[CurrencyId] [int] NULL,
	[Free] [bit] NOT NULL,
	[Description] [nvarchar](max) NULL,
	[TeacherId] [nvarchar](450) NULL,
	[LessonId] [nvarchar](max) NULL,
	[SessionStatus] [int] NOT NULL,
	[isPublic] [bit] NULL,
	[BBBInternalId] [nvarchar](max) NULL,
	[ConfirmedByTeacher] [bit] NULL,
	[StudentCreatorId] [nvarchar](max) NULL,
 CONSTRAINT [PK_TeachingSessions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TutorArea]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TutorArea](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NULL,
	[LanguageId] [int] NOT NULL,
 CONSTRAINT [PK_TutorArea] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TutorDashboard]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TutorDashboard](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LanguageId] [int] NOT NULL,
	[LogoPath] [nvarchar](max) NULL,
	[MenuDashboard] [nvarchar](max) NULL,
	[MenuMyProfile] [nvarchar](max) NULL,
	[MenuSchedule] [nvarchar](max) NULL,
	[MenuMyLessons] [nvarchar](max) NULL,
	[MenuPayments] [nvarchar](max) NULL,
	[MenuBankAccountDetails] [nvarchar](max) NULL,
	[DashboardGeneratedIncome] [nvarchar](max) NULL,
	[StartLessonPopUpText] [nvarchar](max) NULL,
	[StartLessonPopUpTitle] [nvarchar](max) NULL,
	[UpcomingPaymentsBtnText] [nvarchar](max) NULL,
	[DashboardAverageRating] [nvarchar](max) NULL,
	[DashboardNumberOfReviews] [nvarchar](max) NULL,
	[EditTopicsText] [nvarchar](max) NULL,
	[ActiveStudentsIconPath] [nvarchar](max) NULL,
	[ActiveStudentsIconColor] [nvarchar](max) NULL,
	[ActiveStudentsText] [nvarchar](max) NULL,
	[UpcomingSessionsIconPath] [nvarchar](max) NULL,
	[UpcomingSessionsIconColor] [nvarchar](max) NULL,
	[UpcomingSessionsText] [nvarchar](max) NULL,
	[ProfileCompletnessText] [nvarchar](max) NULL,
	[AddYourBankDetailsText] [nvarchar](max) NULL,
	[AddYourBioText] [nvarchar](max) NULL,
	[AddYourSpeakingLanguagesText] [nvarchar](max) NULL,
	[AddYourEducationText] [nvarchar](max) NULL,
	[AddYourLessonsText] [nvarchar](max) NULL,
	[AddYourProfilePictureText] [nvarchar](max) NULL,
	[BioTitle] [nvarchar](max) NULL,
	[EditBioBtnText] [nvarchar](max) NULL,
	[EducationTitle] [nvarchar](max) NULL,
	[EditEducationBtnText] [nvarchar](max) NULL,
	[LanguagesTitle] [nvarchar](max) NULL,
	[EditLanguagesBtnText] [nvarchar](max) NULL,
	[ViewMoreLinkText] [nvarchar](max) NULL,
	[RatingTitle] [nvarchar](max) NULL,
	[ReviewsText] [nvarchar](max) NULL,
	[SelectACourseDateAndTime] [nvarchar](max) NULL,
	[DateText] [nvarchar](max) NULL,
	[FromText] [nvarchar](max) NULL,
	[UntilText] [nvarchar](max) NULL,
	[SubjectTitle] [nvarchar](max) NULL,
	[SubjectFieldText] [nvarchar](max) NULL,
	[CoursePriceTitle] [nvarchar](max) NULL,
	[CurrencyFieldText] [nvarchar](max) NULL,
	[AmountFieldText] [nvarchar](max) NULL,
	[DescriptionTitle] [nvarchar](max) NULL,
	[DescriptionFieldText] [nvarchar](max) NULL,
	[InviteStudentsTitle] [nvarchar](max) NULL,
	[PublicCheckBoxTitle] [nvarchar](max) NULL,
	[CancelPopUpBtnText] [nvarchar](max) NULL,
	[SavePopUpBtnText] [nvarchar](max) NULL,
	[UpcomingLessonsBtnText] [nvarchar](max) NULL,
	[HistoryLessonsBtnText] [nvarchar](max) NULL,
	[TableSubjectTitle] [nvarchar](max) NULL,
	[TableDateAndTimeTitle] [nvarchar](max) NULL,
	[TablePriceTitle] [nvarchar](max) NULL,
	[TableDescriptionTitle] [nvarchar](max) NULL,
	[TableInviteesTitle] [nvarchar](max) NULL,
	[TableInviteesEmailTitle] [nvarchar](max) NULL,
	[TableAttendeesTitle] [nvarchar](max) NULL,
	[TableAttendeesEmailTitle] [nvarchar](max) NULL,
	[TableStatusTitle] [nvarchar](max) NULL,
	[TableRecordingsTitle] [nvarchar](max) NULL,
	[NoUpcomingLessonsMsg] [nvarchar](max) NULL,
	[NoHistoryLessonsMsg] [nvarchar](max) NULL,
	[StartLessonBtnText] [nvarchar](max) NULL,
	[RejoinBtnText] [nvarchar](max) NULL,
	[TableCancelBtnText] [nvarchar](max) NULL,
	[CanceledText] [nvarchar](max) NULL,
	[ViewBtnText] [nvarchar](max) NULL,
	[NoRecordingText] [nvarchar](max) NULL,
	[PaymentDueDateText] [nvarchar](max) NULL,
	[PaymentAmountText] [nvarchar](max) NULL,
	[PaymentSubjectText] [nvarchar](max) NULL,
	[PaymentCourseDateText] [nvarchar](max) NULL,
	[HistoryPaymentDateText] [nvarchar](max) NULL,
	[PaymentTransactionNumberText] [nvarchar](max) NULL,
	[NoUpcomingPaymentsMsg] [nvarchar](max) NULL,
	[NoHistoryPaymentsMsg] [nvarchar](max) NULL,
	[BankAccountTitle] [nvarchar](max) NULL,
	[HistoryPaymentsBtnText] [nvarchar](max) NULL,
	[IBANText] [nvarchar](max) NULL,
	[SwiftAccountText] [nvarchar](max) NULL,
	[BankNameText] [nvarchar](max) NULL,
	[BankBranchNameText] [nvarchar](max) NULL,
	[CurrencyText] [nvarchar](max) NULL,
	[EditAccountDetailsBtnText] [nvarchar](max) NULL,
	[EditBankDetailsPopUpTitle] [nvarchar](max) NULL,
	[MenuAccountSettingsText] [nvarchar](max) NULL,
	[MenuAccountSettingsIcon] [nvarchar](max) NULL,
	[MenuLogoutText] [nvarchar](max) NULL,
	[MenuLogoutIcon] [nvarchar](max) NULL,
	[AttendeeEmailText] [nvarchar](max) NULL,
	[AttendeeNameText] [nvarchar](max) NULL,
	[BankAccountTooltip] [nvarchar](max) NULL,
	[CancelLessonPopUpText] [nvarchar](max) NULL,
	[CancelLessonPopUpTitle] [nvarchar](max) NULL,
	[CompletedText] [nvarchar](max) NULL,
	[CreatedText] [nvarchar](max) NULL,
	[DashboardCompletedLessonsText] [nvarchar](max) NULL,
	[DashboardCompletedLessonsTitle] [nvarchar](max) NULL,
	[DashboardCompletedTeachingHoursText] [nvarchar](max) NULL,
	[DashboardCompletedTeachingHoursTitle] [nvarchar](max) NULL,
	[DashboardUniqueStudentsText] [nvarchar](max) NULL,
	[DashboardUniqueStudentsTitle] [nvarchar](max) NULL,
	[AdditionalDetailsText] [nvarchar](max) NULL,
	[CodeValidationText] [nvarchar](max) NULL,
	[CurrentPasswordText] [nvarchar](max) NULL,
	[CurrentPasswordValidation] [nvarchar](max) NULL,
	[EditAdditionalDetails] [nvarchar](max) NULL,
	[EmailChangeSuccessText] [nvarchar](max) NULL,
	[EnterNewEmail] [nvarchar](max) NULL,
	[NewPasswordText] [nvarchar](max) NULL,
	[NewPasswordValidation] [nvarchar](max) NULL,
	[PasswordChangeSuccessText] [nvarchar](max) NULL,
	[PasswordMatchValidation] [nvarchar](max) NULL,
	[ProvideNewPassword] [nvarchar](max) NULL,
	[ProvideYourEmailAddress] [nvarchar](max) NULL,
	[ReNewPasswordText] [nvarchar](max) NULL,
	[ReNewPasswordValidation] [nvarchar](max) NULL,
	[VerifyNewEmailText] [nvarchar](max) NULL,
	[VerifyNewEmailTitle] [nvarchar](max) NULL,
	[CompletedSessionsIconPath] [nvarchar](max) NULL,
	[ThisEmailAlreadyExists] [nvarchar](max) NULL,
	[YouHaveAnotherScheduledCourse] [nvarchar](max) NULL,
	[SessionWasNotFound] [nvarchar](max) NULL,
	[IncorrectCode] [nvarchar](max) NULL,
	[PasswordsDoNotMatch] [nvarchar](max) NULL,
	[CodeMustBeSupplied] [nvarchar](max) NULL,
	[TeachingTopicsAndRates] [nvarchar](max) NULL,
	[Topic] [nvarchar](max) NULL,
	[RatePerHour] [nvarchar](max) NULL,
	[Education] [nvarchar](max) NULL,
	[Languages] [nvarchar](max) NULL,
	[InterviewedAndVerifiedTitle] [nvarchar](max) NULL,
	[InterviewedAndVerifiedText] [nvarchar](max) NULL,
	[BackBtnText] [nvarchar](max) NULL,
	[NoBtnText] [nvarchar](max) NULL,
	[YestBtnText] [nvarchar](max) NULL,
	[ThereIsACourseAlreadyScheduledForThisTopic] [nvarchar](max) NULL,
	[PaidText] [nvarchar](max) NULL,
	[NotPaid] [nvarchar](max) NULL,
	[CloseBtnText] [nvarchar](max) NULL,
	[EditTitle] [nvarchar](max) NULL,
	[PresentText] [nvarchar](max) NULL,
	[AllTimesListedAreInYourLocalTimeZone] [nvarchar](max) NULL,
	[YourFreeSlots] [nvarchar](max) NULL,
	[PublicLessonsYouScheduled] [nvarchar](max) NULL,
	[PrivateLessonsYouScheduled] [nvarchar](max) NULL,
	[SessionsBookedByStudents] [nvarchar](max) NULL,
	[ImFreeDuringThisTiming] [nvarchar](max) NULL,
	[SetYourAvailabilityText] [nvarchar](max) NULL,
	[SetAvailabilityBtnText] [nvarchar](max) NULL,
	[IWantToScheduleACourse] [nvarchar](max) NULL,
	[FillCourseDetailsText] [nvarchar](max) NULL,
	[ScheduleCourse] [nvarchar](max) NULL,
	[AddYourFreeDateAndTime] [nvarchar](max) NULL,
	[EditYourFreeDateAndtime] [nvarchar](max) NULL,
	[TitleText] [nvarchar](max) NULL,
	[ThisFieldIsRequired] [nvarchar](max) NULL,
	[PleaseChooseASubject] [nvarchar](max) NULL,
	[PleaseChooseCurrency] [nvarchar](max) NULL,
	[PleaseInsertValidAmount] [nvarchar](max) NULL,
	[YouHaveEnteredAnInvalidEmail] [nvarchar](max) NULL,
	[YouCannotAddYourSelfAsStudent] [nvarchar](max) NULL,
	[BiographyValidation] [nvarchar](max) NULL,
	[TitleValidation] [nvarchar](max) NULL,
	[TimeCannotBeInThePast] [nvarchar](max) NULL,
	[UntilTimeShouldBeGreaterThenFromTime] [nvarchar](max) NULL,
	[AddValidFromTime] [nvarchar](max) NULL,
	[NameText] [nvarchar](max) NULL,
	[NoNameText] [nvarchar](max) NULL,
	[StudentDetails] [nvarchar](max) NULL,
	[EmailText] [nvarchar](max) NULL,
	[Reschedule] [nvarchar](max) NULL,
	[ConfirmStudentRequestTitle] [nvarchar](max) NULL,
	[ConfirmStudentRequestText] [nvarchar](max) NULL,
	[ConfirmBtnText] [nvarchar](max) NULL,
	[ThisLessonWasCanceled] [nvarchar](max) NULL,
	[PleaseChooseAnAction] [nvarchar](max) NULL,
	[ConfirmSessionBtnText] [nvarchar](max) NULL,
	[MessageBtnText] [nvarchar](max) NULL,
	[YouHaveAcceptedThisSession] [nvarchar](max) NULL,
 CONSTRAINT [PK_TutorDashboard] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TutorFreeSpots]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TutorFreeSpots](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TeacherId] [nvarchar](450) NULL,
	[StartDate] [datetime2](7) NOT NULL,
	[EndDate] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_TutorFreeSpots] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TutorsListPage]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TutorsListPage](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PageBackGroundColor] [nvarchar](max) NULL,
	[TutorsResultText] [nvarchar](max) NULL,
	[TutorName] [nvarchar](max) NULL,
	[DegreeTitle] [nvarchar](max) NULL,
	[Classes] [nvarchar](max) NULL,
	[TeachesText] [nvarchar](max) NULL,
	[ViewScheduleText] [nvarchar](max) NULL,
	[TutorDescription] [nvarchar](max) NULL,
	[RatingTextColor] [nvarchar](max) NULL,
	[RatingIcon] [nvarchar](max) NULL,
	[NumberOfReviewsText] [nvarchar](max) NULL,
	[PriceStyle] [nvarchar](max) NULL,
	[DiscountPriceStyle] [nvarchar](max) NULL,
	[PerHourText] [nvarchar](max) NULL,
	[SearchBarPlaceHolderText] [nvarchar](max) NULL,
	[SearchBarPlaceHolderColor] [nvarchar](max) NULL,
	[VerifiedIcon] [nvarchar](max) NULL,
	[VerifiedIconColor] [nvarchar](max) NULL,
	[AddToFavouritesIcon] [nvarchar](max) NULL,
	[AddToFavouritesColor] [nvarchar](max) NULL,
	[RemoveFromFavouritesIcon] [nvarchar](max) NULL,
	[RemoveFromFavouritesColor] [nvarchar](max) NULL,
	[BookSessionBorderColor] [nvarchar](max) NULL,
	[BookSessionBackgroundColor] [nvarchar](max) NULL,
	[BookSessionTextColor] [nvarchar](max) NULL,
	[BookSessionText] [nvarchar](max) NULL,
	[SendMessageBorderColor] [nvarchar](max) NULL,
	[SendMessageBackgroundColor] [nvarchar](max) NULL,
	[SendMessageTextColor] [nvarchar](max) NULL,
	[SendMessageText] [nvarchar](max) NULL,
	[SearchButtonBorderColor] [nvarchar](max) NULL,
	[SearchButtonBackgroundColor] [nvarchar](max) NULL,
	[SearchButtonTextColor] [nvarchar](max) NULL,
	[SearchButtonText] [nvarchar](max) NULL,
	[OnlineTooltip] [nvarchar](max) NULL,
	[ReviewTooltip] [nvarchar](max) NULL,
	[AddToFavouritesTooltip] [nvarchar](max) NULL,
	[RemoveFromFavouritesTooltip] [nvarchar](max) NULL,
	[PriceTooltip] [nvarchar](max) NULL,
	[SortByNoneText] [nvarchar](max) NULL,
	[SortByRatingText] [nvarchar](max) NULL,
	[SortByPriceText] [nvarchar](max) NULL,
	[SortByBorderColor] [nvarchar](max) NULL,
	[SortByTextColor] [nvarchar](max) NULL,
	[LanguageId] [int] NOT NULL,
	[DaysOfTheWeekText] [nvarchar](max) NULL,
	[SortByTitleText] [nvarchar](max) NULL,
	[TimeOfTheDayText] [nvarchar](max) NULL,
	[TimeZoneText] [nvarchar](max) NULL,
	[TutorAreaText] [nvarchar](max) NULL,
	[TutorScheduleText] [nvarchar](max) NULL,
	[RatePerHourText] [nvarchar](max) NULL,
	[VerifiedTooltip] [nvarchar](max) NULL,
	[TeachesCourseBackgroundColor] [nvarchar](max) NULL,
	[PricePerHourText] [nvarchar](max) NULL,
	[OfflineTooltip] [nvarchar](max) NULL,
	[AllTimesListedAreInYourLocalTimeZone] [nvarchar](max) NULL,
	[FreeSlotsYouCanBook] [nvarchar](max) NULL,
	[LessonsYouBookedAndPaid] [nvarchar](max) NULL,
	[BroadcastLessonsYouCanSubscribeTo] [nvarchar](max) NULL,
	[LessonsYouAreInvitedToPendindForPayment] [nvarchar](max) NULL,
	[ClickOnAvailableSlotToBookALesson] [nvarchar](max) NULL,
	[RatingAndReviews] [nvarchar](max) NULL,
	[BookYourPreferredCourseWithYourTeacher] [nvarchar](max) NULL,
	[SelectACourse] [nvarchar](max) NULL,
	[BookSession] [nvarchar](max) NULL,
	[DateValidation] [nvarchar](max) NULL,
	[CourseValidation] [nvarchar](max) NULL,
	[StartTimeValidation] [nvarchar](max) NULL,
	[EndTimeValidation] [nvarchar](max) NULL,
	[PleaseSelect] [nvarchar](max) NULL,
 CONSTRAINT [PK_TutorsListPage] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserClasses]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserClasses](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](450) NULL,
	[ClassId] [int] NOT NULL,
 CONSTRAINT [PK_UserClasses] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserCourseTeachingLanguages]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserCourseTeachingLanguages](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserCourseId] [int] NOT NULL,
	[SpokenLanguageId] [int] NOT NULL,
 CONSTRAINT [PK_UserCourseTeachingLanguages] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserEducations]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserEducations](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[InstitutionName] [nvarchar](max) NULL,
	[From] [datetime2](7) NOT NULL,
	[To] [datetime2](7) NOT NULL,
	[Degree] [nvarchar](max) NULL,
	[FilePath] [nvarchar](max) NULL,
	[UserId] [nvarchar](450) NULL,
	[StillStudyingHere] [bit] NOT NULL,
 CONSTRAINT [PK_UserEducations] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UsersCourses]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsersCourses](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](450) NULL,
	[CourseId] [int] NOT NULL,
	[CurrencyId] [int] NULL,
	[DiscountPricePerHour] [decimal](18, 2) NULL,
	[PricePerHour] [decimal](18, 2) NULL,
	[Enabled] [bit] NULL,
 CONSTRAINT [PK_UsersCourses] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UsersLevels]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsersLevels](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](450) NOT NULL,
	[LevelId] [int] NOT NULL,
 CONSTRAINT [PK_UsersLevels] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserSpokenLanguages]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserSpokenLanguages](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SpokenLanguageId] [int] NULL,
	[SpeakLevelId] [int] NULL,
	[UserId] [nvarchar](450) NULL,
 CONSTRAINT [PK_UserSpokenLanguages] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserWallets]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserWallets](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId1] [nvarchar](450) NULL,
	[UserId] [int] NOT NULL,
	[Credits] [decimal](18, 2) NOT NULL,
	[CurrencyId] [int] NOT NULL,
 CONSTRAINT [PK_UserWallets] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WebsiteMenus]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WebsiteMenus](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NULL,
	[Link] [nvarchar](max) NULL,
	[StaticPageId] [int] NULL,
	[ItemPosition] [int] NOT NULL,
	[LanguageId] [int] NOT NULL,
 CONSTRAINT [PK_WebsiteMenus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WishLists]    Script Date: 4/16/2020 11:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WishLists](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[StudentId] [nvarchar](450) NULL,
	[TeacherId] [nvarchar](450) NULL,
 CONSTRAINT [PK_WishLists] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BecomeATutors] ADD  DEFAULT ((0)) FOR [LanguageId]
GO
ALTER TABLE [dbo].[Courses] ADD  DEFAULT ((0)) FOR [LanguageId]
GO
ALTER TABLE [dbo].[Footers] ADD  DEFAULT ((0)) FOR [LanguageId]
GO
ALTER TABLE [dbo].[HomeBannerContents] ADD  DEFAULT ((0)) FOR [LanguageId]
GO
ALTER TABLE [dbo].[HomeSectionCategory] ADD  DEFAULT ((0)) FOR [LanguageId]
GO
ALTER TABLE [dbo].[HomeSections] ADD  DEFAULT ((0)) FOR [LanguageId]
GO
ALTER TABLE [dbo].[Pricings] ADD  DEFAULT ((0)) FOR [LanguageId]
GO
ALTER TABLE [dbo].[PricingTips] ADD  DEFAULT ((0)) FOR [LanguageId]
GO
ALTER TABLE [dbo].[StaticPages] ADD  DEFAULT ((0)) FOR [LanguageId]
GO
ALTER TABLE [dbo].[TeacherReviews] ADD  DEFAULT ((0)) FOR [SessionId]
GO
ALTER TABLE [dbo].[TeacherReviews] ADD  DEFAULT ('0001-01-01T00:00:00.0000000') FOR [DateIn]
GO
ALTER TABLE [dbo].[WebsiteMenus] ADD  DEFAULT ((0)) FOR [LanguageId]
GO
ALTER TABLE [dbo].[AspNetRoleClaims]  WITH CHECK ADD  CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetRoleClaims] CHECK CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUsers]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUsers_Currencies] FOREIGN KEY([CurrencyId])
REFERENCES [dbo].[Currencies] ([Id])
GO
ALTER TABLE [dbo].[AspNetUsers] CHECK CONSTRAINT [FK_AspNetUsers_Currencies]
GO
ALTER TABLE [dbo].[AspNetUsers]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUsers_TutorArea] FOREIGN KEY([AreaId])
REFERENCES [dbo].[TutorArea] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[AspNetUsers] CHECK CONSTRAINT [FK_AspNetUsers_TutorArea]
GO
ALTER TABLE [dbo].[AspNetUserTokens]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserTokens] CHECK CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[BecomeATutors]  WITH CHECK ADD  CONSTRAINT [FK_BecomeATutors_Language_LanguageId] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[Language] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[BecomeATutors] CHECK CONSTRAINT [FK_BecomeATutors_Language_LanguageId]
GO
ALTER TABLE [dbo].[ChatHeadStatus]  WITH CHECK ADD  CONSTRAINT [FK_ChatHeadStatus_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[ChatHeadStatus] CHECK CONSTRAINT [FK_ChatHeadStatus_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[ChatHeadStatus]  WITH CHECK ADD  CONSTRAINT [FK_ChatHeadStatus_ChatHeads_ChatHeadId] FOREIGN KEY([ChatHeadId])
REFERENCES [dbo].[ChatHeads] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ChatHeadStatus] CHECK CONSTRAINT [FK_ChatHeadStatus_ChatHeads_ChatHeadId]
GO
ALTER TABLE [dbo].[ChatMessages]  WITH CHECK ADD  CONSTRAINT [FK_ChatMessages_AspNetUsers_ReceiverId] FOREIGN KEY([ReceiverId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[ChatMessages] CHECK CONSTRAINT [FK_ChatMessages_AspNetUsers_ReceiverId]
GO
ALTER TABLE [dbo].[ChatMessages]  WITH CHECK ADD  CONSTRAINT [FK_ChatMessages_AspNetUsers_SenderId] FOREIGN KEY([SenderId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[ChatMessages] CHECK CONSTRAINT [FK_ChatMessages_AspNetUsers_SenderId]
GO
ALTER TABLE [dbo].[ChatMessages]  WITH CHECK ADD  CONSTRAINT [FK_ChatMessages_ChatHeads_ChatHeadId] FOREIGN KEY([ChatHeadId])
REFERENCES [dbo].[ChatHeads] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ChatMessages] CHECK CONSTRAINT [FK_ChatMessages_ChatHeads_ChatHeadId]
GO
ALTER TABLE [dbo].[ChatSettings]  WITH CHECK ADD  CONSTRAINT [FK_ChatSettings_Language_LanguageId] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[Language] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ChatSettings] CHECK CONSTRAINT [FK_ChatSettings_Language_LanguageId]
GO
ALTER TABLE [dbo].[CourseLevels]  WITH CHECK ADD  CONSTRAINT [FK_CourseLevels_Courses_CourseId] FOREIGN KEY([CourseId])
REFERENCES [dbo].[Courses] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[CourseLevels] CHECK CONSTRAINT [FK_CourseLevels_Courses_CourseId]
GO
ALTER TABLE [dbo].[CourseLevels]  WITH CHECK ADD  CONSTRAINT [FK_CourseLevels_Level_LevelId] FOREIGN KEY([LevelId])
REFERENCES [dbo].[Level] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[CourseLevels] CHECK CONSTRAINT [FK_CourseLevels_Level_LevelId]
GO
ALTER TABLE [dbo].[Courses]  WITH CHECK ADD  CONSTRAINT [FK_Courses_Language_LanguageId] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[Language] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Courses] CHECK CONSTRAINT [FK_Courses_Language_LanguageId]
GO
ALTER TABLE [dbo].[CoursesFilterSettings]  WITH CHECK ADD  CONSTRAINT [FK_CoursesFilterSettings_Language_LanguageId] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[Language] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[CoursesFilterSettings] CHECK CONSTRAINT [FK_CoursesFilterSettings_Language_LanguageId]
GO
ALTER TABLE [dbo].[EmailContent]  WITH CHECK ADD  CONSTRAINT [FK_EmailContent_Language_LanguageId] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[Language] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[EmailContent] CHECK CONSTRAINT [FK_EmailContent_Language_LanguageId]
GO
ALTER TABLE [dbo].[FAQ]  WITH CHECK ADD  CONSTRAINT [FK_FAQ_Language_LanguageId] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[Language] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[FAQ] CHECK CONSTRAINT [FK_FAQ_Language_LanguageId]
GO
ALTER TABLE [dbo].[FAQSettings]  WITH CHECK ADD  CONSTRAINT [FK_FAQSettings_Language_LanguageId] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[Language] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[FAQSettings] CHECK CONSTRAINT [FK_FAQSettings_Language_LanguageId]
GO
ALTER TABLE [dbo].[Footers]  WITH CHECK ADD  CONSTRAINT [FK_Footers_Language_LanguageId] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[Language] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Footers] CHECK CONSTRAINT [FK_Footers_Language_LanguageId]
GO
ALTER TABLE [dbo].[HomeBannerContents]  WITH CHECK ADD  CONSTRAINT [FK_HomeBannerContents_Language_LanguageId] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[Language] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[HomeBannerContents] CHECK CONSTRAINT [FK_HomeBannerContents_Language_LanguageId]
GO
ALTER TABLE [dbo].[HomeSectionCategory]  WITH CHECK ADD  CONSTRAINT [FK_HomeSectionCategory_Language_LanguageId] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[Language] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[HomeSectionCategory] CHECK CONSTRAINT [FK_HomeSectionCategory_Language_LanguageId]
GO
ALTER TABLE [dbo].[HomeSections]  WITH CHECK ADD  CONSTRAINT [FK_HomeSections_HomeSectionCategory_HomeSectionCategoryId] FOREIGN KEY([HomeSectionCategoryId])
REFERENCES [dbo].[HomeSectionCategory] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[HomeSections] CHECK CONSTRAINT [FK_HomeSections_HomeSectionCategory_HomeSectionCategoryId]
GO
ALTER TABLE [dbo].[HomeSections]  WITH CHECK ADD  CONSTRAINT [FK_HomeSections_Language_LanguageId] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[Language] ([Id])
GO
ALTER TABLE [dbo].[HomeSections] CHECK CONSTRAINT [FK_HomeSections_Language_LanguageId]
GO
ALTER TABLE [dbo].[InvitedEmails]  WITH CHECK ADD  CONSTRAINT [FK_InvitedEmails_TeachingSessions_SessionId] FOREIGN KEY([SessionId])
REFERENCES [dbo].[TeachingSessions] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[InvitedEmails] CHECK CONSTRAINT [FK_InvitedEmails_TeachingSessions_SessionId]
GO
ALTER TABLE [dbo].[LoginPageSettings]  WITH CHECK ADD  CONSTRAINT [FK_LoginPageSettings_Language_LanguageId] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[Language] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[LoginPageSettings] CHECK CONSTRAINT [FK_LoginPageSettings_Language_LanguageId]
GO
ALTER TABLE [dbo].[NotificationsContent]  WITH CHECK ADD  CONSTRAINT [FK_NotificationsContent_Language_LanguageId] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[Language] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[NotificationsContent] CHECK CONSTRAINT [FK_NotificationsContent_Language_LanguageId]
GO
ALTER TABLE [dbo].[NotificationUsers]  WITH CHECK ADD  CONSTRAINT [FK_NotificationUsers_Notifications_NotificationId] FOREIGN KEY([NotificationId])
REFERENCES [dbo].[Notifications] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[NotificationUsers] CHECK CONSTRAINT [FK_NotificationUsers_Notifications_NotificationId]
GO
ALTER TABLE [dbo].[Payments]  WITH CHECK ADD  CONSTRAINT [FK_Payments_TeachingSessions_TeachingSessionId] FOREIGN KEY([TeachingSessionId])
REFERENCES [dbo].[TeachingSessions] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Payments] CHECK CONSTRAINT [FK_Payments_TeachingSessions_TeachingSessionId]
GO
ALTER TABLE [dbo].[Pricings]  WITH CHECK ADD  CONSTRAINT [FK_Pricings_Language_LanguageId] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[Language] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Pricings] CHECK CONSTRAINT [FK_Pricings_Language_LanguageId]
GO
ALTER TABLE [dbo].[PricingTips]  WITH CHECK ADD  CONSTRAINT [FK_PricingTips_Language_LanguageId] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[Language] ([Id])
GO
ALTER TABLE [dbo].[PricingTips] CHECK CONSTRAINT [FK_PricingTips_Language_LanguageId]
GO
ALTER TABLE [dbo].[PricingTips]  WITH CHECK ADD  CONSTRAINT [FK_PricingTips_Pricings_PricingId] FOREIGN KEY([PricingId])
REFERENCES [dbo].[Pricings] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PricingTips] CHECK CONSTRAINT [FK_PricingTips_Pricings_PricingId]
GO
ALTER TABLE [dbo].[SessionBookings]  WITH CHECK ADD  CONSTRAINT [FK_SessionBookings_TeachingSessions_SessionId] FOREIGN KEY([SessionId])
REFERENCES [dbo].[TeachingSessions] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SessionBookings] CHECK CONSTRAINT [FK_SessionBookings_TeachingSessions_SessionId]
GO
ALTER TABLE [dbo].[SpeakLevel]  WITH CHECK ADD  CONSTRAINT [FK_SpeakLevel_Language_LanguageId] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[Language] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SpeakLevel] CHECK CONSTRAINT [FK_SpeakLevel_Language_LanguageId]
GO
ALTER TABLE [dbo].[SpokenLanguage]  WITH CHECK ADD  CONSTRAINT [FK_SpokenLanguage_Language_LanguageId] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[Language] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SpokenLanguage] CHECK CONSTRAINT [FK_SpokenLanguage_Language_LanguageId]
GO
ALTER TABLE [dbo].[StaticPages]  WITH CHECK ADD  CONSTRAINT [FK_StaticPages_Language_LanguageId] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[Language] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[StaticPages] CHECK CONSTRAINT [FK_StaticPages_Language_LanguageId]
GO
ALTER TABLE [dbo].[StudentDashboard]  WITH CHECK ADD  CONSTRAINT [FK_StudentDashboard_Language_LanguageId] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[Language] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[StudentDashboard] CHECK CONSTRAINT [FK_StudentDashboard_Language_LanguageId]
GO
ALTER TABLE [dbo].[TeacherRegistrationSettings]  WITH CHECK ADD  CONSTRAINT [FK_TeacherRegistrationSettings_Language_LanguageId] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[Language] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[TeacherRegistrationSettings] CHECK CONSTRAINT [FK_TeacherRegistrationSettings_Language_LanguageId]
GO
ALTER TABLE [dbo].[TeacherReviews]  WITH CHECK ADD  CONSTRAINT [FK_TeacherReviews_AspNetUsers_StudentId] FOREIGN KEY([StudentId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[TeacherReviews] CHECK CONSTRAINT [FK_TeacherReviews_AspNetUsers_StudentId]
GO
ALTER TABLE [dbo].[TeacherReviews]  WITH CHECK ADD  CONSTRAINT [FK_TeacherReviews_TeachingSessions_SessionId] FOREIGN KEY([SessionId])
REFERENCES [dbo].[TeachingSessions] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[TeacherReviews] CHECK CONSTRAINT [FK_TeacherReviews_TeachingSessions_SessionId]
GO
ALTER TABLE [dbo].[TeachingSessions]  WITH CHECK ADD  CONSTRAINT [FK_TeachingSessions_Currencies_CurrencyId] FOREIGN KEY([CurrencyId])
REFERENCES [dbo].[Currencies] ([Id])
GO
ALTER TABLE [dbo].[TeachingSessions] CHECK CONSTRAINT [FK_TeachingSessions_Currencies_CurrencyId]
GO
ALTER TABLE [dbo].[TeachingSessions]  WITH CHECK ADD  CONSTRAINT [FK_TeachingSessions_UsersCourses_CourseId] FOREIGN KEY([CourseId])
REFERENCES [dbo].[UsersCourses] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[TeachingSessions] CHECK CONSTRAINT [FK_TeachingSessions_UsersCourses_CourseId]
GO
ALTER TABLE [dbo].[TutorArea]  WITH CHECK ADD  CONSTRAINT [FK_TutorArea_Language_LanguageId] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[Language] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[TutorArea] CHECK CONSTRAINT [FK_TutorArea_Language_LanguageId]
GO
ALTER TABLE [dbo].[TutorDashboard]  WITH CHECK ADD  CONSTRAINT [FK_TutorDashboard_Language_LanguageId] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[Language] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[TutorDashboard] CHECK CONSTRAINT [FK_TutorDashboard_Language_LanguageId]
GO
ALTER TABLE [dbo].[TutorFreeSpots]  WITH CHECK ADD  CONSTRAINT [FK_TutorFreeSpots_AspNetUsers_TeacherId] FOREIGN KEY([TeacherId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[TutorFreeSpots] CHECK CONSTRAINT [FK_TutorFreeSpots_AspNetUsers_TeacherId]
GO
ALTER TABLE [dbo].[TutorsListPage]  WITH CHECK ADD  CONSTRAINT [FK_TutorsListPage_Language_LanguageId] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[Language] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[TutorsListPage] CHECK CONSTRAINT [FK_TutorsListPage_Language_LanguageId]
GO
ALTER TABLE [dbo].[UserClasses]  WITH CHECK ADD  CONSTRAINT [FK_UserClasses_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[UserClasses] CHECK CONSTRAINT [FK_UserClasses_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[UserClasses]  WITH CHECK ADD  CONSTRAINT [FK_UserClasses_Classes_ClassId] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Classes] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UserClasses] CHECK CONSTRAINT [FK_UserClasses_Classes_ClassId]
GO
ALTER TABLE [dbo].[UserCourseTeachingLanguages]  WITH CHECK ADD  CONSTRAINT [FK_UserCourseTeachingLanguages_SpokenLanguage_SpokenLanguageId] FOREIGN KEY([SpokenLanguageId])
REFERENCES [dbo].[SpokenLanguage] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UserCourseTeachingLanguages] CHECK CONSTRAINT [FK_UserCourseTeachingLanguages_SpokenLanguage_SpokenLanguageId]
GO
ALTER TABLE [dbo].[UserEducations]  WITH CHECK ADD  CONSTRAINT [FK_UserEducations_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UserEducations] CHECK CONSTRAINT [FK_UserEducations_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[UsersCourses]  WITH CHECK ADD  CONSTRAINT [FK_UsersCourses_Courses_CourseId] FOREIGN KEY([CourseId])
REFERENCES [dbo].[Courses] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UsersCourses] CHECK CONSTRAINT [FK_UsersCourses_Courses_CourseId]
GO
ALTER TABLE [dbo].[UsersCourses]  WITH CHECK ADD  CONSTRAINT [FK_UsersCourses_Currencies] FOREIGN KEY([CurrencyId])
REFERENCES [dbo].[Currencies] ([Id])
GO
ALTER TABLE [dbo].[UsersCourses] CHECK CONSTRAINT [FK_UsersCourses_Currencies]
GO
ALTER TABLE [dbo].[UsersLevels]  WITH CHECK ADD  CONSTRAINT [FK_UsersLevels_Level_LevelId] FOREIGN KEY([LevelId])
REFERENCES [dbo].[Level] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UsersLevels] CHECK CONSTRAINT [FK_UsersLevels_Level_LevelId]
GO
ALTER TABLE [dbo].[UserSpokenLanguages]  WITH CHECK ADD  CONSTRAINT [FK_UserSpokenLanguages_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UserSpokenLanguages] CHECK CONSTRAINT [FK_UserSpokenLanguages_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[UserSpokenLanguages]  WITH CHECK ADD  CONSTRAINT [FK_UserSpokenLanguages_SpeakLevel_SpeakLevelId] FOREIGN KEY([SpeakLevelId])
REFERENCES [dbo].[SpeakLevel] ([Id])
GO
ALTER TABLE [dbo].[UserSpokenLanguages] CHECK CONSTRAINT [FK_UserSpokenLanguages_SpeakLevel_SpeakLevelId]
GO
ALTER TABLE [dbo].[UserSpokenLanguages]  WITH CHECK ADD  CONSTRAINT [FK_UserSpokenLanguages_SpokenLanguage_SpokenLanguageId] FOREIGN KEY([SpokenLanguageId])
REFERENCES [dbo].[SpokenLanguage] ([Id])
GO
ALTER TABLE [dbo].[UserSpokenLanguages] CHECK CONSTRAINT [FK_UserSpokenLanguages_SpokenLanguage_SpokenLanguageId]
GO
ALTER TABLE [dbo].[UserWallets]  WITH CHECK ADD  CONSTRAINT [FK_UserWallets_Currencies_CurrencyId] FOREIGN KEY([CurrencyId])
REFERENCES [dbo].[Currencies] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UserWallets] CHECK CONSTRAINT [FK_UserWallets_Currencies_CurrencyId]
GO
ALTER TABLE [dbo].[WebsiteMenus]  WITH CHECK ADD  CONSTRAINT [FK_WebsiteMenus_Language_LanguageId] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[Language] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[WebsiteMenus] CHECK CONSTRAINT [FK_WebsiteMenus_Language_LanguageId]
GO
ALTER TABLE [dbo].[WebsiteMenus]  WITH CHECK ADD  CONSTRAINT [FK_WebsiteMenus_StaticPages_StaticPageId] FOREIGN KEY([StaticPageId])
REFERENCES [dbo].[StaticPages] ([Id])
GO
ALTER TABLE [dbo].[WebsiteMenus] CHECK CONSTRAINT [FK_WebsiteMenus_StaticPages_StaticPageId]
GO
ALTER TABLE [dbo].[WishLists]  WITH CHECK ADD  CONSTRAINT [FK_WishLists_AspNetUsers_StudentId] FOREIGN KEY([StudentId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[WishLists] CHECK CONSTRAINT [FK_WishLists_AspNetUsers_StudentId]
GO
ALTER TABLE [dbo].[WishLists]  WITH CHECK ADD  CONSTRAINT [FK_WishLists_AspNetUsers_TeacherId] FOREIGN KEY([TeacherId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[WishLists] CHECK CONSTRAINT [FK_WishLists_AspNetUsers_TeacherId]
GO
