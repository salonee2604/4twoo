@extends('layouts.app')

@section('content')
<style type="text/css">
    .mt-5a{


        margin-top: 90px;
    }

</style>
<main id="main">


    <!-- ======= Blog Section ======= -->
    <section id="blog" class="blog mt-5a" style="padding-bottom: 0px;">
      <div class="">

        <div class="row m-0 p-0">


<div class="col-lg-6 entries lg welcome-text">
    <h2>
Welcome,<br> Reset password to continue
</h2>
<center><img src="{{url('/')}}/public/assets/img/logo vector.png" style="width: 150px;position: absolute;z-index: 99;top: 37px;left: 38%;right: 0;"></center>
</div>
          <div class="col-lg-6 entries lg">

            <article class="entry entry-single">
                <div id="msg"></div>

              <div class="reply-form">
                <h4>Reset password to continue..</h4>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                <div class="card">

                    <div class="card-header">{{ __('Reset Password') }}</div>

                    <div class="card-body">

                        @if (session('status'))

                            <div class="alert alert-success" role="alert">

                                {{ session('status') }}

                            </div>

                        @endif

                        <form method="POST" action="{{ route('password.email') }}">

                            @csrf



                            <div class="form-group row">

                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>



                                <div class="col-md-6">

                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>



                                    @error('email')

                                        <span class="invalid-feedback" role="alert">

                                            <strong>{{ $message }}</strong>

                                        </span>

                                    @enderror

                                </div>

                            </div>



                            <div class="form-group row mb-0">

                                <div class="col-md-8 offset-md-4">

                                    <button type="submit" class="btn btn-primary">

                                        {{ __('Send Password Reset Link') }}

                                    </button>

                                </div>

                            </div>

                        </form>

                    </div>

                </div>

              </div>

            </div><!-- End blog comments -->


          </div><!-- End blog entries list -->

        </div>

      </div>
    </section><!-- End Blog Section -->

  </main><!-- End #main -->

  @endsection