<!DOCTYPE html>

<html>

<head>

  <meta charset="utf-8">

  <meta http-equiv="X-UA-Compatible" content="IE=edge">

   <?php 

      $logoImg = App\Helpers\Helper::getLogoImg(); 

	

   ?>

   <title> <?php echo !empty($logoImg->title)? $logoImg->title:'4Twoo';?>| Forgot Password</title>



  <!-- Tell the browser to be responsive to screen width -->

  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <!-- Bootstrap 3.3.7 -->

  <link rel="stylesheet" href="{{url('/')}}/resources/assets/bower_components/bootstrap/dist/css/bootstrap.min.css">

  <!-- Font Awesome -->

  <link rel="stylesheet" href="{{url('/')}}/resources/assets/bower_components/font-awesome/css/font-awesome.min.css">

  <!-- Ionicons -->

  <link rel="stylesheet" href="{{url('/')}}/resources/assets/bower_components/Ionicons/css/ionicons.min.css">

  <!-- Theme style -->

  <link rel="stylesheet" href="{{url('/')}}/resources/assets/dist/css/AdminLTE.min.css">

  <!-- iCheck -->

  <link rel="stylesheet" href="{{url('/')}}/resources/assets/plugins/iCheck/square/blue.css">



  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

  <!--[if lt IE 9]>

  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>

  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

  <![endif]-->



  <!-- Google Font -->

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

</head>

<body class="hold-transition login-page">

<div class="login-box">

  <div class="login-logo">

      <!-- height="80px;" -->

      @if(!empty($logoImg->logo_img))

        <img src="{{url('/')}}/public/uploads/logo/{{$logoImg->logo_img}}"  width="100%;" alt="User Image">

      @else

        <img src="{{url('/')}}/public/assets/img/logo vector.png"  width="100%;" alt="User Image">

      @endif
 

  </div>

  <!-- /.login-logo -->

  <div class="login-box-body">

    <p class="login-box-msg">{{ __('Reset Password') }}</p>



    <form method="POST" action="{{ route('password.update') }}">

        @csrf



        <input type="hidden" name="token" value="{{ $token }}">



        <div class="form-group row">

            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>



            <div class="col-md-6">

                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>



                @error('email')

                    <span class="invalid-feedback" role="alert">

                        <strong>{{ $message }}</strong>

                    </span>

                @enderror

            </div>

        </div>



        <div class="form-group row">

            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>



            <div class="col-md-6">

                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">



                @error('password')

                    <span class="invalid-feedback" role="alert">

                        <strong>{{ $message }}</strong>

                    </span>

                @enderror

            </div>

        </div>



        <div class="form-group row">

            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>



            <div class="col-md-6">

                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">

            </div>

        </div>



        <div class="form-group row mb-0">

            <div class="col-md-6 offset-md-4">

                <button type="submit" class="btn btn-primary">

                    {{ __('Reset Password') }}

                </button>

            </div>

        </div>

    </form>



    <a href="{{ url('/login') }}">Login</a><br>

  </div>

  <!-- /.login-box-body -->

</div>

<!-- /.login-box -->



<!-- jQuery 3 -->

<script src="{{url('/')}}/resources/assets/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap 3.3.7 -->

<script src="{{url('/')}}/resources/assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- iCheck -->

<script src="{{url('/')}}/resources/assets/plugins/iCheck/icheck.min.js"></script>

<script>

  $(function () {

    $('input').iCheck({

      checkboxClass: 'icheckbox_square-blue',

      radioClass: 'iradio_square-blue',

      increaseArea: '20%' /* optional */

    });

  });

</script>

</body>

</html>

