@extends('layouts.app')

@section('content')
<style type="text/css">
    .mt-5a{


        margin-top: 90px;
    }

</style>
<main id="main">


    <!-- ======= Blog Section ======= -->
    <section id="blog" class="blog mt-5a" style="padding-bottom: 0px;">
      <div class="">

        <div class="row m-0 p-0">


<div class="col-lg-6 entries lg welcome-text">
    <h2>
Welcome,<br> Sign in to continue
</h2>
<center><img src="{{url('/')}}/public/assets/img/logo vector.png" style="width: 150px;position: absolute;z-index: 99;top: 37px;left: 38%;right: 0;"></center>
</div>
          <div class="col-lg-6 entries lg">

            <article class="entry entry-single">

              <div class="reply-form">
                <h4>Sign up to continue..</h4>
                
                <!-- <form action="{{ route('login') }}" id="loginForm" method="post">
                  <div class="row login-filed">
                    <div class="col-md-12 form-group">
                      <input name="email" type="text" class="form-control" placeholder="Your Email*" value="{{ old('email') }}" required autofocus>
                    </div>
                     <div class="col-md-12 form-group">
                      <input name="password" type="password" class="form-control" placeholder="Your Password*" required autocomplete="current-password">
                    </div>
                  </div>
            
              
                  <button type="submit" class="btn btn-primary">Login</button>

                </form> -->
                <form id="UserLoginForm" method="POST" action="{{ route('login') }}">

                        @csrf



                        <div class="row login-filed">

                           <!--  <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label> -->



                            <div class="col-md-12 form-group">

                                <input id="email" type="text"  placeholder="Your Email*" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>



                                @error('email')

                                    <span class="invalid-feedback" role="alert">

                                        <strong>{{ $message }}</strong>

                                    </span>

                                @enderror

                            </div>

                        </div>



                        <div class="row login-filed">

                            <!-- <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label> -->



                            <div class="col-md-12 form-group">

                                <input id="password" type="password"  placeholder="Your Password*" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">



                                @error('password')

                                    <span class="invalid-feedback" role="alert">

                                        <strong>{{ $message }}</strong>

                                    </span>

                                @enderror

                            </div>

                        </div>



                        <!-- <div class="form-group row">

                            <div class="col-md-6 offset-md-4">

                                <div class="form-check">

                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>



                                    <label class="form-check-label" for="remember">

                                        {{ __('Remember Me') }}

                                    </label>

                                </div>

                            </div>

                        </div> -->



                        <div class="form-group row mb-0">

                            <div class="col-md-8">

                                <button type="submit" class="btn btn-primary">

                                    {{ __('Login') }}

                                </button>



                                @if (Route::has('password.request'))

                                    <a class="btn btn-link" href="{{ route('password.request') }}" style="color: #c52a39;">

                                        {{ __('Forgot Your Password?') }}

                                    </a>

                                @endif

                            </div>

                        </div>

                    </form>

              </div>

            </div><!-- End blog comments -->






          </div><!-- End blog entries list -->

          

        </div>

      </div>
    </section><!-- End Blog Section -->

  </main><!-- End #main -->
@endsection