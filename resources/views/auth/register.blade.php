@extends('layouts.app')

@section('content')
<style type="text/css">
    .mt-5a{


        margin-top: 90px;
    }

</style>
<main id="main">


    <!-- ======= Blog Section ======= -->
    <section id="blog" class="blog mt-5a" style="padding-bottom: 0px;">
      <div class="">

        <div class="row m-0 p-0">


<div class="col-lg-6 entries lg welcome-text">
    <h2>
Welcome,<br> Sign in to continue
</h2>
<center><img src="{{url('/')}}/public/assets/img/logo vector.png" style="width: 150px;position: absolute;z-index: 99;top: 37px;left: 38%;right: 0;"></center>
</div>
          <div class="col-lg-6 entries lg">

            <article class="entry entry-single">
                <div id="msg"></div>

              <div class="reply-form">
                <h4>Sign up to continue..</h4>
               
            
                 <form method="POST" id="UserForm" name="userForm" action="javascript:void(0)">

                        @csrf

                        <div class="row login-filed">

                            <div class="col-md-6 form-group">

                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" autofocus placeholder="First Name*">

                                @error('name')

                                    <span class="invalid-feedback" role="alert">

                                        <strong>{{ $message }}</strong>

                                    </span>

                                @enderror

                            </div>

                       

                       

                            <div class="col-md-6 form-group">

                                <input id="last_name" type="text" class="form-control @error('last_name') is-invalid @enderror" name="last_name" required autofocus placeholder="Last Name*">

                                @error('last_name')

                                    <span class="invalid-feedback" role="alert">

                                        <strong>{{ $message }}</strong>

                                    </span>

                                @enderror

                            </div>

                        </div>

                        <div class="row login-filed">

                            <div class="col-md-6 form-group">

                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="E-Mail Address*">

                                @error('email')

                                    <span class="invalid-feedback" role="alert">

                                        <strong>{{ $message }}</strong>

                                    </span>

                                @enderror

                            </div>

                       

                            <div class="col-md-6 form-group">
                                <input id="dob" type="date" class="form-control" name="dob" required autocomplete="dob" placeholder="Date Of birth*" data-date="" data-date-format="yyyy-mm-dd">
                            </div>

                          @error('dob')

                                <span class="invalid-feedback" role="alert">

                                    <strong>{{ $message }}</strong>

                                </span>

                            @enderror

                        </div>

                        <div class="row login-filed">

                            <div class="col-md-12 form-group">
                                <input id="address" type="text" class="form-control" name="address" required autocomplete="address" placeholder="Address*">
                            </div>

                            @error('address')

                                <span class="invalid-feedback" role="alert">

                                    <strong>{{ $message }}</strong>

                                </span>

                            @enderror

                        </div>

                        <div class="row login-filed">

                            <div class="col-md-12 form-group">
                                <input id="contact_number" type="text" class="form-control" name="contact_number" required autocomplete="contact_number" placeholder="Phone number*">
                            </div>

                          @error('contact_number')

                                <span class="invalid-feedback" role="alert">

                                    <strong>{{ $message }}</strong>

                                </span>

                            @enderror

                        </div>

                        <div class="row login-filed">

                            <div class="col-md-6 form-group">

                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Password*">

                                @error('password')

                                    <span class="invalid-feedback" role="alert">

                                        <strong>{{ $message }}</strong>

                                    </span>

                                @enderror

                            </div>

                       

                            <div class="col-md-6 form-group">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Confirm Password*">
                            </div>

                        </div>

                        <div class="row login-filed">
                            <div class="col-md-12 form-group">
                                <select name="gender" id="gender" class="form-control">
                                  <option value="">Select Gender*</option>
                                  <option value="male">Male</option>
                                  <option value="female">Female</option>
                                  <option value="other">Other</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row mb-0">

                            <div class="col-md-6">

                                <button type="submit" class="btn btn-primary">

                                    {{ __('Register') }}

                                </button>

                            </div>

                        </div>

                    </form>

              </div>

            </div><!-- End blog comments -->


          </div><!-- End blog entries list -->

        </div>

      </div>
    </section><!-- End Blog Section -->

  </main><!-- End #main -->

  
@endsection