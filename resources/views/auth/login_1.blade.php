@extends('layouts.app')

@section('content')
<style type="text/css">
    .mt-5a{


        margin-top: 90px;
    }

</style>
<main id="main">


    <!-- ======= Blog Section ======= -->
    <section id="blog" class="blog mt-5a" style="padding-bottom: 0px;">
      <div class="">

        <div class="row m-0 p-0">


<div class="col-lg-6 entries lg welcome-text">
    <h2>
Welcome,<br> Sign in to continue
</h2>
<center><img src="{{url('/')}}/public/assets/img/logo vector.png" style="width: 150px;position: absolute;z-index: 99;top: 37px;left: 38%;right: 0;"></center>
</div>
          <div class="col-lg-6 entries lg">

            <article class="entry entry-single">

              <div class="reply-form">
                <h4>Sign in to continue..</h4>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                <form action="{{ route('login') }}" id="loginForm" method="post">
                  <div class="row login-filed">
                    <div class="col-md-12 form-group">
                      <input name="email" type="text" class="form-control" placeholder="Your Email*" value="{{ old('email') }}" required autofocus>
                    </div>
                     <div class="col-md-12 form-group">
                      <input name="password" type="password" class="form-control" placeholder="Your Password*" required autocomplete="current-password">
                    </div>
                  </div>
            
              
                  <button type="submit" class="btn btn-primary">Login</button>

                </form>

              </div>

            </div><!-- End blog comments -->






          </div><!-- End blog entries list -->

          

        </div>

      </div>
    </section><!-- End Blog Section -->

  </main><!-- End #main -->
@endsection