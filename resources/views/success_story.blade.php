@extends('layouts.app')

@section('content')
<style type="text/css">
	.mt-5a{


		margin-top: 90px;
	}

</style>
  <main id="main">
    <section id="blog" class="blog mt-5a" style="padding-bottom: 0px;">
      <div class="col-md-12 suc-pride">
        <h3> Welcome to 4Twoo Pride.<br>
        This is where we celebrate  Success Stories.</h3>
      </div>
      <div class="container">
        <div class="row">
            <article class="entry entry-single p-3 w-100">
              <div class="row">
                @foreach($stories as $key => $story)
                  <div class="col-md-3 entries lg">
                    <a href="{{url('/success_story/')}}/<?php echo base64_encode($story->id); ?>">
                      <div class="suces-box  mt-4">
                        <div class="img-story">
                        <img src="{{ url('/public/assets/img/') }}/{{$story->image}}" alt="">
                        </div>
                        <h4>{{$story->title}}</h4>
                        
                      </div>
                    </a>
                  </div>
                @endforeach
           </div>
            </article>
          </div><!-- End blog entries list -->
        </div>
      </div>
    </section><!-- End Blog Section -->
  </main><!-- End #main -->
 @endsection