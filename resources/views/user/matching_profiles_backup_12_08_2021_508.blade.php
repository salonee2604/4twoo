@extends('layouts.app') @section('content') 
<style>
.custom-tb .table td, .table th {
    padding: .75rem;
    vertical-align: inherit;
    border-top: 1px solid #dee2e6;
}
.custom-tb p{
   margin-bottom: 0px;
}
  </style>
}
<main id="main">
  <section id="blog" class="blog mt-5a" style="padding-bottom: 0px; background-color: #f5f5f5;">
  	<div class="col-md-12 questinar">
        <h3>Match</h3> 
    </div>
    <div class="container">
      <div class="row m-0 p-0 ">
        <div class="col-md-10 text-left m-auto mt-3 mb-5 ">
          <div id="reqmsg"></div>
          <div class="row">
            <div class="box-body">
               <div class="col-md-12 mb-5 mt-5 m-xp">
              <div class="table-responsive custom-tb">
              <table id="testimonial_list" class="table table-bordered table-striped ">

                <thead>

                  <tr>

                    <th>Profile Pic</th>

                    <th>User Name</th>

                    <th>Mobile Number</th>

                    <th>Location</th>

                    <th>Match Percentage</th>

                    <th>Send Request</th>

                    <th>View Your Interest</th>

                  </tr>

                </thead>

                <tbody>
                 <?php $i=1; ?>
                 @foreach($usersinfo as $row)
                
                  <tr id="row{{$row['data']->id}}">
                    <td>
                      @if (!empty($row['data']->profile_pic)) 
                      <img src="{{url('/public/uploads/profile_img')}}/{{$row['data']->profile_pic}}" height="100px;" width="100px;">
                      @else
                      <img src="https://votivetech.in/4twoo/public/assets/img/Layer 11.png" height="100px;" width="100px;">
                      @endif</td>
                    <td>{{$row['data']->username}}</td>
                    <td>{{$row['data']->contact_number}}</td>
                    <td>{{$row['data']->address}}</td>
                    <?php

                       $matchedcount = $row['count'];
                       $allquation_count        = DB::table('questionnaire')->count();
                       $matchedpercentage        = ($matchedcount/$allquation_count)*100;

                      if(!empty($matchedpercentage))
                      {
                        $matchedpercentagevalue = round($matchedpercentage,2); 
                      }
                      else
                      {
                        $matchedpercentagevalue = "";
                      }
                    ?>
                    <td>
                      <p class="btn btn-success">
                      <?php echo $matchedpercentagevalue.'%'.'Match'; ?>
                       </p>
                    </td>
                     <td>
                      <a href="javascript:void(0)" class="cross-btn-ser send_request" data-id="{{$row['data']->id}}" id="send_request_{{$row['data']->id}}"><i class='bx bxs-heart' style="display: none;" id="bxs{{$row['data']->id}}"></i><i class='bx bx-heart' id="bxs{{$row['data']->id}}"></i></a>
                    </td>
                    <td>
                      <a href="{{url('/viewmatchProfile')}}/{{base64_encode($row['data']->id)}}" class="btn btn-danger">View</a>
                    </td>

                  </tr>

                  <?php $i++; ?>

                  @endforeach

                
                    
                </tbody>

              </table>
            </div>
          </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</main> 
@endsection