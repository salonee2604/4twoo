@extends('layouts.app') @section('content')
<style type="text/css">
.mt-5a {
  margin-top: 90px;
}
.skipdiv
{

  display: none;
}
</style>
<style type="text/css">
  
  /*form styles*/
#msform {
  width: 100%;
  margin: 50px auto;
  text-align: center;
  position: relative;
}
#msform fieldset {
  background: white;
  border: 0 none;
  border-radius: 3px;
  box-shadow: 0 0 15px 1px rgba(0, 0, 0, 0.4);
  padding: 20px 30px;
  box-sizing: border-box;
  margin: 0 1%;
  z-index: 9;
  
  /*stacking fieldsets above each other*/
  position: relative;
}
/*Hide all except first fieldset*/
#msform fieldset:not(:first-of-type) {
  display: none;
}
/*inputs*/
#msform input, #msform textarea {
  padding: 15px;
  border: 1px solid #ccc;
  border-radius: 3px;
  margin-bottom: 10px;
  width: 100%;
  box-sizing: border-box;
  font-family: montserrat;
  color: #2C3E50;
  font-size: 13px;
}
/*buttons*/
#msform .action-button {
    width: 131px;
    background: #c52a39;
    /* font-weight: bold; */
    color: white;
    border: 0 none;
    border-radius: 1px;
    cursor: pointer;
    font-size: 16px;
    padding: 12px 5px;
    margin: 10px 5px;
}
#msform .action-button:hover, #msform .action-button:focus {
  box-shadow: 0 0 0 2px white, 0 0 0 3px #c52a39;
}
/*headings*/
.fs-title {
  font-size: 15px;
  text-transform: uppercase;
  color: #2C3E50;
  margin-bottom: 10px;
}
.fs-subtitle {
  font-weight: normal;
  font-size: 13px;
  color: #666;
  margin-bottom: 20px;
}
/*progressbar*/
#progressbar {
  margin-bottom: 30px;
  overflow: hidden;
  /*CSS counters to number the steps*/
  counter-reset: step;
}
#progressbar li {
  list-style-type: none;
  color: white;
  text-transform: uppercase;
  font-size: 9px;
  width: 33.33%;
  float: left;
  position: relative;
}
#progressbar li:before {
    content: counter(step);
    counter-increment: step;
    width: 20px;
    line-height: 20px;
    display: block;
    font-size: 10px;
    color: #333;
    background: #9c9c9c;
    border-radius: 3px;
    margin: 0 auto 5px auto;
}
/*progressbar connectors*/
#progressbar li:after {
  content: '';
  width: 100%;
  height: 2px;
  background: #9c9c9c;
  position: absolute;
  left: -50%;
  top: 9px;
  z-index: -1; /*put it behind the numbers*/
}
#progressbar li:first-child:after {
  /*connector not needed before the first step*/
  content: none; 
}
/*marking active/completed steps green*/
/*The number of the step and the connector before it = green*/
#progressbar li.active:before,  #progressbar li.active:after{
  background: #c52a39;
  color: white;
}
.raion-fancy div {
margin-right: 15px;
width: 20%;
}

</style>
<main id="main">
  <!-- ======= Blog Section ======= -->
  <section id="blog" class="blog mt-5a" style="padding-bottom: 0px;">
    <div class="col-md-12 questinar">
      <h3>Questionnaire</h3> 
    </div>
    <div class="container">
      <div class="row mt-5 mb-5">
        <div class="col-md-12 text-left m-auto quest-asne mt-3">
           <!-- multistep form -->
           @if(Session::has('message'))

     <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>

     @endif



     @if(Session::has('error'))

     <p class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('error') }}</p>

     @endif
            <form id="msform" method="POST" action="{{url('/update_quation_answer')}}">
              @csrf
              <?php $toEnd = count($question_answer);  ?>
              <?php $i=1; 
                    $j=0; 
              ?>
               @foreach ($question_answer as $key =>  $question)
              <?php
                      $quation_answer =     (!empty($edit_question_answer[$j]->answer) ? $edit_question_answer[$j]->answer : '');
              ?>
                <fieldset id="hide<?php echo $i; ?>">
                   <input type="hidden" id="skipform" value="">
                  <input type="hidden" name="question_id[]" value="{{ $question->id }}">
                  <input type="hidden" name="id[]" value="{{ $quation_answer }}">

                <div class="quesiotn-select">
                    <h3>{{$question->question}}</h3>
                </div>
                @if($question->type == 1)
                <div class="raion-fancy">
                  <?php
                    $quation_answer_arr = preg_replace('/\s+/', '', $quation_answer);
                    $quationarr = explode(",", $question->answer);
                    $answerarr  = explode(",", $quation_answer_arr);
                    
                    
                  ?>
                 <?php 
                 $k = 1;
                 $l = 1;
                 foreach ($quationarr as $value) 
                 {

                    $spaceremovevalue              =   preg_replace('/\s+/', '', $value);
                    $answer                        =   in_array($spaceremovevalue ,$answerarr);
                    if(!empty($answer))
                    {
                        ?>
                         <div class="wdt_fix_all">
                          <input type="checkbox" id="control_01{{$i}}{{$l}}" name="answer{{$question->id}}[]" value="{{$value}}" checked>
                          <label for="control_01{{$i}}{{$l}}">
                              <i class="gender bx bxs-user"></i>
                            <p>{{$value}}</p>
                          </label>
                        </div>
                      <?php
                    }
                    else
                    {

                       ?>
                         <div>
                          <input type="checkbox" id="control_01{{$i}}{{$l}}" name="answer{{$question->id}}[]" value="{{$value}}">
                          <label for="control_01{{$i}}{{$l}}">
                              <i class="gender bx bxs-user"></i>
                            <p>{{$value}}</p>
                          </label>
                        </div>
                      <?php

                    }
              
                  $l++;
                 }
                ?>
                </div>
                <b> Its mandatory
                for me.</b> 
                   <?php
                 $mandatory_status  =   (!empty($edit_question_answer[$j]->mandatory_status) ? $edit_question_answer[$j]->mandatory_status : '');
                  ?>
                   <input type="checkbox" id="mandatory_status{{$question->id}}" name="mandatory_status{{$question->id}}[]" onclick="getmandatory_status({{$question->id}})" id="mandatory_status{{$question->id}}" value="<?php echo $mandatory_status; ?>" <?php 
                  if($mandatory_status== 1)
                      { echo "checked"; } ?> > 
                   
                  @elseif ($question->type == 2)
                  <div class="raion-fancy">
                  @if($quation_answer == 'yes')
                  <div>
                    <input type="radio" id="control_01{{$key}}" name="answer{{$question->id}}[]" value="yes" checked>
                    <label for="control_01{{$key}}">
                        <i class="gender bx bxs-user-check"></i>
                      <p>Yes</p>
                    </label>
                  </div>
                  @else
                    <div>
                    <input type="radio" id="control_01{{$key}}" name="answer{{$question->id}}[]" value="yes">
                    <label for="control_01{{$key}}">
                        <i class="gender bx bxs-user-check"></i>
                      <p>Yes</p>
                    </label>
                  </div>
                  
                  @endif
                   @if($quation_answer == 'no')
                  <div>
                    <input type="radio" id="control_02{{$key}}" name="answer{{$question->id}}[]" value="no" checked>
                    <label for="control_02{{$key}}">
                    <i class="gender bx bxs-user-x"></i>
                      <p>No</p>
                    </label>
                  </div>
                  @else
                  <div>
                    <input type="radio" id="control_02{{$key}}" name="answer{{$question->id}}[]" value="no">
                    <label for="control_02{{$key}}">
                    <i class="gender bx bxs-user-x"></i>
                      <p>No</p>
                    </label>
                  </div>
                  @endif
                </div>
                @elseif($question->type == 3)
                <input type="text" name="answer{{$question->id}}[]" placeholder="" value="{{$quation_answer}}"/>
                @endif
                
                <?php if($i > 1 ){?>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <?php }?>
                <?php if (0 === --$toEnd) { ?>   
                <input type="submit" name="submit" class="submit action-button mt-5" value="Submit" />
                   <?php } else { ?>
                 <!-- <input type="button" name="Skip" class="Skip action-button mt-5" onclick="skipdata(<?php echo $i; ?>); " value="Skip" /> -->
                <input type="button" name="next" class="next action-button mt-5" value="Next" />
                <?php } ?>

              </fieldset>

              <?php $i++; $j++; $k++; ?>
              @endforeach
            </form>
        </div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
function getmandatory_status(value) 
{

    
  // Get the checkbox
  var checkBox = document.getElementById("mandatory_status"+value);
  
  // Get the output text
  var text = document.getElementById("text");

  // If the checkbox is checked, display the output text
  if (checkBox.checked == true){
    $("#mandatory_status"+value).val(1);
  } 
  else 
  {
    $("#mandatory_status"+value).val();
  }
}
</script>
        <script type="text/javascript">
          //jQuery time
          var current_fs, next_fs, previous_fs, previous ; //fieldsets
          var left, opacity, scale; //fieldset properties which we will animate
          var animating; //flag to prevent quick multi-click glitches

          $(".next").click(function(){
            if(animating) return false;
            animating = true;
            
            current_fs = $(this).parent();
            next_fs = $(this).parent().next();
            prev        = $(this).parent().prev();
            previous    = prev -1 ;
            //activate next step on progressbar using the index of next_fs
            $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
            
            //show the next fieldset
            next_fs.show(); 
            //hide the current fieldset with style
            current_fs.animate({opacity: 0}, {
              step: function(now, mx) {
                //as the opacity of current_fs reduces to 0 - stored in "now"
                //1. scale current_fs down to 80%
                scale = 1 - (1 - now) * 0.2;
                //2. bring next_fs from the right(50%)
                left = (now * 50)+"%";
                //3. increase opacity of next_fs to 1 as it moves in
                opacity = 1 - now;
                current_fs.css({
                  'transform': 'scale('+scale+')',
                  'position': 'absolute'
                });
                next_fs.css({'left': left, 'opacity': opacity});
              }, 
              duration: 800, 
              complete: function(){
                current_fs.hide();
                animating = false;
              }, 
              //this comes from the custom easing plugin
              easing: 'easeInOutBack'
            });
          });

          $(".previous").click(function()
          {
            if(animating) return false;
            animating = true;
            
            current_fs = $(this).parent();
            previous_fs = $(this).parent().prev();
            
            //de-activate current step on progressbar
            skipvalue    = $("#skipform").val();
            valuecurrent = $("fieldset").index(previous_fs);
            // if(valuecurrent == skipvalue)
            // {
              
            //     previous_fs.hide();
            //     current_fs.show();
              

            // }
            // else
            // {

            //     previous_fs.show(); 
            // }
           
            
            
            //show the previous fieldset
            previous_fs.show(); 
            //hide the current fieldset with style
            current_fs.animate({opacity: 0}, {
              step: function(now, mx) {
                //as the opacity of current_fs reduces to 0 - stored in "now"
                //1. scale previous_fs from 80% to 100%
                scale = 0.8 + (1 - now) * 0.2;
                //2. take current_fs to the right(50%) - from 0%
                left = ((1-now) * 50)+"%";
                //3. increase opacity of previous_fs to 1 as it moves in
                opacity = 1 - now;
                current_fs.css({'left': left});
                previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
                

              }, 
              duration: 800, 
              complete: function(){
                current_fs.hide();
                animating = false;
              }, 
              //this comes from the custom easing plugin
              easing: 'easeInOutBack'
            });
          });


          $(".Skip").click(function()
          {
          

            
            if(animating) return false;
            animating = true;
            
            current_fs = $(this).parent();
            value      = $('#skipform').val($("fieldset").index(current_fs));
            skip = $("fieldset").index(current_fs);

            $('#hide'+skip).hide();

            next_fs = $(this).parent().next();
            
            //activate next step on progressbar using the index of next_fs
            $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
            
            //show the next fieldset
            next_fs.show(); 
            //hide the current fieldset with style
            current_fs.animate({opacity: 0}, {
              step: function(now, mx) {
                //as the opacity of current_fs reduces to 0 - stored in "now"
                //1. scale current_fs down to 80%
                scale = 1 - (1 - now) * 0.2;
                //2. bring next_fs from the right(50%)
                left = (now * 50)+"%";
                //3. increase opacity of next_fs to 1 as it moves in
                opacity = 1 - now;
                current_fs.css({
                  'transform': 'scale('+scale+')',
                  'position': 'absolute'
                });
                next_fs.css({'left': left, 'opacity': opacity});
              }, 
              duration: 800, 
              complete: function(){
                current_fs.hide();
                animating = false;
              }, 
              //this comes from the custom easing plugin
              easing: 'easeInOutBack'
            });
          });

          
           

        

         
          $(".submit").click(function(){
            //return false;
          })
        </script>
      <script>
       $(".alert").fadeTo(2000, 500).slideUp(500, function(){
       $(".alert").slideUp(500);
       });
      </script>

      </div>
      <!-- End blog entries list -->
    </div>
  </section>
  <!-- End Blog Section -->
</main>
<!-- End #main --> 
<style type="text/css">
    .raion-fancy i {
      font-size: 60px;
    }
    
    .raion-fancy div {
      margin-right: 15px;
    }
    
    .raion-fancy {
      display: flex;
      justify-content: center;
    }
    
    .raion-fancy input[type=radio] {
      display: none;
    }
    
    .raion-fancy input[type=radio]:not(:disabled) ~ label {
      cursor: pointer;
    }
    
    .raion-fancy input[type=radio]:disabled ~ label {
      color: #bcc2bf;
      border-color: #bcc2bf;
      box-shadow: none;
      cursor: not-allowed;
    }
    
    .raion-fancy label {
      height: 83%;
      display: block;
      background: white;
      border: 1px solid #c52a39;
      border-radius: 6px;
      padding: 1rem;
      margin-bottom: 1rem;
      text-align: center;
      box-shadow: 0px 3px 10px -2px rgb(161 170 166 / 50%);
      position: relative;
    }
    
    .raion-fancy input[type=radio]:checked + label {
      background: #c52a39;
      color: white;
      box-shadow: 0px 0px 9px rgb(197 42 57);
    }
    
    .raion-fancy input[type=radio]#control_05:checked + .raion-fancy label {
      background: red;
      border-color: red;
    }
    
    p {
      font-weight: 900;
    }
    
    @media only screen and (max-width: 700px) {
      section {
        flex-direction: column;
      }
    }
</style>
@endsection