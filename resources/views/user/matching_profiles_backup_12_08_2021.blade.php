@extends('layouts.app') @section('content') 
<main id="main">
  <section id="blog" class="blog mt-5a" style="padding-bottom: 0px; background-color: #f5f5f5;">
  	<div class="col-md-12 questinar">
        <h3>Match</h3> 
    </div>
    <div class="container">
      <div class="row m-0 p-0 ">
        <div class="col-md-10 text-left m-auto mt-3 mb-5 ">
          <div id="reqmsg"></div>
          <div class="row">
            @foreach($usersinfo as $user)
         
            
             	<div class="col-md-4 mb-5 mt-5 m-xp">
                <input type="hidden" name="to_id" value="{{$user->id}}">
               	<a href="{{url('/viewmatchProfile')}}/{{base64_encode($user->id)}}" class="">
               		<div class="match-box">
                    <div class="match-pic">
                      @if (!empty($user->profile_pic)) 
                     	<img src="{{url('/public/uploads/profile_img')}}/{{$user->profile_pic}}">
                      @else
                      <img src="https://votivetech.in/4twoo/public/assets/img/Layer 11.png">
                      @endif
                     	<div class="match-persent">
                     	80% similarities
                       </div>
                    </div>
                      <h3>{{$user->first_name}} {{$user->last_name}}</h3>
                     
                      <div class="cross-btn">
                      	<!-- <a href="#" class="cross-btn-ser"><i class='bx bx-x'></i></a> -->
                      	<a href="javascript:void(0)" class="cross-btn-ser send_request" data-id="{{$user->id}}" id="send_request_{{$user->id}}"><i class='bx bxs-heart' style="display: none;" id="bxs{{$user->id}}"></i><i class='bx bx-heart' id="bxs{{$user->id}}"></i></a>
                      </div>
               		</div>
               	</a>
             	</div>
            @endforeach 
          </div>
        </div>
      </div>
    </div>
  </section>
</main> 
@endsection