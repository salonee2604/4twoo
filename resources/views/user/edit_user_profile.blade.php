@extends('layouts.app')

@section('content')
<?php //echo "<pre>"; print_r($userinfo); ?>
<main id="main">
   <!-- ======= Blog Section ======= -->
   <section id="blog" class="blog mt-5a" style="padding-bottom: 0px;">
      <div class="">
         <div class="row m-0 p-0">
            <div class="container emp-profile">
               <form method="POST" action="{{ route('user_update') }}" enctype="multipart/form-data">
                  @csrf
                  <input type="hidden" value="{{$userinfo->id}}" name="id">
                  <div class="row row-botm">
                     <div class="col-md-2" style="padding-left: 0px;">
                        <label class=newbtn>
                          @if($userinfo->profile_pic =='')         
                                <img id="blah" src="{{url('/public/uploads/profile_img')}}/default.png" alt=""/  style="width: 200px;">      
                          @else
                                <img id="blah" src="{{url('/public/uploads/profile_img')}}/{{$userinfo->profile_pic}}" style="width: 200px;" >     
                          @endif
                          <input id="pic" class='pis' onchange="editreadURL(this);" type="file" name="user_image">
                          <input type="hidden" name="oldimage" value="{{$userinfo->profile_pic}}">
                        </label>
                     </div>
                     <div class="col-md-6">
                        <div class="profile-head">
                           <h5>
                              {{$userinfo->first_name}} {{$userinfo->last_name}}
                           </h5>
                           
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-12">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                           <div class="row">
                              <div class="col-md-2">
                                 <label>First Name</label>
                              </div>
                              <div class="col-md-10">
                                 <div class=" login-filed">
                                    <div class="form-group">
                                       <input id="first_name" type="text" class="form-control" name="first_name" required autocomplete="first_name" placeholder="first_name" value="{{$userinfo->first_name}}">
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-2">
                                 <label>Last Name</label>
                              </div>
                              <div class="col-md-10">
                                 <div class=" login-filed">
                                    <div class="form-group">
                                       <input id="last_name" type="text" class="form-control" name="last_name" required autocomplete="last_name" placeholder="Last name" value="{{$userinfo->last_name}}">
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-2">
                                 <label>Phone</label>
                              </div>
                              <div class="col-md-10">
                                 <div class=" login-filed">
                                    <div class="form-group">
                                       <input id="contact_number" type="text" class="form-control" name="contact_number" required autocomplete="contact_number" placeholder="contact_number" value="{{$userinfo->contact_number}}">
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-2">
                                 <label>Email</label>
                              </div>
                              <div class="col-md-10">
                                 <div class=" login-filed">
                                    <div class="form-group">
                                       <input id="contact_number" type="text" class="form-control" name="contact_number" required autocomplete="contact_number" placeholder="contact_number" value="{{$userinfo->email}}">
                                    </div>
                                 </div>
                              </div>

                              
                           </div>
                           
                      
                          
                  
                  <div class="row">
                     <div class="col-md-12">
                        <center>  <input type="submit" name="submit" class="neslat-update" value="Update"></center>
                     </div>
                  </div>


               </form>

            </div>

         </div>

         
         <!-- End blog entries list -->
      </div>
      </div>
   </section>
   <!-- End Blog Section -->
</main>
<style type="text/css">
   label {
    padding-top: 12px;
}
</style>

<!-- End #main -->
@endsection