@extends('layouts.app') @section('content') 
<style type="text/css">
   .mt-5a{
   margin-top: 90px;
   }
</style>
<main id="main">
  
   <!-- ======= Blog Section ======= -->
   <section id="blog" class="blog mt-5a" style="padding-bottom: 0px;">
      <div class="">
         <div class="row m-0 p-0">
            <div class="col-md-12 questinar">
               <h3>Match</h3>
            </div>
            <div class="container">
               <?php $i=1; ?>
               @foreach($usersinfo as $row)
               <a href="{{url('/viewmatchProfile')}}/{{base64_encode($row['data']->id)}}" style="color: #444444;">
                  <div class="row justify-content-center mt-2 ">
                     <div class="col-md-9 emp-profile-list ">
                        <div class="row">
                           <div class="col-md-3">
                              <div class="prof-list">
                                 @if (!empty($row['data']->profile_pic)) 
                                 <img src="{{url('/public/uploads/profile_img')}}/{{$row['data']->profile_pic}}" height="100px;" width="100px;">
                                 @else
                                 <img src="https://votivetech.in/4twoo/public/assets/img/Layer 11.png" height="100px;" width="100px;">
                                 @endif
                              </div>
                           </div>
                           <div class="col-md-9 main-focser pl-0">
                              <div class="top-descrip">
                                 <div class="u-infor">
                                   <h3>{{$row['data']->first_name}}  {{$row['data']->last_name}}</h3>
                                 </div>
                                    <?php
                                        $matchedcount = $row['count'];
                                        $allquation_count        = DB::table('questionnaire')->count();
                                        $matchedpercentage        = ($matchedcount/$allquation_count)*100;

                                        if(!empty($matchedpercentage))
                                        {
                                        $matchedpercentagevalue = round($matchedpercentage,2); 
                                        }
                                        else
                                        {
                                        $matchedpercentagevalue = "";
                                        }
                                    ?>
                                 <div class="complet-reso">
                                    <div class="circle_percent" data-percent="<?php echo $matchedpercentagevalue; ?>">
                                       <div class="circle_inner">
                                          <div class="round_per"></div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="list-info">
                                 <address>
                                    <?php
                                     $date2        = date('Y-m-d');
                                     $date1        = $row['data']->dob;
                                     $diff         = abs(strtotime($date2) - strtotime($date1));
                                     $years        = floor($diff / (365*60*60*24));
                                     if(!empty( $years))

                                    {

                                        $dob = $years;
                                    }
                                    else
                                    {
                                        $dob  = '';
                                    }
                                     ?>
                                   
                                    <?php echo $dob ; ?> years old,<br>
                                    {{$row['data']->address}}
                                 </address>
                                 <div class="col-md-12 p-0">
                                    <div class="dislike">
                                       <button class="like-dislike"> <i class='bx bxs-heart'></i></button>
                                       <button href="#" class="like-dislike"><i class='bx bx-envelope'></i></button>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </a>
               <?php $i++; ?>
               @endforeach
            </div>
         </div>
         <!-- End blog entries list -->
      </div>
      </div>
   </section>
   <!-- End Blog Section -->
   <!-- End Blog Section -->
</main>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
   $(".circle_percent").each(function() {
   var $this = $(this),
       $dataV = $this.data("percent"),
       $dataDeg = $dataV * 3.6,
       $round = $this.find(".round_per");
   $round.css("transform", "rotate(" + parseInt($dataDeg + 180) + "deg)"); 
   $this.append('<div class="circle_inbox"><span class="percent_text"></span></div>');
   $this.prop('Counter', 0).animate({Counter: $dataV},
   {
       duration: 2000, 
       easing: 'swing', 
       step: function (now) {
           $this.find(".percent_text").text(Math.ceil(now)+"%");
       }
   });
   if($dataV >= 51){
       $round.css("transform", "rotate(" + 360 + "deg)");
       setTimeout(function(){
           $this.addClass("percent_more");
       },1000);
       setTimeout(function(){
           $round.css("transform", "rotate(" + parseInt($dataDeg + 180) + "deg)");
       },1000);
   } 
   });
</script>
@endsection