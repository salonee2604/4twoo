@extends('layouts.app')

@section('content')

<?php //echo "<pre>"; print_r($userinfo); ?>

  <main id="main">
 
    <!-- ======= Blog Section ======= -->
    <section id="blog" class="blog mt-5a" style="padding-bottom: 0px;">
      <div class="">
        
        <div class="row m-0 p-0">
            <div class="container emp-profile">
                <div id="uploaded_image"></div>
                
                
                 <form method="post">
                    @csrf
                    <div class="row">
                       
                      
                     

                    </div>
                    <div class="row">
                        
                        <div class="col-md-12 ctr_box_prop">
                            <div class="text-center">
                @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-success') }}" style="text-align: center">{{ Session::get('message') }}</p>
                <?php redirect('/') ?>
                @endif
                @if(Session::has('error'))
                <p class="alert {{ Session::get('alert-class', 'alert-danger') }}" style="text-align: center">{{ Session::get('error') }}</p>
                @endif
               </div>
                            <div class="tab-content profile-tab" id="myTabContent">
                                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label>User Id</label>
                                                </div>
                                                <div class="col-md-6">
                                                    <p>{{$userinfo->id}} </p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label>Name</label>
                                                </div>
                                                <div class="col-md-6">
                                                    <p>{{$userinfo->first_name}} {{$userinfo->last_name}}</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label>Email</label>
                                                </div>
                                                <div class="col-md-6">
                                                    <p>{{$userinfo->email}}</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label>Phone</label>
                                                </div>
                                                <div class="col-md-6">
                                                    <p>{{$userinfo->contact_number}}</p>
                                                </div>
                                            </div>
                                           
                                            <div class="row fl_l">
                                                   <div class="col-md-12 " style="display: flex;justify-content: center;align-items: center; border-top: 1px solid #ddd; margin-left: 25px;">
                            <a href="{{url('/delete_profile')}}" class="profile-edit-btn" name="btnAddMore">Delete Profile</a>
                           <a href="{{url('/edituser/dashboard/')}}" class="profile-edit-btn" name="btnAddMore">Edit Profile</a>
                           <a href="{{url('/changepassword')}}" class="profile-edit-btn" name="btnAddMore">ChangePaasword</a>
                        </div></div>
                                </div>
                                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>Experience</label>
                                                </div>
                                                <div class="col-md-6">
                                                    <p>Expert</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>Hourly Rate</label>
                                                </div>
                                                <div class="col-md-6">
                                                    <p>10$/hr</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>Total Projects</label>
                                                </div>
                                                <div class="col-md-6">
                                                    <p>230</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>English Level</label>
                                                </div>
                                                <div class="col-md-6">
                                                    <p>Expert</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>Availability</label>
                                                </div>
                                                <div class="col-md-6">
                                                    <p>6 months</p>
                                                </div>
                                            </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label>Your Bio</label><br/>
                                            <p>Your detail description</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>           
            </div>
        </div><!-- End blog entries list -->

        </div> 
    </section><!-- End Blog Section -->

  </main><!-- End #main -->

@endsection
<script type="text/javascript">
   <?php 
         if(Session::has('message'))
         {
           $session = "session";
         }
         else
         {
               $session = "nosession";
         }
    ?>
    var session = '<?php echo $session; ?>';

    if(session == 'session')
    {
        setTimeout(function () {    
         window.location.href = "{{url('logout_profile')}}"; 
         },4000);  
    }
    
    
</script>
<style type="text/css">
    div#home p {
    color: #444444;
    
}
.ctr_box_prop {
    background-color: #f5f5f5;
    padding: 20px;
    border: 1px solid #ddd;
    border-radius: 4px;
    text-align: center;
}
a.profile-edit-btn {
    border-radius: 4px;
    width: unset;
    margin-left: 10px;
    margin-top: 10px;
}
.row.fl_l {
    float: left;
    width: 100%;
    /* margin-top: 0px; */

    text-align: center;
    display: flex;
    justify-content: center;
    align-items: center;
}

</style>