@extends('layouts.app') @section('content')

<main id="main">
	    <section id="blog" class="blog mt-5a" style="padding-bottom: 0px; background-color: #f5f5f5;">
	    	<div class="col-md-12 questinar">
      <h3> Details </h3> 
    </div>
      <div class="container">
      <div class="row m-0 p-0 d-flex justify-content-center">
  <div class="col-md-10 mt-3 mb-5 m-xp d-flex justify-content-center">
       		<div class="match-box-d">
            <div class="match-pic-d">
                @if($userinfo->profile_pic =='')
                  <img src="https://votivetech.in/4twoo/public/assets/img/Layer 11.png">
                @else
                  <img src="{{url('/public/uploads/profile_img')}}/{{$userinfo->profile_pic}}">
                @endif
            <div class="match-persent"><?php echo $matchpercentage.'%'.' '.'Match' ; ?></div>
            </div>
              <h3>{{$userinfo->first_name}} {{$userinfo->last_name}}</h3>
              <!-- <p> Full time foodic love to make new FriendsAlways slepping</p> -->
             <!-- <div class="brk"></div> -->
            <div class="bg-more-profile">
             <div class="loction"> <i class='bx bx-map'></i>  {{$userinfo->address}}</div>
              <div class="list-details d-flex justify-content-between align-items-center"> 
                <div class="gri-box">
                <h2> Looking for</h2>
                <?php
                 $date2        = date('Y-m-d');
                                     $date1        = $userinfo->dob;
                                     $diff         = abs(strtotime($date2) - strtotime($date1));
                                     $years        = floor($diff / (365*60*60*24));
                                     $maxyear      = $years + 5;
                ?>
                <p> A man <?php echo $years.'-'.$maxyear; ?> year</p>
                   </div>
               </div>

             <div class="list-details d-flex justify-content-between align-items-center"> 
                <div class="gri-box">
                <h2>Age</h2>
                <p><?php echo $years; ?>year</p>
                   </div>
                   
              </div>
               <div class="list-details d-flex justify-content-between align-items-center"> 
                <div class="gri-box">
                <h2>Email</h2>
                <p><?php echo $userinfo->email; ?></p>
                   </div>
                   
              </div>
               <div class="list-details d-flex justify-content-between align-items-center"> 
                <div class="gri-box">
                <h2>Mobile</h2>
                <p><?php echo $userinfo->contact_number; ?></p>
                   </div>
                   
              </div>
              <div class="list-details d-flex justify-content-between align-items-center"> 
                <div class="gri-box">
                <h2>Profession</h2>
                <p><?php echo $userinfo->occupation; ?></p>
                   </div>
                   
              </div>
               

            </div>

            <div class="col-md-12">
                    <center>  <a href="#" class="neslat-update mt-5"> Send Request </a></center>  
                      </div>

             </div>
       	
     	
 </div>
</div>
</div>
</section>
</main>


@endsection