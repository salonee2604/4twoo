@extends('layouts.app')
@section('content')
<main id="main">
   <!-- ======= Blog Section ======= -->
   <section id="blog" class="blog mt-5a" style="padding-bottom: 0px;">
      <div class="">
         <div class="row m-0 p-0">
            <div class="container emp-profile">
               <center>
                  <h3 class="mb-5">Welcome in 4Two Restaurent<br> <small>Book your table</small></h3>
               </center>
               <div id="msg"></div>
               <form class="forms-sample" id="add_table_booking" name="add_table_booking" enctype='multipart/form-data' method="post" action="{{url('admin/table_booking_post')}}">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <div class="row d-flex justify-content-center booking-filed">
                     <div class="col-md-4">
                        <input type="text" class="form-control" id="username" placeholder="User Name" name="username" autocomplete="off" value="{{$userinfo->username}}">          
                     </div>
                     <div class="col-md-4">
                        <input type="text" name="mobile" id="mobile" placeholder="Mobile" class="form-control"value="{{$userinfo->contact_number}}">
                     </div>
                  </div>
                  <div class="row d-flex justify-content-center booking-filed mt-3">
                     <div class="col-md-4">
                        <input type="text" id="email" name="email" placeholder="Email" class="form-control" value="{{$userinfo->email}}">
                     </div>
                     <div class="col-md-4">
                        <input type="date" id="booking_date" name="booking_date" class="form-control">
                     </div>
                  </div>
                  <div class="row d-flex justify-content-center booking-filed mt-3">
                     <div class="col-md-4">
                        <select id="day" name="day" class="form-control">
                           <option value="">-- Day--</option>
                           <option value="Monday">Monday</option>
                           <option value="Tuesday">Tuesday</option>
                           <option value="Wednesday">Wednesday</option>
                           <option value="Thursday">Thursday</option>
                           <option value="Friday">Friday</option>
                           <option value="Saturday">Saturday</option>
                           <option value="Sunday">Sunday</option>
                        </select>
                     </div>
                     <div class="col-md-4">
                        <select class="form-control hasDatepicker valid" id="time_slot_id" name="time_slot_id">
                           <option value="">Time Slot--</option>
                        </select>
                     </div>
                  </div>
                  <div class="row d-flex justify-content-center booking-filed mt-3">
                     <div class="col-md-4">
                        <input type="number"  class="form-control" id="number_of_memebers" placeholder="Number Of Members" name="number_of_memebers" value="" autocomplete="off" min="1" step="1"/>
                     </div>
                     <div class="col-md-4">
                        <input type="number"  class="form-control" id="total_table" placeholder="Total Table" name="total_table" value="" autocomplete="off" min="1" step="1"/>
                     </div>
                  </div>
                  <div class="row d-flex justify-content-center booking-filed mt-3">
                     <div class="col-md-4">
                        <select id="table_type" name="table_type" class="form-control" value="">
                          <option value="">Select Table Type</option>
                           @foreach($table_type as $value)
                             <option value="{{(!empty($value->section_id) ? $value->section_id : '')}}" >{{ $value->section_name }}</option>
                             @endforeach
                        </select>
                     </div>
                     <div class="col-md-4">
                        <input type="text" name="message" id="message" placeholder="Message" class="form-control">
                     </div>
                  </div>
                  <div class="row d-flex justify-content-center booking-filed mt-3">
                     <div class="col-md-8">
                        <input type="text" name="address" placeholder="Address" class="form-control" value="{{$userinfo->address}}">
                     </div>
                  </div>
                  <div class="row d-flex justify-content-center booking-filed mt-4">
                     <div class="col-md-4">
                        <button type="submit" class="btn btn-primary w-100 p-" id="reservation_btn">
                        Confirm Booking
                        </button>
                     </div>
                  </div>
                  <div class="row d-flex justify-content-center booking-filed mt-3">
                     <div class="col-md-8">
                        <p class="tc-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever. </p>
                     </div>
                  </div>
               </form>
            </div>
         </div>
         <!-- End blog entries list -->
      </div>
      </div>
   </section>
   <!-- End Blog Section -->
</main>
<!-- End #main -->
<script type="text/javascript">
   /*$(document).ready(function() {
     $('#pwd').bind("cut copy paste", function(event) {
       event.preventDefault();
     });
   })/*/
   //myElement.addEventListener('paste', e => e.preventDefault());
</script>

@endsection