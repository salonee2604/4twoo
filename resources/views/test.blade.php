@extends('layouts.app')



@section('content')

  <section id="hero1" class="d-flex justify-cntent-center align-items-center">
    <div class="container carousel carousel-fade mt-5" >
    <div class="w-75 text-center m-auto slider-text-love ">
     <h1>Euer Single-Leben ist langweilig? <br>App herunterladen und sofort gematcht<br>werden<img src="{{ url('/public/assets/img/imoz.png') }}">  </h1><br>
     <h4> Wir finden für Dich das perfekte Date. <br>Dafür benötigen wir ein paar angaben von Dir. </h4>
     <center><img src="{{ url('/public/assets/img/aroimg.png') }}"></center>
      </div>
     
    </div>
  </section><!-- End Hero -->

  <main id="main">

    <!-- ======= Icon Boxes Section ======= -->
    <section id="icon-boxes" class="icon-boxes">
      <div class="container">

        <div class="row justify-content-md-center">
          <div class="col-md-9 col-lg-9 d-flex align-items-stretch mb-5 mb-lg-0 form-text " data-aos="fade-up">
           
            <div class="icon-box">
               <h3> Hier kannst Du Dein Profil eingeben</h3>
              
              <form>
                <div class="row dating-form">
                  <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group">
                      <input type="text" name="first_name" id="first_name" class="form-control input-sm" placeholder="Vorname">
                    </div>
                  </div>
                  <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group">
                      <input type="text" name="last_name" id="last_name" class="form-control input-sm" placeholder="Nachname">
                    </div>
                  </div>
                </div>

              </form>

              <h4 class="title mt-3"><a href="">Wähle dein Geschlecht</a></h4>
              <div class="row justify-content-md-center raid-fanc">
<div class="col-md-3">
            <div class="radio">
    <input id="radio-1" name="radio" type="radio" checked>
    <label for="radio-1" class="radio-label">MÄNNLICH</label>
  </div>
</div>
<div class="col-md-3">
  <div class="radio">
    <input id="radio-2" name="radio" type="radio">
    <label  for="radio-2" class="radio-label">WEIBLICH</label>
  </div></div>
  <div class="col-md-3">
    <div class="radio">
    <input id="radio-3" name="radio" type="radio" checked>
    <label for="radio-3" class="radio-label">ANDERES</label>
  </div></div>


<div class="col-md-12"><a href="#" class="weiter-btn">weiter <i class="icofont-long-arrow-right"></i> </a></div>

</div>
<img src="{{ url('/public/assets/img/hart.png') }}" class="img-side">

</div>
            </div>
          </div>

        

        </div>

      </div>
    </section>

 <!-- ======= Why Us Section ======= -->
    <section id="about" class="why-us">
      <div class="container-fluid">

        <div class="row">

          <div class="col-lg-6 align-items-stretch video-box" style='background-image: url("public/assets/img/dier-about.png"); height: 539px;' data-aos="fade-right">
         
          </div>

          <div class="col-lg-6 d-flex flex-column justify-content-center align-items-stretch" data-aos="fade-left">
            <div class="uber-unchi">
            <center>
         <img src="{{ url('/public/assets/img/logo vector.png') }}" class="unch-about">
         <h3>über uns</h3></center>
            <div class="content">
            
              <p>
             Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
              </p>
              <a href="#"  class="weiter-btn-right ">Weiterlesen <i class="icofont-thin-right"></i> </a>
            </div>
</div>
          

          </div>

        </div>

      </div>
    </section><!-- End Why Us Section -->

<!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
      <div class="container" data-aos="fade-up">

        
        <div class="row mt-1 d-flex justify-content-center">

          <div class="col-lg-9">
            <h4> 

Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book
<a href="#" class="weiter-btn-resto">Tisch reservieren </a>

            </h4>

          </div>

          

        </div>

      </div>
    </section><!-- End Contact Section -->


    <?php //echo "<pre>"; print_r($stories); ?>
    <!-- ======= Clients Section ======= -->
    <section id="clients" class="clients" style="padding: 10px 0px;">

      <div class="container" data-aos="zoom-in">
<div class="pic-foru"><h2> Erfolgsgeschichte</h2> </div>
        <div class="owl-carousel clients-carousel">
           @foreach($stories as $key => $story)
          <img src="{{ url('/public/assets/img/') }}/{{$story->image}}" alt="">
          <!-- <img src="{{ url('/public/assets/img/s2.png') }}" alt="">
          <img src="{{ url('/public/assets/img/s3.png') }}" alt="">
          <img src="{{ url('/public/assets/img/s4.png') }}" alt=""> -->
          @endforeach
        </div>
     <a href="#" class="view-all">View All Photos ({{ $stories->count() }}) </a>
      </div>
    </section>

<!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex justify-cntent-center align-items-center">
    <div id="heroCarousel" class="container carousel carousel-fade" data-ride="carousel">
  
     <center><h2 class="animate__animated animate__fadeInDown">UNSER TESTIMONIAL</h2> </center>
    

      @foreach($testimonials as $key => $testimonial)
      <div class="carousel-item @if($key == 0)active @endif">
        <div class="carousel-container">
           <div class="circle-img"> <img src="{{ url('/public/assets/img/') }}/{{$testimonial->image}}"></div>
          <p class="animate__animated animate__fadeInUp">{{$testimonial->content}}</p>
        </div>
      </div>
      @endforeach

      <a class="carousel-control-prev" href="#heroCarousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon bx bx-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>

      <a class="carousel-control-next" href="#heroCarousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon bx bx-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>

    </div>
  </section><!-- End Hero -->
    <!-- ======= About Us Section ======= -->
    <section id="about" class="about">
      <div class="container">

     <img src="{{ url('/public/assets/img/play.png') }}" class="pos-play">
     <div class="row">
  <div class="col-md-7"> 
    <div class="port-fil">
      <h3> Hier das 4twoo App herunterladen.<br>Das Profil ausfüllen, matchen und<br>einen romantischen Tisch reservieren.</h3>

      <div class="app-box">
        <a href="#"><img src="{{ url('/public/assets/img/go-ply.png') }}"> </a>
         <a href="#"><img src="{{ url('/public/assets/img/andor.png') }}"> </a>
        
      </div>
    </div>

     </div>
     <div class="col-md-5"> 
       <img src="{{ url('/public/assets/img/playmp.png') }}" class="mobile-play">
     </div>
</div>
      </div>
    </section><!-- End About Us Section --> 
  </main><!-- End #main -->
@endsection

