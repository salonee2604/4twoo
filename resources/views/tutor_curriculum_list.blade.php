@extends('tutor.layout.layout')
@section('title', 'Tutor - Dashboard')

@section('current_page_css')
 <link href="{{url('/')}}/resources/assets/css/master.css" rel="stylesheet">
@endsection

@section('current_page_js')
<script type="text/javascript">
    $('.alert_close').click(function(){
      $( ".alert_box" ).fadeOut( "slow", function() {
      });
    });
 
  
    function Readnotification(notificationID){
         $.post("{{url('/tutor/read-notification')}}",{"_token": "{{ csrf_token() }}",notificationID:notificationID},function(response){
            if(response.status){
               $('#totalnotification').html(response.totalnotification);
               window.location.href='{{url("/tutor/notification-details")}}'+'/'+response.notificationID;
            }else{
               console.log('No new notification');
            }
         });
      }

      function load_unseen_notification(){
         $.post("{{url('/tutor/getNotification')}}",{"_token": "{{ csrf_token() }}",},function(response){
            if(response.status){
          
               $('#totalnotification').html(response.totalnotification);
               var str ='';
           
               if(response.notification) {
                  $.each(response.notification, function(index,val){
                     str += '<li onclick="Readnotification('+val.id+')"><a href="#!"><p>'+val.ready_msg+'</p></a></li>';
                  });
               } else {
                  str ='<li><a href="#"><span class="title">No notification available</span></a></li>';
               }
               $('#dropdown').html('');
               $('#dropdown').html(str);
          
            } else {
               console.log('No new notification');
            }
         });
      } 
    
      setInterval(function(){ load_unseen_notification(); }, 5000);
  </script>
  <!-- Notification code close -->
@endsection

@section('content')
<div class="main_tutor">
  <div class="container">
      <div id="cirruculum_list" class="col s12">            
           @if(!empty($curriculum_list))
            <div class="main_div_str">
                <div class="main_div_str_main">
                 @foreach($curriculum_list as $curriculumslistVal)
                    <div class="main_div_str_one">
                        <a href="#">
                            <div class="img_di_n"> 
                                <img src="{{ url('public/uploads/img_two.png') }}"></div>
                        </a>
<!--                        <a class="quick-view" rel=""><span style="font-size:15px;">Fichier PS</span></a>-->
                    </div>
                  @endforeach
                </div>
                <div class="main_div_str_bott_s">
                    <img src="{{ url('public/uploads/img_one.png') }}">
                </div>
            </div>
    @else
       <div class="card-panel red lighten-1"> No Data available</div>
     @endif 
          
      </div>
  </div>
</div> 

@endsection