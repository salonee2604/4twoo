<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
	<head>
<meta name="viewport" content="width=device-width" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Email </title>
<style type="text/css">
img {
max-width: 100%;
}
body {
-webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; line-height: 1.6em;
}
body {
background-color: #f6f6f6;
}
@media only screen and (max-width: 640px) {
  body {
    padding: 0 !important;
  }
  h1 {
    font-weight: 800 !important; margin: 20px 0 5px !important;
  }
  h2 {
    font-weight: 800 !important; margin: 20px 0 5px !important;
  }
  h3 {
    font-weight: 800 !important; margin: 20px 0 5px !important;
  }
  h4 {
    font-weight: 800 !important; margin: 20px 0 5px !important;
  }
  h1 {
    font-size: 22px !important;
  }
  h2 {
    font-size: 18px !important;
  }
  h3 {
    font-size: 16px !important;
  }
  .container {
    padding: 0 !important; width: 100% !important;
  }
  .content {
    padding: 0 !important;
  }
  .content-wrap {
    padding: 10px !important;
  }
  .invoice {
    width: 100% !important;
  }
}
</style>
</head>

<body style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; line-height: 1.6em; background-color: #f6f6f6; margin: 0;" bgcolor="#f6f6f6">


  <table style="width: 500px; margin:0 auto;   border: solid 1px #FFC72A;" cellpadding="0" cellspacing="0">
      <thead>
        <tr>
          <th style="background: #EEE; padding: 15px;">
            <img src="http://localhost/virtualclass/public/uploads/logo/logo_1593754629.png" width="220px" height="50px;" /> 
          </th>
       </tr>
       </thead>
		 <tr>
         <td style="font-family: Arial; font-size:14px; padding:15px; background: #fff">
        Hi {0},
        </td>
       </tr>
       <tr>
         <td style="font-family: Arial; font-size:14px; padding:15px; background: #fff">
          {1} has invited you for the {2} session scheduled on {3} from {4} to {5}.
        </td>
       </tr>
	    <tr>
         <td style="font-family: Arial; font-size:14px; padding:15px; background: #fff">
			Thank you
        </td>
       </tr>
      <tr>
        <td style="font-family: Arial; font-size:12px; padding:10px 15px; background: #EEE">
         <center> @virtualclass</center> 
        </td>
      </tr>

 </table>
</body>
</html>





