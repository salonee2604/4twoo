@extends('admin.layout.layout')
@section('title', 'Student List')

@section('current_page_css')
<!-- DataTables -->
<link rel="stylesheet" href="{{url('/')}}/resources/assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="{{url('/')}}/resources/assets/css/bootstrap-toggle.min.css">
@endsection

@section('current_page_js')
<!-- DataTables -->
<script src="{{url('/')}}/resources/assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="{{url('/')}}/resources/assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="{{url('/')}}/resources/assets/js/bootstrap-toggle.min.js"></script>
<script type="text/javascript">
  $(function () {
    $('#tutor_list').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
<script type="text/javascript">
 function delete_tutor(tutor_id){
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  $.ajax({
   type: 'POST',
   url: "<?php echo url('/admin/delete_tutor'); ?>",
   enctype: 'multipart/form-data',
   data:{tutor_id:tutor_id,'_token':'<?php echo csrf_token(); ?>'},
     beforeSend:function(){
       return confirm("Are you sure you want to delete this item?");
     },
     success: function(resultData) { 
       console.log(resultData);
       var obj = JSON.parse(resultData);
       if (obj.status == 'success') {
          $('#success_message').fadeIn().html(obj.msg);
          setTimeout(function() {
            $('#success_message').fadeOut("slow");
          }, 2000 );
          $("#row" + tutor_id).remove();
       }
     },
     error: function(errorData) {
      console.log(errorData);
      alert('Please refresh page and try again!');
    }
  });
}
</script>
<script>
  $('.toggle-class').on("change", function() {
    var status = $(this).prop('checked') == true ? 1 : 0; 
    var tutor_id = $(this).data('id'); 
    
    $.ajax({
      type: "GET",
      dataType: "json",
      url: "<?php echo url('/admin/change_tutor_status'); ?>",
      data: {'status': status, 'tutor_id': tutor_id},
      success: function(data){
        $('#success_message').fadeIn().html(data.success);
        setTimeout(function() {
          $('#success_message').fadeOut("slow");
        }, 2000 );
      },
      error: function(errorData) {
        console.log(errorData);
        alert('Please refresh page and try again!');
      }
    });
  })
</script>
@endsection

@section('content')
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Notification List
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Notifications</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <p style="display: none;" id="success_message" class="alert alert-success"></p>
      @if ($errors->any())
      <div class="alert alert-danger">
       <ul>
         @foreach ($errors->all() as $error)
         <li>{{ $error }}</li>
         @endforeach
       </ul>
     </div>
     @endif

      @if (Session::has('error_arr'))
          <?php $error_arr = Session::get('error_arr'); ?>
          <div class="alert alert-info">
            <ul>
              <?php
              for($i=0; $i < count($error_arr); $i++){
                if(!empty($error_arr[$i])){
                  ?><li>{{$error_arr[$i]}}</li><?php
                }
              }
              ?>
            </ul>
          </div>
          <?php Session::forget('error_arr'); ?>
      @endif

     @if(Session::has('message'))
     <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
     @endif

     @if(Session::has('error'))
     <p class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('error') }}</p>
     @endif
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
              
              <table id="tutor_list" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>SNo.</th>
                    <th>Student Name</th>
                    <th>Grade</th>
                    <th>Session Date</th>
                    <th>Session Time</th>
                    <th>Description</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody>
                  @if(!$notification_list->isEmpty())
                    <?php $i=1; ?>
                    @foreach($notification_list as $arr)
                      <tr>
                        <td>{{$i}}</td>
                        <td>{{$arr->first_name.' '.$arr->last_name}}</td>
                        <td>{{$arr->grade_name}}</td>
                        <td>{{(!empty($arr->lesson_date) ? date('d-m-Y',strtotime($arr->lesson_date)) : 'N/A')}}</td>
                        <td>{{(!empty($arr->time_from) ? date('H:i A',strtotime($arr->time_from)) : 'N/A')}} TO {{(!empty($arr->time_until) ? date('H:i A',strtotime($arr->time_until)) : 'N/A')}}</td>
                        <td>{{$arr->description}}</td>
                        <td>{{($arr->notification_status == 0 ? 'Unread By Student' : 'Read By Student')}}</td>
                      </tr>
                      <?php $i++; ?>
                    @endforeach
                  @endif
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection