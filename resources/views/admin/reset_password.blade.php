<!DOCTYPE html>

<html>

<head>

  <meta charset="utf-8">

  <meta http-equiv="X-UA-Compatible" content="IE=edge">

   <?php $logoImg = App\Helpers\Helper::getLogoImg();  ?>

  <link href="{{ url('/public/assets/img/logo vector.png') }}" rel="icon">
  <link href="{{ url('/public/assets/img/logo vector.png') }}" rel="apple-touch-icon">

    <title> <?php echo !empty($logoImg->title)? $logoImg->title:'4Twoo';?>| Forget Password</title>

  <!-- Tell the browser to be responsive to screen width -->

  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <!-- Bootstrap 3.3.7 -->

  <link rel="stylesheet" href="{{url('/')}}/resources/assets/bower_components/bootstrap/dist/css/bootstrap.min.css">

  <!-- Font Awesome -->

  <link rel="stylesheet" href="{{url('/')}}/resources/assets/bower_components/font-awesome/css/font-awesome.min.css">

  <!-- Ionicons -->

  <link rel="stylesheet" href="{{url('/')}}/resources/assets/bower_components/Ionicons/css/ionicons.min.css">

  <!-- Theme style -->

  <link rel="stylesheet" href="{{url('/')}}/resources/assets/dist/css/AdminLTE.min.css">

  <!-- iCheck -->

  <link rel="stylesheet" href="{{url('/')}}/resources/assets/plugins/iCheck/square/blue.css">



  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

  <!--[if lt IE 9]>

  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>

  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

  <![endif]-->



  <!-- Google Font -->

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <style ="text/css">

      .background_img {

   width: 100%;

   height: 100vh;

   display: inline-block;

   background: url("https://votivetech.in/4twoo/public/assets/img/client.png") no-repeat;
 }

.background_img:before {

   background: #00000085;

   position: absolute;

   content: "";

   width: 100%;

   height: 100%;

   top: 0px;

   z-index: 0;

 }

.background_img:before {

   background: #00000085;

   position: absolute;

   content: "";

   width: 100%;

   height: 100%;

   top: 0px;

   z-index: 0;

 }

 .login-box{

position: relative;

}
.reset-btn button{
    background-color: #b9222f;
    border-color: #b9222f;
    transition: 0.5s;
    color: #FFF;
}
.form-control:focus {
    border-color: #b7222f;
    box-shadow: none;
}
.msg{
margin-bottom: 10px;
}
  </style>


  

</head>
<body class="hold-transition login-page">

<div class="background_img">

    <div class="login-box">

      <div class="login-logo">

        <!-- height="80px;" -->

        @if(!empty($logo_info->homelogo_img))

        <img src="{{url('/')}}/public/uploads/logo/{{$logo_info->homelogo_img}}"  width="100px;" alt="User Image">



        @else

        <img src="{{url('/')}}/public/assets/img/logo vector.png"  width="100px;" alt="User Image">

        @endif

        

      </div>

      <!-- /.login-logo -->

      <div class="login-box-body">
            <div id="msg"></div>
            <form id="reset_password" method="POST">
              {{ csrf_field() }}
                <input type="hidden" name="admin_id" id="admin_id" value="{{ $user_id }}" />
                <div class="msg">Reset your password</div>
                <div class="form-group has-feedback">
                    <div class="form-line">
                        <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
                    </div>
                </div>
                <div class="form-group has-feedback">
                    <div class="form-line">
                        <input type="password" class="form-control" name="confirm_password" placeholder="Comfirm Password" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-8 p-t-5">

                    </div>
                    <div class="col-xs-12 reset-btn">
                        <button class="btn btn-block bg-pink waves-effect" type="submit">Reset</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
<input type="hidden" name="baseurl" id="baseurl" value="{{ url('/') }}" />
<input type="hidden" name="siteurl" id="siteurl" value="{{ url('admin') }}" />
<script src="{{url('/')}}/resources/assets/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap 3.3.7 -->

<script src="{{url('/')}}/resources/assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- iCheck -->

<script src="{{url('/')}}/resources/assets/plugins/iCheck/icheck.min.js"></script>

<!-- <script src="{{ url('public/adminassets/plugins/jquery/jquery.min.js')}}"></script> -->

<!-- Bootstrap Core Js -->
<!-- <script src="{{ url('public/adminassets/plugins/bootstrap/js/bootstrap.js')}}"></script> -->

<!-- Waves Effect Plugin Js -->
<!-- <script src="{{ url('public/adminassets/plugins/node-waves/waves.js')}}"></script> -->

<!-- Validation Plugin Js -->
<!-- <script src="{{ url('public/adminassets/plugins/jquery-validation/jquery.validate.js')}}"></script> -->

<!-- Custom Js -->
<!-- <script src="{{ url('public/adminassets/js/admin.js')}}"></script> -->


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>  
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/additional-methods.min.js"></script>

<!-- page scripts -->
<script src="{{ url('public/adminassets/js/form-validations.js') }}"></script>

<script type="text/javascript">
    $('#reset_password').validate({
  rules: {
    password: {
      required: true,
     
    },
    confirm_password:{
      required : true,
      equalTo : '#password',
    }
  },
  messages :{
    password : {
      required : 'Password is required',
    },
    confirm_password :{
      required : 'Confirm that the password is required',
      equalTo : 'Confirm that the password should match the password'
    }
  },
  submitHandler:function(form){
    // admin_reset_password();
    // return false;
    $(".btn").attr('disabled',true) ;
    $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
      
      $.ajax({
        url: "{{route('admin/reset-password-function')}}" ,
        type: "POST",
        data: $('#reset_password').serialize(),
        success: function( response ) {
            $('#std_btn').html('Submit');
            if(response.status==false){
              $('#msg').html('<p style="text-align:center;color:red">'+response.message+'</p>');
            }else{
              $('#msg').html('<p style="text-align:center;color:green">'+response.message+'</p>');
            }
          
            if(response.status==true){
              document.getElementById("reset_password").reset();
              setTimeout(function(){
              $('#msg').hide();
                window.location.href = site_url+'/admin';
              },5000);
          }
          
        }
      });
  }
});

</script>
</body>
</html>