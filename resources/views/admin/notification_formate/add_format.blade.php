@extends('admin.layout.layout')

@section('title', 'Add Formate')



@section('current_page_css')
  <style type="text/css">
    .error{
      color: red;
    }
  </style>
@endsection



@section('current_page_js')

<script src="{{url('/')}}/resources/assets/js/additional-methods.js"></script>
<script type="text/javascript">
$('#notificationFormateForm').validate({ 
    // initialize the plugin
    rules: {
      icon_file: {
        required: true,
        accept: "jpg,jpeg,png,ico,bmp"
      },
      message: {
        required: true
      },
      notification_type: {
        required: true
      },
      /*for_student: {
        required: true
      },*/
      language: {
        required: true
      }
    },
    submitHandler: function(form) {
      form.submit();
    }
  });

</script>

@endsection



@section('content')

 <!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">

    <!-- Content Header (Page header) -->

    <section class="content-header">

      <h1>Add Notification</h1>

      <ol class="breadcrumb">

        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

        <li class="active">Notification</li>

      </ol>

    </section>
    <!-- Main content -->

    <section class="content">

      @if ($message = Session::get('message'))

       <div class="alert alert-success alert-block">  

        <button type="button" class="close" data-dismiss="alert">×</button> 

        <strong>{{ $message }}</strong>

      </div>

      @endif
      @if ($message = Session::get('error'))

      <div class="alert alert-danger alert-block">

        <button type="button" class="close" data-dismiss="alert">×</button> 

        <strong>{{ $message }}</strong>

      </div>

      @endif

   @if ($message = Session::get('info'))

      <div class="alert alert-info alert-block">

        <button type="button" class="close" data-dismiss="alert">×</button> 

        <strong>{{ $message }}</strong>

      </div>

      @endif
      @if ($errors->any())

      <div class="alert alert-danger">

        <button type="button" class="close" data-dismiss="alert">×</button> 

        <ul>

         @foreach ($errors->all() as $error)

         <li>{{ $error }}</li>

         @endforeach

       </ul>

     </div>

     @endif

      <!-- Small boxes (Stat box) -->

      <div class="row">

        <div class="col-xs-12">

          <!-- SELECT2 EXAMPLE -->

        <div class="box box-default">

          <div class="box-header">

            <div class="box-tools pull-right">

                <a href="{{url('admin/formate_list')}}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Back </a>

            </div>

          </div>

          <!-- /.box-header -->

          <form action="{{url('/admin/submit_formate')}}" id="notificationFormateForm" method="POST" enctype="multipart/form-data">

            <input type="hidden" name="_token" id="csrf-token" value="{{csrf_token()}}" />

            <div class="box-body">

              <div class="row">

                <div class="col-md-6">
                  <div class="form-group">
                    <label>Icon</label>
                    <input type="file" class="form-control" name="icon_file" id="icon_file">
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label>Message</label>
                    <textarea class="form-control" name="message" id="message" placeholder="Message">{{ old('message') }}</textarea>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label>Notification Type</label>
                    <select class="form-control" id="notification_type" name="notification_type">
                      <option value="">Select Type</option>
                      <option {{ (old('notification_type') == 'StudentLessonBookingAndPayment' ? 'selected' : '') }}>StudentLessonBookingAndPayment</option>         
                      <option {{ (old('notification_type') == 'TutorLessonRequestConfirmation' ? 'selected' : '') }}>TutorLessonRequestConfirmation</option>
                      <option {{ (old('notification_type') == 'TutorLessonInvite' ? 'selected' : '') }}>TutorLessonInvite</option>
                      <option {{ (old('notification_type') == 'TutorLessonCancellation' ? 'selected' : '') }}>TutorLessonCancellation</option>
                      <option {{ (old('notification_type') == 'StudentLessonCancellation' ? 'selected' : '') }}>StudentLessonCancellation</option>
                      <option {{ (old('notification_type') == 'LessonReminder' ? 'selected' : '') }}>LessonReminder</option>
                      <option {{ (old('notification_type') == 'TutorStartLesson' ? 'selected' : '') }}>TutorStartLesson</option>
                      <option {{ (old('notification_type') == 'LessonCompletion' ? 'selected' : '') }}>LessonCompletion</option>
                      <option {{ (old('notification_type') == 'StudentLessonBookingAndPayment' ? 'selected' : '') }}>StudentLessonBookingAndPayment</option>
                      <option {{ (old('notification_type') == 'TutorLessonCancelation' ? 'selected' : '') }}>TutorLessonCancelation</option>
                      <option {{ (old('notification_type') == 'StudentLessonCancelation' ? 'selected' : '') }}>StudentLessonCancelation</option>
                      <option {{ (old('notification_type') == 'TutorReschedualSession' ? 'selected' : '') }}>TutorReschedualSession</option>
                      <option {{ (old('notification_type') == 'StudentSessionInvitiationPayment' ? 'selected' : '') }}>StudentSessionInvitiationPayment</option>
                      <option {{ (old('notification_type') == 'TutorLessonInvite' ? 'selected' : '') }}>TutorLessonInvite</option>
                      <option {{ (old('notification_type') == 'RecordedLesson' ? 'selected' : '') }}>RecordedLesson</option>
                      <option {{ (old('notification_type') == 'TutorLessonRequestConfirmation' ? 'selected' : '') }}>TutorLessonRequestConfirmation</option>
                    </select>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label>For Student</label>
                    <input type="checkbox" value="1" {{(old('for_student') == 1 ? 'checked' : '')}} name="for_student" id="for_student">
                    <br>
                    <label id="for_student-error" class="error" for="for_student" style=""></label>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label>Language</label>
                    <!-- <input type="text" value="{{ old('language') }}" class="form-control" name="language" id="language" placeholder="Language"> -->
                    <select class="form-control" name="language" id="language">
                      <option value="">Select Language</option>
					  <?php
					     if(!empty($language_list)){
							foreach($language_list as $language){?>
							 <option {{old('language') == 'English' ? 'selected' : ''}} value="{{ empty($language)? $language->id : '' }}">{{$language->name}}</option>
						<?php	}
						}
					  ?>
					 <!-- <option {{old('language') == 'English' ? 'selected' : ''}} value="English">English</option>
                      <option {{old('language') == 'العربية' ? 'selected' : ''}} value="العربية">العربية</option>-->
                    </select>
                  </div>
                </div>
              </div>

            </div>

            <div class="box-footer">

              <button type="submit" class="btn btn-primary">Submit</button>

            </div>

            <!-- /.row -->

          </form>

          </div>

          <!-- /.box-body -->

        </div>

        <!-- /.box -->

      </div>

      <!-- /.row -->



    </section>

    <!-- /.content -->

  </div>

  <!-- /.content-wrapper -->

@endsection