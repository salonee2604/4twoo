@extends('admin.layout.layout')
@section('title', 'Notification Formate List')


@section('current_page_css')
<!-- DataTables -->
<link rel="stylesheet" href="{{url('/')}}/resources/assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="{{url('/')}}/resources/assets/css/bootstrap-toggle.min.css">
@endsection
@section('current_page_js')
<!-- DataTables -->
<script src="{{url('/')}}/resources/assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="{{url('/')}}/resources/assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="{{url('/')}}/resources/assets/js/bootstrap-toggle.min.js"></script>

<script type="text/javascript">
$(function () {
    $('#grade_list').DataTable({
        'paging': true,
        'lengthChange': false,
        'searching': true,
        'ordering': true,
        'info': true,
        'autoWidth': false
    });
});
</script>

<script type="text/javascript">
    function delete_format(format_id) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'POST',
            url: "<?php echo url('/admin/delete_format'); ?>",
            enctype: 'multipart/form-data',
            data: {format_id: format_id, '_token': '<?php echo csrf_token(); ?>'},
            beforeSend: function () {
                return confirm("Are you sure you want to delete this format?");
            },
            success: function (resultData) {
                console.log(resultData);
                var obj = JSON.parse(resultData);
                if (obj.status == 'success') {
                    $('#success_message').fadeIn().html(obj.msg);
                    setTimeout(function () {
                        $('#success_message').fadeOut("slow");
                    }, 2000);
                    $("#row" + grade_id).remove();
                }
            },
            error: function (errorData) {
                console.log(errorData);
                alert('Please refresh page and try again!');
            }
        });
    }

</script>

<script>

    $('.toggle-class').on("change", function () {

        var status = $(this).prop('checked') == true ? 1 : 0;

        var format_id = $(this).data('id');
       $.ajax({

            type: "GET",

            dataType: "json",

            url: "<?php echo url('/admin/change_format_status'); ?>",

            data: {'status': status, 'format_id': format_id},

            success: function (data) {

                $('#success_message').fadeIn().html(data.success);

                setTimeout(function () {

                    $('#success_message').fadeOut("slow");

                }, 2000);

            },

            error: function (errorData) {

                console.log(errorData);

                alert('Please refresh page and try again!');

            }

        });

    })

</script>

@endsection
@section('content')
<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Notification Format List</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Notification</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <p style="display: none;" id="success_message" class="alert alert-success"></p>
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
               @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
               @endforeach
            </ul>
        </div>
        @endif
        @if(Session::has('message'))

        <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>

        @endif
        @if(Session::has('error'))
        <p class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('error') }}</p>
        @endif
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title pull-right">
                            <a href="{{url('/admin/add_format')}}" class="btn btn-primary">
                                Add Notification
                            </a>
                        </h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="grade_list" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>S.No.</th>
                                    <th>Icon</th>
                                    <th>Message</th>
                                    <th>Notification Type</th>
                                    <th>ForStudent</th>
                                    <th>Language</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                               @if(!$formate_list->isEmpty())

                                <?php $i = 1; ?>

                                @foreach($formate_list as $arr)

                                <tr id="row{{$arr->id}}">
                                    <td>{{$i}}</td>
                                    <td>
                                        @if(!empty($arr->notification_icon))
                                        <img style="width: 40px;" src="{{url('/').'/public/uploads/notification_icon/'.$arr->notification_icon}}">      
                                        @endif
                                    </td>
                                    <td>{{$arr->notification_msg}}</td>
                                    <td>{{$arr->notification_type}}</td>
                                    <td>
                                        <input data-id="{{$arr->id}}" class="toggle-class" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive" {{ $arr->status ? 'checked' : '' }}>
                                    </td>
                                    <td>
                                        <?php echo $language = App\Helpers\Helper::getLanguageByCode($arr->language); ?>
                                    </td>
                                    <td>
                                        <a href="{{url('/admin/edit_format')}}/{{base64_encode($arr->id)}}">Edit</a> |
                                        <a href="javascript:void(0)" onclick="delete_format('<?php echo $arr->id; ?>');">Delete</a>
                                    </td>
                                </tr>

                                <?php $i++; ?>

                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
           <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection