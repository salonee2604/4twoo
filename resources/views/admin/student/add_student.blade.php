@extends('admin.layout.layout')
@section('title', 'Add User')

@section('current_page_css')
@endsection
@section('content')
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Add User </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">User</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      @if ($message = Session::get('message'))
       <div class="alert alert-success alert-block">  
        <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong>{{ $message }}</strong>
      </div>
      @endif
    
     <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-xs-12">
          <!-- SELECT2 EXAMPLE -->
        <div class="box box-default">
          <div class="box-header">
             <div class="box-tools pull-right">
                <a href="{{url('admin/user_list')}}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Back </a>
            </div>
          </div>
          <!-- /.box-header -->
          <form action="{{url('/admin/submit_student')}}" id="studentForm" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="_token" id="csrf-token" value="{{csrf_token()}}" />
            <div class="box-body">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Name</label>
                    <input type="text" class="form-control" name="name" placeholder="Enter Full Name">
                    @if ($errors->has('name'))
                        <span class="help-block {{ $errors->has('name') ? ' has-error' : '' }}">
                          <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                  </div>
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Username</label>
                    <input type="text" class="form-control" name="username" placeholder="Enter Username">
                    @if ($errors->has('username'))
                        <span class="help-block {{ $errors->has('username') ? ' has-error' : '' }}">
                          <strong>{{ $errors->first('username') }}</strong>
                        </span>
                    @endif
                  </div>
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Email</label>
                    <input type="text" class="form-control" name="email" placeholder="Enter Email Address">
                    @if ($errors->has('email'))
                        <span class="help-block {{ $errors->has('email') ? ' has-error' : '' }}">
                          <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                  </div>
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Password</label>
                    <input type="password" class="form-control" name="password"  placeholder="Enter Password">
                      @if ($errors->has('password'))
                        <span class="help-block {{ $errors->has('password') ? ' has-error' : '' }}">
                          <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                  </div>
                     <div class="form-group">
                    <label> Confirm Password</label>
                    <input type="password" class="form-control" name="confirm_password"  placeholder="Enter Password">
                      @if ($errors->has('confirm_password'))
                        <span class="help-block {{ $errors->has('confirm_password') ? ' has-error' : '' }}">
                          <strong>{{ $errors->first('confirm_password') }}</strong>
                        </span>
                    @endif
                  </div>
        
                </div>
                <!-- /.col -->
              </div>
            </div>
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            <!-- /.row -->
          </form>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection

@section('current_page_js')
<script type="text/javascript">
 
</script>
@endsection
