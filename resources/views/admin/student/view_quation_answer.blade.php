@extends('admin.layout.layout')

@section('title', 'Edit User')



@section('current_page_css')

@endsection



@section('current_page_js')

@endsection



@section('content')
<style type="text/css">
  .question-brack fieldset {
    border-bottom: 1px solid #e1e5ea;
    box-shadow: 0px 0px 2px #ccc;
    padding: 11px;
}
</style>

 <!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">

    <!-- Content Header (Page header) -->

    <section class="content-header">

      <h1>

    View Quation Answer

      </h1>

      <ol class="breadcrumb">

        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

        <li class="active">View Quation Answer</li>

      </ol>

    </section>



    <!-- Main content -->

    <section class="content">

      @if ($message = Session::get('message'))

       <div class="alert alert-success alert-block">  

        <button type="button" class="close" data-dismiss="alert">×</button> 

        <strong>{{ $message }}</strong>

      </div>

      @endif



      @if ($message = Session::get('error'))

      <div class="alert alert-danger alert-block">

        <button type="button" class="close" data-dismiss="alert">×</button> 

        <strong>{{ $message }}</strong>

      </div>

      @endif



      <!-- Small boxes (Stat box) -->

      <div class="row">

        <div class="col-xs-12">

          <!-- SELECT2 EXAMPLE -->

        <div class="box box-default">

          <div class="box-header pull-right">

                <a href="{{url('admin/user_list')}}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Back </a>

         </div>

         <div class="row">
          <div class="col-md-12 question-brack">

          <form id="msform" method="POST" action="{{url('admin/update_quation_answer_admin')}}">
              @csrf
              <?php $toEnd = count($question_answer);  ?>
              <?php $i=1; 
                    $j=0; 
              ?>
               @foreach ($question_answer as $key =>  $question)
              <?php
                      $quation_answer =     (!empty($edit_question_answer[$j]->answer) ? $edit_question_answer[$j]->answer : '');
              ?>
                <fieldset id="hide<?php echo $i; ?>">
                   <input type="hidden" id="skipform" value="">
                  <input type="hidden" name="question_id[]" value="{{ $question->id }}">
                  <input type="hidden" name="id[]" value="{{ $quation_answer }}">
                  <?php

                   $segment = Request::segment(3);
                   $user_id = base64_decode($segment);
                  
                         
                  ?>
                  <input type="hidden" name="user_id" value="{{$user_id}}">
                 <div class="quesiotn-select">
                    <h3>{{$question->question}}</h3>
                </div>
                @if($question->type == 1)
                <div class="raion-fancy">
                  <?php
                    $quation_answer_arr = preg_replace('/\s+/', '', $quation_answer);
                    $quationarr = explode(",", $question->answer);
                    $answerarr  = explode(",", $quation_answer_arr);
                  ?>
                 <?php 
                 $k = 1;
                 foreach ($quationarr as $value) 
                 {

                    $spaceremovevalue              =   preg_replace('/\s+/', '', $value);
                    $answer                        =   in_array($spaceremovevalue ,$answerarr);
                    if(!empty($answer))
                    {
                        ?>
                         <div>
                          <input type="checkbox" id="control_01{{$key}}" name="answer{{$question->id}}[]" value="{{$value}}" checked>
                          <label for="control_01{{$key}}">
                              <i class="gender bx bxs-user"></i>
                            <p>{{$value}}</p>
                          </label>
                        </div>
                      <?php
                    }
                    else
                    {

                       ?>
                         <div>
                          <input type="checkbox" id="control_01{{$key}}" name="answer{{$question->id}}[]" value="{{$value}}">
                          <label for="control_01{{$key}}">
                              <i class="gender bx bxs-user"></i>
                            <p>{{$value}}</p>
                          </label>
                        </div>
                      <?php

                    }
              
                  
                 }
                ?>
                </div>
                <b> Its mandatory
                for me.</b> 
                  <?php

                  $mandatory_status  =   (!empty($edit_question_answer[$j]->mandatory_status) ? $edit_question_answer[$j]->mandatory_status : '');
                  ?>
                   <input type="checkbox" id="mandatory_status{{$question->id}}" name="mandatory_status{{$question->id}}[]" onclick="getmandatory_status({{$question->id}})" id="mandatory_status{{$question->id}}" value="<?php echo $mandatory_status; ?>" <?php 
                  if($mandatory_status== 1)
                      { echo "checked"; } ?> > 
                   
                  @elseif ($question->type == 2)
                  <div class="raion-fancy">
                  @if($quation_answer == 'yes')
                  <div>
                    <input type="radio" id="control_01{{$key}}" name="answer{{$question->id}}[]" value="yes" checked>
                    <label for="control_01{{$key}}">
                        <i class="gender bx bxs-user-check"></i>
                      <p>Yes</p>
                    </label>
                  </div>
                  @else
                    <div>
                    <input type="radio" id="control_01{{$key}}" name="answer{{$question->id}}[]" value="yes">
                    <label for="control_01{{$key}}">
                        <i class="gender bx bxs-user-check"></i>
                      <p>Yes</p>
                    </label>
                  </div>
                  
                  @endif
                   @if($quation_answer == 'no')
                  <div>
                    <input type="radio" id="control_02{{$key}}" name="answer{{$question->id}}[]" value="no" checked>
                    <label for="control_02{{$key}}">
                    <i class="gender bx bxs-user-x"></i>
                      <p>No</p>
                    </label>
                  </div>
                  @else
                  <div>
                    <input type="radio" id="control_02{{$key}}" name="answer{{$question->id}}[]" value="no">
                    <label for="control_02{{$key}}">
                    <i class="gender bx bxs-user-x"></i>
                      <p>No</p>
                    </label>
                  </div>
                  @endif
                </div>
                @elseif($question->type == 3)
                <input type="text" name="answer{{$question->id}}[]" placeholder="" value="{{$quation_answer}}"/>
                @endif
                
                <!-- <?php if($i > 1 ){?>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <?php }?>
                <?php if (0 === --$toEnd) { ?>   
                <input type="submit" name="submit" class="submit action-button mt-5" value="Submit" />
                   <?php } else { ?>
                 <!-- <input type="button" name="Skip" class="Skip action-button mt-5" onclick="skipdata(<?php echo $i; ?>); " value="Skip" /> -->
                <!-- <input type="button" name="next" class="next action-button mt-5" value="Next" /> -->
                <!-- <?php } ?> -->


              </fieldset>

              <?php $i++; $j++; $k++; ?>
              @endforeach
              <br>
               <input type="submit" name="submit" class="submit action-button mt-5 btn-primary" value="Submit" />
            </form>
          </div>

          <!-- /.box-body -->

        </div>

        <!-- /.box -->

      </div>

      <!-- /.row -->



    </section>

    <!-- /.content -->

  </div>

  <!-- /.content-wrapper -->

@endsection
@section('current_page_js')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script src="{{url('/')}}/resources/assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>

<script src="{{url('/')}}/resources/assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script src="{{url('/')}}/resources/assets/js/bootstrap-toggle.min.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
function getmandatory_status(value) 
{

    
  // Get the checkbox
  var checkBox = document.getElementById("mandatory_status"+value);
  
  // Get the output text
  var text = document.getElementById("text");

  // If the checkbox is checked, display the output text
  if (checkBox.checked == true){
    $("#mandatory_status"+value).val(1);
  } 
  else 
  {
    $("#mandatory_status"+value).val();
  }
}
</script>
<script type="text/javascript">


$(function () {

  $('#quation_answer').DataTable({

    'paging'      : true,

    'lengthChange': false,

    'searching'   : true,

    'ordering'    : true,

    'info'        : true,

    'autoWidth'   : false

  })

})



jQuery(function() {

    var table = jQuery('#quation_answer').DataTable();



    jQuery("#btnExport").click(function(e) {

        table.page.len(-1).draw();

        window.open('data:application/vnd.ms-excel,' +

            encodeURIComponent(jQuery('#quation_answer').parent().html()));

        setTimeout(function() {

            table.page.len(10).draw();

        }, 1000)



    });

});

</script>