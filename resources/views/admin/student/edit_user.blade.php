@extends('admin.layout.layout')

@section('title', 'Edit User')



@section('current_page_css')

@endsection



@section('current_page_js')

@endsection



@section('content')

 <!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">

    <!-- Content Header (Page header) -->

    <section class="content-header">

      <h1>

       Edit User

      </h1>

      <ol class="breadcrumb">

        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

        <li class="active">User</li>

      </ol>

    </section>



    <!-- Main content -->

    <section class="content">

      @if ($message = Session::get('message'))

       <div class="alert alert-success alert-block">  

        <button type="button" class="close" data-dismiss="alert">×</button> 

        <strong>{{ $message }}</strong>

      </div>

      @endif



      @if ($message = Session::get('error'))

      <div class="alert alert-danger alert-block">

        <button type="button" class="close" data-dismiss="alert">×</button> 

        <strong>{{ $message }}</strong>

      </div>

      @endif



      <!-- Small boxes (Stat box) -->

      <div class="row">

        <div class="col-xs-12">

          <!-- SELECT2 EXAMPLE -->

        <div class="box box-default">

          <div class="box-header pull-right">

                <a href="{{url('admin/user_list')}}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Back </a>

          </div>

          <!-- /.box-header -->

          <form action="{{url('/admin/update_user')}}" id="studentForm" method="POST" enctype="multipart/form-data">

            <input type="hidden" name="_token" id="csrf-token" value="{{csrf_token()}}" />

            <div class="box-body">

              <div class="row">

                <div class="col-md-6">

                  <div class="form-group">

                    <label>First Name</label>

                    <input type="text" class="form-control" name="name" value="{{(!empty($student_info->first_name) ? $student_info->first_name : '')}}" placeholder="Enter First Name">

                  </div>

                  <!-- /.form-group -->

                  <div class="form-group">

                    <label>Last Name</label>

                    <input type="text" class="form-control" name="username" value="{{(!empty($student_info->last_name) ? $student_info->last_name : '')}}" placeholder="Enter Username">

                  </div>



                  <!-- /.form-group -->

                  <div class="form-group">

                    <label>Phone</label>

                    <input type="text" class="form-control" name="contact_number" value="{{(!empty($student_info->contact_number) ? $student_info->contact_number : '')}}" placeholder="Enter Contact Number">

                  </div>

                  <div class="form-group">

                    <label>Password</label>

                    <input type="password" class="form-control" name="password" value="" placeholder="Enter password">

                  </div>



                  <!-- /.form-group --> 

                  <div class="form-group">

                    <label>Post Image</label>

                    <img src="{{url('/public/uploads/profile_img/'.$student_info->profile_pic)}}" style="width:100px;height:100px;" id="image_change">

                    <input name="image" type="file" accept="image/*" onchange="document.getElementById('image_change').src = window.URL.createObjectURL(this.files[0])">

                  </div>

                  

                  <!-- /.form-group -->



                  <input type="hidden" class="form-control" name="user_id" value="{{(!empty($student_info->id) ? $student_info->id : '')}}">

                  <!-- /.form-group -->

                </div>

                <!-- /.col -->

              </div>

            </div>

            <div class="box-footer">

              <button type="submit" class="btn btn-primary">Submit</button>

            </div>

            <!-- /.row -->

          </form>

          </div>

          <!-- /.box-body -->

        </div>

        <!-- /.box -->

      </div>

      <!-- /.row -->



    </section>

    <!-- /.content -->

  </div>

  <!-- /.content-wrapper -->

@endsection