@extends('admin.layout.layout')

@section('title', 'Student List')

@section('current_page_css')

<!-- DataTables -->

<link rel="stylesheet" href="{{url('/')}}/resources/assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<link rel="stylesheet" href="{{url('/')}}/resources/assets/css/bootstrap-toggle.min.css">

<style>

    .error{

        color: red;

    }

</style>

@endsection

@section('current_page_js')

<!-- DataTables -->

<script src="{{url('/')}}/resources/assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>

<script src="{{url('/')}}/resources/assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script src="{{url('/')}}/resources/assets/js/bootstrap-toggle.min.js"></script>

<script type="text/javascript">

  $(function () {

	   var student_list =  $('#student_list').DataTable({

		processing: true,

		serverSide: true,

		"order": [[ 0, "desc" ]],

		   "ajax":{

				"url": "{{url('/admin/getStudentList')}}",

				"dataType": "json",

				"type": "POST",

				"data":{ _token: "{{csrf_token()}}"}

			   },

			   "drawCallback": function (settings) { 

				var response = settings.json;

				$('.toggle-class').bootstrapToggle();

			},

		 columns: [

			{ "data": "id", name: 'id',searchable: false},

			{ "data": "full_name" , name: 'full_name',"orderable": false},

			{ "data": "username" , name: 'username',searchable: false,"orderable": false},

			{ "data": "email" , name: 'email' ,"orderable": false,searchable: false},

			{ "data": "grade", name: 'grade',"orderable": false,searchable: false},

			{ "data": "status" , name: 'status' ,"orderable": false,searchable: false},

			{ "data": "created_at" , name: 'created_at',"orderable": false },

			{ "data": "action" , name: 'action',"orderable": false,searchable: false},

		],

		'createdRow': function( row, data, dataIndex ) {

			$(row).attr('id',"row"+data.id);

			$('td:eq(3)', row).attr( 'id',"toemail_"+data.id);

		},

	});

  })

</script>

<script type="text/javascript">

 function delete_student(student_id){

  $.ajaxSetup({

    headers: {

      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

    }

  });

  $.ajax({

   type: 'POST',

   url: "<?php echo url('/admin/delete_student'); ?>",

   enctype: 'multipart/form-data',

   data:{student_id:student_id,'_token':'<?php echo csrf_token(); ?>'},

     beforeSend:function(){

       return confirm("Are you sure you want to delete this item?");

     },

     success: function(resultData) { 

       console.log(resultData);

       var obj = JSON.parse(resultData);

       if (obj.status == 'success') {

          $('#success_message').fadeIn().html(obj.msg);

          setTimeout(function() {

            $('#success_message').fadeOut("slow");

          }, 2000 );

          $("#row" + student_id).remove();

       }

     },

     error: function(errorData) {

      console.log(errorData);

      alert('Please refresh page and try again!');

    }

  });

}



    function sendemail(student_id){

	   $('#emailtosend').val($('#track_email_'+student_id).attr('data-email'));

	   $('#studentID').val(student_id);

	   $('#myModal').modal('show');

    }



$('#sendemailfrm').validate({ 

      rules: {

       email: {

        required: true,

        email:true

      },

      language: "required",

      

    },

    submitHandler: function(form) {

        $.ajax({

         type: "POST",

         dataType: "json",

         url: "<?php echo url('/admin/send-email'); ?>",

         data: $('#sendemailfrm').serialize(),

         success: function(data){

           $('#notify').html(data.message).css('color','green').fadeIn();

           setTimeout(function() {

             $('#notify').fadeOut("slow");

              $('#myModal').modal('hide');

           }, 2000 );

         },

         error: function(errorData) {

           console.log(errorData);

           

         }

       });

    }

  });

</script>

<script>

  $(document).on("change",'.toggle-class', function() {

    var status = $(this).prop('checked') == true ? 1 : 0; 

    var student_id = $(this).data('id'); 

    

    $.ajax({

      type: "GET",

      dataType: "json",

      url: "<?php echo url('/admin/change_student_status'); ?>",

      data: {'status': status, 'student_id': student_id},

      success: function(data){

        $('#success_message').fadeIn().html(data.success);

        setTimeout(function() {

          $('#success_message').fadeOut("slow");

        }, 2000 );

      },

      error: function(errorData) {

        console.log(errorData);

        alert('Please refresh page and try again!');

      }

    });

  })



</script>

@endsection

@section('content')

 <!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">

    <!-- Content Header (Page header) -->

    <section class="content-header">

      <h1>Student List</h1>

      <ol class="breadcrumb">

        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

        <li class="active">Student</li>

      </ol>

    </section>

    <!-- Main content -->

    <section class="content">

      <p style="display: none;" id="success_message" class="alert alert-success"></p>

      @if ($errors->any())

      <div class="alert alert-danger">

       <ul>

         @foreach ($errors->all() as $error)

         <li>{{ $error }}</li>

         @endforeach

       </ul>

     </div>

     @endif

      @if (Session::has('error_arr'))

          <?php $error_arr = Session::get('error_arr'); ?>

          <div class="alert alert-info">

            <ul>

              <?php

              for($i=0; $i < count($error_arr); $i++){

                if(!empty($error_arr[$i])){

                  ?><li>{{$error_arr[$i]}}</li><?php

                }

              }

              ?>

            </ul>

          </div>

          <?php Session::forget('error_arr'); ?>

      @endif



     @if(Session::has('message'))

     <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>

     @endif



     @if(Session::has('error'))

     <p class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('error') }}</p>

     @endif

      <div class="row">

        <div class="col-xs-12">

          <div class="box">

            <div class="box-header">

			 <div class ="row">

			   <div class="col-md-6">

			     <form action="{{ url('admin/StudentImportData') }}" class="form-horizontal" method="POST" enctype="multipart/form-data">

                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

					<div class="col-md-5">

					  <div class="form-group">

						<input type="file" name="import_file" />

					  </div>

					</div>

                    <button class="btn btn-primary">Submit</button>

                  </form>

			   </div>

			    <div class="col-md-6">

				   <h3 class="box-title pull-right btn-toolbar"><a href="{{url('/admin/add_student')}}" class="btn btn-primary">Add Student</a></h3>

                   <h3 class="box-title pull-right"><a class="btn btn-primary" href="{{ url('/admin/StudentExportData') }}">Export Student Data</a></h3>

			    </div>

			 </div>

	        </div>

            <!-- /.box-header -->

            <div class="box-body">

              <table id="student_list" class="table table-bordered table-striped">

                <thead>

                  <tr>

                    <th>S.No.</th>

                    <th>Fullname</th>

                    <th>Username</th>

                    <th>Email</th>

                    <th>Grade</th>

                    <th>Status</th>

                    <th>Create Date</th>

                    <th>Action</th>

                  </tr>

                </thead>

                <tbody>

				  <?php /* ?>

				

                  @if(!$student_list->isEmpty())

                  <?php $i=1; ?>

                  @foreach($student_list as $arr)

                  <tr id="row{{$arr->id}}">

                    <td>{{$i}}</td>

                    <td>{{$arr->first_name}} {{$arr->last_name}}</td>

                    <td>{{$arr->username}}</td>

                    <td id="toemail_<?php echo $arr->id; ?>">{{$arr->email}}</td>

                    <td>{{$arr->grade}}</td>

                    <td>

                      <input data-id="{{$arr->id}}" class="toggle-class" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive" {{ $arr->status ? 'checked' : '' }}>

                    </td>

                    <td>{{(!empty($arr->created_at) ? date('d-m-Y H:i A',strtotime($arr->created_at)) : 'N/A')}}</td>

                    <td>

                      <a href="{{url('/admin/change_student_password')}}/{{base64_encode($arr->id)}}">Update Password</a> |



                      <a href="{{url('/admin/edit_student')}}/{{base64_encode($arr->id)}}">Edit</a> |



                      <a href="javascript:void(0)" onclick="delete_student('<?php echo $arr->id; ?>');">Delete</a>|

                      <a href="javascript:void(0)" onclick="sendemail('<?php echo $arr->id; ?>');">Send Email</a>

                      </td>

                  </tr>

                  <?php $i++; ?>

                  @endforeach

                  @endif

				  <?php */ ?>

				  

                </tbody>

              </table>

            </div>

            <!-- /.box-body -->

          </div>

          <!-- /.box -->

        </div>

        </div>

        <!-- /.col -->

      <!-- /.row -->

    </section>

    <!-- /.content -->

  </div>

  <!-- /.content-wrapper -->

  <!-- Modal -->

<div id="myModal" class="modal fade" role="dialog">

  <div class="modal-dialog">

    <!-- Modal content-->

    <form id="sendemailfrm" name="sendemailfrm" method="post" role="form">

           <input type="hidden" name="_token" value="{{csrf_token()}}">

           <input type="hidden" id="studentID" name="student_id" value="">

    <div class="modal-content">

      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal">&times;</button>

        <h4 class="modal-title">Send Email</h4>

      </div>

      <div class="modal-body">

                     <div class="controls">

                 <div class="row">

                      <div class="col-md-6">

                          <div class="form-group">

                              <label for="form_email">Email *</label>

                              <input id="emailtosend" type="email" name="email" class="form-control" placeholder="Please enter your email *" required="required" data-error="Valid email is required.">

                              <div class="help-block with-errors"></div>

                          </div>

                      </div>

                     <div class="col-md-6"></div>

                  </div>

                  <div class="row">

                      <div class="col-md-12">

                          <div class="form-group">

                              <label for="form_message">Language *</label>

                              <select class="form-control" name="language" id="language">

                                  <option value="">Select Language</option>

                                  <?php

                                  if (!empty($language_list)) {

                                      foreach ($language_list as $language) { ?>

                                          <option value="<?php echo $language->code; ?>" <?php if (!empty($format_info->language) && $format_info->language == $language->code) { ?> selected <?php } ?>>{{$language->name}}</option>

                                          <?php

                                      }

                                  }

                                  ?>

                              </select>

                              <div class="help-block with-errors"></div>

                          </div>

                      </div>

                      </div>

                  </div>

                <span id="notify"></span>

              </div>

          

      <div class="modal-footer">

             <input type="submit" class="btn btn-success btn-send" value="Send">

             <button type="button" class="btn btn-small btn-default" data-dismiss="modal">Close</button>

      </div></div>

       </form>

    </div>

 </div>

@endsection