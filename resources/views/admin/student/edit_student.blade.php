@extends('admin.layout.layout')
@section('title', 'Edit Student')

@section('current_page_css')
@endsection

@section('current_page_js')
@endsection

@section('content')
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Edit Student
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Student</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      @if ($message = Session::get('message'))
       <div class="alert alert-success alert-block">  
        <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong>{{ $message }}</strong>
      </div>
      @endif

      @if ($message = Session::get('error'))
      <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong>{{ $message }}</strong>
      </div>
      @endif

      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-xs-12">
          <!-- SELECT2 EXAMPLE -->
        <div class="box box-default">
          <div class="box-header pull-right">
                <a href="{{url('admin/student_list')}}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Back </a>
          </div>
          <!-- /.box-header -->
          <form action="{{url('/admin/update_student')}}" id="studentForm" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="_token" id="csrf-token" value="{{csrf_token()}}" />
            <div class="box-body">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Name</label>
                    <input type="text" class="form-control" name="name" value="{{(!empty($student_info->first_name) ? $student_info->first_name : '')}} {{(!empty($student_info->last_name) ? $student_info->last_name : '')}}" placeholder="Enter Full Name">
                  </div>
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Username</label>
                    <input type="text" class="form-control" name="username" value="{{(!empty($student_info->username) ? $student_info->username : '')}}" placeholder="Enter Username">
                  </div>

                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Phone</label>
                    <input type="text" class="form-control" name="contact_number" value="{{(!empty($student_info->contact_number) ? $student_info->contact_number : '')}}" placeholder="Enter Contact Number">
                  </div>
                  <div class="form-group">
                    <label>Password</label>
                    <input type="password" class="form-control" name="password" value="{{(!empty($student_info->password) ? $student_info->password : '')}}" placeholder="Enter password">
                  </div>
                  <!-- /.form-group -->

                  <input type="hidden" class="form-control" name="student_id" value="{{(!empty($student_info->id) ? $student_info->id : '')}}">
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
              </div>
            </div>
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            <!-- /.row -->
          </form>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection