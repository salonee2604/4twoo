@extends('admin.layout.layout')

@section('title', 'Add Grade')
@section('current_page_css')
@endsection
@section('current_page_js')
@endsection
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Add Media</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Media</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        @if ($message = Session::get('message'))
        <div class="alert alert-success alert-block">  
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
        </div>
        @endif
        @if ($message = Session::get('error'))
        <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
        </div>
        @endif
           <div class="row">
            <div class="col-xs-12">
               <!-- SELECT2 EXAMPLE -->
                <div class="box box-default">
                    <div class="box-header">
                        <div class="box-tools pull-right">
                            <a href="{{url('admin/manage-media-list')}}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Back </a>
                        </div>
                    </div>
                   <!-- /.box-header -->

                    <form action="{{url('/admin/save-manage-media')}}" id="studentForm" method="POST" enctype="multipart/form-data">
                        <input type="hidden" name="_token" id="csrf-token" value="{{csrf_token()}}" />
                        <input type="hidden" name="media_id" value="<?php echo !empty($manage_media->id)? $manage_media->id :'';?>" />
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Title</label>
                                        <input type="text" class="form-control" name="title" value="<?php echo !empty($manage_media->title)? $manage_media->title :'';?>" placeholder="Enter Title">
                                        <span class="error">{{ $errors->first('title') }}</span>
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon">Icon:</span>
                                            <input type="file" name="files" id="icon" class="form-control">
                                            <span class="error">{{ $errors->first('files') }}</span>
                                        </div>
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                        <!-- /.row -->
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection