@extends('admin.layout.layout')

@section('title', 'Add Formate')
@section('current_page_css')
<style type="text/css">
    .error{
        color: red;
    }
</style>
@endsection
@section('current_page_js')

<script src="{{url('/')}}/resources/assets/js/additional-methods.js"></script>
<script type="text/javascript">
$('#notificationFormateForm').validate({
    // initialize the plugin
    rules: {
        icon_file: {
            required: true,
            accept: "jpg,jpeg,png,ico,bmp"
        },
        message: {
            required: true
        },
        email_type: {
            required: true
        },
        /*for_student: {
         required: true
         },*/
        language: {
            required: true
        }
    },
    submitHandler: function (form) {
        form.submit();
    }
});

</script>

@endsection
@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1>Add Email</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Email</li>
        </ol>
    </section>
    <section class="content">
        @if ($message = Session::get('message'))
        <div class="alert alert-success alert-block">  
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
        </div>
        @endif
        @if ($message = Session::get('error'))
        <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
        </div>
        @endif

        @if ($message = Session::get('info'))
        <div class="alert alert-info alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
        </div>
        @endif
        @if ($errors->any())
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-xs-12">
                <!-- SELECT2 EXAMPLE -->
                <div class="box box-default">
                    <div class="box-header">
                        <div class="box-tools pull-right">
                            <a href="{{url('admin/email-system-list')}}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Back </a>
                        </div>
                    </div>
                    <!-- /.box-header -->

                    <form action="{{url('/admin/savemailformate')}}" id="savemailformate" method="POST" enctype="multipart/form-data">
                        <input type="hidden" name="_token" id="csrf-token" value="{{csrf_token()}}" />
                        <input type="hidden" name="fomate_id" id="csrf-token" value="<?php echo !empty($emailformate->id)? $emailformate->id :'';?>" />
                        <div class="box-body">
                                <div class="row">  
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Email Type</label>
                                        <select class="form-control" id="email_type" name="email_type">
                                            <option value="">Select Type</option>
                                            <option value="RegistrationConfirmEmail" <?php if(!empty($emailformate->email_type) && $emailformate->email_type=='RegistrationConfirmEmail') { ?> selected <?php } ?>>RegistrationConfirmEmail</option>         
                                            <option value="ChatMessageByStudent" <?php if(!empty($emailformate->email_type) && $emailformate->email_type=='ChatMessageByStudent') { ?> selected <?php } ?>>ChatMessageByStudent</option>
                                            <option value="ChatMessageFromTutor" <?php if(!empty($emailformate->email_type) && $emailformate->email_type=='ChatMessageFromTutor') { ?> selected <?php } ?>>ChatMessageFromTutor</option>
                                            <option value="ChatMessageFromTutortoclass" <?php if(!empty($emailformate->email_type) && $emailformate->email_type=='ChatMessageFromTutortoclass') { ?> selected <?php } ?>>ChatMessageFromTutortoclass</option>
                                            <option value="SessionInvitationByTutorToStudent" <?php if(!empty($emailformate->email_type) && $emailformate->email_type=='SessionInvitationByTutorToStudent') { ?> selected <?php } ?>>SessionInvitationByTutorToStudent</option>
                                            <option value="SessionInvitationByTutorToclass" <?php if(!empty($emailformate->email_type) && $emailformate->email_type=='SessionInvitationByTutorToclass') { ?> selected <?php } ?>>SessionInvitationByTutorToclass</option>
                                            <option value="SessionReminderToStudent" <?php if(!empty($emailformate->email_type) && $emailformate->email_type=='SessionReminderToStudent') { ?> selected <?php } ?>>SessionReminderToStudent</option>
                                            <option value="SessionReminderToTutor" <?php if(!empty($emailformate->email_type) && $emailformate->email_type=='SessionReminderToTutor') { ?> selected <?php } ?>>SessionReminderToTutor</option>
                                            <option value="SessionStartedByTutorToStudent" <?php if(!empty($emailformate->email_type) && $emailformate->email_type=='SessionStartedByTutorToStudent') { ?> selected <?php } ?>>SessionStartedByTutorToStudent</option>
                                            <option value="SessionStartedByTutorToClass" <?php if(!empty($emailformate->email_type) && $emailformate->email_type=='SessionStartedByTutorToClass') { ?> selected <?php } ?>>SessionStartedByTutorToClass</option>
                                            <option value="StudentLessonCancellation" <?php if(!empty($emailformate->email_type) && $emailformate->email_type=='StudentLessonCancellation') { ?> selected <?php } ?>>StudentLessonCancellation</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Subject</label>
                                        <input type="text" name="subject" id="subject" value="<?php echo !empty($emailformate->subject)? $emailformate->subject :'';?>" class="form-control" placeholder="subject">
                                    </div>
                                </div>
                            </div>
                            <div class="row">  
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Body</label>
                                        <textarea class="form-control" name="body" id="body" placeholder="body" <textarea class="form-control" name="body" id="body" placeholder="body" style="margin: 0px -102.5px 0px 0px; width: 623px; height: 175px;"><?php echo !empty($emailformate->body)? $emailformate->body :'';?></textarea>
                                    </div>
                                </div>
                            </div>    
                          <div class="row">  
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Language</label>
                                        <select class="form-control" name="language" id="language">
                                            <option value="">Select Language</option>
                                            <?php
                                            if (!empty($language_list)) {
                                                foreach ($language_list as $language) { ?>
                                                    <option value="<?php echo $language->code ?>" <?php if(!empty($emailformate->email_type) && $emailformate->language == $language->code) { ?> selected <?php } ?>>{{$language->name}}</option>
                                                <?php
                                                }
                                            }
                                            ?>
                                 </select>
                                    </div>
                                </div>

                            </div>         
                        </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                </form>
            </div>
        </div>
</div>
</section>
</div>
@endsection