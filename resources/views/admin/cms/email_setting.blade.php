@extends('admin.layout.layout')

@section('title', 'Add Formate')
@section('current_page_css')
<style type="text/css">
    .error{
        color: red;
    }
</style>
@endsection
@section('current_page_js')

<script src="{{url('/')}}/resources/assets/js/additional-methods.js"></script>
<script type="text/javascript">
$('#notificationFormateForm').validate({
    // initialize the plugin
    rules: {
        icon_file: {
            required: true,
            accept: "jpg,jpeg,png,ico,bmp"
        },
        message: {
            required: true
        },
        email_type: {
            required: true
        },
        /*for_student: {
         required: true
         },*/
        language: {
            required: true
        }
    },
    submitHandler: function (form) {
        form.submit();
    }
});

</script>

@endsection
@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1>Email Settings</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Email Setting</li>
        </ol>
    </section>
    <section class="content">
        @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">  
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
        </div>
        @endif
        @if ($message = Session::get('error'))
        <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
        </div>
        @endif
       <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-xs-12">
                <!-- SELECT2 EXAMPLE -->
                <div class="box box-default">
                    <div class="box-header">
                        <div class="box-tools pull-right">
                          
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <form action="{{url('/admin/save-email-setting')}}" id="savemailformate" method="POST" enctype="multipart/form-data">
                        <input type="hidden" name="_token" id="csrf-token" value="{{csrf_token()}}" />
                        <input type="hidden" name="fomate_id" id="csrf-token" value="<?php echo !empty($emailsetting->id)? $emailsetting->id :'';?>" />
                        <div class="box-body">
                                <div class="row">  
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Email Name</label>
                                        <input type="text" name="email_name" id="email_name" value="<?php echo !empty($emailsetting->email_name)? $emailsetting->email_name :'';?>" class="form-control" placeholder="email name">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Email User</label>
                                        <input type="text" name="email_user" id="email_user" value="<?php echo !empty($emailsetting->email_user)? $emailsetting->email_user :'';?>" class="form-control" placeholder="email user">
                                    </div>
                                </div>
                            </div>
                            <div class="row">  
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Email Password</label>
                                           <input type="text" name="email_password" id="email_password" value="<?php echo !empty($emailsetting->email_password)? $emailsetting->email_password :'';?>" class="form-control" placeholder="email password">
                                    </div>
                                </div>
                            </div> 
                             <div class="row">  
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Email Port</label>
                                          <input type="text" name="email_port" id="email_port" value="<?php echo !empty($emailsetting->email_port)? $emailsetting->email_port :'';?>" class="form-control" placeholder="email port">
                                    </div>
                                </div>
                            </div>
                             <div class="row">  
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Email SMTP</label>
                                          <input type="text" name="email_smtp" id="email_smtp" value="<?php echo !empty($emailsetting->email_smtp)? $emailsetting->email_smtp :'';?>" class="form-control" placeholder="email smtp">
                                    </div>
                                </div>
                            </div>
                             <div class="row">  
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>IsEmailSSL</label>
                                        <input type="checkbox"  name="is_email_ssl" id="is_email_ssl" value="1" <?php if(!empty($emailsetting->is_email_ssl)){?> checked="checked" <?php }?> class="form-check-input" >
                                           
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                </form>
            </div>
        </div>
</div>
</section>
</div>
@endsection