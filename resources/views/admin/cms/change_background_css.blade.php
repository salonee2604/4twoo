@extends('admin.layout.layout')
@section('title', 'Update Video Link')

@section('current_page_css')
  <style type="text/css">
    .error{
      color: red;
    }
  </style>
@endsection

@section('current_page_js')
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.js"></script>
<script type="text/javascript">
  $('#changeBackgroundkForm').validate({ 
    // initialize the plugin
    rules: {
        color_code: {
          required: true
        }
    },
    submitHandler: function(form) {
      form.submit();
    }
  });
</script>
@endsection

@section('content')
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Change Background Admin Panel</h1>
    </section>

    <!-- Main content -->
    <section class="content">
      @if ($message = Session::get('message'))
       <div class="alert alert-success alert-block">  
        <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong>{{ $message }}</strong>
      </div>
      @endif
   
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-xs-12">
          <!-- SELECT2 EXAMPLE -->
        <div class="box box-default">
          <div class="box-header with-border">
            <h3 class="box-title">Change Background Admin Panel</h3>
            <div class="box-tools pull-right">
            </div>
          </div>
        
          <!-- /.box-header -->
          @if(Session::has('success'))
          <p class="alert alert-success">{{ Session::get('success') }}</p>
          @endif

          @if(Session::has('error'))
          <p class="alert alert-danger">{{ Session::get('error') }}</p>
          @endif
            <div class="box-body">
              <div class="row">
                <div class="col-md-6">
				  <form action="{{url('/admin/save-change-background')}}" id="changeBackgroundkForm" name="changeBackgroundkForm" method="POST" enctype="multipart/form-data">
                  <input type="hidden" name="_token" id="csrf-token" value="{{csrf_token()}}" />
                  <div class="form-group">
                    <label>Color Code </label>
                    <input type="text" required value="{{(!empty($colorCodeCss->color_code) ? $colorCodeCss->color_code : '')}}" class="form-control" name="color_code" placeholder="Color Code">
                     @if ($errors->has('color_code'))
                        <span class="help-block {{ $errors->has('color_code') ? ' has-error' : '' }}">
                          <strong>{{ $errors->first('color_code') }}</strong>
                        </span>
                    @endif
                  </div>
              <div class="box-footer">
              <button type="submit" name="btnUpdate" class="btn btn-primary">Update</button>
            </div>
            <!-- /.row -->
          </form>
                </div>
	          </div>
            </div>
          </div>
         <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection