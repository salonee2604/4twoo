@extends('admin.layout.layout')
@section('title', 'Update Video Link')

@section('current_page_css')
  <style type="text/css">
    .error{
      color: red;
    }
  </style>
@endsection

@section('current_page_js')
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.js"></script>
<script type="text/javascript">
  $('#linkForm').validate({ 
    // initialize the plugin
    rules: {
        video_link: {
          required: true
        }
    },
    submitHandler: function(form) {
      form.submit();
    }
  });
</script>
@endsection

@section('content')
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Video Link
        <small>Update Video Link</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Tutors</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      @if ($message = Session::get('message'))
       <div class="alert alert-success alert-block">  
        <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong>{{ $message }}</strong>
      </div>
      @endif
   
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-xs-12">
          <!-- SELECT2 EXAMPLE -->
        <div class="box box-default">
          <div class="box-header with-border">
            <h3 class="box-title">Update Video Link</h3>
            <div class="box-tools pull-right">
              <!--<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>-->
            </div>
          </div>
          
          <!-- /.box-header -->
          @if(Session::has('success'))
          <p class="alert alert-success">{{ Session::get('success') }}</p>
          @endif

          @if(Session::has('error'))
          <p class="alert alert-danger">{{ Session::get('error') }}</p>
          @endif

        
            <div class="box-body">
              
              <div class="row">
                <div class="col-md-6">
				 <label>Video Link Tutors</label>
				<div class="form-group">
                  @if(!empty($video_info->video_link))
                    <iframe src="{{$video_info->video_link}}" frameborder="0" allowfullscreen></iframe>
                  @endif
                </div>
                </div>
				 <div class="col-md-6">
				     <label>Video Link Student</label>
					 <div class="form-group">
					  @if(!empty($video_info->video_link_student))
						<iframe src="{{$video_info->video_link_student}}" frameborder="0" allowfullscreen></iframe>
					  @endif
					</div>
                </div>
              </div>
              
              <div class="row">
                <div class="col-md-6">
				  <form action="{{url('/admin/update_video')}}" id="linkForm" method="POST" enctype="multipart/form-data">
                  <input type="hidden" name="_token" id="csrf-token" value="{{csrf_token()}}" />
                  <div class="form-group">
                    <label>Video Link </label>
                    <input type="text" required value="{{(!empty($video_info->video_link) ? $video_info->video_link : '')}}" class="form-control" name="video_link" placeholder="Video Link">
                     @if ($errors->has('video_link'))
                        <span class="help-block {{ $errors->has('video_link') ? ' has-error' : '' }}">
                          <strong>{{ $errors->first('video_link') }}</strong>
                        </span>
                    @endif
                  </div>
                              <div class="box-footer">
              <button type="submit" name="btnUpdate" class="btn btn-primary">Update</button>
            </div>
            <!-- /.row -->
          </form>
                </div>
				 <div class="col-md-6">
                  <form action="{{url('/admin/update_video_student')}}" id="linkForm" method="POST" enctype="multipart/form-data">
						  <input type="hidden" name="_token" id="csrf-token" value="{{csrf_token()}}" />
						  <div class="form-group">
							<label>Video Link </label>
							<input type="text" required value="{{(!empty($video_info->video_link_student) ? $video_info->video_link_student : '')}}" class="form-control" name="video_link_student" placeholder="Video Link">
							 @if ($errors->has('video_link'))
								<span class="help-block {{ $errors->has('video_link') ? ' has-error' : '' }}">
								  <strong>{{ $errors->first('video_link') }}</strong>
								</span>
							@endif
						  </div>
					<div class="box-footer">
					  <button type="submit" name="btnUpdate" class="btn btn-primary">Update</button>
					</div>
					<!-- /.row -->
				  </form>
                </div>
                <!-- /.col -->
              </div>

            </div>

          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection