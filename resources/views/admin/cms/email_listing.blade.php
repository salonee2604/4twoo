@extends('admin.layout.layout')
@section('title', 'Notification Formate List')
@section('current_page_css')
<!-- DataTables -->
<link rel="stylesheet" href="{{url('/')}}/resources/assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="{{url('/')}}/resources/assets/css/bootstrap-toggle.min.css">
@endsection
@section('current_page_js')
<!-- DataTables -->
<script src="{{url('/')}}/resources/assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="{{url('/')}}/resources/assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="{{url('/')}}/resources/assets/js/bootstrap-toggle.min.js"></script>

<script type="text/javascript">
$(function () {
    $('#grade_list').DataTable({
        'paging': true,
        'lengthChange': false,
        'searching': true,
        'ordering': true,
        'info': true,
        'autoWidth': false
    });
});
</script>
<script type="text/javascript">
    function delete_format(format_id) {
       if(confirm("Are you sure you want to delete this?")){
           window.location="<?php echo url('/admin/delete_format'); ?>/"+format_id;
       }
    }
</script>

@endsection
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Email Format List</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Email</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
       <p style="display: none;" id="success_message" class="alert alert-success"></p>
        @if ($errors->any())
       <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @if(Session::has('message'))
        <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
        @endif
        @if(Session::has('error'))
        <p class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('error') }}</p>
        @endif
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title pull-right">
                            <a href="{{url('/admin/addemailformate')}}" class="btn btn-primary">
                                Add Email
                            </a>
                        </h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="grade_list" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>S.No.</th>
                                    <th>Email Type</th>
                                    <th>Subject</th>
                                    <th>Email Body</th>
                                    <th>Language</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(!$formate_list->isEmpty())
                                <?php $i = 1; ?>
                                @foreach($formate_list as $arr)
                                <tr id="row{{$arr->id}}">
                                    <td>{{$i}}</td>
                                    <td>{{$arr->email_type}}</td>
                                    <td>{{$arr->subject}}</td>
                                    <td>{{$arr->body}}</td>
                                    <td>
                                        <?php echo $language = App\Helpers\Helper::getLanguageByCode($arr->language); ?>
                                    </td>
                                    <td>
                                        <a href="{{url('/admin/editemailformate')}}/{{base64_encode($arr->id)}}">Edit</a> |
                                        <a href="javascript:void(0)" onclick="delete_format('<?php echo $arr->id; ?>');">Delete</a>
                                    </td>
                                </tr>
                                <?php $i++; ?>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
           <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection