@extends('admin.layout.layout')
@section('title', 'Update Logo')

@section('current_page_css')
  <style type="text/css">
    .error{
      color: red;
    }
  </style>
@endsection

@section('current_page_js')
<script src="{{url('/resources/assets/js/')}}additional-methods.js"></script>
<script type="text/javascript">
 /*  $('#logoForm').validate({ 
    // initialize the plugin
    rules: {
        logo_file: {
          required: true,
          extension: "jpg|jpeg|png"
        }
    },
    submitHandler: function(form) {
      form.submit();
    }
  });
   */
      function readURL(input,ID) {
		  
		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function (e) {
				$('#'+ID).attr('src', e.target.result);
			};

			reader.readAsDataURL(input.files[0]);
		}
	}
</script>
@endsection

@section('content')
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Setting</h1>
    </section>
    <!-- Main content -->
    <section class="content">
      @if ($message = Session::get('message'))
       <div class="alert alert-success alert-block">  
        <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong>{{ $message }}</strong>
      </div>
      @endif
   
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-xs-12">
          <!-- SELECT2 EXAMPLE -->
        <div class="box box-default">
          <div class="box-header with-border">
            <h3 class="box-title">Update Setting</h3>
            <div class="box-tools pull-right">
            </div>
          </div>
          
          <!-- /.box-header -->
          @if(Session::has('success'))
          <p class="alert alert-success">{{ Session::get('success') }}</p>
          @endif

          @if(Session::has('error'))
          <p class="alert alert-danger">{{ Session::get('error') }}</p>
          @endif
          <form action="{{url('/admin/edit_logo')}}" id="logoForm" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="_token" id="csrf-token" value="{{csrf_token()}}" />
            <input type="hidden" name="id" value="{{$logo_info->id}}" />
            <div class="box-body">
              <div class="row">
               
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Name</label>
                    <input type="text" required class="form-control" name="title" value="<?php echo !empty($logo_info->title)? $logo_info->title : '';?>">
                     @if ($errors->has('title'))
                        <span class="help-block {{ $errors->has('title') ? ' has-error' : '' }}">
                          <strong>{{ $errors->first('title') }}</strong>
                        </span>
                    @endif
                  </div>
                </div>
                <!-- /.col -->
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Logo</label>
                     <input type="file" name="logo_file" onchange="readURL(this,'logoID');" class="form-control" >
                     @if ($errors->has('logo_file'))
                        <span class="help-block {{ $errors->has('logo_file') ? ' has-error' : '' }}">
                          <strong>{{ $errors->first('logo_file') }}</strong>
                        </span>
                    @endif
                  </div>
                </div>
				 <div class="col-md-6">
                  @if(!empty($logo_info->logo_img))
                  <img src="{{url('/').'/public/uploads/logo/'.$logo_info->logo_img}}" id="logoID" width="100px;" height="100px;" style="border: 1px solid; margin-top: 10px;">
                @else
					 <img src="{{url('/').'/resources/assets/images/no_img.jpg'}}" id="logoID" width="100px;" height="100px;" style="border: 1px solid; margin-top: 10px;">  
				  @endif
                </div>
                <!-- /.col -->
              </div>
			  <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>favicon</label>
                    <input type="file" name="favicon_file" onchange="readURL(this,'faviconID');"  class="form-control" >
                     @if ($errors->has('favicon_file'))
                        <span class="help-block {{ $errors->has('favicon_file') ? ' has-error' : '' }}">
                          <strong>{{ $errors->first('favicon_file') }}</strong>
                        </span>
                    @endif
                  </div>
                </div>
				 <div class="col-md-6">
                  @if(!empty($logo_info->favicon))
                  <img src="{{url('/').'/'.$logo_info->favicon}}" id="faviconID" width="100px;" height="100px;" style="border: 1px solid; margin-top: 10px;">
               
				   @else
					 <img src="{{url('/').'/resources/assets/images/no_img.jpg'}}" id="faviconID" width="100px;" height="100px;" style="border: 1px solid; margin-top: 10px;">  
				  @endif
                </div>
                <!-- /.col -->
              </div>
			  <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Logo Home page </label>
                     <input type="file" name="logo_home_page" onchange="readURL(this,'logo_home_pageID')" class="form-control" >
                     @if ($errors->has('logo_home_page'))
                        <span class="help-block {{ $errors->has('logo_home_page') ? ' has-error' : '' }}">
                          <strong>{{ $errors->first('logo_home_page') }}</strong>
                        </span>
                    @endif
                  </div>
                </div>
				 <div class="col-md-6">
                  @if(!empty($logo_info->homelogo_img))
                  <img src="{{url('/').'/public/uploads/logo/'.$logo_info->homelogo_img}}" id="logo_home_pageID" width="100px;" height="100px;" style="border: 1px solid; margin-top: 10px;">
                
				   @else
					 <img src="{{url('/').'/resources/assets/images/no_img.jpg'}}" id="logo_home_pageID" width="100px;" height="100px;" style="border: 1px solid; margin-top: 10px;">  
				  @endif
                </div>
                <!-- /.col -->
              </div>
			  <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Admin Panel Backgound Image </label>
                     <input type="file" name="admin_background_img" onchange="readURL(this,'admin_background_img');" class="form-control" >
                     @if ($errors->has('admin_background_img'))
                        <span class="help-block {{ $errors->has('admin_background_img') ? ' has-error' : '' }}">
                          <strong>{{ $errors->first('admin_background_img') }}</strong>
                        </span>
                    @endif
                  </div>
                </div>
				 <div class="col-md-6">
                  @if(!empty($logo_info->admin_background_img))
                  <img src="{{url('/').'/public/uploads/logo/'.$logo_info->admin_background_img}}" id="admin_background_img" width="100px;" height="100px;" style="border: 1px solid; margin-top: 10px;">
                  @else
					 <img src="{{url('/').'/resources/assets/images/no_img.jpg'}}" id="admin_background_img" width="100px;" height="100px;" style="border: 1px solid; margin-top: 10px;">  
				  @endif
                </div>
                <!-- /.col -->
              </div>
            </div>
            <div class="box-footer">
              <button type="submit" name="btnUpdate" class="btn btn-primary">Update</button>
            </div>
            <!-- /.row -->
          </form>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection