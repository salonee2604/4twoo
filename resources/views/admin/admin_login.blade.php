<!DOCTYPE html>

<html>

<head>

  <meta charset="utf-8">

  <meta http-equiv="X-UA-Compatible" content="IE=edge">

   <?php $logoImg = App\Helpers\Helper::getLogoImg();  ?>

  <link href="{{ url('/public/assets/img/logo vector.png') }}" rel="icon">
  <link href="{{ url('/public/assets/img/logo vector.png') }}" rel="apple-touch-icon">

    <title> <?php echo !empty($logoImg->title)? $logoImg->title:'4Twoo';?>| Log in</title>

  <!-- Tell the browser to be responsive to screen width -->

  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <!-- Bootstrap 3.3.7 -->

  <link rel="stylesheet" href="{{url('/')}}/resources/assets/bower_components/bootstrap/dist/css/bootstrap.min.css">

  <!-- Font Awesome -->

  <link rel="stylesheet" href="{{url('/')}}/resources/assets/bower_components/font-awesome/css/font-awesome.min.css">

  <!-- Ionicons -->

  <link rel="stylesheet" href="{{url('/')}}/resources/assets/bower_components/Ionicons/css/ionicons.min.css">

  <!-- Theme style -->

  <link rel="stylesheet" href="{{url('/')}}/resources/assets/dist/css/AdminLTE.min.css">

  <!-- iCheck -->

  <link rel="stylesheet" href="{{url('/')}}/resources/assets/plugins/iCheck/square/blue.css">



  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

  <!--[if lt IE 9]>

  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>

  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

  <![endif]-->



  <!-- Google Font -->

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <style ="text/css">

   .background_img {

   width: 100%;

   height: 100vh;

   display: inline-block;
   
   background: url("../4twoo/public/assets/img/client.png") no-repeat;
 }

.background_img:before {

   background: #00000085;

   position: absolute;

   content: "";

   width: 100%;

   height: 100%;

   top: 0px;

   z-index: 0;

 }

 .login-box{

position: relative;

}
.login-btn .btn-primary {
    background-color: #c52a39;
    border-color: #c52a39;
}
.form-control:focus {
    border-color: #c52a39 !important;
    box-shadow: none;
}
.login-box-body .form-control-feedback, .register-box-body .form-control-feedback {
    color: #c52a39 !important;
}

.forget-pas {
    color: #c52a39;
    font-weight: 600;
}
.forget-pas:hover {
    color: #cc505c;
    font-weight: 600;
}
  </style>



  

</head>

<body class="hold-transition login-page">

<div class="background_img">

		<div class="login-box">

		  <div class="login-logo">

			  <!-- height="80px;" -->

			  @if(!empty($logo_info->homelogo_img))

				<img src="{{url('/')}}/public/uploads/logo/{{$logo_info->homelogo_img}}"  width="100px;" alt="User Image">



			  @else

				<img src="{{url('/')}}/public/assets/img/logo vector.png"  width="100px;" alt="User Image">

			  @endif

			  

		  </div>

		  <!-- /.login-logo -->

		  <div class="login-box-body">

			<p class="login-box-msg">Sign in to start your session</p>



			<form action="{{ route('login') }}" id="loginForm" method="post">

			  <input type="hidden" name="_token" id="csrf-token" value="{{csrf_token()}}" />

			  <div class="form-group has-feedback">

				<input id="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="Email address">

				@error('email')

					<span class="invalid-feedback" role="alert">

						<strong>{{ $message }}</strong>

					</span>

				@enderror

				<span class="glyphicon glyphicon-envelope form-control-feedback"></span>

			  </div>

			  <label id="email-error" class="error" for="email"></label>

			  <div class="form-group has-feedback">

				<input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password">

				@error('password')

					<span class="invalid-feedback" role="alert">

						<strong>{{ $message }}</strong>

					</span>

				@enderror

				<span class="glyphicon glyphicon-lock form-control-feedback"></span>

			  </div>

			  <label id="password-error" class="error" for="password"></label>

			  <div class="row">

				<!-- <div class="col-xs-8">

				  <div class="checkbox icheck">

					<input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

					<label class="form-check-label" for="remember">

						{{ __('Remember Me') }}

					</label>

				  </div>

				</div> -->

				<!-- /.col -->

				<div class="col-xs-4 login-btn">

				  <button type="submit" name="btnLogin" class="btn btn-primary btn-block btn-flat">{{ __('Login') }}</button>

				</div>

				<!-- /.col -->

			  </div>

			</form>
			<a href="{{ url('admin/forgot-password')}}" class="forget-pas">{{ __('Forgot Your Password?') }}</a><br>

		  </div>

		  <!-- /.login-box-body -->

		</div>

</div>

<!-- /.login-box -->



<!-- jQuery 3 -->

<script src="{{url('/')}}/resources/assets/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap 3.3.7 -->

<script src="{{url('/')}}/resources/assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- iCheck -->

<script src="{{url('/')}}/resources/assets/plugins/iCheck/icheck.min.js"></script>

<script>

  $(function () {

    $('input').iCheck({

      checkboxClass: 'icheckbox_square-blue',

      radioClass: 'iradio_square-blue',

      increaseArea: '20%' /* optional */

    });

  });

</script>

</body>

</html>

