@extends('admin.layout.layout')@section('content') <!-- Content Wrapper. Contains page content -->  

<div class="content-wrapper">

   <!-- Content Header (Page header) -->    

   <section class="content-header">

      <h1>        Dashboard        <small>Control panel</small>      </h1>

      <ol class="breadcrumb">

         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

         <li class="active">Dashboard</li>

      </ol>

   </section>

   <!-- Main content -->    

   <section class="content">

      <!-- Small boxes (Stat box) -->      

      <div class="row">

         <div class="col-lg-3 col-xs-6">

            <!-- small box -->          

            <div class="small-box bg-aqua">

               <div class="inner">

                  <h3>{{$total_users}}</h3>

                  <p>Users</p>

               </div>

            </div>

         </div>


         <div class="col-lg-3 col-xs-6">

            <!-- small box -->          

            <div class="small-box bg-aqua">

               <div class="inner">

                  <h3>{{$total_stories}}</h3>

                  <p>Success Stories</p>

               </div>

            </div>

         </div>

          <div class="col-lg-3 col-xs-6">

            <!-- small box -->          

            <div class="small-box bg-aqua">

               <div class="inner">

                  <h3>{{$total_testimonial}}</h3>

                  <p>Testimonials</p>

               </div>

            </div>

         </div>

         <div class="col-lg-3 col-xs-6">

            <!-- small box -->          

            <div class="small-box bg-aqua">

               <div class="inner">

                  <h3>{{$total_questionnaire}}</h3>

                  <p>Question</p>

               </div>

            </div>

         </div>

         

      </div>

      <!-- /.row -->    

   </section>

   <!-- /.content -->  

</div>

<!-- /.content-wrapper -->@endsection