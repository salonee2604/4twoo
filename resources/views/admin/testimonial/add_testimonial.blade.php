@extends('admin.layout.layout')

@section('title', 'Add Testimonial')



@section('current_page_css')

@endsection

@section('content')

 <!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">

    <!-- Content Header (Page header) -->

    <section class="content-header">

      <h1>Add testimonial </h1>

      <ol class="breadcrumb">

        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

        <li class="active">testimonial</li>

      </ol>

    </section>

    <!-- Main content -->

    <section class="content">

      @if ($message = Session::get('message'))

       <div class="alert alert-success alert-block">  

        <button type="button" class="close" data-dismiss="alert">×</button> 

        <strong>{{ $message }}</strong>

      </div>

      @endif

    

     <!-- Small boxes (Stat box) -->

      <div class="row">

        <div class="col-xs-12">

          <!-- SELECT2 EXAMPLE -->

        <div class="box box-default">

          <div class="box-header">

             <div class="box-tools pull-right">

                <a href="{{url('admin/testimonial_list')}}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Back </a>

            </div>

          </div>

          <!-- /.box-header -->

          <form action="{{url('/admin/submit_testimonial')}}" id="studentForm" method="POST" enctype="multipart/form-data">

            <input type="hidden" name="_token" id="csrf-token" value="{{csrf_token()}}" />

            <div class="box-body">

              <div class="row">

                <div class="col-md-6">

                  <div class="form-group">

                    <label>Title</label>

                    <input type="text" class="form-control" name="title" placeholder="Enter Title">

                    @if ($errors->has('title'))

                        <span class="help-block {{ $errors->has('title') ? ' has-error' : '' }}">

                          <strong>{{ $errors->first('title') }}</strong>

                        </span>

                    @endif

                  </div>
                  <!-- /.form-group -->
                  <div class="form-group">

                    <label>Content</label>
                    
                    <textarea class="form-control" name="content" placeholder="Enter testimonial Content"></textarea>
                    
                    @if ($errors->has('content'))
                        <span class="help-block {{ $errors->has('content') ? ' has-error' : '' }}">
                    
                          <strong>{{ $errors->first('content') }}</strong>
                    
                        </span>
                    
                    @endif
                  
                  </div>
                  <!-- /.form-group -->

                  <div class="form-group">

                    <label>testimonial Image</label>

                    <input type="file" class="form-control" name="image"/>

                      @if ($errors->has('image'))

                        <span class="help-block {{ $errors->has('image') ? ' has-error' : '' }}">

                          <strong>{{ $errors->first('image') }}</strong>

                        </span>

                    @endif

                  </div>

        

                </div>

                <!-- /.col -->

              </div>

            </div>

            <div class="box-footer">

              <button type="submit" class="btn btn-primary">Submit</button>

            </div>

            <!-- /.row -->

          </form>

          </div>

          <!-- /.box-body -->

        </div>

        <!-- /.box -->

      </div>

      <!-- /.row -->



    </section>

    <!-- /.content -->

  </div>

  <!-- /.content-wrapper -->

@endsection



@section('current_page_js')

<script type="text/javascript">

 

</script>

@endsection

