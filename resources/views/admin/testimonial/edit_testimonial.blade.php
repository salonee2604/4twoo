@extends('admin.layout.layout')
@section('title', 'Edit Testimonial')

@section('current_page_css')
@endsection


@section('content')
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Edit testimonial
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">Testimonial</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      @if ($message = Session::get('message'))
       <div class="alert alert-success alert-block">  
        <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong>{{ $message }}</strong>
      </div>
      @endif

      @if ($message = Session::get('error'))
      <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong>{{ $message }}</strong>
      </div>
      @endif

      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-xs-12">
          <!-- SELECT2 EXAMPLE -->
        <div class="box box-default">
          <div class="box-header pull-right">
                <a href="{{url('admin/testimonial_list')}}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Back </a>
          </div>
          <!-- /.box-header -->
          <form action="{{url('/admin/update_testimonial')}}" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="_token" id="csrf-token" value="{{csrf_token()}}" />
            <div class="box-body">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Title</label>
                    <input type="text" class="form-control" name="title" value="{{(!empty($testimonial_info->title) ? $testimonial_info->title : '')}}" placeholder="Enter Title">
                  </div>
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Content</label>
                    <textarea class="form-control" name="content" placeholder="Enter Content">{{(!empty($testimonial_info->content) ? $testimonial_info->content : '')}}</textarea>
                  </div>
  
                  <!-- /.form-group --> 
                  <div class="form-group">
                    <label>Image</label>
                    <img src="{{url('/public/assets/img/'.$testimonial_info->image)}}" style="width:100px;height:100px;" id="image_change">
                    <input name="image" type="file" accept="image/*" onchange="document.getElementById('image_change').src = window.URL.createObjectURL(this.files[0])">
                  </div>
                  <!-- /.form-group -->

                  <input type="hidden" class="form-control" name="testimonial_id" value="{{(!empty($testimonial_info->id) ? $testimonial_info->id : '')}}">
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
              </div>
            </div>
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            <!-- /.row -->
          </form>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
@section('current_page_js')
<script type="text/javascript">
 $(function() {
    $('.multiselect-ui').multiselect({
        includeSelectAllOption: true
    });
});
</script>
@endsection