<header class="main-header">
  <!-- Logo -->
  <a href="{{url('/admin/dashboard')}}" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <?php $logoImg=App\Helpers\Helper::getLogoImg(); ?> <span class="logo-mini"><?php echo !empty($logoImg->title)? $logoImg->title:'<b>4</b>Twoo';?> <!-- <img src="{{url('/').'/public/uploads/logo/'.$logoImg->logo_img}}" width="100%"> --></span> 
    <!-- logo for regular state and mobile devices --> <span class="logo-lg"><?php echo !empty($logoImg->title)? $logoImg->title:'<b>4</b>Twoo';?></span> 
  </a>
  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button"> <span class="sr-only">Toggle navigation</span> 
    </a>
    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <!-- User Account: style can be found in dropdown.less -->
        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <img src="{{url('/')}}/resources/assets/dist/img/user2-160x160.jpg" class="user-image" alt="User Image"> <span class="hidden-xs">Admin</span>
          </a>
          <ul class="dropdown-menu">
            <!-- User image -->
            <li class="user-header">
              <img src="{{url('/')}}/resources/assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
              <p>Admin</p>
            </li>
            <!-- Menu Body -->
            <li class="user-body"></li>
            <!-- Menu Footer-->
            <li class="user-footer">
              <div class="pull-left"> <a href="#" class="btn btn-default btn-flat">Profile</a>
              </div>
              <div class="pull-right"> <a href="{{url('/admin/logout')}}" class="btn btn-default btn-flat">Sign out</a>
              </div>
            </li>
          </ul>
        </li>
      </ul>
    </div>
  </nav>
  <!-- Navbar -->
</header>