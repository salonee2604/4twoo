<footer class="main-footer">
	<div class="pull-right hidden-xs"></div>
	<?php $logoImg=App\Helpers\Helper::getLogoImg(); ?> <strong>Copyright &copy; <?php echo date('Y')?> <a href="#"><?php echo !empty($logoImg->title)? $logoImg->title:'4Twoo';?></a>.</strong> All rights reserved.</footer>
<!-- Control Sidebar -->