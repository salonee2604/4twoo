@extends('admin.layout.layout')

@section('title', 'Add Question')



@section('current_page_css')

@endsection

@section('content')

 <!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">

    <!-- Content Header (Page header) -->

    <section class="content-header">

      <h1>Edit Time Slot</h1>

      <ol class="breadcrumb">

        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

        <li class="active">Edit Time Slot</li>

      </ol>

    </section>

    <!-- Main content -->

    <section class="content">

      @if ($message = Session::get('message'))

       <div class="alert alert-success alert-block">  

        <button type="button" class="close" data-dismiss="alert">×</button> 

        <strong>{{ $message }}</strong>

      </div>

      @endif

    

     <!-- Small boxes (Stat box) -->

      <div class="row">

        <div class="col-xs-12">

          <!-- SELECT2 EXAMPLE -->

        <div class="box box-default">

          <div class="box-header">

             <div class="box-tools pull-right">

                <a href="{{url('admin/timeSlotList/day')}}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Back </a>

            </div>

          </div>

          <!-- /.box-header -->

          <form action="{{url('/admin/update_timeslot')}}" id="studentForm" method="POST" enctype="multipart/form-data">

            <input type="hidden" name="quns_type" value="1">
            <input type="hidden" name="_token" id="csrf-token" value="{{csrf_token()}}" />

            <div class="box-body">

              <div class="row">

                <div class="col-md-6">
                            <div class="form-group">
                            <label for="status">Day</label>
                              <select class="form-control" id="day" name="day">
                              <option value="">Select One</option>
                              <option value="Monday" @if($Tableslot->day == 'Monday') selected  @endif>Monday</option>
                              <option value="Tuesday" @if($Tableslot->day == 'Tuesday') selected  @endif>Tuesday</option>
                               <option value="Wednesday" @if($Tableslot->day == 'Wednesday') selected  @endif>Wednesday</option>
                               <option value="Thursday" @if($Tableslot->day == 'Thursday') selected  @endif>Thursday</option>
                               <option value="Friday" @if($Tableslot->day == 'Friday') selected  @endif>Friday</option>
                               <option value="Saturday" @if($Tableslot->day == 'Saturday') selected  @endif>Saturday</option>
                               <option value="Sunday" @if($Tableslot->day == 'Sunday') selected  @endif>Sunday</option>
                            </select>
                           </div> 
                          <div class="form-group">
                            <label for="table_number">Start Time</label><span class="error">*</span>
                             <div class="input-group"><div class="input-group-addon">
                                          <i class="fa fa-clock-o"></i>
                             </div>
                             <input type="text"  class="time form-control" id="start_time"  name="start_time" placeholder="Start Time"value="{{$Tableslot->start_time}}">
                              <input type="hidden"  class="form-control" id="time_slot_id" placeholder="Table Number" name="time_slot_id" value="{{(!empty($Tableslot->time_slot_id) ? $Tableslot->time_slot_id : '')}}" min="1">
                            @if ($errors->has('start_time'))
                            <span class="help-block {{ $errors->has('start_time') ? ' has-error' : '' }}">
                            <strong>{{ $errors->first('start_time') }}</strong>
                            </span>
                            @endif
                          </div>   
                          <div class="form-group">
                             <label for="menu_compare_price">End Time</label>
                                            <div class="input-group"><div class="input-group-addon">
                                          <i class="fa fa-clock-o"></i>
                                        </div>
                                            <input type="text" class="time form-control" id="end_time" name="end_time" placeholder="End Time" value="{{$Tableslot->end_time}}">
                             @if ($errors->has('table_seet_capacity'))
                            <span class="help-block {{ $errors->has('table_seet_capacity') ? ' has-error' : '' }}">
                            <strong>{{ $errors->first('table_seet_capacity') }}</strong>
                            </span>
                            @endif
                          </div>   
                          <div class="form-group">
                            <label for="status">Status</label>
                               <input type="radio"  name="status"  id="status" value="1" checked> Active
                              <input type="radio" name="status" id="status" value="0"> Inactive
                           </div> 
                  

                  
                  
                  
                </div>

                <!-- /.col -->

              </div>

            </div>
            </div>
          </div>
            <div class="box-footer">

              <button type="submit" class="btn btn-primary">Submit</button>

            </div>

            <!-- /.row -->

          </form>

          </div>

          <!-- /.box-body -->

        </div>

        <!-- /.box -->

      </div>

      <!-- /.row -->



    </section>

    <!-- /.content -->

  </div>

  <!-- /.content-wrapper -->

@endsection



@section('current_page_js')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-datetimepicker/2.7.1/css/bootstrap-material-datetimepicker.min.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-datetimepicker/2.7.1/js/bootstrap-material-datetimepicker.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/locale/ja.js"></script>
<script src="https://openlayers.org/api/2.10/OpenLayers.js" type="text/javascript">
</script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css" rel="stylesheet"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.4/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
<script>
   $('#end_time_input').prop('disabled', true);
   $('#start_time').datetimepicker({
   format: 'HH:mm',
   useCurrent: false,
   });
   
   $('#end_time').datetimepicker({
   format: 'HH:mm',
   useCurrent: false,
   });
   
   $("#start_time").on("dp.change", function (e) {
   $('#end_time_input').prop('disabled', false);
   if( e.date ){
     $('#end_time').data("DateTimePicker").date(e.date.add(1, 'h'));
   }
   
   $('#end_time').data("DateTimePicker").minDate(e.date);
   });
</script>
<!--  <script type="text/javascript">
    $(document).ready(function()
    {

      $('.time').bootstrapMaterialDatePicker
      ({
        date: false,
        shortTime: false,
        format: 'HH:mm'
      });
      $.material.init()
    });
    </script>
 -->
@endsection

