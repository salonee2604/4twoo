@extends('admin.layout.layout')

@section('title', 'Testimonial List')

@section('current_page_css')

<!-- DataTables -->

<link rel="stylesheet" href="{{url('/')}}/resources/assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<link rel="stylesheet" href="{{url('/')}}/resources/assets/css/bootstrap-toggle.min.css">

<style>

    .error{

        color: red;

    }

</style>

@endsection

@section('current_page_js')

<!-- DataTables -->

<script src="{{url('/')}}/resources/assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>

<script src="{{url('/')}}/resources/assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script src="{{url('/')}}/resources/assets/js/bootstrap-toggle.min.js"></script>
<script>
  $('.datepicker').datepicker({
    format: 'yyyy-mm-dd',
    startDate: '-3d'
});
</script>


<script type="text/javascript">


$(function () {

  $('#testimonial_list').DataTable({

    'paging'      : true,

    'lengthChange': false,

    'searching'   : true,

    'ordering'    : true,

    'info'        : true,

    'autoWidth'   : false

  })

})



jQuery(function() {

    var table = jQuery('#testimonial_list').DataTable();



    jQuery("#btnExport").click(function(e) {

        table.page.len(-1).draw();

        window.open('data:application/vnd.ms-excel,' +

            encodeURIComponent(jQuery('#testimonial_list').parent().html()));

        setTimeout(function() {

            table.page.len(10).draw();

        }, 1000)



    });

});

</script>

<script type="text/javascript">

 function delete_table_list(table_id){

  $.ajaxSetup({

    headers: {

      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

    }

  });

  $.ajax({

   type: 'POST',

   url: "<?php echo url('/admin/delete_table_list'); ?>",

   enctype: 'multipart/form-data',

   data:{table_id:table_id,'_token':'<?php echo csrf_token(); ?>'},

     beforeSend:function(){

       return confirm("Are you sure you want to delete this Table Management?");

     },

     success: function(resultData) { 

       console.log(resultData);
       

       var obj = JSON.parse(resultData);

       if (obj.status == 'success') {

          $('#success_message').fadeIn().html(obj.msg);

          setTimeout(function() {

            $('#success_message').fadeOut("slow");

          }, 2000 );

          $("#row" + table_id).remove();

       }

     },

     error: function(errorData) {

      console.log(errorData);

      alert('Please refresh page and try again!');

    }

  });

} 

</script>

<script>

  $(document).on("change",'.toggle-class', function() {

    var status = $(this).prop('checked') == true ? 1 : 0; 

    var testimonial_id = $(this).data('id'); 

    

    $.ajax({

      type: "GET",

      dataType: "json",

      url: "<?php echo url('/admin/change_testimonial_status'); ?>",

      data: {'status': status, 'testimonial_id': testimonial_id},

      success: function(data){

        $('#success_message').fadeIn().html(data.success);

        setTimeout(function() {

          $('#success_message').fadeOut("slow");

        }, 2000 );

      },

      error: function(errorData) {

        console.log(errorData);

        alert('Please refresh page and try again!');

      }

    });

  })

</script>
<script type="text/javascript">
  $(document).ready(function(){
  $('#filter_booktbl').click(function(){
  //$("form").valid();
 
  var status       = $('#status').val();
  var booking_date = $('#booking_date').val();
  var day          = $('#day').val();

 $.ajax({  
        url:"{{url('/')}}/admin/ajax_booktbl_filter",  
        method:"GET",  
        data:{status:status,booking_date:booking_date,day:day,"_token": "{{ csrf_token() }}"},  
         success:function(output)  
         {  
          //alert(data);
          $('tbody').html(output);  
        },
        error: function(){
       
        
        }  
         });  
     }); 
});
</script>

@endsection

@section('content')

 <!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">

    <!-- Content Header (Page header) -->

    <section class="content-header">

      <h1>Customer Table Booking List</h1>

      <ol class="breadcrumb">

        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

        <li class="active">Customer Table Booking List</li>

      </ol>

    </section>

    <!-- Main content -->

    <section class="content">

      <p style="display: none;" id="success_message" class="alert alert-success"></p>

      @if ($errors->any())

      <div class="alert alert-danger">

       <ul>

         @foreach ($errors->all() as $error)

         <li>{{ $error }}</li>

         @endforeach

       </ul>

     </div>

     @endif

      @if (Session::has('error_arr'))

          <?php $error_arr = Session::get('error_arr'); ?>

          <div class="alert alert-info">

            <ul>

              <?php

              for($i=0; $i < count($error_arr); $i++){

                if(!empty($error_arr[$i])){

                  ?><li>{{$error_arr[$i]}}</li><?php

                }

              }

              ?>

            </ul>

          </div>

          <?php Session::forget('error_arr'); ?>

      @endif



     @if(Session::has('message'))

     <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>

     @endif



     @if(Session::has('error'))

     <p class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('error') }}</p>

     @endif

      <div class="row">

        <div class="col-xs-12">

          <div class="box">

            <div class="box-header">

             <div class ="row">

               <div class="col-md-6">

               </div>

                <div class="col-md-6">

                 <h3 class="box-title pull-right btn-toolbar"><a href="{{ url('admin/add_table_booking') }}" class="btn btn-primary">Add Reservation</a></h3>

                

                </div>

             </div>
             <div class="col-md-12">
               <form method="GET" class="forms-sample" action="" id="waiting_searchfilter" name="" enctype='multipart/form-data'>
               <div class="row">
                <div class="col-md-3">
                  <div class="form-group">
                      <label for="email">Status</label>
                       <select class="form-control" id="status" name="status">
                        <option value="">--Select Status--</option>
                        <option value="1">Pending</option> 
                         <option value="2">Cancel</option> 
                         <option value="3">Accept</option>
                           <option value="4">Complete</option>
                       </select>
                           
                    </div>
                  </div>
                  <div class="col-md-3">
                  <div class="form-group">
                       <label for="email">Date</label>
                      <input type="text" class="form-control datepicker" id="booking_date" placeholder="Booking Date" name="booking_date" autocomplete="off">
                    </div>
                  </div>
                    <div class="col-md-3">
                  <div class="filterTable">
                    <label for="seat_capacity">     Day</label><span class="error">*</span>
                            <select class="form-control" id="day" name="day">
                              <option value="">--Select Day--</option>
                              <option value="Monday">Monday</option>
                              <option value="Tuesday">Tuesday</option>
                               <option value="Wednesday">Wednesday</option>
                               <option value="Thursday">Thursday</option>
                               <option value="Friday">Friday</option>
                               <option value="Saturday">Saturday</option>
                               <option value="Sunday">Sunday</option>
                              </select>
                  </div>

                </div>
                <br>
                    <div class="col-md-3">
                        <button type="button" name="waiting_searchfilter" id="filter_booktbl" value="" class="btn btn-primary">Filter</button>
                        <button type="reset" onclick="window.location.reload()" class="btn btn-primary">Reset</button>                 
                    
                   </div>
                </div>                  
           </form>
          </div>

            </div>

            <!-- /.box-header -->

            <div class="box-body">

              <table id="testimonial_list" class="table table-bordered table-striped">

                <thead>

                  <tr>

                    <th>S.No.</th>
                    
                    <th>Booking Id</th>

                    <th>User Name</th>

                    <th>Mobile Number</th>

                    <th>No Of Members</th>

                    <th>Day</th>

                    <th>Booking Date</th>

                    <th>Time</th>

                    <th>Table Number</th>

                    <th>Message</th>

                    <th>Status</th>

                    <th>Action</th>

                  </tr>

                </thead>

                <tbody>

                  @if(!$booking_list->isEmpty())

                  <?php $i=1; ?>

                  @foreach($booking_list as $row)

                  <tr id="row{{$row->table_booking_id}}">
                    <?php

                     
                      if ($row->booking_status==1) 
                      {
                       $Status ='Pending';
                      
                      }
                      if ($row->booking_status==2) 
                      {
                       $Status ='Cancel';
                       
                      }
                      if ($row->booking_status==3) 
                      {
                       $Status ='Accept';
                       
                      }
                      if ($row->booking_status==4) 
                      {
                       $Status ='Complete';
                       
                      }
                    ?>
                    <td>{{$i}}</td>
                    <td>{{$row->booking_unique_id}}</td>
                    <td>{{$row->username}}</td>
                    <td>{{$row->contact_number}}</td>
                    <td>{{$row->people}}</td>
                    <td>{{$row->day}}</td>
                    <td>{{$row->booking_date}}</td>                    
                    <td>{{$row->start_time}}-{{$row->end_time}}</td>
                    <td></td>
                    <td>{{$row->message}}</td>
                    <td>{{$Status}}</td>
                     <td>
                      <a href="{{url('/admin/view_table_booking_list')}}/{{base64_encode($row->table_booking_id)}}">View</a>
                    </td>

                  </tr>

                  <?php $i++; ?>

                  @endforeach

                  @endif

                </tbody>

              </table>

            </div>

            <!-- /.box-body -->

          </div>

          <!-- /.box -->

        </div>

      </div>

        <!-- /.col -->

      <!-- /.row -->

    </section>

    <!-- /.content -->

  </div>

  <!-- /.content-wrapper -->
  

@endsection
@section('current_page_js')
