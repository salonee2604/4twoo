@extends('admin.layout.layout')
@section('title', 'Edit Question')

@section('current_page_css')
@endsection


@section('content')




<style type="text/css">
  .tableStatus:hover {
    box-shadow: 0px 0px 5px #696969;
    border: 1px solid #828282;
}
.tblPrson
{
   padding: 6px 4px;
}

.tableStatus {
    height: 128px;
}
.tableStatus {
    float: left;
    width: 18.4%;
    padding: 15px;
    margin: 10px;
    background-color: #eaeaea;
    cursor: pointer;
    display: grid;
    border: 1px solid #ddd;
}
{
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

.tblStatus.statusTwo {
    font-size: 16px;
}
.tblStatus.statusTwo {
    background-color: #05af0a;
    color: #fff;
    align-items: center;
    display: flex;
    /* justify-content: center; */
}
.tblStatus.statusOne {
    padding: 5px;
    background-color: #e8e800;
    color: black;
    border-radius: 4px;
    margin-top: 10px;
     font-size: 16px;
}
</style>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
   <!--  <section class="content-header">
      <h1>
       All Users
        <small>Users Tables</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> User Managment</a></li>
        <li><a href="#">All Users</a></li>
      </ol>
    </section> -->

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"><b>Table Status List</b></h3>
            </div>
            <!-- /.box-header -->
            
            <div class="box-body">
              <div class="row">
                <form method="get" id="tblfilter">
                <div class="col-xs-12">
                  <div class="col-md-3">
                  <div class="filterTable">
                  <label for="table_number">Booking Date</label><span class="error">*</span>
                  <input type="text" class="form-control datepicker" id="booking_date" placeholder="Booking Date" name="booking_date" autocomplete="off">
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="filterTable">
                    <label for="seat_capacity">     Day</label><span class="error">*</span>
                            <select class="form-control" id="day" name="day">
                              <option value="">--Select Day--</option>
                              <option value="Monday">Monday</option>
                              <option value="Tuesday">Tuesday</option>
                               <option value="Wednesday">Wednesday</option>
                               <option value="Thursday">Thursday</option>
                               <option value="Friday">Friday</option>
                               <option value="Saturday">Saturday</option>
                               <option value="Sunday">Sunday</option>
                              </select>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="filterTable">
                    <label>Time Slot </label>
                    <select name="time_slot" class="form-control" id="time_slot_id" required="">
                      <option value="">Select Time Slot</option>
                    </select>
                  </div>
                </div>

                <div class="col-md-2">
                  <div class="filterTable">
                  <!--<input type="text" name="" placeholder="Search Table">
                    <div class="SearchIcon"><i class="fa fa-search"></i></div>-->
                      <label>Search</label>
                    <button type="button" name="search" id="search" class="form-control btn btn-primary">Search</button>
                  </div>
                </div>
              </div>
            </form>
              </div>


            <div class="row" id="test">
                <div class="col-md-12">
                  <div class="allTable">
                      <?php
                    foreach($booktbl as $row){
                     $people=$row->people;
                     $table_no=$row->table_no;
                     $person=$people;
                     $status=$row->status;
                     $Status='';
                  if ($row->status==1) 
                      {
                       $Status ='Pending';
                      }
                      if ($row->status==2) 
                      {
                       $Status ='Cancel';
                      }
                      if ($row->status==3) 
                      {
                       $Status ='Accept';
                      }
                      if ($row->status==4) 
                      {
                       $Status ='Complete';
                      }
                      ?>

                    <div class="tableStatus">
                      <strong>Table <?php echo $table_no;?></strong>
                    
                    <?php if($row->status==1){?>
                    <div class="tblStatus statusFour sts label pending">Table Status <?php echo $Status;?></div>
                          <?php }else if($row->status==3){ ?>
                    <div class="tblStatus statusTwo sts label accept">Table Status <?php echo $Status;?></div>
                          <?php }else if($row->status==2){?>
                    <div class="tblStatus statusOne sts label cancel">Table Status <?php echo $Status;?></div>
                           <?php }else if($row->status==4){?>
                    <div class="tblStatus statusOne sts label complete">Table Status <?php echo $Status;?></div>
                            <?php } ?>
                   
                    <span class="tblPrson"><?php echo $person;?> Persons</span>
                    </div>
                      <?php 
                    }
                  ?>
                    <!--<div class="tableStatus">
                      <strong>Table 102</strong>
                      <span class="tblStatus statusTwo">Table Status two</span>
                      <span class="tblPrson">4 Persons</span>
                    </div>

                    <div class="tableStatus">
                      <strong>Table 103</strong>
                      <span class="tblStatus statusThree">Table Status three</span>
                      <span class="tblPrson">4 Persons</span>
                    </div>

                    <div class="tableStatus">
                      <strong>Table 104</strong>
                      <span class="tblStatus statusFour">Table Status Four</span>
                      <span class="tblPrson">4 Persons</span>
                    </div>

                    <div class="tableStatus">
                      <strong>Table 105</strong>
                      <span class="tblStatus statusOne">Table Status one</span>
                      <span class="tblPrson">4 Persons</span>
                    </div>

                    <div class="tableStatus">
                      <strong>Table 106</strong>
                      <span class="tblStatus statusOne">Table Status one</span>
                      <span class="tblPrson">4 Persons</span>
                    </div>

                    <div class="tableStatus">
                      <strong>Table 107</strong>
                      <span class="tblStatus statusOne">Table Status one</span>
                      <span class="tblPrson">4 Persons</span>
                    </div>

                    <div class="tableStatus">
                      <strong>Table 108</strong>
                      <span class="tblStatus statusOne">Table Status one</span>
                      <span class="tblPrson">4 Persons</span>
                    </div>

                    <div class="tableStatus">
                      <strong>Table 109</strong>
                      <span class="tblStatus statusOne">Table Status one</span>
                      <span class="tblPrson">4 Persons</span>
                    </div>

                    <div class="tableStatus">
                      <strong>Table 110</strong>
                      <span class="tblStatus statusOne">Table Status one</span>
                      <span class="tblPrson">4 Persons</span>
                    </div>-->

                  </div>
                </div>
               
              </div>
          
            </div>
            <!-- /.box-body -->
          </div>        
        </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

<!-- SlimScroll -->
<!-- page script -->
 


<!-- Modal -->


  <!-- /.content-wrapper -->
@endsection
@section('current_page_js')
<!-- Time slot script -->
<script>
  $('#tblfilter'). on("change","#day",function(){
        var rest_id = 1;
        var day = $(this).val();
        $.ajax({
          dataType: "html",
          method:"post", 
          url:"{{url('/')}}/admin/ajax_timeslot",  
          data:{rest_id:rest_id,day:day,"_token": "{{ csrf_token() }}"}, 
           success:function(response)
           {       
               
                if(response)
                {
                   $('#time_slot_id').html(response);  

                }else{
                  $("#time_slot_id").html("<option> not available</option>");
                }
           }
              // }
        });
       });
   

</script>

<!-- search booking -->
<script type="text/javascript">
$(document).ready(function(){
$('#search').click(function(){
$("form").valid();
  var booking_date = $('#booking_date').val();
  var day = $('#day').val();
  var timeslot = $('#time_slot_id').val();
   $.ajax({  
        url:"{{url('/')}}/admin/ajaxval",  
        method:"get",  
        data:{booking_date:booking_date,day:day,timeslot:timeslot,"_token": "{{ csrf_token() }}"},  
         success:function(output)  
         {  
          //alert(data);
          $('#test').html(output);  
        },
        error: function(){
       
        
        }  
         });  
     }); 
});
</script>
<script>
  $('.datepicker').datepicker({
    format: 'yyyy-mm-dd',
    startDate: '-3d'
});
</script>

@endsection