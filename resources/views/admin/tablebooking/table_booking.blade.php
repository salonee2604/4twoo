@extends('admin.layout.layout')

@section('title', 'Testimonial List')

@section('current_page_css')

<!-- DataTables -->

<link rel="stylesheet" href="{{url('/')}}/resources/assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<link rel="stylesheet" href="{{url('/')}}/resources/assets/css/bootstrap-toggle.min.css">

<style>

    .error{

        color: red;

    }

</style>

@endsection

@section('current_page_js')

<!-- DataTables -->

<script src="{{url('/')}}/resources/assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>

<script src="{{url('/')}}/resources/assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script src="{{url('/')}}/resources/assets/js/bootstrap-toggle.min.js"></script>

<script type="text/javascript">


$(function () {

  $('#testimonial_list').DataTable({

    'paging'      : true,

    'lengthChange': false,

    'searching'   : true,

    'ordering'    : true,

    'info'        : true,

    'autoWidth'   : false

  })

})



jQuery(function() {

    var table = jQuery('#testimonial_list').DataTable();



    jQuery("#btnExport").click(function(e) {

        table.page.len(-1).draw();

        window.open('data:application/vnd.ms-excel,' +

            encodeURIComponent(jQuery('#testimonial_list').parent().html()));

        setTimeout(function() {

            table.page.len(10).draw();

        }, 1000)



    });

});

</script>

<script type="text/javascript">

 function delete_table_list(table_id){

  $.ajaxSetup({

    headers: {

      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

    }

  });

  $.ajax({

   type: 'POST',

   url: "<?php echo url('/admin/delete_table_list'); ?>",

   enctype: 'multipart/form-data',

   data:{table_id:table_id,'_token':'<?php echo csrf_token(); ?>'},

     beforeSend:function(){

       return confirm("Are you sure you want to delete this Table Management?");

     },

     success: function(resultData) { 

       console.log(resultData);
       

       var obj = JSON.parse(resultData);

       if (obj.status == 'success') {

          $('#success_message').fadeIn().html(obj.msg);

          setTimeout(function() {

            $('#success_message').fadeOut("slow");

          }, 2000 );

          $("#row" + table_id).remove();

       }

     },

     error: function(errorData) {

      console.log(errorData);

      alert('Please refresh page and try again!');

    }

  });

} 

</script>

<script>

  $(document).on("change",'.toggle-class', function() {

    var status = $(this).prop('checked') == true ? 1 : 0; 

    var testimonial_id = $(this).data('id'); 

    

    $.ajax({

      type: "GET",

      dataType: "json",

      url: "<?php echo url('/admin/change_testimonial_status'); ?>",

      data: {'status': status, 'testimonial_id': testimonial_id},

      success: function(data){

        $('#success_message').fadeIn().html(data.success);

        setTimeout(function() {

          $('#success_message').fadeOut("slow");

        }, 2000 );

      },

      error: function(errorData) {

        console.log(errorData);

        alert('Please refresh page and try again!');

      }

    });

  })

</script>

@endsection

@section('content')

 <!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">

    <!-- Content Header (Page header) -->

    <section class="content-header">

      <h1>Table Management</h1>

      <ol class="breadcrumb">

        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

        <li class="active">Table Management</li>

      </ol>

    </section>

    <!-- Main content -->

    <section class="content">

      <p style="display: none;" id="success_message" class="alert alert-success"></p>

      @if ($errors->any())

      <div class="alert alert-danger">

       <ul>

         @foreach ($errors->all() as $error)

         <li>{{ $error }}</li>

         @endforeach

       </ul>

     </div>

     @endif

      @if (Session::has('error_arr'))

          <?php $error_arr = Session::get('error_arr'); ?>

          <div class="alert alert-info">

            <ul>

              <?php

              for($i=0; $i < count($error_arr); $i++){

                if(!empty($error_arr[$i])){

                  ?><li>{{$error_arr[$i]}}</li><?php

                }

              }

              ?>

            </ul>

          </div>

          <?php Session::forget('error_arr'); ?>

      @endif



     @if(Session::has('message'))

     <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>

     @endif



     @if(Session::has('error'))

     <p class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('error') }}</p>

     @endif

      <div class="row">

        <div class="col-xs-12">

          <div class="box">

            <div class="box-header">

             <div class ="row">

               <div class="col-md-6">

               </div>

                <div class="col-md-6">

                 <h3 class="box-title pull-right btn-toolbar"><a href="{{ url('admin/addTableList') }}" class="btn btn-primary">Add Table Management</a></h3>

                

                </div>

             </div>

            </div>

            <!-- /.box-header -->

            <div class="box-body">

              <table id="testimonial_list" class="table table-bordered table-striped">

                <thead>

                  <tr>

                    <th>S.No.</th>

                    <th>Table Type</th>

                    <th>Table Number</th>

                    <th>Table Capacity</th>

                    <th>Create Date</th>

                    <th>Action</th>

                  </tr>

                </thead>

                <tbody>

                  @if(!$table_list->isEmpty())

                  <?php $i=1; ?>

                  @foreach($table_list as $arr)

                  <tr id="row{{$arr->table_id}}">

                    <td>{{$i}}</td>
                   
                    <td>{{$arr->section_name}}</td>

                    <td>{{$arr->table_no}}</td>

                    <td>{{$arr->table_seet_capacity}}</td>

                    <td>{{(!empty($arr->created_at) ? date('d-m-Y H:i A',strtotime($arr->created_at)) : 'N/A')}}</td>

                    <td>
                      <a href="{{url('/admin/view_table_list')}}/{{base64_encode($arr->table_id)}}">View</a> |
                      <a href="{{url('/admin/edittablelist')}}/{{base64_encode($arr->table_id)}}">Edit</a> |
                      <a href="javascript:void(0)" onclick="delete_table_list('<?php echo $arr->table_id; ?>');">Delete</a>

                    </td>

                  </tr>

                  <?php $i++; ?>

                  @endforeach

                  @endif

                </tbody>

              </table>

            </div>

            <!-- /.box-body -->

          </div>

          <!-- /.box -->

        </div>

      </div>

        <!-- /.col -->

      <!-- /.row -->

    </section>

    <!-- /.content -->

  </div>

  <!-- /.content-wrapper -->

@endsection