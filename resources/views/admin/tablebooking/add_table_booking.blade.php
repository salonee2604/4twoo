@extends('admin.layout.layout')

@section('title', 'Add Question')



@section('current_page_css')

@endsection

@section('content')
<style>
  .error
  {
    color :red;
  }
</style>
 <!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">

    <!-- Content Header (Page header) -->

    <section class="content-header">

      <h1>Add Table Reservation</h1>

      <ol class="breadcrumb">

        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

        <li class="active">Add Table Reservation</li>

      </ol>

    </section>

    <!-- Main content -->

    <section class="content">

      @if ($message = Session::get('message'))

       <div class="alert alert-success alert-block">  

        <button type="button" class="close" data-dismiss="alert">×</button> 

        <strong>{{ $message }}</strong>

      </div>

      @endif

    

     <!-- Small boxes (Stat box) -->

      <div class="row">

        <div class="col-xs-12">

          <!-- SELECT2 EXAMPLE -->

        <div class="box box-default">

          <div class="box-header">

             <div class="box-tools pull-right">

                <a href="{{url('admin/booktable_list')}}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Back </a>

            </div>

          </div>

          <!-- /.box-header -->

             <div class="box-body">
                 <div class="row">
                <!-- left column -->
                  <div class="col-md-12">

                     <form class="forms-sample" id="add_table_booking" name="add_table_booking" enctype='multipart/form-data' method="post" action="{{url('admin/table_booking_post')}}">

                     <input type="hidden" name="_token" value="{{ csrf_token() }}">

                   <div class="row">
                    <div id="msg"></div>
                      <!-- left column -->
                        <div class="col-md-6">
                          
                          <div class="form-group">
                            <label for="seat_capacity">Customer Type</label><span class="error">*</span>
                            <select class="form-control" id="customer_type" name="customer_type">
                              <option value="">--Select Customer Type--</option>
                              <option value="1">Existing User</option>
                              <option value="2">New User</option>

                            </select>
 
                              </div>   

                               <div class="form-group" style="display:none;" id="fetch_username">
                            <label for="section_name">Customer Name</label><span class="error">*</span>
                             <select class="form-control"  id="exist_username" name="exist_username">
                              <option value="">--Select User Name--</option>
                              @foreach($userlist as $k => $v)
                              <option value="{{$v->id}}">{{$v->username}}</option>
                              @endforeach
                            </select>
                          </div>

                         

                           <div class="form-group" style="display:none;" id="userid">
                            <label for="table_number">Customer Name</label><span class="error">*</span>
                            <input type="text" class="form-control" id="user_name" placeholder="Customer Name" name="fullname" autocomplete="off">
                          </div>

                           <div class="form-group" style="display:none;" id="email_address">
                            <label for="table_number">Email</label><span class="error">*</span>
                            <input type="text" class="form-control" id="email" placeholder="Email" name="email" autocomplete="off">
                          </div>

                          <div class="form-group" style="display:none;" id="contact">
                            <label for="table_number">Mobile No.</label><span class="error">*</span>
                            <input type="text" class="form-control" id="mobile" placeholder="Mobile No." name="mobile" autocomplete="off" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" minlength="8"  maxlength="12">
                          </div>
                          <div class="form-group">
                            <label for="table_number">Booking Date</label><span class="error">*</span>
                            <input type="text" class="form-control datepicker" id="booking_date" placeholder="Booking Date" name="booking_date" autocomplete="off">
                          </div>   

                           <div class="form-group">
                            <label for="seat_capacity">     Day</label><span class="error">*</span>
                            <select class="form-control" id="day" name="day">
                              <option value="">--Select Day--</option>
                              <option value="Monday">Monday</option>
                              <option value="Tuesday">Tuesday</option>
                               <option value="Wednesday">Wednesday</option>
                               <option value="Thursday">Thursday</option>
                               <option value="Friday">Friday</option>
                               <option value="Saturday">Saturday</option>
                               <option value="Sunday">Sunday</option>
                              </select>
                             </div> 
                           <div class="form-group">
                            <label for="section_name">Time Slot</label><span class="error">*</span>
                             <select class="form-control hasDatepicker valid" id="time_slot_id" name="time_slot_id">
                              <option value="">--Select Time Slot--</option>
                            </select>
                          </div>   

                             

                             <div class="form-group">
                            <label for="seat_capacity">Number Of Members</label><span class="error">*</span>
                            <input type="number"  class="form-control" id="number_of_memebers" placeholder="Number Of Members" name="number_of_memebers" value="" autocomplete="off" min="1" step="1"/>
                          </div>   


                            
                       
                       <div class="form-group">
                            <label for="seat_capacity">Message</label><span class="error">*</span>
                            <textarea class="form-control" id="message" placeholder="Message" name="message" maxlength="200"></textarea>
                          </div>   

                      <div class="margin-top">        
                         <button type="submit" class="btn btn-primary mr-2" id="reservation_btn">Add Reservation</button> 

                          </div>
                        </div>
                         <!--  <button class="btn btn-light">Cancel</button> -->
                      </div>
                   
                      </form>
                      </div>
                  </div>
               
                
              </div>

          </div>

          <!-- /.box-body -->

        </div>

        <!-- /.box -->

      </div>

      <!-- /.row -->



    </section>

    <!-- /.content -->

  </div>

  <!-- /.content-wrapper -->

@endsection



@section('current_page_js')
<!-- cutomer stype show hide -->
<script type = "text/javascript" >
    $(function() {
        $("#customer_type").change(function() {

            if ($(this).val() == "1") {

                $("#fetch_username").show();
            } else {
                $("#fetch_username").hide();
            }

            if ($(this).val() == "2") {
                $("#userid").show();
            } else {
                $("#userid").hide();
            }
            if ($(this).val() == "2") {
                $("#email_address").show();
            } else {
                $("#email_address").hide();
            }
            if ($(this).val() == "2") {
                $("#contact").show();
            } else {
                $("#contact").hide();
            }

        });
    }); 
</script>
<!-- get Time slot List -->
<script>
  $('#add_table_booking'). on("change","#day",function(){

      
        var rest_id = 1;
        var day = $(this).val();
        $.ajax({
          dataType: "html",
          method:"post", 
          url:"{{url('/')}}/admin/ajax_timeslot",  
          data:{rest_id:rest_id,day:day,"_token": "{{ csrf_token() }}"}, 
           success:function(response)
           {       
               
                if(response)
                {
                   $('#time_slot_id').html(response);  

                }else{
                  $("#time_slot_id").html("<option> not available</option>");
                }
           }
              // }
        });
       });
   

</script>
<!-- Date time picker/ -->
<script>
  $('.datepicker').datepicker({
    format: 'yyyy-mm-dd',
    startDate: '-3d'
});
</script>
<!-- add booking validation -->
<script>
$('#add_table_booking').validate({

    rules:{
      fullname:{
        required:true
      },
     exist_username:{
        required:true
      },
     customer_type:{
        required:true
      },
      username:{
        required:true
      },
      rest_id:{
        required:true
      },
      day:{
        required:true
      },
      booking_date:{
        required:true
      },
     time_slot_id:{
        required:true
      },
     number_of_memebers:{
        required:true
      },
      message:{
        required:true
      },
      email:{
        required:true,
        
      },
      mobile:{
        required:true,
        
      },
    },
    messages:{
      email:{
        required:'Enter Email'
      },
      mobile:{
        required:'Enter Mobile Number'
      },
       booking_date:{
        required:'Enter Booking Date'
      },
      rest_id:{

        required:'Please Select Restaurent Name',
      },
      customer_type:{

        required:'Please Select Customer Type',
      },
    fullname:{

        required:'Enter Customer Name',
      },
      day:{
       required:'Please Select Day'
      },
      time_slot_id:{
       required:'Please Select Time Slot',
      },
      number_of_memebers:{
       required:'Enter No. Of Members',
      },
   message:{
       required:'Please Enter Message no more than 200 characters.',
      }
    },
    submitHandler:function(form){
      $('#reservation_btn').attr('disabled', true);
      form.submit();
    }
  });
</script>
@endsection

