@extends('admin.layout.layout')
@section('title', 'Edit Question')

@section('current_page_css')
@endsection


@section('content')




<style type="text/css">
    .error
    {
      color :red;
    }
    .nav-tabs.wizard1 {
      background-color: transparent;
      padding: 0;
      width: 100%;
      margin: 1em auto;
      border-radius: .25em;
      clear: both;
      border-bottom: none;
    }

    .nav-tabs.wizard1 li {
      width: 100%;
      float: none;
      margin-bottom: 3px;
    }

    .nav-tabs.wizard1 li>* {
      position: relative;
      padding: 1em .8em .8em 2.5em;
      color: #999999;
      background-color: #dedede;
      border-color: #dedede;
    }

    .nav-tabs.wizard1 li.completed>* {
      color: #fff !important;
      background-color: #96c03d !important;
      border-color: #96c03d !important;
      border-bottom: none !important;
    }

    .nav-tabs.wizard1 li.active>* {
      color: #fff !important;
      background-color: #2c3f4c !important;
      border-color: #2c3f4c !important;
      border-bottom: none !important;
    }

    .nav-tabs.wizard1 li::after:last-child {
      border: none;
    }

    .nav-tabs.wizard1 > li > a {
      opacity: 1;
      font-size: 14px;
    }

    .nav-tabs.wizard1 a:hover {
      color: #fff;
      background-color: #2c3f4c;
      border-color: #2c3f4c
    }

    span.nmbr {
        display: inline-block;
        padding: 0px 0 0 0;
        background: #ffffff;
        width: 50px;
        line-height: 100%;
        height: 50px;
        margin: auto;
        border-radius: 50%;
        font-weight: bold;
        font-size: 16px;
        color: #555;
        margin-bottom: 10px;
        text-align: center;
    }

    @media(min-width:992px) {
      .nav-tabs.wizard1 li {
        position: relative;
        padding: 0;
        margin: 4px 4px 4px 0;
        width: 19.6%;
        float: left;
        text-align: center;
      }
      .nav-tabs.wizard1 li.active a {
        padding-top: 15px;
      }
      .nav-tabs.wizard1 li::after,
      .nav-tabs.wizard1 li>*::after {
        content: '';
        position: absolute;
        top: 1px;
        left: 100%;
        height: 0;
        width: 0;
        border: 52px solid transparent;
        border-right-width: 0;
        /*border-left-width: 20px*/
      }
      .nav-tabs.wizard1 li::after {
        z-index: 1;
        -webkit-transform: translateX(4px);
        -moz-transform: translateX(4px);
        -ms-transform: translateX(4px);
        -o-transform: translateX(4px);
        transform: translateX(4px);
        border-left-color: #fff;
        margin: 0
      }
      .nav-tabs.wizard1 li>*::after {
        z-index: 2;
        border-left-color: inherit
      }
      .nav-tabs.wizard1 > li:nth-of-type(1) > a {
        border-top-left-radius: 10px;
        border-bottom-left-radius: 10px;
      }
      .nav-tabs.wizard1 li:last-child a {
        border-top-right-radius: 10px;
        border-bottom-right-radius: 10px;
      }
      .nav-tabs.wizard1 li:last-child {
        margin-right: 0;
      }
      .nav-tabs.wizard1 li:last-child a:after,
      .nav-tabs.wizard1 li:last-child:after {
        content: "";
        border: none;
      }
      span.nmbr {
        display: block;
      }
    }
    .manag_steps {
        float: left;
        width: 100%;
        margin-bottom: 20px;
    }
    span.nmbr img {
        opacity: 0.3;
    }
    .nav-tabs.wizard1 li.active span.nmbr img {
        opacity: 1;
    }
    .nav-tabs.wizard1 li.completed span.nmbr img {
        opacity: 1;
    }

    button.statsCnsl:hover {
        background-color: #1f4a70;
    }

    button.statsCnsl {
        float: left;
        color: #fff;
        border: none;
        padding: 6px 12px;
        background-color: #3c8dbc;
        border-color: #367fa9;
        border-radius: 4px;
    }
    .my-custs .btn-default {
        color: #fff !important;
    border: 1px solid #ccc !important;
    background: #c52a39 !important;
}

    #img_cancl {
        display:none;
        width: 100px;
    }
</style>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
   <!--  <section class="content-header">
      <h1>
       All Users
        <small>Users Tables</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> User Managment</a></li>
        <li><a href="#">All Users</a></li>
      </ol>
    </section> -->

    <!-- Main content -->
    <section class="content">
                 <div class="col-md-12" id="tabexample">
          <div class="box-header">
              <h3 class="box-title"><b> Table Status</b></h3>
            </div>
             @if(Session::has('success'))
              <div class="alert alert-success">{{Session::get('success')}}</div>
              @endif
              @if(Session::has('error'))
              <div class="alert alert-danger">{{Session::get('error')}}</div>
              @endif
          <div class="manag_steps">
        <img src="{{url('resources/assets/images/icon-cancel.png')}}" id='img_cancl'>

            <div class="tabbable">
 @foreach ($viewtabl_datas as $rows)
 <?php if($rows->status=='2'){ ?>
            <img src="{{url('resources/assets/images/icon-cancel.png')}}" style="width:100px;">

         <ul class="nav nav-tabs wizard1" style="display: none;">
               
         <li data-id="1" class="<?php if($rows->status >= 1) { echo "active"; } ?> st_1"><a href="#i9" data-toggle="tab" aria-expanded="false"><input type="hidden" name="table_status" class="tab1" value="1">
        <input type="hidden" name="table_booking_id" class="tab2" value="<?php echo Request::segment(3);?>">
         
        <input type="hidden" name="status" class="status" value="{{$rows->status}}">
        <span class="nmbr"><img src="{{url('resources/assets/images/icon-pro1.png')}}"></span> Requested11222</a></li>
          
         <li data-id="2" class="<?php if($rows->status >= 3) { echo "active"; } ?> st_2"><a href="#w4" data-toggle="tab" aria-expanded="false"><input type="hidden" name="table_status" class="tab1" value="3">
        <input type="hidden" name="table_booking_id" class="tab2" value="<?php echo Request::segment(3);?>"><input type="hidden" name="status" class="status" value="{{$rows->status}}"><span class="nmbr"><img src="{{url('public/uploads/icon-pro2.png')}}"></span> Table confirm</a></li>
        
         <li data-id="3" class="<?php if($rows->status >= 4) { echo "active"; }?> st_3"><a href="#stateinfo" data-toggle="tab" aria-expanded="false"><input type="hidden" name="table_status" class="tab1" value="4">
         <input type="hidden" name="table_booking_id" class="tab2" value="<?php echo Request::segment(3);?>"><input type="hidden" name="status" class="status" value="{{$rows->status}}"><span class="nmbr"><img  src="{{url('public/uploads/icon-pro3.png')}}"></span>Customer arrived</a></li>
        
        </ul>
        <?php
      }else{
      ?>
           <ul class="nav nav-tabs wizard1">
               
         <li data-id="1" class="<?php if($rows->status >= 1) { echo "active"; } ?> st_1"><a href="#i9" data-toggle="tab" aria-expanded="false"><input type="hidden" name="table_status" class="tab1" value="1">
        <input type="hidden" name="table_booking_id" class="tab2" value="<?php echo Request::segment(3);?>">
         
        <input type="hidden" name="status" class="status" value="{{$rows->status}}">
        <span class="nmbr"><img src="{{url('resources/assets/images/icon-pro1.png')}}"></span> Requested</a></li>
          
         <li data-id="2" class="<?php if($rows->status >= 3) { echo "active"; } ?> st_2"><a href="#w4" data-toggle="tab" aria-expanded="false"><input type="hidden" name="table_status" class="tab1" value="3">
        <input type="hidden" name="table_booking_id" class="tab2" value="<?php echo Request::segment(3);?>"><input type="hidden" name="status" class="status" value="{{$rows->status}}"><span class="nmbr"><img src="{{url('resources/assets/images/icon-pro2.png')}}"></span> Table confirm</a></li>
        
         <li data-id="3" class="<?php if($rows->status >= 4) { echo "active"; }?> st_3"><a href="#stateinfo" data-toggle="tab" aria-expanded="false"><input type="hidden" name="table_status" class="tab1" value="4">
         <input type="hidden" name="table_booking_id" class="tab2" value="<?php echo Request::segment(3);?>"><input type="hidden" name="status" class="status" value="{{$rows->status}}"><span class="nmbr"><img src="{{url('resources/assets/images/icon-pro3.png')}}"></span>Customer arrived</a></li>
        
        </ul>
<?php } ?>
        @endforeach
      </div>

       @foreach ($viewtabl_datas as $rows_val)
<?php if($rows_val->status=='2' OR $rows_val->status=='4'){?>
      <button type="button" id="btn_cancel" data-id="4" class="statsCnsl" style="display: none;"><input type="hidden" name="tablestatus" class="tablecancl" value="2"><input type="hidden" name="table_id" class="table_id" value="<?php echo Request::segment(3);?>">Cancel Table</button> 
       <?php 
     }
       else{?>
 <!-- <button type="button" id="btn_cancel" data-id="4" class="statsCnsl"><input type="hidden" name="tablestatus" class="tablecancl" value="2"><input type="hidden" name="table_id" class="table_id" value="<?php //echo Request::segment(3);?>">Cancel Table</button>  -->

            <button class="statsCnsl" data-toggle="modal" data-target="#modalOrderCancelForm">
                Cancel Table
            </button> 

       <?php } ?>
       @endforeach
          </div>
        </div>

      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header"> <h3 class="box-title">View Table Booking</h3>
              <div class="card-body pull-right">
                <a href="{{url('/admin/booktable_list')}}" class="btn btn-primary mr-2 "><i class="fa fa-arrow-circle-left">Back</i></a>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="msg"></div>          
            <div class="box-body">
              <div class="row">
                @foreach($viewtabl_datas as $row)
              
               <?php 
                $tdlt=explode(",",$row->table_id);
                $tname= DB::table('table_list')->whereIn('table_id',$tdlt)->get();
               ?>

                <div class="col-md-6 for-divrsn">
                 <div class="form-group">
                  <div class="row">
                        <div class="col-md-4"><strong>Number Of Members :</strong></div>
                        <div class="col-md-8">{{$row->people}}</div>
                      </div>
                      </div>
                      <div class="form-group">
                      <div class="row">
                        <div class="col-md-4"><strong>Total Table:</strong></div>
                        <div class="col-md-8">{{$row->total_table}}</div>
                      </div>
                      </div>
                      <div class="form-group">
                      <div class="row">
                        <div class="col-md-4"><strong>Table Type:</strong></div>
                        <div class="col-md-8">{{$row->section_name}}</div>
                      </div>
                      </div>
                       <div class="form-group">
                      <div class="row">
                        <div class="col-md-4"><strong>Time:</strong></div>
                        <div class="col-md-8">{{$row->start_time}} - {{$row->end_time}}</div>
                      </div>
                      </div>
                     <div class="form-group">
                      <div class="row">
                        <div class="col-md-4"><strong>Booking Date:</strong></div>
                        <div class="col-md-8">{{$row->booking_date}}</div>
                      </div>
                      </div>
                       <div class="form-group">
                      <div class="row">
                        <div class="col-md-4"><strong>Table No.:</strong></div>
                        <div class="col-md-8">
                          <?php $tbl=''; ?>
                          @foreach($tname AS $item) <?php  $tbl .=$item->table_no.','; ?> @endforeach 
                          <?php echo $tablels_name=rtrim($tbl,","); ?></div>
                      </div>
                      </div>
                         <?php

                     $Status='';
                      $class='';
                      if($row->status==1) 
                      {
                         $Status ='Pending';
                       
                      }
                      if($row->status==2) 
                      {
                         $Status ='Cancel';
                      
                      }
                      if($row->status==3) 
                      {
                         $Status ='Accept';
                       
                      }
                      if($row->status==4) 
                      {
                         $Status ='Complete';
                       
                      }
                      
                  ?>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-4"><strong>Table Status:</strong></div>
                        <div class="col-md-8"><p>{{$Status}}<p></div>
                      </div>
                      </div>

                  <div class="form-group">
                      <div class="row">
                        <div class="col-md-4"><strong>User Name:</strong></div>
                      <div class="col-md-8">{{$row->username}}</div>
                      </div>
                      </div>
                  <div class="form-group">
                      <div class="row">
                        <div class="col-md-4"><strong>User Email:</strong></div>
                        <div class="col-md-8">{{$row->email}}</div>
                      </div>
                      </div>
                  <div class="form-group">
                      <div class="row">
                        <div class="col-md-4"><strong>User Mobile Number:</strong></div>
                        <div class="col-md-8">{{$row->contact_number}}</div>
                      </div>                      
                    </div>
              
                  <div class="form-group">
                      <div class="row">
                        <div class="col-md-4"><strong>Message:</strong></div>
                        <div class="col-md-8">{!! $row->message !!}</div>
                      </div>
                    </div>
                     
                </div>
              @endforeach   

                            <!--manage assign table-->
                        <div class="col-md-6">
              <div class="box-header"> <h3 class="box-title">Assign Table No.</h3></div>
              <div class="box-body">
                <div class="row">
                  <form method="post">
                  <div class="col-md-12">
                     <div class="form-group">
                        <div class="row">
                          <div class="col-md-4"><strong>Table No.:</strong></div>
                          <div class="col-md-8 my-custs">

                           <?php
                            foreach($viewtabl_datas as $row){
                              $bookingdate=$row->booking_date;
                             $restid= 1;
                             $timeslot=$row->time_slot_id;
                            $id=$row->table_booking_id;
                           $status=$row->status;

                              }
                            $assign_tbl = DB::select("SELECT table_booking.*,table_list.* from table_booking INNER join table_list on table_list.rest_id=table_booking.rest_id where table_booking.booking_date='$bookingdate' AND table_booking.rest_id='$restid' AND table_booking.time_slot_id='$timeslot' AND (table_booking.status='1' OR table_booking.status='3') GROUP BY table_list.table_id ORDER BY table_list.table_no ASC");

                            
                           ?>
                           <input type="hidden" name="id" id="book_id" value="<?php echo $id;?>">
                            <select  name="tabl_no" class="form-control multiselect-ui " multiple="multiple" id="tabl_no" multiple="multiple">
                              <option value="">--Select Table No.--</option>
                               @foreach($assign_tbl as $k => $v)
                              <option value="{{$v->table_id}}" <?php $people=explode(",",$row->table_id); 
                     if (in_array($v->table_id, $people)){ echo 'selected'; } ?> >{{$v->table_no}} (st. cpt. {{$v->table_seet_capacity}})</option>
                    @endforeach
                            </select>
                          </div>
                          <div class=" margin-top">
                            @if($row->status == 1 or $row->status == 3)
                            <button type="button" name="submit" id="assign_btn" class="btn btn-primary">Assign</button>
                            @endif
                          </div>
                        </div>
                      </div>
                      
                  
                   
                </div>
              </form>
                </div>
            
              </div>

             </div>
             <!--end assign table-->       
      
             </div>
             
            </div>
           

                      
            </div>
            <!-- /.box-body -->
          </div>
        
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

<!-- SlimScroll -->
<!-- page script -->
 


<!-- Modal -->
<div class="modal fade" id="modalOrderCancelForm" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Table Cancel</h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
                <p class="statusMsg"></p>
                <form action="" method="post" id="tableCleForm">
                  
                    <div class="form-group">
                        <label for="inputMessage">Why do you want to cancel this book table?</label>
                        <textarea class="form-control" name="reasonMessage" id="reasonMessage" placeholder="Enter table cancel reason"></textarea>
                        <input type="hidden" name="orderstatus" class="ordercancl" value="2">
                        <input type="hidden" name="table_id" class="table_id" value="<?php echo Request::segment(3);?>">
                    </div>
                     <!-- Modal Footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                        <button type="submit" id="tablecancelSubmit" class="btn btn-primary submitBtn">SUBMIT</button>
                    </div>
                </form>
            </div>
            
           
        </div>
    </div>
</div>

  <!-- /.content-wrapper -->
@endsection
@section('current_page_js')
<script type="text/javascript">
 $(function() {
    $('.multiselect-ui').multiselect({
        includeSelectAllOption: true
    });
});
</script>
<script type="text/javascript">
   $(document).ready(function(){
      $('#assign_btn').click(function(){
      //$("form").valid();
       var book_id = $('#book_id').val();
       var tabl_no = $('#tabl_no').val();
       $.ajax({  
            url:"{{url('/')}}/admin/assign_table",  
            method:"post",  
            data:{book_id:book_id,tabl_no:tabl_no,"_token": "{{ csrf_token() }}"},  
             success:function(data)  
             {  


               setTimeout(function(){
                       location.reload(); 
                       }, 1500); 
               $(this).modal('hide');                          
            },
            error: function(){
           //alert(error);
            
              }  
            });  
         }); 
});
</script>
<script type="text/javascript">
    $('#tableCleForm').validate({
          rules: { 
                 reasonMessage:{
                       required: true
                               }          
                  },
          messages:
                  {
                  },
          submitHandler: function(form) {          
            var table_id=$('#tableCleForm').find('.table_id').val();
            var tablestatus = $('#tableCleForm').find('.ordercancl').val();
            var reasonMessage = $('#tableCleForm').find('#reasonMessage').val();
            var data_attr=6;

              if(data_attr='6'){
                    $('.wizard li').hide();
                    $('.statsCnsl').attr("disabled",true);
                    $('#img_cancl').show();
                       }
                    
             $.ajaxSetup({
                headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
              });

           $('#tablecancelSubmit').attr('disabled', true);
               $.ajax({
                  url: '{{url('/')}}/admin/ajax_table_cancel',
                  type: 'GET',
                  data: {tablestatus:tablestatus, table_id:table_id, reasonMessage:reasonMessage},
                  success: function(data){
                    setTimeout(function(){
                        $('#tablecancelSubmit').attr('disabled', false);
                     location.reload(); // then reload the page.(3)
                    }, 1000); 
                                   
                  }
                });   
         }
   });

   
</script>


@endsection