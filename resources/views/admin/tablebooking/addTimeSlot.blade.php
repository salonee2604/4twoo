@extends('admin.layout.layout')
@section('title', 'Add Question')
@section('current_page_css')
@endsection
@section('content')
<style>
  .error {
  color: red;
  margin-left: 5px;
}

</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>Add Time Slot</h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li class="active">Add Time Slot</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      @if ($message = Session::get('message'))
      <div class="alert alert-success alert-block">  
         <button type="button" class="close" data-dismiss="alert">×</button> 
         <strong>{{ $message }}</strong>
      </div>
      @endif
      <!-- Small boxes (Stat box) -->
      <div class="row">
         <div class="col-xs-12">
            <!-- SELECT2 EXAMPLE -->
            <div class="box box-default">
               <div class="box-header">
                  <div class="box-tools pull-right">
                     <a href="{{url('admin/timeSlotList/day')}}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Back </a>
                  </div>
               </div>
               <!-- /.box-header -->
               <form action="{{url('/admin/addslot_post')}}" id="addslot" method="POST" enctype="multipart/form-data" class="commentForm">
                  <input type="hidden" name="quns_type" value="1">
                  <input type="hidden" name="_token" id="csrf-token" value="{{csrf_token()}}" />
                  <div class="box-body">
                     <div class="row">
                        <!-- left column -->
                        <div class="col-md-6">
                          <div class="row">
                           <div class="col-md-4">
                           <div class="form-group">
                            <label for="seat_capacity">     Day</label><span class="error">*</span>
                            <select class="form-control" id="day" name="day">
                              <option value="">--Select Day--</option>
                              <option value="Monday">Monday</option>
                              <option value="Tuesday">Tuesday</option>
                               <option value="Wednesday">Wednesday</option>
                               <option value="Thursday">Thursday</option>
                               <option value="Friday">Friday</option>
                               <option value="Saturday">Saturday</option>
                               <option value="Sunday">Sunday</option>
                              </select>
                             </div> 
                                @if ($errors->has('day'))

                                <span class="help-block {{ $errors->has('day') ? ' has-error' : '' }}">

                                <strong>{{ $errors->first('day') }}</strong>

                                  </span>

                                @endif
                              </div> 
                              </div> 
                           <div class="row input_fields_wrap">
                              <div class="col-md-4">
                                 <div class="form-group">
                                    <label for="menu_price">Start Time</label><span class="error">*</span>
                                    <div class='input-group date start_time' id="start_time">
                                       <input type='text' class="form-control" name="start_time[]" value="" id="start_time_input0"/>
                                       <span class="input-group-addon">
                                       <span class="glyphicon glyphicon-calendar"></span></span>
                                       </div>
                                       @if ($errors->has('start_time[]'))
                                       <span class="help-block {{ $errors->has('start_time[]') ? ' has-error' : '' }}">
                                       <strong>{{ $errors->first('start_time[]') }}</strong>
                                       </span>
                                       @endif
                                 </div>
                              </div>
                              <div class="error"></div>
                              <div class="col-md-4">
                                 <div class="form-group">
                                    <label for="menu_compare_price">End Time</label><span class="error">*</span>
                                    <div class="form-group">
                                       <div class='input-group date end_time' id="end_time">
                                          <input type='text' class="form-control end_time_input" id="end_time_input0" name="end_time[]"/>
                                          <span class="input-group-addon">
                                          <span class="glyphicon glyphicon-calendar"></span>
                                          </span>
                                       </div>
                                       @if ($errors->has('end_time'))
                                       <span class="help-block {{ $errors->has('end_time') ? ' has-error' : '' }}">
                                       <strong>{{ $errors->first('end_time') }}</strong>
                                       </span>
                                       @endif
                                    </div>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <a href="#" class="add_field_button" title="Add field"><img src="{{url('resources/assets/icons/add-icon.png')}}"/></a>
                              </div>
                           </div>
                           <div class="form-group">
                              <label for="section_name">Status</label>
                              <input type="radio"  name="status"  id="status" value="1" checked> Active
                              <input type="radio" name="status" id="status" value="0"> Inactive
                           </div>
                           <div class="margin-top">        
                              <button type="submit" class="btn btn-primary mr-2" id="submit">Submit</button>
                           </div>
                        </div>
                     </div>
                     <!--  <button class="btn btn-light">Cancel</button> -->
                  </div>
               </form>
            </div>
         </div>
      </div>
</div>
<!-- /.row -->
</form>
</div>
<!-- /.box-body -->
</div>
<!-- /.box -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection
@section('current_page_js')
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-datetimepicker/2.7.1/css/bootstrap-material-datetimepicker.min.css"> -->
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-datetimepicker/2.7.1/js/bootstrap-material-datetimepicker.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/locale/ja.js"></script>
<script src="https://openlayers.org/api/2.10/OpenLayers.js" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>

<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css" rel="stylesheet"/> -->
<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.css" rel="stylesheet"/> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
   $(document).ready(function() {
   var max_fields      = 13; //maximum input boxes allowed
   var wrapper         = $(".input_fields_wrap"); //Fields wrapper
   var add_button      = $(".add_field_button"); //Add button ID
   
   var x = 0; //initlal text box count
   $(add_button).click(function(e){ //on add input button click
     e.preventDefault();
     if(x < max_fields){ //max input box allowed
       x++; //text box increment
      
      $(wrapper).append('<div class="col-md-12"><div class="row" id=""><div class="col-md-12"><div class="row input_fields_wrap"><div class="col-md-4"><div class="form-group"><label for="menu_price">Start Time</label><span class="error">*</span><div class="input-group date start_time" id="start_time'+x+'"><input type="text" class="form-control" name="start_time[]" id="start_time_input'+x+'"/><span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span></div></div></div><div class="error"></div><div class="col-md-4"><div class="form-group"><label for="menu_compare_price">End Time</label><span class="error">*</span><div class="form-group"><div class="input-group date end_time" id="end_time'+x+'"><input type="text" class="form-control end_time_input" id="end_time_input'+x+'" name="end_time[]"/><span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span></div></div></div></div></div><a href="#" class="remove_field"><img src="{{url("resources/assets/icons/remove-icon.png")}}"/></a></div></div>'); //add input box
      //      $('#end_time_input').prop('disabled', true);
   $('#start_time'+x).datetimepicker({
   format: 'HH:mm',
   useCurrent: false,
   });
   
   $('#end_time'+x).datetimepicker({
   format: 'HH:mm',
   useCurrent: false,
   });
   
   $("#start_time"+x).on("dp.change", function (e) {
   $('#end_time_input'+x).prop('disabled', false);
   if( e.date ){
     $('#end_time'+x).data("DateTimePicker").date(e.date.add(1, 'h'));
   }
   
   $('#end_time'+x).data("DateTimePicker").minDate(e.date);
   });
     }
    
   
   });
   
   $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
     e.preventDefault(); $(this).parent('div').remove(); x--;
   })
   
   });
    
</script>
<script>
   $('#end_time_input').prop('disabled', true);
   $('#start_time').datetimepicker({
   format: 'HH:mm',
   useCurrent: false,
   });
   
   $('#end_time').datetimepicker({
   format: 'HH:mm',
   useCurrent: false,
   });
   
   $("#start_time").on("dp.change", function (e) {
   $('#end_time_input').prop('disabled', false);
   if( e.date ){
     $('#end_time').data("DateTimePicker").date(e.date.add(1, 'h'));
   }
   
   $('#end_time').data("DateTimePicker").minDate(e.date);
   });
</script>
<script>
  jQuery("#addslot").validate({
        rules: {
            'start_time[]': {
                required: true
            },
            'end_time[]': {
                required: true
            },
            'day': {
                required: true
            },
        },
      messages:{
      'start_time[]':{
        required:'Enter Start Time'
      },
      'end_time[]':{
        required:'Enter End Time'
      },
      'day':{
        required:'Enter Day'
      },
    }
    });
</script>

@endsection