@extends('admin.layout.layout')

@section('title', 'Add Question')



@section('current_page_css')

@endsection

@section('content')

 <!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">

    <!-- Content Header (Page header) -->

    <section class="content-header">

      <h1>Add Table Management</h1>

      <ol class="breadcrumb">

        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

        <li class="active">Add Table</li>

      </ol>

    </section>

    <!-- Main content -->

    <section class="content">

      @if ($message = Session::get('message'))

       <div class="alert alert-success alert-block">  

        <button type="button" class="close" data-dismiss="alert">×</button> 

        <strong>{{ $message }}</strong>

      </div>

      @endif

    

     <!-- Small boxes (Stat box) -->

      <div class="row">

        <div class="col-xs-12">

          <!-- SELECT2 EXAMPLE -->

        <div class="box box-default">

          <div class="box-header">

             <div class="box-tools pull-right">

                <a href="{{url('admin/tableList')}}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Back </a>

            </div>

          </div>

          <!-- /.box-header -->

          <form action="{{url('/admin/submit_tablelist')}}" id="studentForm" method="POST" enctype="multipart/form-data">

            <input type="hidden" name="quns_type" value="1">
            <input type="hidden" name="_token" id="csrf-token" value="{{csrf_token()}}" />

            <div class="box-body">

              <div class="row">

                <div class="col-md-6">
                            <div class="form-group">
                            <label for="section_name">Table Type</label><span class="error">*</span>
                             <select class="form-control" id="section_name" name="table_type">
                              <option value="">Select One</option>
                              @foreach($all_section_list as $value)
                             <option value="{{(!empty($value->section_id) ? $value->section_id : '')}}" >{{ $value->section_name }}</option>
                             @endforeach
                            </select>
                            @if ($errors->has('table_type'))
                            <span class="help-block {{ $errors->has('table_type') ? ' has-error' : '' }}">
                            <strong>{{ $errors->first('table_type') }}</strong>
                            </span>
                            @endif

                          </div>   
                           <div class="form-group">
                            <label for="table_number">Table Number</label><span class="error">*</span>
                            <input type="number"  class="form-control" id="table_number" placeholder="Table Number" name="table_number" value="" min="1">
                            @if ($errors->has('table_number'))
                            <span class="help-block {{ $errors->has('table_number') ? ' has-error' : '' }}">
                            <strong>{{ $errors->first('table_number') }}</strong>
                            </span>
                            @endif
                          </div>   
                          <div class="form-group">
                            <label for="seat_capacity">Table Seating Capacity</label><span class="error">*</span>
                            <input type="number" class="form-control" id="table_seet_capacity" placeholder="Table Seating Capacity" name="table_seet_capacity" value="" min="1" max="20">
                             @if ($errors->has('table_seet_capacity'))
                            <span class="help-block {{ $errors->has('table_seet_capacity') ? ' has-error' : '' }}">
                            <strong>{{ $errors->first('table_seet_capacity') }}</strong>
                            </span>
                            @endif
                          </div>   
                          <div class="form-group">
                            <label for="status">Status</label>
                               <input type="radio"  name="status"  id="status" value="1" checked> Active
                              <input type="radio" name="status" id="status" value="0"> Inactive
                           </div> 
                  

                  
                  
                  
                </div>

                <!-- /.col -->

              </div>

            </div>

            <div class="box-footer">

              <button type="submit" class="btn btn-primary">Submit</button>

            </div>

            <!-- /.row -->

          </form>

          </div>

          <!-- /.box-body -->

        </div>

        <!-- /.box -->

      </div>

      <!-- /.row -->



    </section>

    <!-- /.content -->

  </div>

  <!-- /.content-wrapper -->

@endsection



@section('current_page_js')

<script type="text/javascript">

 

</script>

@endsection

