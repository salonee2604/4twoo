@extends('admin.layout.layout')
@section('title', 'Student List')
@section('current_page_css')
<!-- DataTables -->
<link rel="stylesheet" href="{{url('/')}}/resources/assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="{{url('/')}}/resources/assets/css/bootstrap-toggle.min.css">
@endsection
@section('current_page_js')
<!-- DataTables -->
<script src="{{url('/')}}/resources/assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="{{url('/')}}/resources/assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="{{url('/')}}/resources/assets/js/bootstrap-toggle.min.js"></script>
<script type="text/javascript">
  $(function () {
    $('#student_list').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
<script type="text/javascript">
 function deleteUser(user_id){
    if(confirm("Are you sure you want to delete this?")){
        window.location="<?php echo url('/admin/deleteUser'); ?>/"+user_id;
    }  
  }

 function sendemail(student_id){
 var toemail = $('#toemail_'+student_id).text();
               $('#emailtosend').val(toemail);
               $('#studentID').val(student_id);
               $('#myModal').modal('show');
}

$('#sendemailfrm').validate({ 
      rules: {
       email: {
        required: true,
        email:true
      }
    },
    submitHandler: function(form) {
        $.ajax({
         type: "POST",
         dataType: "json",
         url: "<?php echo url('/admin/send-email'); ?>",
         data: $('#sendemailfrm').serialize(),
         success: function(data){
           $('#notify').html(data.message).css('color','green').fadeIn();
           setTimeout(function() {
             $('#notify').fadeOut("slow");
           }, 2000 );
         },
         error: function(errorData) {
           console.log(errorData);
           
         }
       });
    }
  });

</script>
<script>
  $('.toggle-class').on("change", function() {
    var status = $(this).prop('checked') == true ? 1 : 0; 
    var student_id = $(this).data('id'); 
    
    $.ajax({
      type: "GET",
      dataType: "json",
      url: "<?php echo url('/admin/change_student_status'); ?>",
      data: {'status': status, 'student_id': student_id},
      success: function(data){
        $('#success_message').fadeIn().html(data.success);
        setTimeout(function() {
          $('#success_message').fadeOut("slow");
        }, 2000 );
      },
      error: function(errorData) {
        console.log(errorData);
        alert('Please refresh page and try again!');
      }
    });
  })

</script>
@endsection
@section('content')
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Admin subuser List</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">admin subuser</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <p style="display: none;" id="success_message" class="alert alert-success"></p>
      @if ($errors->any())
      <div class="alert alert-danger">
       <ul>
         @foreach ($errors->all() as $error)
         <li>{{ $error }}</li>
         @endforeach
       </ul>
     </div>
     @endif
     @if(Session::has('message'))
     <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
     @endif

     @if(Session::has('error'))
     <p class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('error') }}</p>
     @endif
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
               <h3 class="box-title pull-right btn-toolbar"><a href="{{url('/admin/addsubuser')}}" class="btn btn-primary">Add User</a></h3>

            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="student_list" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>S.No.</th>
                    <th>Fullname</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>User Role</th>
                    <th>Status</th>
                    <th>Create Date</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  @if(!$subuserlist->isEmpty())
                  <?php $i=1; ?>
                  @foreach($subuserlist as $arr)
                  <tr id="row{{$arr->id}}">
                    <td>{{$i}}</td>
                    <td>{{$arr->first_name}} {{$arr->last_name}}</td>
                    <td>{{$arr->username}}</td>
                    <td id="toemail_<?php echo $arr->id; ?>">{{$arr->email}}</td>
                    <td>
					    @if($arr->role_id == 1)
					     <span  class="label label-primary">Admin</span>
          				@else
						 <span  class="label label-info">Moderator</span>
						@endif
				     </td>
                    <td>
                      <input data-id="{{$arr->id}}" class="toggle-class" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive" {{ $arr->status ? 'checked' : '' }}>
                    </td>
                    <td>{{(!empty($arr->created_at) ? date('d-m-Y H:i A',strtotime($arr->created_at)) : 'N/A')}}</td>
                    <td>
                      <a href="{{url('/admin/changeuserpassword')}}/{{base64_encode($arr->id)}}">Update Password</a> |

                      <a href="{{url('/admin/updateuser')}}/{{base64_encode($arr->id)}}">Edit</a> |

                      <a href="javascript:void(0)" onclick="deleteUser('<?php echo $arr->id; ?>');">Delete</a>|
                      <a href="javascript:void(0)" onclick="sendemail('<?php echo $arr->id; ?>');">Send Email |</a>
                        @if(!empty($arr->role_id) && $arr->role_id == 4)
					      <a href="{{url('/admin/UpdatePermission')}}/{{base64_encode($arr->id)}}">Permission</a>
          				@endif
					  </td>
                  </tr>
                  <?php $i++; ?>
                  @endforeach
                  @endif
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <form id="sendemailfrm" name="sendemailfrm" method="post" role="form">
           <input type="hidden" name="_token" value="{{csrf_token()}}">
           <input type="hidden" id="studentID" name="student_id" value="">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Send Email</h4>
      </div>
      <div class="modal-body">
                     <div class="controls">
                 <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                              <label for="form_email">Email *</label>
                              <input id="emailtosend" type="email" name="email" class="form-control" placeholder="Please enter your email *" required="required" data-error="Valid email is required.">
                              <div class="help-block with-errors"></div>
                          </div>
                      </div>
                     <div class="col-md-6"></div>
                  </div>
                  <div class="row">
                      <div class="col-md-12">
                          <div class="form-group">
                              <label for="form_message">Message *</label>
                              <textarea id="form_message" name="message" class="form-control" placeholder="Message for*" rows="4" required="required" data-error="Please, leave us a message."></textarea>
                              <div class="help-block with-errors"></div>
                          </div>
                      </div>
                      </div>
                  </div>
                <span id="notify"></span>
              </div>
          
      <div class="modal-footer">
             <input type="submit" class="btn btn-success btn-send" value="Send">
             <button type="button" class="btn btn-small btn-default" data-dismiss="modal">Close</button>
      </div></div>
       </form>
    </div>
 </div>
@endsection