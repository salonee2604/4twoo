@extends('admin.layout.layout')

@section('title', 'Add Student')
@section('current_page_css')
 <style>
 ul {
  list-style-type: none;
}
 </style>
@endsection
@section('content')
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Update Permission</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Update Permission</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      @if ($message = Session::get('message'))
       <div class="alert alert-success alert-block">  
        <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong>{{ $message }}</strong>
      </div>
      @endif
     <div class="row">
        <div class="col-xs-12">
          <!-- SELECT2 EXAMPLE -->
        <div class="box box-default">
          <div class="box-header">
             <div class="box-tools pull-right">
                <a href="{{url('admin/subuserlist')}}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Back </a>
            </div>
          </div>
          <!-- /.box-header -->
		<?php //print_r($permission);?>
          <form action="{{url('/admin/saveupdatepermission')}}" id="studentForm" method="POST" enctype="multipart/form-data">

            <input type="hidden" name="_token" id="csrf-token" value="{{csrf_token()}}" />
            <input type="hidden" name="user_id" id="user_id" value="{{$user_id}}" />
            <div class="box-body">
              <div class="row">
			 
                <div class="col-md-6">
				   <h3>Menu List</h3>
				  <ul>
				  <?php if(!empty($menulist)){
					   foreach($menulist as $key=>$menulistVal){
						   $mainMenu = App\Helpers\Helper::checkPermission($menulistVal->id,$user_id);
					  ?>
						<li>
							<input type="checkbox" name="{{$menulistVal->menu_slug}}" value="{{$menulistVal->id}}" <?php echo !empty($mainMenu)? 'checked':''; ?>>{{$menulistVal->menu_name}}</input>
							<?php if(!empty($menulistVal->is_child) && is_array($menulistVal->is_child)) {?>
								<ul>
								  <?php foreach($menulistVal->is_child as $key=>$child_menul){ 
								      $childMenu = App\Helpers\Helper::checkPermission($child_menul->id,$user_id);
								  ?>
									<li>
										<input type="checkbox" name="{{$child_menul->menu_slug}}" value="{{$child_menul->id}}" <?php echo !empty($childMenu)? 'checked':''; ?>>{{$child_menul->menu_name}}</input>
									</li>
								  <?php } ?>
								</ul>
							<?php }?>
						</li>
				  <?php }}?>
				</ul>
                </div>
              </div>
            </div>
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">save</button>
            </div>
          </form>
          </div>
        </div>
     </div>
  </section>
  </div>
@endsection
@section('current_page_js')

<script type="text/javascript">
	$('input[type=checkbox]').click(function () {
    $(this).parent().find('li input[type=checkbox]').prop('checked', $(this).is(':checked'));
    var sibs = false;
    $(this).closest('ul').children('li').each(function () {
        if($('input[type=checkbox]', this).is(':checked')) sibs=true;
    })
    $(this).parents('ul').prev().prop('checked', sibs);
});
 </script>

@endsection

