@extends('admin.layout.layout')

@section('title', 'Add Question')



@section('current_page_css')

@endsection

@section('content')

 <!-- Content Wrapper. Contains page content -->
 <style>
  .error {
  color: red;
  margin-left: 5px;
}

</style>

  <div class="content-wrapper">

    <!-- Content Header (Page header) -->

    <section class="content-header">

      <h1>Add Question</h1>

      <ol class="breadcrumb">

        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

        <li class="active">Question</li>

      </ol>

    </section>

    <!-- Main content -->

    <section class="content">

      @if ($message = Session::get('message'))

       <div class="alert alert-success alert-block">  

        <button type="button" class="close" data-dismiss="alert">×</button> 

        <strong>{{ $message }}</strong>

      </div>

      @endif

    

     <!-- Small boxes (Stat box) -->

      <div class="row">

        <div class="col-xs-12">

          <!-- SELECT2 EXAMPLE -->

        <div class="box box-default">

          <div class="box-header">

             <div class="box-tools pull-right">

                <a href="{{url('admin/question_list')}}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Back </a>

            </div>

          </div>

          <!-- /.box-header -->

          <form action="{{url('/admin/submit_question')}}" id="studentForm" method="POST" enctype="multipart/form-data">

            <input type="hidden" name="quns_type" value="1">

                    <div class="col-md-3 wd-15">
                        <div class="box-qa">
                            <a href="{{ url('admin/add_question')}}">
                            <p style="color:#de3e00">Radio Choice</p>
                            </a>
                        </div>
                     </div>
        
                    <div class="col-md-3 wd-15">
                      <div class="box-qa">
                          <a href="{{ url('admin/addquestiontruefalse')}}">
                          <p>True False</p>
                          </a>
                      </div>
                    </div>
        
                    <!-- <div class="col-md-2 wd-15">
                      <div class="box-qa">
                          <a href="{{ url('admin/addquestionselect')}}">
                          <p>Select</p>
                          </a>
                      </div>
                    </div> -->
          
                    <div class="col-md-3 wd-15">
                      <div class="box-qa">
                          <a href="{{ url('admin/addquestionInsertText')}}">
                          <p>Insert Text</p>
                          </a>
                      </div>
                    </div>


            <input type="hidden" name="_token" id="csrf-token" value="{{csrf_token()}}" />

            <div class="box-body">

              <div class="row">

                <div class="col-md-6">
                  <div class="form-group">

                    

                    <label>Question</label>

                    <textarea class="form-control" name="question" id="question" placeholder="Enter Question"></textarea>
                    <span style="display: none" id="error" class="error">Please Enter Quation</span>
                    @if ($errors->has('question'))
                      
                        <span class="help-block {{ $errors->has('question') ? ' has-error' : '' }}">

                          <strong>{{ $errors->first('question') }}</strong>

                        </span>

                    @endif

                  </div>

                   <span style="display: none" id="errormultiple" class="error">Please Enter question  And two Option Option A and Option B</span>
                   <span style="display: none" id="errormultipleanswer" class="error">Please fil two Option Option A and Option B</span>

                  <div class="form-group">

                    <label>Option A</label>

                    <input type="text" class="form-control" name="answer[]" placeholder="Enter Option" id="optiona" required>

                    @if ($errors->has('answer'))

                        <span class="help-block {{ $errors->has('answer') ? ' has-error' : '' }}">

                          <strong>{{ $errors->first('answer') }}</strong>

                        </span>

                    @endif

                  </div>

                  <div class="form-group">

                    <label>Option B</label>

                    <input type="text" class="form-control" name="answer[]" placeholder="Enter Option" id="optionb" required>

                    @if ($errors->has('answer'))

                        <span class="help-block {{ $errors->has('answer') ? ' has-error' : '' }}">

                          <strong>{{ $errors->first('answer') }}</strong>

                        </span>

                    @endif

                  </div>

                  <div class="form-group">

                    <label>Option C</label>

                    <input type="text" class="form-control" name="answer[]" placeholder="Enter Option">

                    @if ($errors->has('answer'))

                        <span class="help-block {{ $errors->has('answer') ? ' has-error' : '' }}">

                          <strong>{{ $errors->first('answer') }}</strong>

                        </span>

                    @endif

                  </div>

                  <div class="form-group">

                    <label>Option D</label>

                    <input type="text" class="form-control" name="answer[]" placeholder="Enter Option">

                    @if ($errors->has('answer'))

                        <span class="help-block {{ $errors->has('answer') ? ' has-error' : '' }}">

                          <strong>{{ $errors->first('answer') }}</strong>

                        </span>

                    @endif

                  </div>

                   <div class="form-group">

                    <label>Option E</label>

                    <input type="text" class="form-control" name="answer[]" placeholder="Enter Option">

                    @if ($errors->has('answer'))

                        <span class="help-block {{ $errors->has('answer') ? ' has-error' : '' }}">

                          <strong>{{ $errors->first('answer') }}</strong>

                        </span>

                    @endif

                  </div>
                   <div class="form-group">

                    <label>Option F</label>

                    <input type="text" class="form-control" name="answer[]" placeholder="Enter Option">

                    

                    @if ($errors->has('answer'))

                        <span class="help-block {{ $errors->has('answer') ? ' has-error' : '' }}">

                          <strong>{{ $errors->first('answer_d') }}</strong>

                        </span>

                    @endif

                  </div>

                  
                  
                </div>

                <!-- /.col -->

              </div>

            </div>

            <div class="box-footer">

              <button type="submit" class="btn btn-primary" onClick="return empty()">Submit</button>

            </div>

            <!-- /.row -->

          </form>

          </div>

          <!-- /.box-body -->

        </div>

        <!-- /.box -->

      </div>

      <!-- /.row -->



    </section>

    <!-- /.content -->

  </div>

  <!-- /.content-wrapper -->

@endsection



@section('current_page_js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
<script>
  jQuery("#studentForm").validate({
       rules: {
            'question': {
                required: true
            },
            'answer[]': {
                required: true
            },
            
        },
      messages:{
      'question':{
        required:'Enter Quation'
      },
      'answer[]':{
        minlength: 2,
        required:'Enter Answer'
      },
      
    }
    });
</script>
<script>
function empty() {
  var answer = $("input[name='answer[]']")
              .map(function(){return $(this).val();}).get();
  var a       = $("#optiona").val();
  var quation = $("#optiona").val();
  var b = $("#optionb").val();
  if (quation == null || a == null || b =='')
   {
         $("#errormultiple").css("display", "block") 
         return false; 
   }
   else if(a == null || b =='')
   {
      $("#errormultipleanswer").css("display", "block") 
         return false; 
   }
   else
   {
     
     return true;  

   }
  }
 
</script>

@endsection

