@extends('admin.layout.layout')

@section('title', 'Add Question')



@section('current_page_css')

@endsection

@section('content')

 <!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">

    <!-- Content Header (Page header) -->

    <section class="content-header">

      <h1>Add Question </h1>

      <ol class="breadcrumb">

        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

        <li class="active">Question</li>

      </ol>

    </section>

    <!-- Main content -->

    <section class="content">

      @if ($message = Session::get('message'))

       <div class="alert alert-success alert-block">  

        <button type="button" class="close" data-dismiss="alert">×</button> 

        <strong>{{ $message }}</strong>

      </div>

      @endif

    

     <!-- Small boxes (Stat box) -->

      <div class="row">

        <div class="col-xs-12">

          <!-- SELECT2 EXAMPLE -->

        <div class="box box-default">

          <div class="box-header">

             <div class="box-tools pull-right">

                <a href="{{url('admin/question_list')}}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Back </a>

            </div>

          </div>

          <!-- /.box-header -->

          <form action="{{url('/admin/submit_questiontruefalse')}}" id="studentForm" method="POST" enctype="multipart/form-data">

            <input type="hidden" name="quns_type" value="2">

                    <div class="col-md-3 wd-15">
                        <div class="box-qa">
                            <a href="{{ url('admin/add_question')}}">
                            <p>Radio Choice</p>
                            </a>
                        </div>
                     </div>
        
                    <div class="col-md-3 wd-15">
                      <div class="box-qa">
                          <a href="{{ url('admin/addquestiontruefalse')}}">
                          <p style="color:#de3e00">True False</p>
                          </a>
                      </div>
                    </div>
        
                    <!-- <div class="col-md-2 wd-15">
                      <div class="box-qa">
                          <a href="{{ url('admin/addquestionselect')}}">
                          <p>Select</p>
                          </a>
                      </div>
                    </div> -->
          
                    <div class="col-md-3 wd-15">
                      <div class="box-qa">
                          <a href="{{ url('admin/addquestionInsertText')}}">
                          <p>Insert Text</p>
                          </a>
                      </div>
                    </div>


            <input type="hidden" name="_token" id="csrf-token" value="{{csrf_token()}}" />

            <div class="box-body">

              <div class="row">

                <div class="col-md-6">
                  <div class="form-group">

                    

                    <label>Question</label>

                    <textarea class="form-control" name="question" placeholder="Enter Question"></textarea>

                    @if ($errors->has('question'))

                        <span class="help-block {{ $errors->has('question') ? ' has-error' : '' }}">

                          <strong>{{ $errors->first('question') }}</strong>

                        </span>

                    @endif

                  </div>

                  <div class="form-group">

                    <label>True</label>

                    <input type="radio" name="ansvalue" id="ansvalue" value="true" checked>

                  </div>

                  <div class="form-group">

                    <label>False</label>

                    <input type="radio" name="ansvalue" id="ansvalue" value="false">

                  </div>
                  
                  
                </div>

                <!-- /.col -->

              </div>

            </div>

            <div class="box-footer">

              <button type="submit" class="btn btn-primary">Submit</button>

            </div>

            <!-- /.row -->

          </form>

          </div>

          <!-- /.box-body -->

        </div>

        <!-- /.box -->

      </div>

      <!-- /.row -->



    </section>

    <!-- /.content -->

  </div>

  <!-- /.content-wrapper -->

@endsection



@section('current_page_js')

<script type="text/javascript">

 

</script>

@endsection

