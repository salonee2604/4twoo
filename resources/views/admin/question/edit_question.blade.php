@extends('admin.layout.layout')
@section('title', 'Edit Question')

@section('current_page_css')
@endsection


@section('content')
 <style>
  .error {
  color: red;
  margin-left: 5px;
}

</style>
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Edit question
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">Question</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      @if ($message = Session::get('message'))
       <div class="alert alert-success alert-block">  
        <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong>{{ $message }}</strong>
      </div>
      @endif

      @if ($message = Session::get('error'))
      <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong>{{ $message }}</strong>
      </div>
      @endif

      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-xs-12">
          <!-- SELECT2 EXAMPLE -->
        <div class="box box-default">
          <div class="box-header pull-right">
                <a href="{{url('admin/question_list')}}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Back </a>
          </div>
          <!-- /.box-header -->
          <form action="{{url('/admin/update_question')}}" method="POST"
           enctype="multipart/form-data" id="studentForm">
            <input type="hidden" name="_token" id="csrf-token" value="{{csrf_token()}}" />
            <div class="box-body">
              <div class="row">
                <div class="col-md-6">
                  <input type="hidden" class="form-control" name="question_id" value="{{(!empty($question_info->id) ? $question_info->id : '')}}">
                  <div class="form-group">
                    <label>Question</label>
                    <input type="text" class="form-control" name="title" value="{{(!empty($question_info->question) ? $question_info->question : '')}}" placeholder="Enter Question">
                  </div>
                  <?php  
                    $implodeanswer = explode(",",$question_info->answer);
                   
                   
                
                 ?>
                 <span style="display: none" id="errormultiple" class="error">Please Enter question  Option A and Option B</span>
                  <div class="form-group">
                    <label>Option A</label>
                    <input type="text" class="form-control" name="answer[]" placeholder="Enter Option" id="optiona" value="{{(!empty($implodeanswer[0]) ? $implodeanswer[0] : '')}}">
                    @if ($errors->has('answer'))
                      <span class="help-block {{ $errors->has('answer') ? ' has-error' : '' }}">
                        <strong>{{ $errors->first('answer') }}</strong>
                      </span>
                    @endif
                  </div>
                  <div class="form-group">
                    <label>Option B</label>
                    <input type="text" class="form-control" name="answer[]" placeholder="Enter Option" id="optionb" value="{{(!empty($implodeanswer[1]) ? $implodeanswer[1] : '')}}">
                    @if ($errors->has('answer'))
                      <span class="help-block {{ $errors->has('answer') ? ' has-error' : '' }}">
                        <strong>{{ $errors->first('answer') }}</strong>
                      </span>
                    @endif
                  </div>
                  <div class="form-group">
                    <label>Option C</label>
                    <input type="text" class="form-control" name="answer[]" placeholder="Enter Option" value="{{(!empty($implodeanswer[2]) ? $implodeanswer[2] : '')}}">
                    @if ($errors->has('answer'))
                      <span class="help-block {{ $errors->has('answer') ? ' has-error' : '' }}">
                        <strong>{{ $errors->first('answer') }}</strong>
                      </span>
                    @endif
                  </div>
                  <div class="form-group">
                    <label>Option D</label>
                    <input type="text" class="form-control" name="answer[]" placeholder="Enter Option" value="{{(!empty($implodeanswer[3]) ? $implodeanswer[3] : '')}}">
                    @if ($errors->has('answer'))
                      <span class="help-block {{ $errors->has('answer') ? ' has-error' : '' }}">
                        <strong>{{ $errors->first('answer') }}</strong>
                      </span>
                    @endif
                  </div>
             
                <div class="form-group">
                    <label>Option E</label>
                    <input type="text" class="form-control" name="answer[]" placeholder="Enter Option" value="{{(!empty($implodeanswer[4]) ? $implodeanswer[4] : '')}}">
                    @if ($errors->has('answer'))
                      <span class="help-block {{ $errors->has('answer') ? ' has-error' : '' }}">
                        <strong>{{ $errors->first('answer') }}</strong>
                      </span>
                    @endif
                  </div>
                   <div class="form-group">
                    <label>Option F</label>
                    <input type="text" class="form-control" name="answer[]" placeholder="Enter Option" value="{{(!empty($implodeanswer[5]) ? $implodeanswer[5] : '')}}">
                    @if ($errors->has('answer'))
                      <span class="help-block {{ $errors->has('answer') ? ' has-error' : '' }}">
                        <strong>{{ $errors->first('answer') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>
              </div>
            </div>
            <div class="box-footer">
              <button type="submit" class="btn btn-primary" onClick="return empty()">Submit</button>
            </div>
            <!-- /.row -->
          </form>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
@section('current_page_js')
<script type="text/javascript">
 $(function() {
    $('.multiselect-ui').multiselect({
        includeSelectAllOption: true
    });
});
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>

<script>
  jQuery("#studentForm").validate({
       rules: {
            'question': {
                required: true
            },
            'answer[]': {
                required: true
            },
            
        },
      messages:{
      'question':{
        required:'Enter Quation'
      },
      'answer[]':{
        minlength: 2,
        required:'Enter Answer'
      },
      
    }
    });
</script>
<script>
function empty() {
  var answer = $("input[name='answer[]']")
              .map(function(){return $(this).val();}).get();
  var a = $("#optiona").val();
  var b = $("#optionb").val();
  if (a == null || b =='')
   {
         $("#errormultiple").css("display", "block") 
         return false; 
   }
   else
   {
     
     return true;  

   }
  }
 
</script>


@endsection