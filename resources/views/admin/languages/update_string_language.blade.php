@extends('admin.layout.layout')
@section('title', 'Add Language')
@section('current_page_css')
@endsection
@section('current_page_js')
<script type="text/javascript">
    $('#languageForm').validate({
        // initialize the plugin
        rules: {
            name: {
                required: true
            },
            code: {
                required: true
            }
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
</script>
@endsection
@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1>Update String Languages</h1>
    </section>
    <section class="content">
        @if ($message = Session::get('message'))
        <div class="alert alert-success alert-block">  
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message}}</strong>
        </div>
        @endif
        @if ($message = Session::get('error'))
        <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message}}</strong>
        </div>
        @endif
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-xs-12">
                <!-- SELECT2 EXAMPLE -->
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h5 class="box-title">Update string</h5>
                        <div class="box-tools pull-right">
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <?php
                    $arr = !empty($langinfo->lang_content) ? json_decode($langinfo->lang_content) : '';
                    ?>
                    <form method="POST" action="{{url('/admin/saveupdatestring')}}" accept-charset="UTF-8" novalidate="" name="languagesettingsfrm" enctype="multipart/form-data">
                        <input type="hidden" name="_token" id="csrf-token" value="{{csrf_token()}}" />
                        <input type="hidden" name="language_id" id="language_id" value="<?php echo!empty($langinfo->id) ? $langinfo->id : '' ?>" />
                        <div class="box-body">
                            <div class="row">			
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Dashboard</label>
                                        <input type="text" name="dashboard" value="<?php echo!empty($arr->dashboard) ? $arr->dashboard : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label>My Schedule</label>
                                        <input type="text" name="my_schedule" value="<?php echo!empty($arr->my_schedule) ? $arr->my_schedule : '' ?>" class="form-control"  required="true">
                                    </div>
                                </div>
                            </div>	
                            <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label>My Lesson</label>
                                        <input type="text" name="my_lesson" value="<?php echo!empty($arr->my_lesson) ? $arr->my_lesson : '' ?>" class="form-control"  required="true">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>My Teacher</label>
                                        <input type="text" name="my_teacher" value="<?php echo!empty($arr->my_teacher) ? $arr->my_teacher : '' ?>" class="form-control" required="true">
                                    </div>
                                </div>	
                            </div>	
                            <div class="row">			
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>My CURRICULUM</label>
                                        <input type="text" name="my_curriculm" value="<?php echo!empty($arr->my_curriculm) ? $arr->my_curriculm : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="success">My profile</label>
                                        <input type="text" name="my_profile" value="<?php echo!empty($arr->my_profile) ? $arr->my_profile : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>
                            </div>	
                            <div class="row">			
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="success">HOW IT WORKS</label>
                                        <input type="text" name="how_it_work" value="<?php echo!empty($arr->how_it_work) ? $arr->how_it_work : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>	
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>MY CLASSES</label>
                                        <input type="text" name="my_classes" value="<?php echo!empty($arr->my_classes) ? $arr->my_classes : '' ?>" class="form-control"  required="true">
                                    </div>
                                </div>	
                            </div>	
                            <div class="row">			
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="success">MY STUDENTS</label>
                                        <input type="text" name="my_student" value="<?php echo!empty($arr->my_student) ? $arr->my_student : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>	
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Grade</label>
                                        <input type="text" name="grade" value="<?php echo!empty($arr->grade) ? $arr->grade : '' ?>" class="form-control"  required="true">
                                    </div>
                                </div>	
                            </div>	
                            <div class="row">			
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="success">Upcoming</label>
                                        <input type="text" name="upcoming" value="<?php echo!empty($arr->upcoming) ? $arr->upcoming : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>	
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>History</label>
                                        <input type="text" name="history" value="<?php echo!empty($arr->history) ? $arr->history : '' ?>" class="form-control"  required="true">
                                    </div>
                                </div>	
                            </div>	
                            <div class="row">			
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="success">STUDENTS</label>
                                        <input type="text" name="students" value="<?php echo!empty($arr->students) ? $arr->students : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>	
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>History Lesson</label>
                                        <input type="text" name="history_lesson" value="<?php echo!empty($arr->history_lesson) ? $arr->history_lesson : '' ?>" class="form-control"  required="true">
                                    </div>
                                </div>	
                            </div>	 
                            <div class="row">			
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="success">Pending Lesson</label>
                                        <input type="text" name="pending_lesson" value="<?php echo!empty($arr->pending_lesson) ? $arr->pending_lesson : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>	
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Upcoming Lesson</label>
                                        <input type="text" name="upcoming_lesson" value="<?php echo!empty($arr->upcoming_lesson) ? $arr->upcoming_lesson : '' ?>" class="form-control"  required="true">
                                    </div>
                                </div>	
                            </div>
                            <div class="row">			
                               <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Curriculum</label>
                                        <input type="text" name="curriculum" value="<?php echo!empty($arr->curriculum) ? $arr->curriculum : '' ?>" class="form-control"  required="true">
                                    </div>
                                </div>	
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Invite Student</label>
                                        <input type="text" name="invite_student" value="<?php echo!empty($arr->invite_student) ? $arr->invite_student : '' ?>" class="form-control"  required="true">
                                    </div>
                                </div>	
                            </div>
                             <div class="row">			
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Send Message</label>
                                        <input type="text" name="send_message" value="<?php echo!empty($arr->send_message) ? $arr->send_message : '' ?>" class="form-control"  required="true">
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label>My Grade</label>
                                        <input type="text" name="my_grade" value="<?php echo!empty($arr->my_grade) ? $arr->my_grade : '' ?>" class="form-control"  required="true">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="success">Student Name</label>
                                        <input type="text" name="student_name" value="<?php echo!empty($arr->student_name) ? $arr->student_name : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Users</label>
                                        <input type="text" name="users" value="<?php echo!empty($arr->users) ? $arr->users : '' ?>" class="form-control"  required="true">
                                    </div>
                                </div>
                            </div>
                           
                             <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="success">SNo</label>
                                        <input type="text" name="sno" value="<?php echo!empty($arr->sno) ? $arr->sno : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="success">Date</label>
                                        <input type="text" name="date" value="<?php echo!empty($arr->date) ? $arr->date : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>
                             </div>
                            <div class="row">			
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="success">From</label>
                                        <input type="text" name="from" value="<?php echo!empty($arr->from) ? $arr->from : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>	
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Until</label>
                                        <input type="text" name="until" value="<?php echo!empty($arr->until) ? $arr->until : '' ?>" class="form-control"  required="true">
                                    </div>
                                </div>	
                            </div>
                            <div class="row">			
                               	<div class="col-md-6">
                                    <div class="form-group">
                                        <label for="success">Description</label>
                                        <input type="text" name="description" value="<?php echo!empty($arr->description) ? $arr->description : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>	
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Student Lesson</label>
                                        <input type="text" name="student_lession" value="<?php echo!empty($arr->student_lession) ? $arr->student_lession : '' ?>" class="form-control"  required="true">
                                    </div>
                                </div>	
                            </div>
                            <div class="row">			
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Meeting Status</label>
                                        <input type="text" name="meeting_status" value="<?php echo!empty($arr->meeting_status) ? $arr->meeting_status : '' ?>" class="form-control"  required="true">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Recording</label>
                                        <input type="text" name="recording" value="<?php echo!empty($arr->recording) ? $arr->recording : '' ?>" class="form-control"  required="true">
                                    </div>
                                </div>
                            </div>
                           <div class="row">			
                               	 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="success">Start</label>
                                        <input type="text" name="start" value="<?php echo!empty($arr->start) ? $arr->start : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Create</label>
                                        <input type="text" name="create" value="<?php echo!empty($arr->create) ? $arr->create : '' ?>" class="form-control"  required="true">
                                    </div>
                                </div>	
                            </div>
                            <div class="row">			
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="success">Not Start</label>
                                        <input type="text" name="not_start" value="<?php echo!empty($arr->not_start) ? $arr->not_start : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>	
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Play</label>
                                        <input type="text" name="play" value="<?php echo!empty($arr->play) ? $arr->play : '' ?>" class="form-control"  required="true">
                                    </div>
                                </div>	
                            </div>
                            <div class="row">			
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="success">Cancel</label>
                                        <input type="text" name="cancel" value="<?php echo!empty($arr->cancel) ? $arr->cancel : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>	
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>This session have no recoding</label>
                                        <input type="text" name="session_no_recoding" value="<?php echo!empty($arr->session_no_recoding) ? $arr->session_no_recoding : '' ?>" class="form-control"  required="true">
                                    </div>
                                </div>	
                            </div>
                               <div class="row">			
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="success">Name</label>
                                        <input type="text" name="name" value="<?php echo!empty($arr->name) ? $arr->name : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>	
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>EMAIL</label>
                                        <input type="text" name="email" value="<?php echo!empty($arr->email) ? $arr->email : '' ?>" class="form-control"  required="true">
                                    </div>
                                </div>	
                            </div>
                            <div class="row">			
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>First Name</label>
                                        <input type="text" name="first_name" value="<?php echo!empty($arr->first_name) ? $arr->first_name : '' ?>" class="form-control"  required="true">
                                    </div>
                                </div>	
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="success">Last Name</label>
                                        <input type="text" name="last_name" value="<?php echo!empty($arr->last_name) ? $arr->last_name : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>
                            </div>
                            <div class="row">			
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="success">User Name</label>
                                        <input type="text" name="user_name" value="<?php echo!empty($arr->user_name) ? $arr->user_name : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>	
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Gender</label>
                                        <input type="text" name="gender" value="<?php echo!empty($arr->gender) ? $arr->gender : '' ?>" class="form-control"  required="true">
                                    </div>
                                </div>	
                            </div>
                            <div class="row">			
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="success">Image</label>
                                        <input type="text" name="image" value="<?php echo!empty($arr->image) ? $arr->image : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>	
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Edit user</label>
                                        <input type="text" name="edit_user" value="<?php echo!empty($arr->edit_user) ? $arr->edit_user : '' ?>" class="form-control"  required="true">
                                    </div>
                                </div>	
                            </div>
                            <div class="row">			
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="success">Update</label>
                                        <input type="text" name="update" value="<?php echo!empty($arr->update) ? $arr->update : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>	
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Select File</label>
                                        <input type="text" name="select_file" value="<?php echo!empty($arr->select_file) ? $arr->select_file : '' ?>" class="form-control"  required="true">
                                    </div>
                                </div>	
                            </div>
                            <div class="row">			
                               <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Curriculum List</label>
                                        <input type="text" name="curriculum_list" value="<?php echo!empty($arr->curriculum_list) ? $arr->curriculum_list : '' ?>" class="form-control"  required="true">
                                    </div>
                                </div>	
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Users</label>
                                        <input type="text" name="users" value="<?php echo!empty($arr->users) ? $arr->users : '' ?>" class="form-control"  required="true">
                                    </div>
                                </div>	
                            </div>
                            <div class="row">			
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Invite Class</label>
                                        <input type="text" name="invite_class" value="<?php echo!empty($arr->invite_class) ? $arr->invite_class : '' ?>" class="form-control"  required="true">
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="success">Delete</label>
                                        <input type="text" name="delete" value="<?php echo!empty($arr->delete) ? $arr->delete : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>
                            </div>
                            <div class="row">			
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="success">Edit</label>
                                        <input type="text" name="edit" value="<?php echo!empty($arr->edit) ? $arr->edit : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>	
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Add user</label>
                                        <input type="text" name="add_user" value="<?php echo!empty($arr->add_user) ? $arr->add_user : '' ?>" class="form-control"  required="true">
                                    </div>
                                </div>	
                            </div>
                             <div class="row">			
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="success">Teacher Name</label>
                                        <input type="text" name="teacher_name" value="<?php echo!empty($arr->teacher_name) ? $arr->teacher_name : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>	
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Curriculum List</label>
                                        <input type="text" name="curriculum_list" value="<?php echo!empty($arr->curriculum_list) ? $arr->curriculum_list : '' ?>" class="form-control"  required="true">
                                    </div>
                                </div>	
                            </div>
                             <div class="row">			
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Course Status</label>
                                        <input type="text" name="course_status" value="<?php echo!empty($arr->course_status) ? $arr->course_status : '' ?>" class="form-control"  required="true">
                                    </div>
                                </div>	
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="success">Message Class</label>
                                        <input type="text" name="message_class" value="<?php echo!empty($arr->message_class) ? $arr->message_class : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>
                            </div>
                          <div class="row">			
                              	 <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Title</label>
                                        <input type="text" name="title" value="<?php echo!empty($arr->title) ? $arr->title : '' ?>" class="form-control"  required="true">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Add</label>
                                        <input type="text" name="add" value="<?php echo!empty($arr->add) ? $arr->add : '' ?>" class="form-control"  required="true">
                                    </div>
                                </div>	
                            </div>
                           <div class="row">			
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="text" name="password" value="<?php echo!empty($arr->password) ? $arr->password : '' ?>" class="form-control"  required="true">
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Forgot Password</label>
                                        <input type="text" name="forgot_password" value="<?php echo!empty($arr->forgot_password) ? $arr->forgot_password : '' ?>" class="form-control"  required="true">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">			
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Current password</label>
                                        <input type="text" name="current_password" value="<?php echo!empty($arr->current_password) ? $arr->current_password : '' ?>" class="form-control"  required="true">
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Confirm Password</label>
                                        <input type="text" name="confirm_password" value="<?php echo!empty($arr->confirm_password) ? $arr->confirm_password : '' ?>" class="form-control"  required="true">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="success">Login</label>
                                        <input type="text" name="login" value="<?php echo!empty($arr->login) ? $arr->login : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>	
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="success">Edit profile</label>
                                        <input type="text" name="edit_profile" value="<?php echo!empty($arr->edit_profile) ? $arr->edit_profile : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>	
                            </div>	
                            <div class="row">			
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="success">Back</label>
                                        <input type="text" name="back" value="<?php echo!empty($arr->back) ? $arr->back : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>	
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>DOB</label>
                                        <input type="text" name="dob" value="<?php echo!empty($arr->dob) ? $arr->dob : '' ?>" class="form-control"  required="true">
                                    </div>
                                </div>	
                            </div>
                            <div class="row">			
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="success">Upcoming Session</label>
                                        <input type="text" name="upcoming_session" value="<?php echo!empty($arr->upcoming_session) ? $arr->upcoming_session : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>	
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Unread Notification</label>
                                        <input type="text" name="unread_notification" value="<?php echo!empty($arr->unread_notification) ? $arr->unread_notification : '' ?>" class="form-control"  required="true">
                                    </div>
                                </div>	
                            </div> 
                            <div class="row">			
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="success">Pending Session</label>
                                        <input type="text" name="pendding_session" value="<?php echo!empty($arr->pendding_session) ? $arr->pendding_session : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>	
								
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Completed Session</label>
                                        <input type="text" name="completed_session" value="<?php echo!empty($arr->completed_session) ? $arr->completed_session : '' ?>" class="form-control"  required="true">
                                    </div>
                                </div>	
                            </div>
                            <div class="row">			
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="success">Canceled Session</label>
                                        <input type="text" name="canceled_session" value="<?php echo!empty($arr->canceled_session) ? $arr->canceled_session : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>	
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Classes</label>
                                        <input type="text" name="classes" value="<?php echo!empty($arr->classes) ? $arr->classes : '' ?>" class="form-control"  required="true">
                                    </div>
                                </div>	
                            </div> 
                            <div class="row">			
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="success">Tutors</label>
                                        <input type="text" name="tutors" value="<?php echo!empty($arr->tutors) ? $arr->tutors : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>	
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Number Of Subject</label>
                                        <input type="text" name="number_of_subject" value="<?php echo!empty($arr->number_of_subject) ? $arr->number_of_subject : '' ?>" class="form-control"  required="true">
                                    </div>
                                </div>	
                            </div>

                            <div class="row">			
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Upload File</label>
                                        <input type="text" name="upload_file" value="<?php echo!empty($arr->upload_file) ? $arr->upload_file : '' ?>" class="form-control"  required="true">
                                    </div>
                                </div>	
                              <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="success">Edit Picture</label>
                                        <input type="text" name="edit_picture" value="<?php echo!empty($arr->edit_picture) ? $arr->edit_picture : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>
                            </div>
                            <div class="row">			
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Message</label>
                                        <input type="text" name="message" value="<?php echo!empty($arr->message) ? $arr->message : '' ?>" class="form-control"  required="true">
                                    </div>
                                </div>	
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="success">Teacher</label>
                                        <input type="text" name="teacher" value="<?php echo!empty($arr->teacher) ? $arr->teacher : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>
                            </div>
                            <div class="row">	
                                   <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="success">Status</label>
                                        <input type="text" name="status" value="<?php echo!empty($arr->status) ? $arr->status : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="success">Lesson</label>
                                        <input type="text" name="lesson" value="<?php echo!empty($arr->lesson) ? $arr->lesson : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>	
                            </div>
                              <div class="row">	
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="success">Logout</label>
                                        <input type="text" name="logout" value="<?php echo!empty($arr->logout) ? $arr->logout : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>
                                   <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="success">Change Password</label>
                                        <input type="text" name="change_password" value="<?php echo!empty($arr->change_password) ? $arr->change_password : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>	
                            </div>
			 <div class="row">	
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="success">Action</label>
                                        <input type="text" name="action" value="<?php echo!empty($arr->action) ? $arr->action : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>
                                   <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="success">Active Students</label>
                                        <input type="text" name="active_students" value="<?php echo!empty($arr->active_students) ? $arr->active_students : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>	
                            </div>
                            <div class="row">	
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="success">Accept Request</label>
                                        <input type="text" name="accept_request" value="<?php echo!empty($arr->accept_request) ? $arr->accept_request : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>
                                   <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="success">Reject Request</label>
                                        <input type="text" name="reject_request" value="<?php echo!empty($arr->reject_request) ? $arr->reject_request : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>	
                            </div>
                            <div class="row">	
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="success">Yes</label>
                                        <input type="text" name="yes" value="<?php echo!empty($arr->yes) ? $arr->yes : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>
                                   <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="success">Submit</label>
                                        <input type="text" name="submit" value="<?php echo!empty($arr->submit) ? $arr->submit : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>	
                            </div>
                            <div class="row">	
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="success">Attending</label>
                                        <input type="text" name="attending" value="<?php echo!empty($arr->attending) ? $arr->attending : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>
                                   <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="success">Not Attending</label>
                                        <input type="text" name="not_attending" value="<?php echo!empty($arr->not_attending) ? $arr->not_attending : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>	
                            </div>
                            <div class="row">	
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="success">Select a grade</label>
                                        <input type="text" name="select_a_grade" value="<?php echo!empty($arr->select_a_grade) ? $arr->select_a_grade : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Unread Messages</label>
                                        <input type="text" name="unread_messages" value="<?php echo!empty($arr->unread_messages) ? $arr->unread_messages : '' ?>" class="form-control"  required="true">
                                    </div>
                                </div>
                            </div>
                            <div class="row">	
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="success">Recorder</label>
                                        <input type="text" name="recorder" value="<?php echo!empty($arr->recorder) ? $arr->recorder : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Start Recording</label>
                                        <input type="text" name="start_recording" value="<?php echo!empty($arr->start_recording) ? $arr->start_recording : '' ?>" class="form-control"  required="true">
                                    </div>
                                </div>
                            </div>
                            <div class="row">	
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="success">Search Placeholder </label>
                                        <input type="text" name="search_placeholder" value="<?php echo!empty($arr->search_placeholder) ? $arr->search_placeholder : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Lesson Description Placeholder</label>
                                        <input type="text" name="lesson_description_placeholder" value="<?php echo!empty($arr->lesson_description_placeholder) ? $arr->lesson_description_placeholder : '' ?>" class="form-control"  required="true">
                                    </div>
                                </div>
                            </div>
                            <div class="row">	
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="success">See All</label>
                                        <input type="text" name="see_all" value="<?php echo!empty($arr->see_all) ? $arr->see_all : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="success">Student Attendance</label>
                                        <input type="text" name="student_attendance" value="<?php echo!empty($arr->student_attendance) ? $arr->student_attendance : '' ?>"class="form-control" required="true">
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <div class="buttons text-center">
                            <button class="btn btn-lg btn-success button" ng-disabled="!formTopics.$valid">Update</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </section>
</div>
@endsection