@extends('admin.layout.layout')
@section('title', 'Edit Language')

@section('current_page_css')
@endsection

@section('current_page_js')
@endsection

@section('content')
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Languages
        <small>Edit Language</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Languages</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      @if ($message = Session::get('message'))
       <div class="alert alert-success alert-block">  
        <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong>{{ $message }}</strong>
      </div>
      @endif

      @if ($message = Session::get('error'))
      <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong>{{ $message }}</strong>
      </div>
      @endif

      @if ($message = Session::get('warning'))
      <div class="alert alert-warning alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong>{{ $message }}</strong>
      </div>
      @endif

      @if ($message = Session::get('info'))
      <div class="alert alert-info alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong>{{ $message }}</strong>
      </div>
      @endif

      @if ($errors->any())
      <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert">×</button> 
        <ul>
         @foreach ($errors->all() as $error)
         <li>{{ $error }}</li>
         @endforeach
       </ul>
     </div>
     @endif
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-xs-12">
          <!-- SELECT2 EXAMPLE -->
        <div class="box box-default">
          <div class="box-header with-border">
            <h3 class="box-title">Edit Language</h3>
            <div class="box-tools pull-right">
              <!--<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>-->
            </div>
          </div>
          <!-- /.box-header -->
          <form action="{{url('/admin/update_language')}}" id="languageForm" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="_token" id="csrf-token" value="{{csrf_token()}}" />
            <div class="box-body">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Language Name</label>
                    <input type="text" class="form-control" name="name" value="{{(!empty($lang_info->name) ? $lang_info->name : '')}}" placeholder="Enter Language Name">
                  </div>
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Language Code</label>
                    <input type="text" class="form-control" name="code" value="{{(!empty($lang_info->code) ? $lang_info->code : '')}}" placeholder="Enter Language Code">
                  </div>
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>IsEnabled</label>
                    <select class="form-control" name="isenabled">
                      <option value="Yes" {{ $lang_info->isenabled == 'Yes' ? 'selected' : '' }}>Yes</option>
                      <option value="No" {{ $lang_info->isenabled == 'No' ? 'selected' : '' }}>No</option>
                    </select>
                    </select>
                  </div>
                  <!-- /.form-group -->                  <!--<div class="form-group">                    <label>IsDefault</label>                    <select class="form-control" name="isdefault">                      <option value="Yes" {{ $lang_info->isdefault == 'Yes' ? 'selected' : '' }}>Yes</option>                      <option value="No" {{ $lang_info->isdefault == 'No' ? 'selected' : '' }}>No</option>                    </select>                  </div>-->                  <!-- /.form-group -->                  <input type="hidden" class="form-control" name="lang_id" value="{{(!empty($lang_info->id) ? $lang_info->id : '')}}">                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
              </div>
            </div>
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            <!-- /.row -->
          </form>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection