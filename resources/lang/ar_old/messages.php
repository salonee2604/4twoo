<?php
return [
	
	'home' => 'Accueil',
	'dashboard' => 'Tableau de bord',
    'my_schedule' => 'Mon emploi du temps',
    'my_lesson' => 'Ma leçon',
	'my_teacher' => 'Mon professeur',
    'my_curriculm' => 'Mon CURRICULUM',
    'my_profile' => 'Mon profil',
    'my_classes' => 'MES CLASSES',
    'my_student' => 'MES ÉTUDIANTS',
    'how_it_work'=> 'COMMENT ÇA FONCTIONNE',
    'grade'=> 'Classe',
	'student'=> 'Étudiants',
	'invite_student'=> 'INVITER UN ÉTUDIANT',
	'invite_class'=> 'INVITER LA CLASSE',
	'send_message'=> 'ENVOYER LE MESSAGE',
	'message'=> 'MESSAGE',
	'upcoming'=> 'A venir',
	'history'=> "L'histoire",
	'lesson'=> 'Leçon',
	'history_lesson'=> "Leçon d'histoire",
	'pending_lesson'=> 'Leçon en attente',
	'upcoming_lesson'=> 'Prochaine leçon',
    'curriculum'=> 'Curriculum',
    'curriculum_list'=> 'Liste des programmes',
	'description'=> 'Description',
    'until'=> "Jusqu'à ce que",
    'from'=> 'De',
    'gender'=> 'Le sexe',
    'my_grade'=> 'Ma classe',
    'first_name'=> 'Prénom',
    'last_name'=> 'Nom de famille',
    'email'=> 'Email',
	'user_name'=> "Nom d'utilisateur",
    'dob'=> 'DOB',
    'password'=> 'mot de passe',
    'back'=> 'arrière',
	'edit_picture'=> "Modifier l'image",
	'view_schedule'=> 'Voir le calendrier',
	'account_settings'=> 'Paramètres du compte',
    'no_notification'=> 'Aucune notification disponible',
	'edit_personal_details'=> 'Modifier les informations personnelles',
	'no_teacher_available'=> 'Aucun enseignant disponible',
	
    'logout'=> 'Logout',
];