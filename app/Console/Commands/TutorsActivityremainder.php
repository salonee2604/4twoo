<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Helpers\Helper;
use App\Common;
use Mail;

class TutorsActivityremainder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tutors:activityremainder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This Command will send email to favorite activty remainder.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**  Execute the console command. @return mixed */
    public function handle() {

        $getJoinStudent = DB::table('schedule')
                         ->select('schedule.*','grades.grade')
                        //->Join('lesson_student_status', 'lesson_student_status.lesson_id', '=', 'schedule.id')
                      //  ->leftJoin('bbb_meetings', 'bbb_meetings.lesson_id', '=', 'schedule.id')
                        ->Join('grades', 'grades.id', '=', 'schedule.grade')
                        //->where('lesson_student_status.status', '=', 1)
                        ->where('schedule.status', '=', 1)
                        ->where('schedule.lesson_date', '=', date('Y-m-d'))
                        ->where('schedule.time_from', '>=', date("H:i:s"))
                        ->get()->all();
      //  print_r($getJoinStudent);
      // die;
        
        if (!empty($getJoinStudent)) {

            foreach ($getJoinStudent as $results) {
                $time1 = explode(':', $results->time_from);
                $time2 = date("H:i:s");
                $time2 = explode(':', $time2);
                $hours1 = $time1[0];
                $hours2 = $time2[0];
                $mins1 = $time1[1];
                $hours = $hours2 - $hours1;
                $mins = 0;
                if ($hours < 0) {
                    
                } else {
                    if ($mins1 == 20) {

                        $checkReminderSent = DB::table('reminder')->where('lesson_id', '=', $results->id)->first();
                        if (empty($checkReminderSent)) {
                            $NotificationArr = array();

                            $tutor_id = $results->tutor_id;
                            $tutor_info = DB::table('users')->where('id', '=', $tutor_id)->first();

                            $tutor_name = !empty($tutor_info->first_name) ? $tutor_info->first_name . " " . $tutor_info->last_name : '';
                            $lessonDate = !empty($results->lesson_date) ? date('d-m-Y', strtotime($results->lesson_date)) : '';
                            $timeFrom = !empty($results->time_from) ? date('H:i a', strtotime($results->time_from)) : '';
                            $timeUntil = !empty($results->time_until) ? date('H:i a', strtotime($results->time_until)) : '';

                            $studentID = "";
                            if ($results->invite_type == 1) { //invite_type 1 - classs, 2 student
                                $query = DB::table('notification')
                                        ->join('users', 'users.id', '=', 'notification.user_id')
                                        ->join('schedule', 'schedule.id', '=', 'notification.schedule_id')
                                        ->select('notification.*', DB::raw('CONCAT(users.first_name," ", users.last_name) AS student_name'), 'users.email')
                                        ->where('notification.schedule_id', '=', $results->id);
                                $query->where(function ($query) {
                                    $query->where('notification.is_panding', '=', 0)
                                            ->orWhere('notification.is_panding', '=', 1);
                                });
                                $checkInvite = $query->get();
                                $LessonReminderStudent = DB::table('notification_format')->where(['notification_type' => 'LessonReminder', 'for_student' => 1])->first();
                                $LessonReminderEmailStudent = DB::table('email_formate')->where('email_type', '=', 'SessionReminderToStudent')->first();

                                $notify_msg_Student = str_replace("{0}", $results->grade, $LessonReminderStudent->notification_msg);
                                $notify_msg_Student = str_replace("{1}", $timeFrom, $notify_msg_Student);
                                $notify_msg_Student = str_replace("{2}", $timeUntil, $notify_msg_Student);

                                $subject_student = str_replace("{0}", $results->grade, $LessonReminderEmailStudent->subject);
                                $email_content_student = str_replace("{1}", $results->grade, $LessonReminderEmailStudent->body);
                                $email_content_student = str_replace("{2}", $lessonDate, $email_content_student);
                                $email_content_student = str_replace("{3}", $timeFrom, $email_content_student);
                                $email_content_student = str_replace("{4}", $timeUntil, $email_content_student);

                                if (!$checkInvite->isEmpty()) {
                                    foreach ($checkInvite as $k => $studentlist) {
                                        $studentID .= $studentlist->user_id . ',';

                                        $NotificationArr[] = array(
                                            'schedule_id' => $results->id,
                                            'user_id' => $studentlist->user_id,
                                            'notification_type' => 4,
                                            'notification_msg' => $notify_msg_Student,
                                            'notification_status' => 0,
                                            'format_id' => $LessonReminderStudent->id,
                                            'created_at' => date('Y-m-d H:i:s'),
                                            'updated_at' => date('Y-m-d H:i:s')
                                        );
                                        $email_content_student = str_replace("{0}", $studentlist->student_name, $email_content_student);
                                        $studentData = [
                                            'name' => $studentlist->student_name,
                                            'email' => $studentlist->email,
                                            'subject' => $subject_student,
                                            'content' => $email_content_student,
                                        ];

                                        if ($_SERVER['HTTP_HOST'] != 'localhost') {
                                            try {
                                                Helper::send_mail($studentData);
                                            } catch (Exception $e) {
                                                echo 'Message: ' . $e->getMessage();
                                            }
                                        }
                                    }
                                    trim($studentID, ',');
                                }
                            } else {

                                $query = DB::table('notification')
                                        ->join('users', 'users.id', '=', 'notification.user_id')
                                        ->select('notification.*', DB::raw('CONCAT(users.first_name," ", users.last_name) AS student_name'), 'users.email')
                                        ->where('schedule_id', '=', $results->id);
                                $query->where(function ($query) {
                                    $query->where('notification.is_panding', '=', 0)
                                            ->orWhere('notification.is_panding', '=', 1);
                                });
                                $checkInvite = $query->get()->first();


                                $LessonReminderStudent = DB::table('notification_format')->where(['notification_type' => 'LessonReminder', 'for_student' => 1])->first();
                                $LessonReminderEmailStudent = DB::table('email_formate')->where('email_type', '=', 'SessionReminderToStudent')->first();

                                $notify_msg_Student = str_replace("{0}", $results->grade, $LessonReminderStudent->notification_msg);
                                $notify_msg_Student = str_replace("{1}", $timeFrom, $notify_msg_Student);
                                $notify_msg_Student = str_replace("{2}", $timeUntil, $notify_msg_Student);

                                $NotificationArr[] = array(
                                    'schedule_id' => $results->id,
                                    'user_id' => $checkInvite->user_id,
                                    'notification_type' => 4,
                                    'notification_msg' => $notify_msg_Student,
                                    'notification_status' => 0,
                                    'format_id' => $LessonReminderStudent->id,
                                    'created_at' => date('Y-m-d H:i:s'),
                                    'updated_at' => date('Y-m-d H:i:s')
                                );
                                $studentID = $checkInvite->user_id;
                                $subject_student = str_replace("{0}", $results->grade, $LessonReminderEmailStudent->subject);

                                $email_content_student = str_replace("{0}", $checkInvite->student_name, $LessonReminderEmailStudent->body);
                                $email_content_student = str_replace("{1}", $results->grade, $email_content_student);
                                $email_content_student = str_replace("{2}", $lessonDate, $email_content_student);
                                $email_content_student = str_replace("{3}", $timeFrom, $email_content_student);
                                $email_content_student = str_replace("{4}", $timeUntil, $email_content_student);
                                $studentData = [
                                    'name' => $checkInvite->student_name,
                                    'email' => $checkInvite->email,
                                    'subject' => $subject_student,
                                    'content' => $email_content_student,
                                ];

                                if ($_SERVER['HTTP_HOST'] != 'localhost') {
                                    try {
                                        Helper::send_mail($studentData);
                                    } catch (Exception $e) {
                                        echo 'Message: ' . $e->getMessage();
                                    }
                                }
                            }

                            $LessonReminderTutor = DB::table('notification_format')->where(['notification_type' => 'LessonReminder', 'for_student' => 0])->first();
                            $notify_msg_tutor = str_replace("{0}", $results->grade, $LessonReminderTutor->notification_msg);
                            $notify_msg_tutor = str_replace("{1}", $timeFrom, $notify_msg_tutor);
                            $notify_msg_tutor = str_replace("{2}", $timeUntil, $notify_msg_tutor);
                            $NotificationArr[] = array(
                                'schedule_id' => $results->id,
                                'user_id' => $tutor_id,
                                'notification_type' => 4,
                                'notification_msg' => $notify_msg_tutor,
                                'notification_status' => 0,
                                'format_id' => $LessonReminderTutor->id,
                                'created_at' => date('Y-m-d H:i:s'),
                                'updated_at' => date('Y-m-d H:i:s')
                            );
                            DB::table('notification')->insert($NotificationArr);

                            $LessonReminderEmailTutor = DB::table('email_formate')->where('email_type', '=', 'SessionReminderToTutor')->first();
                            $subject_tutor = str_replace("{0}", $results->grade, $LessonReminderEmailTutor->subject);

                            $email_content_tutor = str_replace("{0}", $tutor_name, $LessonReminderEmailTutor->body);
                            $email_content_tutor = str_replace("{1}", $results->grade, $email_content_tutor);
                            $email_content_tutor = str_replace("{2}", $lessonDate, $email_content_tutor);
                            $email_content_tutor = str_replace("{3}", $timeFrom, $email_content_tutor);
                            $email_content_tutor = str_replace("{4}", $timeUntil, $email_content_tutor);
                            $tutorData = [
                                'name' => $tutor_info->first_name . ' ' . $tutor_info->last_name,
                                'email' => $tutor_info->email,
                                'subject' => $subject_tutor,
                                'content' => $email_content_tutor,
                            ];

                            if ($_SERVER['HTTP_HOST'] != 'localhost') {
                                try {
                                    Helper::send_mail($tutorData);
                                } catch (Exception $e) {
                                    echo 'Message: ' . $e->getMessage();
                                }
                            }

                            $reminder['lesson_id'] = $results->id;
                            $reminder['tutor_id'] = $tutor_id;
                            $reminder['student_id'] = $studentID;
                            $reminder_id = DB::table('reminder')->insertGetId($reminder);
                        }
                    }
                }
            }
        }
    }

}
