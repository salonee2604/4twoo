<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class FormateModel extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'notification_format';
    protected $fillable = [
        'notification_type','notification_msg','notification_icon','for_student','language','status','created_at','updated_at'
    ];
    public $timestamps = false;
}
