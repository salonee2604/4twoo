<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithMapping;

class StudentExport implements FromCollection, WithHeadings,ShouldAutoSize,WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return User::where('role_id', '=', 3)->get(['first_name','last_name','username', 'email', 'temp_password', 'grade', 'curriculum']);
    }
     public function map($row): array
    {
        return [
            $row['first_name'],
            $row['last_name'],
            $row['username'],
            $row['email'],
            $row['temp_password'],
            $row['grade'],
            $row['curriculum'],
        ];
    }
    public function headings(): array
        {
            return [
                'First Name',
                'Last Name',
                'UserName',  
                'Email',  
                'Password',  
                'Grade',  
                'Curriculum',  
            ];
        }
}
