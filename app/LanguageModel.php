<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class LanguageModel extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'languages';
    protected $fillable = [
        'name', 'code', 'icon', 'isenabled', 'isdefault'
    ];
    //public $timestamps = false;
}
