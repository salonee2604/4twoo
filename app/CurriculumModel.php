<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class CurriculumModel extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'curriculums';
    protected $fillable = [
        'curriculum'
    ];
    //public $timestamps = false;
}
