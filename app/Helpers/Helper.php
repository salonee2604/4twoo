<?php



namespace App\Helpers;



use DB,Mail;



class Helper {


    public static function encode($id) {
        return base64_encode($id);
    }


    public static function getParentCategoryById($parent_categoty_id) {

        return $category_info = DB::table('sub_category')->where(['id' => $parent_categoty_id])->first();

    }

    public static function decode($id) {
        return base64_decode($id);
    }

    public static function getSubCategoryById($subcategoty_id) {

        $subcategory = DB::table('sub_category')->where(['id' => $subcategoty_id])->first();

        if (!empty($subcategory)) {

            return $subcategory;

        } else {

            return false;

        }

    }



    public static function getCategoryById($categoty_id) {



        return $category_info = DB::table('category')->where(['id' => $categoty_id])->first();

    }



    public static function money_formate($number) {



        return env('CURRENCY_SYMBOL', '₺') . '&nbsp;' . number_format($number, 2, ".", ",");

    }



    /* display money in specified formate */



    public static function datetime_formate($date, $time = true) {



        $date = new DateTime($date);



        if ($time) {



            return $date->format('Y-m-d H:i:s');

        }



        return $date->format('Y-m-d');

    }



    /* check string is serialize or not */



    public static function is_serial($string) {



        return (@unserialize($string) !== false || $string == 'b:0;');

    }



    public static function date_formate($date) {

        $date = new DateTime($date);



        return $date->format('D d M Y');

    }



    public static function dateformate($date) {

        $date = date('d-M-Y', strtotime($date));



        return $date;

    }



    public static function setNotification($data) {



        return DB::table('notification')->insertGetId($data);

    }



    public static function getNotification($user_id) {

        return DB::table('notification')

                        ->leftJoin('notification_format', 'notification_format.id', '=', 'notification.format_id')

                        ->select('notification.id', 'notification.schedule_id', 'notification.user_id', 'notification.notification_type', 'notification.notification_msg', 'notification_format.notification_icon', 'notification.format_id')

                        ->where('notification.user_id', $user_id)

                        ->where('notification.notification_status', 0)

                        ->limit(6)

                        ->orderBy('notification.id', 'DESC')

                        ->get();

    }



    public static function getTotalNotification($user_id) {

        return $totalnotification = DB::table('notification')

                        ->where('notification.user_id', $user_id)

                        ->where('notification.notification_status', 0)

                        ->get()->count();

    }



    public static function send_mail($data) {

       
        Mail::send('sendmail', $data, function($message) use ($data) {

            $message->from('info@4twoo.com', '4Twoo');

            $message->to($data['email'])->subject($data['subject']);

        });

    }



    public static function getLogoImg() {



        return $info = DB::table('logo_file')->first();

    }

    public static function getDefaultLanguage() {

       return $languages = DB::table('languages')->where('isdefault',"Yes")->where('isenabled', "Yes")->get()->first();

    }



    public static function getLanguage() {



        return $languages = DB::table('languages')->where('isenabled', "Yes")->get()->all();

    }



    public static function getLanguageByID($languages_id) {

        $languages = DB::table('languages')->where('id', $languages_id)->get()->first();

        $name = !empty($languages->name) ? $languages->name : '';

        return $name;

    }



    public static function getLanguageByCode($code) {

        $languages = DB::table('languages')->where('code', $code)->get()->first();

        $name = !empty($languages->name) ? $languages->name : '';

        return $name;

    }



    public static function getActiveStudent($tutor_id) {



        $result = DB::table('member_grade')

                        ->select(DB::raw("GROUP_CONCAT(grade_id) as grade_id"))

                        ->where('member_grade.user_id', $tutor_id)

                        ->get()->first();

        $result->grade_id;

        if (!empty($result->grade_id)) {

            return DB::table('member_grade')

                            ->select('users.first_name', 'users.last_name')

                            ->join('users', 'users.id', '=', 'member_grade.user_id')

                            ->where('users.role_id', '=', 3)

                            ->where('users.status', '=', 1)

                            ->where('member_grade.status', '=', 1)

                            ->whereIn('grade_id', explode(",", $result->grade_id))

                            ->count();

        } else {

            return 0;

        }

    }



    public static function getTutorsCompleteSession($tutor_id) {

        return DB::table('schedule')

                        ->join('bbb_meetings', 'bbb_meetings.lesson_id', '=', 'schedule.id')

                        ->where('schedule.tutor_id', '=', $tutor_id)

                        ->where('bbb_meetings.is_create', '=', 1)

                        ->count();

    }



    public static function checkMediaContent($curriculum_id, $manage_media_id) {



        $result = DB::table('manage_media_curriculum')

                        ->where(['curriculum_id' => $curriculum_id, 'media_type' => $manage_media_id])

                        ->get()->all();

        if (empty($result)) {

            $flage = true;

        } else {

            $flage = false;

        }

        return $flage;

    }



    public static function getMessageCount($user_id) {

        return DB::table('chat_msg_status')

                        ->where('receiver_id', '=', $user_id)

                        ->where('chat_msg_status.is_read', '=', 0)

                        ->where('chat_msg_status.status', '=', 0)

                        ->count();

    }



    public static function getGroupMessageCount($user_id, $group_id) {

        return $result = DB::table('chat_msg_status')

                ->join('chat_msg', 'chat_msg.id', '=', 'chat_msg_status.chat_msg_id')

                ->where('receiver_id', '=', $user_id)

                ->where('chat_msg_status.status', '=', 0)

                ->where('chat_msg.group_id', '=', $group_id)

                ->count();

    }

    public static function getUserEmail($userID) {

      return  $result = DB::table('users')

                  ->select('users.first_name', 'users.email','users.role_id')

                  ->where('id', '=', $userID)

                   ->get()->first();

        

    }

	public static function getgrade($recorder_id) {

       $result = DB::table('recorder_grade')

						->join('grades', 'grades.id', '=', 'recorder_grade.grade_id')

					    ->select('grades.grade')

					    ->where('recorder_grade.recorder_id', '=', $recorder_id)

						->groupBy('recorder_grade.recorder_id')

					    ->get()->first();

						

			if(!empty($result->grade)){

				return $result->grade;

			}else{

				 return false;

			}

		

        

    }

	public static function getMenu() {

       $menu = DB::table('menu')->where('is_parent',0)->get();

		if(!empty($menu)){

			foreach($menu as $key=>$menuVal){

			

			   $childMenu = DB::table('menu')->where('is_parent',$menuVal->id)->get()->all();

			   if(!empty($childMenu)){

				   $menu[$key]->is_child = $childMenu; 

			   }else{

				   $menu[$key]->is_child = ""; 

			   }

			}

		}

	   return $menu;	

	}

	public static function checkPermission($menu_id,$user_id) {

       $result = DB::table('permission')

					    ->select('menu_id')

					    ->where('menu_id', '=', $menu_id)

					    ->where('user_id', '=', $user_id)

					    ->get()->first();

						

			if(!empty($result->menu_id)){

				return true;

			}else{

				 return false;

			}

	}

	public static function getAccessPath($uri,$user_id){

	   $result = DB::table('permission')

						 ->select('menu.url')

					    ->join('menu', 'menu.id', '=', 'permission.menu_id')

					    ->where('menu.url', '=', $uri)

					    ->where('permission.user_id', '=', $user_id)

					    ->get()->first();

						

			if(!empty($result->url)){

				return true;

			}else{

				 return false;

			}	

	}

	

	public static function is_url($uri)
    {

		if(preg_match( '/^(http|https):\\/\\/[a-z0-9_]+([\\-\\.]{1}[a-z_0-9]+)*\\.[_a-z]{2,5}'.'((:[0-9]{1,5})?\\/.*)?$/i' ,$uri)){

		  return $uri;

		}

		else{

			return false;

		}

    }

   

	

}



?>