<?php 
use App\Common;

/* display money in specified formate */
function money_formate($number){
	return  env('CURRENCY_SYMBOL', '₺').'&nbsp;'.number_format($number,2,".",",");
}

/* display money in specified formate */

function datetime_formate($date,$time=true){
	$date = new DateTime($date);
	if($time){
		return $date->format('Y-m-d H:i:s');
	}
	return $date->format('Y-m-d');
}

/* check string is serialize or not */

function is_serial($string) {
	return (@unserialize($string) !== false || $string == 'b:0;');
}

function date_formate($date){
	 
	 $date = new DateTime($date);
	 return $date->format('D d M Y');
}
 
function dateformate($date){
	 
	 $date = date('d-M-Y',strtotime($date));
	 return $date;
}
function setNotification($data){
   return DB::table('notification')->insertGetId($data);
}

function getNotificationCount($user_id){
	 
	return DB::table('schedule_students')
                    ->where('schedule_students.user_id',$user_id)
                    ->where('schedule_students.status',0)
                    ->get()->count();
} 
 
 function getNotification($user_id){
	
	$comman = new Common();
	return  $result =  $comman->notification($user_id);	
 }
 function getParentCategoryById($parent_categoty_id){
     return $category_info = DB::table('sub_category')->where(['parent_id'=>$parent_categoty_id])->first();	
 }
 function getCategoryById($categoty_id){
     return $category_info = DB::table('category')->where(['id'=>$categoty_id])->first();	
 }
 ?>