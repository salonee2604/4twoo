<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use Validator,DB,Helper;

use App\User;

use Illuminate\Validation\Rule;

use Twilio\Rest\Client;

use Session;

use Exception;

use File;

use App\Libraries\BigBlueButton;

use ZipArchive;

use Hash;

use Artisan; 



class HomeController extends Controller

{

    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function __construct()

    {

        //$this->middleware('auth');

    }



    /**

     * Show the application dashboard.

     *

     * @return \Illuminate\Contracts\Support\Renderable

     */

    public function index()

    {
        $data['stories'] = DB::table('success_story')->where('status', '=', 1)->get();
        $data['testimonials'] = DB::table('testimonial')->where('status', '=', 1)->get();
        return view('home')->with($data);

    } 

	public function logout(){

        Auth::logout();

       return redirect('/');

    }

    public function newsletter(Request $request)
    {
        $email = $request->input('subscribe_email');
        $data_insert = array(
            'email'=>$email,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        );
        $mailData = array(
            'email'=>$email,
            'subject'=>'Subcribe newsletter',
            'mailBody'=>'<html><body><b>Dear, User</b><br><br>Thank your for subcribe newsletter<br>Thanks<br><b>4Twoo Team</b></body></html>'
        );
        DB::table('newsletter')->insert($data_insert);
        Helper::send_mail($mailData); 
        $id = DB::getPdo()->lastInsertId();
        if($id){
            $data = array('success'=>true,'message'=>"Newsletter subcribe successfully");
        }else{
            $data = array('success'=>false,'message'=>'error');
        };
        return Response()->json($data);
    }

    public function contact()
    {
        return view('contact');
    }

    public function impressum()
    {
        return view('impressum');
    }

    public function privacy()
    {
        return view('privacy');
    }

    public function career()
    {
        return view('career');
    } 

    public function faq()
    {
        return view('faq');
    }

    public function success_story()
    {   $data['stories'] = DB::table('success_story')->where('status', '=', 1)->get();
        return view('success_story')->with($data);
    }

    public function success_details(Request $request)
    {
        $id = base64_decode($request->id); 
        $data['story'] = DB::table('success_story')->where('id', '=', $id)->where('status', '=', 1)->first();
        return view('success_details')->with($data);
    }

    public function question_answer($user_id = null)
    { 
        $data['user_id'] = $user_id;
        $data['question_answer'] = DB::table('questionnaire')->where('status', '=', 1)->get();

        return view('user/question_answer')->with($data);
    }

     public function submit_user_answer(Request $request)
    {
        
         

        $forminput  = $request->all();
       
        $uid = Auth::user()->id;
        if(!empty($forminput)){
          DB::table('user_answer')->where('user_id', $uid)->delete();
            $datam = $forminput['question_id'];
            foreach ($datam as $key => $value) 
            {
                  
                 if(!empty($forminput['answer'.$value]))
                 {
                    $answer_array     =  implode(",", $forminput['answer'.$value]);
                 }
                 else
                 {
                    $answer_array = "";
                 }

                 
                  if(!empty($answer_array))
                  {
                    if(!empty($forminput['mandatory_status'.$value]))  
                    {
                        $mandatory_status_arry =$forminput['mandatory_status'.$value];
                        $mandatory_status = $mandatory_status_arry[0];

                    }
                    else
                    {
                      $mandatory_status = 0;
                    }  
                    $applyjob = DB::table('user_answer')->insertGetId(
                    array('user_id' => $uid,            
                     'question_id' =>  $value,
                     'answer' => $answer_array,
                     'mandatory_status' =>$mandatory_status,
                     'created_at' =>  date('Y-m-d H:i:s'),
                     ));
                  }
                  else 
                  {
                       $applyjob = DB::table('user_answer')->insertGetId(
                    array('user_id' => $uid,            
                     'question_id' =>  $value,
                     'answer' => "",
                     'mandatory_status' =>"",
                     'created_at' =>  date('Y-m-d H:i:s'),
                     ));
                  }
                   
            }
            session::flash('message', 'Thank you for answering the question.');
            return redirect('/editquestion_answer');
        }
    }

    public function match($user_id = null)
    { 
      return view('user/matching_profiles');
    }

    public function editquestion_answer()
    {



        if (empty(Auth::user())) {
            return redirect('/');
        }
        else
        {
             $user_id = Auth::user()->id;
             $data['user_id'] = $user_id;
             $data['question_answer'] = DB::table('questionnaire')->where('status', '=', 1)->get();
             $data['edit_question_answer'] = DB::table('user_answer')->where('user_id', '=', $user_id)->get();
             return view('user/edit_question_answer')->with($data);;
        }

    }


    public function update_quation_answer(Request $request)
    {



        $forminput  = $request->all();
        $uid = Auth::user()->id;
        $forminput  = $request->all();
        $uid = Auth::user()->id;
        if(!empty($forminput)){
            
            DB::table('user_answer')->where('user_id', '=', $uid)->delete();
            $datam = $forminput['question_id'];
            foreach ($datam as $key => $value) 
            {
                  if(!empty($forminput['answer'.$value]))
                  {
                    $answer_array     =  implode(",", $forminput['answer'.$value]);
                  if(!empty($answer_array))
                  {
                    if(!empty($forminput['mandatory_status'.$value]))  
                    {
                        $mandatory_status_arry =$forminput['mandatory_status'.$value];
                        $mandatory_status = $mandatory_status_arry[0];

                    }
                    else
                    {
                        $mandatory_status = 0;
                    }  
                    $applyjob = DB::table('user_answer')->insertGetId(
                    array('user_id' => $uid,            
                     'question_id' =>  $value,
                     'answer' => $answer_array,
                     'mandatory_status' =>$mandatory_status,
                     'created_at' =>  date('Y-m-d H:i:s'),
                     ));
                  }
                  else 
                  {
                       $applyjob = DB::table('user_answer')->insertGetId(
                    array('user_id' => $uid,            
                     'question_id' =>  $value,
                     'answer' =>"",
                     'mandatory_status' =>0,
                     'created_at' =>  date('Y-m-d H:i:s'),
                     ));
                  }
                }
                else
                {
                  $applyjob = DB::table('user_answer')->insertGetId(
                    array('user_id' => $uid,            
                     'question_id' =>  $value,
                     'answer' =>"",
                     'mandatory_status' =>0,
                     'created_at' =>  date('Y-m-d H:i:s'),
                     ));
                }
                   
            }
            session::flash('message', 'Answer updated Successfully');
            return redirect('/editquestion_answer');
        }     

    }







}

