<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use DB, Mail, Hash, View, Session;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Helpers\Helper;
use App\Http\Controllers\API\MailSendController;
use Illuminate\Support\Facades\Password;

class ApiController extends Controller

{

  public function getUserDetails(Request $request)
  {
    $formdata = $request->all();
    $data     = User::where('id', $formdata['user_id'])->get();
    $answer = DB::table('user_answer')->where('user_id', '=', $formdata['user_id'])->first();
    if(!empty($answer))
    {
       $answer_status = 1;
    }
    else
    {
      $answer_status   = 0; 
    }
    $arr      = $data->toArray();

    $search = null;
    $replace = '""';
    array_walk($arr,
    function (&$v) use ($search, $replace){
        $v = str_replace($search, $replace, $v);    
        }                                                                     
    );
  
     $response = [
                'status' => "true",
                
                'message' => "User Details",
                
                'response' => array('data' =>$arr,'answer_status'=>$answer_status)
    ];
    return response()->json($response, 200); 
  }


   public function Login (Request $request) {
      
       
        $formdata = $request->all();
       
      
        if(empty($formdata['email'])){
           $response = [
                'status' => "false",
                
                'message' => "Username or email required",
                
                'response' => array('data' =>array() )
            ];
            return response()->json($response, 200);
        }
       elseif(empty($formdata['password'])){
            $response = [
                'status' => "false",
                
                'message' => "Password required",
                
                'response' => array('data' =>array() )
            ];
            return response()->json($response, 200);
        }
        else
        {

            $user = User::where('email', $formdata['email'])->orwhere('username', $formdata['email'])
                 ->orderBy('id', 'desc')->first();
            if ($user) 
            {
                if (Hash::check($request->password, $user->password)) {
                $data = User::where('email', $formdata['email'])->orwhere('username', $formdata['email'])
                 ->orderBy('id', 'desc')->get();
                 $arr = $data->toArray();
                    $search = null;
                    $replace = '""';
                    array_walk($arr,
                    function (&$v) use ($search, $replace){
                        $v = str_replace($search, $replace, $v);    
                        }                                                                     
                    );
                $response = [
                'status' => "true",
                
                'message' => "Login Successfully",
                
                'response' => array('data' =>$arr)
            ];
            return response()->json($response, 200);
            } 
            else 
            {

                    $response = [
                        'status' => "false",
                        
                        'message' => "Password mismatch",
                        
                        'response' => array('data' =>array())
                    ];
                    return response($response, 422);
            }

            } 
            else 
            {

                $response = [
                        'status' => "false",
                        
                        'message' => "User does not exist",
                        
                        'response' => array('data' =>array())
                    ];
                return response($response, 422);
              
            }


        }
    
   }



    public function registration(Request $request)
    { 

       $formdata = $request->all();
       $status = "false";
       $array = array();
       $array['response'] = (object) array();
        if(empty($formdata['first_name'])){
            $array['status'] = "false";
            $array['message'] = 'First name required'; 
        }elseif(empty($formdata['last_name'])){
          $array['status'] = "false";
          $array['message'] = 'Lastname required';  
        }
        elseif(empty($formdata['gender'])){
          $array['status'] = "false";
          $array['message'] = 'gender required';  
        }
        elseif(empty($formdata['dob'])){
          $array['status'] = "false";
          $array['message'] = 'dob required';  
        }elseif(empty($formdata['email'])){
          $array['status'] = "false";
          $array['message'] = 'email required';  
        }elseif(empty($formdata['password'])){
          $array['status'] = "false";
          $array['message'] = 'password required';  
        }elseif(empty($formdata['contact_number'])){
          $array['status'] = "false";
          $array['message'] = 'Contact Number required';  
        }else{
            $input = $request->all();
            $emailcheck = User::where('email',$input['email'])->first();
            if(!empty($emailcheck)){
                $array['status'] = "false";
                $array['message'] = 'Email Already exist.';
                return response()->json($array, 200);
            }
            if($formdata['password'] !== $formdata['confirm_pass'] ){
                $array['status'] = "false";
                $array['message'] = 'Password and confirm_pass not match.';
                return response()->json($array, 200);
            }
            $input['password'] = bcrypt($input['password']);
            $input['temp_password'] = $input['confirm_pass'];
            $input['contact_number'] = $input['contact_number'];
            $input['vrfn_code']= str_random(20);
            $input['status']= '1';
            $input['role_id']= '2';
            $input['username']= $input['first_name']." ".$input['last_name'];
            $user = User::create($input);
            $user_last_id = User::all()->last()->id;
            $data=array('user_id'=>$user_last_id);
            DB::table('users_details')->insert($data);
            $array['status'] = "true";
            $array['message'] = 'User registered successfully';
            $search = null;
            $replace = '""';
            $arr = $user->toArray();
            array_walk($arr,
            function (&$v) use ($search, $replace){
                $v = str_replace($search, $replace, $v);    
                }                                                                     
            ); 
            $array['response'] = $arr;

            $response = [
                'status' =>  $array['status'],
                
                'message' => $array['message'],
                
                'response' => array('data' =>$arr )
            ];
            return response()->json($response, 200);
        }

        $response = [
                'status' => "false",
                
                'message' => $array['message'],
                
                'response' => array('data' =>array() )
            ];
        return response()->json($response, 200);
    }

public function forgotpassword(Request $request) 

{


        $formdata = $request->all();
        $email    = $formdata['email'];
        $user = User::where('email', $formdata['email'])
                 ->orderBy('id', 'desc')->first();
       if(!empty($user))
       {
        if(!empty($formdata['email']))
        {
            $credentials = request()->validate(['email' => 'required|email']); 
            Password::sendResetLink($credentials);
            $response = [
                    'status' => "true",
                    
                    'message' => 'Reset password link sent on your email id',
                    
                    'response' => array('data' =>array() )
                ];
        }
        else
        {

                $response = [
                    'status' => "false",
                    
                    'message' => 'Email Required',
                    
                    'response' => array('data' =>array() )
                ];   
        }
      }
      else
        {

                $response = [
                    'status' => "false",
                    
                    'message' => 'Email Not Exist',
                    
                    'response' => array('data' =>array() )
                ];   
        }
       return response()->json($response, 200);

}



public function changePassword(Request $request)

{

    $input = $request->all();

    $userid = $request->user_id;

    $rules = array(

        'current_password' => 'required',

        'new_password' => 'required|min:6',

        

    );

    $validator = Validator::make($input, $rules);

    if ($validator->fails()) {

        $arr = array("status" =>'false', "message" => $validator->errors()->first(), "data" => array());

    } else {

             $user = user::where('id', $request->user_id)->first();

              if ((Hash::check($request->current_password, $user->password)) == false) {

                $arr = array("status" =>'false', "message" => "Check your old password.", "data" => array());

            } else if ((Hash::check($request->new_password, $user->password)) == true) {

                $arr = array("status" =>'false', "message" => "Please enter a password which is not similar then current password.", "data" => array());

            } else {

                User::where('id', $userid)->update(['password' => Hash::make($input['new_password'])]);

                $arr = array("status" =>true, "message" => "Password updated successfully.", "data" => array());

            }

        

    }

    return response()->json($arr, 200);

}

public function delete(Request $request) 

{

     $id = $request->id; 

     $result=DB::table('users')->where('id', $id)->delete();

     if($result){ 

         // Session::flash('success', 'Data is deleted successfully.');

            echo json_encode(array('status'=>1, 'msg'=>'User Deleted Successfully'));



        }

        else{



        //  Session::flash('error', 'Data is deleted successfully.');



            echo json_encode(array('status'=>0, 'msg'=>'Something went wrong!'));



        }


}

public function getQuationAnswer(Request $request)
{
    
    $fromdata        = $request->all();
    $user_id         = $fromdata['user_id'];
    $user_answer     = DB::table('user_answer')->where('user_id', '=', $user_id)->first();
    if(!empty($user_answer->user_id))
    {
     $question_answer = DB::table('questionnaire')->where('status', '=', 1)->get();
     $question_answer_arry = array();
     foreach ($question_answer as $value) 
     {

        $question_answer_by_id = DB::table('questionnaire')->where('id', '=', $value->id)->first();
        // answer by user id and quation id
        $answer_all     = DB::table('user_answer')->where('user_id', '=', $user_id)->where('question_id', '=', $value->id)->first();
        if(!empty($answer_all->answer))
        {
          $quationanswer  = $question_answer_by_id->answer;
          $answer         = explode(",",$answer_all->answer);
          $nans           = explode(",",$quationanswer);
          $arra_diff      = array_diff($nans,$answer);
          $newval = array();
          foreach ($answer as $key => $value) 
          {
              $nansn['ans']     = $value;
              $nansn['answer_status']  = 1;
              $newval[] = $nansn;  

          }
          $newval_answer = array();
          if(!empty($arra_diff))
          {
            foreach ($arra_diff as $value) 
            {
               $nansn['ans']     = $value;
               $nansn['answer_status']  = 0;
               $newval_answer[] = $nansn;      
            } 
          }
         $answer_main_array = array_merge($newval,$newval_answer);
        }
        else
        {
          $quationanswer  = $question_answer_by_id->answer;
          $arra_diff      =  explode(",",$quationanswer);
          $newval_answer  = array();
          foreach ($arra_diff as $value) 
            {
               $nansn['ans']     = $value;
               $nansn['answer_status']  = 0;
               $newval_answer[] = $nansn;      
            }
          $answer_main_array = $newval_answer; 
        }
        if(!empty($answer_all->mandatory_status))
        {
          $mandatory_status = 1;
        }
        else
        {
            $mandatory_status = 0;
        }

        $question_answer_arry[] = array('id'=>$question_answer_by_id->id,
          'question'=>$question_answer_by_id->question,
          'type'=>$question_answer_by_id->type,
          'answer'=>$answer_main_array ,
          'status'=>$question_answer_by_id->status,
          'mandatory_status'=>$mandatory_status,
          'created_at'=>$question_answer_by_id->created_at,
          'updated_at'=>$question_answer_by_id->updated_at
        ) ;


    }
    }
  else
  {
    $question_answer = DB::table('questionnaire')->where('status', '=', 1)->get();
    $question_answer_arry = array();
    foreach ($question_answer as $value) 
    {

        $question_answer_by_id = DB::table('questionnaire')->where('id', '=', $value->id)->first();

        $answer  = $question_answer_by_id->answer;

         $nans = explode(",",$answer);

         $newval = array();

         foreach ($nans as $key => $value) {

          $nansn['ans']     = $value;
          $nansn['answer_status']  = 0;

          $newval[] = $nansn;  

         }
         
        $question_answer_arry[] = array('id'=>$question_answer_by_id->id,
          'question'=>$question_answer_by_id->question,
          'type'=>$question_answer_by_id->type,
          'answer'=>$newval,
          'status'=>$question_answer_by_id->status,
          'mandatory_status'=>0,
          'created_at'=>$question_answer_by_id->created_at,
          'updated_at'=>$question_answer_by_id->updated_at
        ) ;


    }
  }
    if(!empty($question_answer_arry))
    {
           
            $response = [
                    'status' => "true",
                    
                    'message' => 'Get Quation Answer Successfully',
                    
                    'response' => array('data' =>$question_answer_arry)
                ];
  }
  else
  {

                $response = [
                    'status' => "false",
                    
                    'message' => 'No Data Fount',
                    
                    'response' => array('data' =>array() )
                ];   
        }
       return response()->json($response, 200);
}


public function getAnswerByUserId(Request $request)
{
      
    $formdata = $request->all();
    
    if (empty($formdata['user_id'])) {

       $response = array("status" =>'false', "message" =>'User Id Required', "data" => array());
       return response()->json($response, 200);


    }
    else
    {
       $user_id = $formdata['user_id'];
       $answer = DB::table('user_answer')->where('user_id', '=', $user_id)->get();
      
       if(!empty($answer[0]))
        {
           
            $response = [
                    'status' => "true",
                    
                    'message' => 'Get Answer Successfully',
                    
                    'response' => array('data' =>$answer)
                ];
        }
        else
        {

                $response = [
                    'status' => "false",
                    
                    'message' => 'No Data Fount',
                    
                    'response' => array('data' =>array() )
                ];   
        }

        return response()->json($response, 200);

    }
}

public function addQuationAnswer(Request $request)
{

        $forminput  = $request->all();
        if (empty($forminput["data"][0]['user_id'])) 
        {
         

           
            $response = array("status" =>'false', "message" =>'data Field Required', "data" => array());
            return response()->json($response, 200);
        }
        else
        {


        if(!empty($forminput)){
           $result=DB::table('user_answer')->where('user_id', $forminput["data"][0]['user_id'])->delete();
            $datam = $forminput['data'];
            foreach ($datam as $value) 
            {

                  if(!empty($value))
                  {
                      
                    if(!empty($value['answer']))
                    {
                      $answer = $value['answer'];
                      $mandatory_status  = $value['mandatory_status'];
                    }
                    else
                    {
                      $answer ="";
                      $mandatory_status = 0;
                    }
                    $applyjob = DB::table('user_answer')->insertGetId(
                    array('user_id' => $value['user_id'],            
                     'question_id' =>  $value['question_id'],
                     'answer' => $answer,
                     'mandatory_status' => $mandatory_status,
                     'created_at' =>  date('Y-m-d H:i:s'),
                     ));
                    $response = [
                    'status' => "true",
                    
                    'message' => 'Answer added Successfully',
                    
                    'response' => array('data' =>$applyjob )
                ];   
                  }
                  else 
                  {
                       $answer = "";
                  }
                   
            }

             return response()->json($response, 200);
           
          }
        else
        {
             
                  $response = [
                    'status' => "false",
                    
                    'message' => 'Answer not added',
                    
                    'response' => array('data' =>array() )
                ];   

        }
    }
}

public function addAnswerOneQuation(Request $request)
{

          $forminput  = $request->all();
          if(empty($forminput['data'][0]['user_id']))
          {

                
                $array['status'] = "false";
                $array['message'] = 'User Id required'; 
                return response()->json($array, 200);
          }
         elseif(empty($forminput['data'][0]['question_id']))
         {
          $array['status'] = "false";
          $array['message'] = 'Question id';
           return response()->json($array, 200);  
         }
         elseif(empty($forminput['data'][0]['answer'])){
          $array['status'] = "false";
          $array['message'] = 'Answer required';
           return response()->json($array, 200);  
        }
        else
        {
           if(!empty($forminput)){
            $datam = $forminput['data'];
            foreach ($datam as $value) 
            {
                  
                  if(!empty($value))
                  {
                     if(!empty($value['answer']))
                     {
                       $answer           = $value['answer'];
                       $mandatory_status = $value['mandatory_status'];
                     }
                     else
                     {
                        $answer = "";
                        $mandatory_status ="";
                     }
                     $result=DB::table('user_answer')->where('question_id', $forminput["data"][0]['question_id'])->delete();
                    $applyjob = DB::table('user_answer')->insertGetId(
                    array('user_id'     => $value['user_id'],            
                     'question_id'      => $value['question_id'],
                     'answer'           => $answer,
                     'mandatory_status' => $mandatory_status,
                     'created_at'   =>  date('Y-m-d H:i:s'),
                     ));
                    $response = [
                    'status' => "true",
                    
                    'message' => 'Answer added Successfully',
                    
                    'response' => array('data' =>$applyjob )
                 ];   
                }
                  else 
                  {
                       $answer = "";
                  }
                   
            }

             return response()->json($response, 200);
           
          }

        }

}


public function getAnswerByUserIdAndQuationId(Request $request)
{
    
    
    $formdata = $request->all();
    if (empty($formdata['user_id'])) {

       $response = array("status" =>'false', "message" =>'User Id Required', "data" => array());
       return response()->json($response, 200);


    }
    elseif(empty($formdata['question_id']))
    {

      
       $response = array("status" =>'false', "message" =>'Quation Required', "data" => array());
       return response()->json($response, 200);
  
    }
    else
    {
      $user_id = $formdata['user_id'];
      $question_id = $formdata['question_id'];
      $question_answer_by_id = DB::table('questionnaire')->where('id', '=', $question_id)->first();
      $answer_all = DB::table('user_answer')->where('user_id', '=', $user_id)->where('question_id', '=', $question_id)->first();
      $quationanswer  = $question_answer_by_id->answer;
      $answer         = explode(",",$answer_all->answer);
      $nans           = explode(",",$quationanswer);
      $arra_diff      = array_diff($nans,$answer);
      $newval = array();
      foreach ($answer as $key => $value) 
      {
          $nansn['ans']     = $value;
          $nansn['answer_status']  = 1;
          $newval[] = $nansn;  

      }
      $newval_answer = array();
      if(!empty($arra_diff))
      {
          foreach ($arra_diff as $value) 
          {
             $nansn['ans']     = $value;
             $nansn['answer_status']  = 0;
            
             $newval_answer[] = $nansn;      
          } 
      }
       $answer_main_array = array_merge($newval,$newval_answer);
       $question_answer_arry[] = array('id'=>$question_answer_by_id->id,
          'question'=>$question_answer_by_id->question,
          'type'=>$question_answer_by_id->type,
          'answer'=>$answer_main_array,
          'status'=>$question_answer_by_id->status,
          'status'=>$question_answer_by_id->status,
          'mandatory_status'=>$answer_all->mandatory_status,
          'created_at'=>$question_answer_by_id->created_at,
          'updated_at'=>$question_answer_by_id->updated_at
        ) ;


       if(!empty($question_answer_arry))
        {
           
            $response = [
                    'status' => "true",
                    
                    'message' => 'Get Answer Successfully',
                    
                    'response' => array('data' =>$question_answer_arry)
                ];
        }
        else
        {

                $response = [
                    'status' => "false",
                    
                    'message' => 'No Data Fount',
                    
                    'response' => array('data' =>array() )
                ];   
        }

        return response()->json($response, 200);

    }
}

// for edit quation answer //

public function editQuationAnswer()
{

   $formdata = $request->all();
    
    if (empty($formdata['user_id'])) {

       $response = array("status" =>'false', "message" =>'User Id Required', "data" => array());
       return response()->json($response, 200);


    }
    else
    {
       $user_id = $formdata['user_id'];
       $answer = DB::table('user_answer')->where('user_id', '=', $user_id)->get();
       if(!empty($answer[0]))
        {
           
            $response = [
                    'status' => "true",
                    
                    'message' => 'Get Answer Successfully',
                    
                    'response' => array('data' =>$answer)
                ];
        }
        else
        {

                $response = [
                    'status' => "false",
                    
                    'message' => 'No Data Fount',
                    
                    'response' => array('data' =>array() )
                ];   
        }

        return response()->json($response, 200);

    } 

}

 public function profile_update(Request $request)
    {
        $formdata = $request->all();
        $status = "false";
        $array = array();
        $array['response'] = (object) array();
        if(empty($formdata['user_id'])){            
            $array['status'] = "false";
            $array['msg'] = 'user_id required'; 
        }elseif(empty($formdata['email'])){
          $array['status'] = "false";
          $array['msg'] = 'email required';  
        // }elseif(empty($formdata['user_gender'])){
        //   $array['status'] = "false";
        //   $array['msg'] = 'user_gender required';  
         }elseif(empty($formdata['first_name'])){
          $array['status'] = "false";
          $array['msg'] = 'firstname required';  
        }elseif(empty($formdata['last_name'])){
          $array['status'] = "false";
          $array['msg'] = 'lastname required';  
        }elseif(empty($formdata['contact_number'])){
          $array['status'] = "false";
          $array['msg'] = 'phone required';  
        }
        elseif(empty($formdata['email'])){
          $array['status'] = "false";
          $array['msg'] = 'Email required';  
        }
         elseif(empty($formdata['gender'])){
          $array['status'] = "false";
          $array['msg'] = 'Gender required';  
        }else{
            
            $input = $request->all();

            $checkstatus = User::where('id', $input['user_id'])->first();
             

            
            $user_id = $input['user_id'];
            $post['first_name'] = $input['first_name'];
            $post['last_name']  = $input['last_name'];   
            $post['email']      = $input['email'];
            $post['gender']     = $input['gender'];
            $post['dob']        = $input['dob'];
            $post['contact_number']     = $input['contact_number'];

            DB::table('users')->where('id', $user_id)->update($post);
            $data = User::where('id', $input['user_id'])->get();
            $array['status'] = "true";
            $array['msg'] = 'Profile updated successfully';
            $search = null;
            $replace = '""';
            $arr = $data->toArray();
            array_walk($arr,
            function (&$v) use ($search, $replace){
            $v = str_replace($search, $replace, $v);    
            }                                                                     
            ); 
            $array['response'] = $arr;

             $response = [
                    'status' => "true",
                    
                    'message' => 'Profile updated successfully',
                    
                    'response' => array('data' =>$arr )
                ];   

            return response()->json($response, 200);
        
        }

         $response = [
                    'status' => "false",
                    
                    'message' => 'Profile Not Updated',
                    
                    'response' => array('data' =>$arr )
                ];   
        return response()->json($response, 200);
    }


    public function deleteAccount(Request $request)
    {

      $formdata         = $request->all();
      $user_id          = $formdata['user_id']; 
      $update           = User::where('id','=',$user_id)->update(array('status'=>0));
      $arr              = User::where('id', $user_id)->get();
      $response = [
                    'status' => "true",
                    
                    'message' => 'User Delete successfully',
                    
                    'response' => array('data' =>$arr )
                ];    
      return response()->json($response, 200);

    }




}

    













?>



