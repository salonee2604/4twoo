<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator,DB;
use App\User;
use Illuminate\Validation\Rule;
use Twilio\Rest\Client;
use Session;
use Exception;
use File;
use App\Libraries\BigBlueButton;
use ZipArchive;

class LoginController extends Controller {
	public function login() {
		$data['logo_info'] = DB::table('logo_file')->first();
		return view('auth/login')->with($data);
	}
}
?>