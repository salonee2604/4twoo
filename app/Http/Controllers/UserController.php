<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\User, Hash; 
use Illuminate\Support\Facades\Auth; 
use Validator, DB;
use Illuminate\Validation\Rule;
use Twilio\Rest\Client;
use Session;
use Helper; 

class UserController extends Controller 
{
public $successStatus = true;
public $failureStatus = false;
    /** 
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */ 

    public function signin(){

        return view('auth/user_login');
    }

    public function login(Request $request){ 
        $validator = Validator::make($request->all(), [ 
            //'email' => 'required|email',
            'email' => 'required', 
            'password' => 'required', 
        ],
        [
            'email.required' => "Email is required",
            'password.required' =>  "Password is required",
        ]
        );
        if ($validator->fails()) {
            $messages = $validator->messages();
            foreach ($messages->all() as $message)
            {   
                return response()->json(['status'=>$this->failureStatus,'msg'=>$message]);            
            }
        }
        $email = $request->email;
        $user = DB::table('users')
                ->where('status', '=', 1)
                ->where('role_id', '!=', 1)
                // ->orwhere('role_id', '=', 3)
                ->where(function($query) use ($email){
                    $query->where('email', $email);
                })           
                ->first();
        if(!empty($user)){
            $credentials = array(
                        'email'         => $user->email,
                        'password'      => $request->password,
                        'status'   => '1'
                    );          
            if (Auth::attempt($credentials)){
                $status = ''; 
                $msg = ''; 
                if(($user->status == 0) && ($user->is_verify_email== 0)){
                    Auth::logout();
                    $status = $this->successStatus; 
                    $msg = "Your account is not verify";  
                }
                
                if(($user->status == 0) && ($user->is_verify_email== 1)){
                    Auth::logout();
                    $status = $this->failureStatus; 
                    $msg = "Your profile is inactivate please contact to admin";                    
                }
                if (($user->status == 1) && ($user->is_verify_email== 1)){
                    $user = Auth::user(); 
                    //$data = User::findorfail(Auth::user()->id);
                    // $data->device_type  = $request->device_type;
                    // $data->device_token = $request->device_token; 
                    //$data->save();
                    $status = $this->successStatus; 
                    $msg = Auth::user()->first_name. " Logged in successfully";

                    // $Query = DB::table('users')->where('id',$user->id)->update([
                    //     "firebase_token" => $request->firebase_token
                    // ]);                   
                }
                if(($user->status == 1) && ($user->is_verify_email== 0)){
                    $status = $this->successStatus; 
                    $msg = "Your account is not verify";                    
                } 
                return response()->json([
                        'status'=>$status, 
                        'msg' => $msg,
                        'user_role'=>Auth::user()->role_id, 
                        'response' => 
                            [
                                'user_id'           => $user->id,
                                'fullname'          => $user->first_name,
                                'email'             => $user->email,
                                'role_id'           => $user->role_id,
                            ]
                    ]);
            }else{
                return response()->json([
                    'status'=>$this->failureStatus, 
                    'msg' => "Email id password not exists",
                    ]); 
            }
               
           
        }else{
            return response()->json(['status'=>$this->failureStatus, 'msg' => "User does not exists"]); 
        }
    }

    public function registration(){
        $data['country_code'] =DB::table('countrycode')->get();
        return view('auth/register')->with($data);
    }
 
    public function user_save(Request $request) 
    {   
        //print_r($request->all());die;

        $validator = Validator::make($request->all(), [ 
            'name'  => 'required',
            'last_name'  => 'required',
          
            'email' => [
                    'required',
                    'email',
                    Rule::unique('users')->where(function ($query) {
                        return $query->where('status','=', 1);
                    }),
                ],
            'contact_number' => [
                'required',
                'min:9',
                'max:16',
                Rule::unique('users')->where(function ($query) {
                    return $query->where('status','=', 1);
                }),
            ], 
           
            'password'  => [
                'required',  
                
           ],
          
        ],
        [   
            'first_name.required'       =>"First name is required",
            'last_name.required'        =>"last name is required",
            'email.email'               =>"Please enter vailid email id",
            'email.unique'              =>"Email id already exists",
            'contact_number.required'   =>"Contact number is required",
            'contact_number.min'        => "Please enter vailid contact number",
            'contact_number.max'        => "Please enter vailid contact number",
            'contact_number.unique'     => "Contact number already exists",
            'password.required'         => "Password is required",
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages();
            foreach ($messages->all() as $message)
            {   
                return response()->json(['status'=>$this->failureStatus,'msg'=>$message]);            
            }
        }
        $forminput  = $request->all();
     
            $digits                 = 6;
            $otp                    = rand(pow(10, $digits-1), pow(10, $digits)-1);
            $forminput              = $request->all();
            $user                   = new User; 
            $user->first_name       = $forminput['name'];
            $user->last_name        = $forminput['last_name'];
            $user->username         = $forminput['name'].$forminput['last_name'];
            $user->password         = Hash::make($forminput['password']);
            $user->status      = '1';
            $user->role_id     = '2';
            $user->vrfn_code       = $otp;
            $user->is_verify_email  = '0';
            $user->email            = $forminput['email'];
            $user->dob          = date("Y-m-d", strtotime($forminput['dob']));
            $user->address   = $forminput['address']; 
            $user->contact_number   = $forminput['contact_number']; 
            $user->gender   = $forminput['gender'];          
            $user->created_at       = Date('Y-m-d H:i:s');
               
            if($user->save()){

                $user_last_id = User::all()->last()->id;

                $data=array('user_id'=>$user_last_id);

                DB::table('users_details')->insert($data);
                $email_content = DB::Table('email_formate')->where('id', 1)->select('email_type','subject','body')->first();
                $searchArray = array("{first_name}","{last_name}");
                $replaceArray = array($user->first_name,$user->last_name);
                $content = str_replace($searchArray, $replaceArray, $email_content->body);
                $data = [
                        'name'      => $forminput['name'],
                        'email'     => $forminput['email'],                    
                        'subject'   => $email_content->subject,
                        'content'   => $content,
                    ];
                Helper::send_mail($data);

                return response()->json(          
                    [
                        'status'=>$this->successStatus, 
                        'user_id'=>$user_last_id,
                        'msg' => "You have registered successfully if you want login click here",
                    ]
                ); 
            }else{
                return response()->json(['status'=>$this->failureStatus, 'msg' => "some thing is wrong please try again later."]);
            }
            
    
    }
 
   
    public function verification(){
        return view('auth/verify');
    }

    public function verifyotp(Request $request){
        $forminput =  $request->all();
        $validator = Validator::make($request->all(), [ 
                'otp'  => 'required|min:6',           
            ],
            [   
                'otp.required'     => "OTP is required",
                'otp.min'          => "OTP is 6 digit please enter vailid otp",
            ]
        );
            if ($validator->fails()) { 
                $messages = $validator->messages();
                foreach ($messages->all() as $message)
                {   
                    return response()->json(['status'=>$this->failureStatus,'msg'=>$message]);            
                }            
            }

    
            $user = User::where('vrfn_code', $forminput['otp'])->where('status', '!=', 1)->first();
            if(/*(count($user)>0) &&*/ (!empty($user))){
                $user->status = 1;                
                $user->vrfn_code = null;
                $user->is_verify_email = 1;
                $user->save();

                $post = array('password' => $user->password, 'email' => $user->email);
                Auth::loginUsingId($user->id);


                return response()->json(['status'=>$this->successStatus, 'msg' => 'Your account has been verify successfully', 'user_id'=>$user->id , 'role_id'=>$user->role_id , 'response'=>['user_id' => $user->id]]); 
            }else{
                return response()->json(['status'=>$this->failureStatus, 'msg' => "You have entered Invailid otp "]); 
            }
        

    } 
    public function forgotpass(Request $request)
    {
        
        $email = $request->email;
        $pass_token = rand(100000, 999999); 

        //echo $pass_token  = str_random(6); die;   
         
        $sql =DB::table('users')->where('email', $email)->update(['vrfn_code' =>$pass_token]); 
        if($sql){      
            $data['usersdata']  = DB::table('users')->where('email', $email)->where('vrfn_code', $pass_token)->get();
        $id =   $data['usersdata'][0]->id;

        $searchArray = array("{user_email}");  

        $replaceArray = array($email); 
        
            $email_content = "<h3>Hello Dear,</h3>
            Admin<br>
            <p>Please use this code <h4><b>".$pass_token."</b></h4></p>
            <p>Click on the button below the reset your password.</p>
            <a href='".url('resetpass/'.base64_encode($id).'')."'>Click Here</a>";
        
            $content = str_replace($searchArray, $replaceArray, $email_content);
            $data = [
                'name'      => 'Admin',
                'email'     => $email,
                'subject'   => "Forgot Password",
                'content'   => $content,
            ];
            Helper::send_mail($data); 
            return redirect('password/reset')->with('success', 'Email sent successfully.');
        }
        else {
         return redirect('password/reset')->with('warning', 'Please enter vaild  mail-id.');
      
        }
    }


    public function resetpass($id)
    {
         $data['user_id']= $id;
         $data['title']= 'Reset Password';
             return view('auth.passwords.reset', ['data'=>$data]);
    } 

    public function updateforgot(Request $request)
    {
      
    $token = base64_decode($request->input('token'));
    $token_id = $request->input('token_id');
    $password = $request->input('password');
    $password_confirmation = Hash::make($request->input('password_confirmation'));

    if($request->input('password')!= $request->input('password_confirmation')){
    return redirect('admin/resetpass/'.$token.'')->with('warning', 'Password confirmation does not match password!');
    }else {
    $data['usersdata']  = DB::table('users')->where('id', $token)->where('vrfn_code', $token_id)->get();
    $datauser = count($data['usersdata']);
    if($datauser!=0){
    DB::table('users')
             ->where('id',$token)
             ->update(['vrfn_code'=>null, 'password'=> $password_confirmation]);  

       return redirect('resetpass/'.base64_encode($token).'')->with('success', 'Password Update successfully.');
       }
       else {

      return redirect('resetpass/'.base64_encode($token).'')->with('warning', 'Please Enter correct code.');
       }
    }   
    }
    // Table Booking//
    public function table_reservation(Request $request)
    {  


          if(!empty(Auth::user()->id))
           {
              $user_id = Auth::user()->id;
              $data['userinfo'] = $userinfo = DB::table('users')->where('users.id', '=', $user_id)->where('users.role_id', '=', 2)->first();
              $data['table_type'] = DB::table('table_section')->where('status',1)->orderBy('section_id', 'DESC')->get();
              return view('user/table_booking')->with($data);
           }
           else
           {

               return redirect('login');

           }

    

    }
    public function reservation_save(Request $request) 
    {   
        $validator = Validator::make($request->all(), [ 
            'username'  => 'required',
        ],
        [   
            'username.required'         =>"First name is required",
            'last_name.required'        =>"last name is required",
            'email.email'               =>"Please enter vailid email id",
            'email.unique'              =>"Email id already exists",
            'contact_number.required'   =>"Contact number is required",
            'contact_number.min'        => "Please enter vailid contact number",
            'contact_number.max'        => "Please enter vailid contact number",
            'contact_number.unique'     => "Contact number already exists",
            
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages();
            foreach ($messages->all() as $message)
            {   
                return response()->json(['status'=>$this->failureStatus,'msg'=>$message]);            
            }
        }
            $forminput              = $request->all();
            $rest_id                = 1;
            $day                    = $forminput['day'];
            $booking_date           = $forminput['booking_date'];
            $time_slot_id           = $forminput['time_slot_id'];
            $people                 = $forminput['number_of_memebers'];
            $message                = $forminput['message'];
            $total_table            = $forminput['total_table'];
            $table_type             = $forminput['table_type'];
            $user_id                = Auth::user()->id;
            $booking_unique_id      = strtoupper(substr(md5(time()), 0, 8));
            $data_insert=array('rest_id'=>$rest_id,"booking_date"=>$booking_date,"booking_day"=>$day,"time_slot_id"=>$time_slot_id,"people"=>$people,"message"=>$message,"user_id"=>$user_id,"booking_unique_id"=>$booking_unique_id,'total_table'=>$total_table,'table_type'=>$table_type);
            $query_insert=DB::table('table_booking')->insert($data_insert);
            if($query_insert == 1){

                

                return response()->json(          
                    [
                        'status'=>true, 
                        'user_id'=>$user_id,
                        'msg' => "Table book Successfully",
                    ]
                ); 
            }else{
                return response()->json(['status'=>false, 'msg' => "some thing is wrong please try again later."]);
            }
            
    
    }

    public function booking_list()
    {
      
       return view('user/booking_list');  
    }
    



}