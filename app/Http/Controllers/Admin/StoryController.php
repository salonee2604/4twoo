<?php



namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Story;

use Illuminate\Support\Facades\Auth;

use Validator, DB;

use Illuminate\Validation\Rule;

use Twilio\Rest\Client;

use Session;

use Maatwebsite\Excel\Facades\Excel;

use App\Helpers\Helper;

use Mail;



class StoryController extends Controller {

    public function __construct()
    {
        if (empty(Auth::user())) {
            return redirect('/admin');
        }
    }


    public function sendEmail(Request $req) {

     

     $student_id= $req->student_id;

     $email= $req->email;

     $languageCode= $req->language;

     $emailFormate = DB::table('email_formate')

                    ->where('email_type', '=', 'RegistrationConfirmEmail')

                    ->where('language', '=', $languageCode)

                    ->first();

        $logoImg = DB::table('logo_file')->first();

        $studentInfo = DB::table('users')->where(['id'=>$student_id,'email'=>$email])->get()->first();

   

        $subject = $emailFormate->subject;

        $logo_img = url('/').'/public/uploads/logo/'.$logoImg->logo_img;

        $loginurl = url('/');

        $email_content = str_replace("{logo}", $logo_img, $emailFormate->body);

        $email_content = str_replace("{url}", $loginurl, $email_content);

        $email_content = str_replace("{0}",ucfirst($studentInfo->first_name), $email_content);

        $email_content = str_replace("{1}", $studentInfo->username, $email_content);

        $email_content = str_replace("{2}",$studentInfo->temp_password, $email_content);

      



        // $email = 'votivephp.dharmaraj@gmail.com';

        //      $content = "<h3>Dear {name},</h3><p>Email :{email} and your Password Is {password}<p>Regards,<br>Virtual Class</p>";

        //      $content = str_replace("{0}",ucfirst($studentInfo->first_name),$content); 

        //      $content = str_replace("{password}",ucfirst($studentInfo->temp_password),$content); 

        //      $content = str_replace("{email}",ucfirst($studentInfo->email),$content);

      

      $data = [

            'name' => $studentInfo->first_name,

            'email' => $email,

            'subject' =>$subject,

            'content' =>$email_content,

        ];

      $res=  Helper::send_mail($data);

      return response()->json(['status' => true, 'message' =>'Email sent successfully']); 

    }



    public function story_list(Request $request) {
        if (empty(Auth::user())) {
            return redirect('/admin');
        }else{

            $data['stories_list'] = DB::table('success_story')->where('status', 1)->get();
            //echo "<pre>"; print_r($data);die;

            return view('admin/story/story_list')->with($data);
        }

    }



    public function add_story() {
        if (empty(Auth::user())) {
            return redirect('/admin');
        }else{
             return view('admin/story/add_story'); 
        }

    }



    public function submit_story(Request $request) {

       $validator = Validator::make($request->all(), [

                    'title' => 'required',

                    'content' => 'required',

                    'image' => 'required'

        ]);

        if ($validator->fails()) {

            return redirect('/admin/add_story')->withErrors($validator)->withInput();

        } else {

            $title = $request->title;
           
            $content = $request->content;
           
            $status = 1;
           
            $created_at = date('Y-m-d H:i:s');
           
            if ($_FILES["image"]["name"]) {
           
            $postImage = time().'_'.$_FILES["image"]["name"];
           
            $posturl="public/assets/img/".time().'_'.$postImage;
           
            $name = $_FILES["image"]["name"];
           
            move_uploaded_file( $_FILES["image"]["tmp_name"], "public/assets/img/" .time().'_'.$_FILES['image']['name']);
           
            }

            $data = array(
                'title' => $title,
                'image' => $postImage,
                'content' => $content,
                'status' => $status,
                'created_at' => $created_at
            );
           
            $updateRow = DB::table('success_story')->insert($data);
           
            $id = DB::getPdo()->lastInsertId();

            if ($updateRow) {

                session::flash('message', 'Success story addeed succesfully.');

                return redirect('admin/story_list');

            } else {

                session::flash('error', 'Success story records not inserted.');

                return redirect('admin/story_list');

            }

        }

    }



    public function edit_story(Request $request) {

        $story_id = base64_decode($request->id);

        $data['story_info'] = DB::table('success_story')->where('id',$story_id)->where('status', 1)->first();
        return view('admin/story/edit_story')->with($data);

    }



    public function update_story(Request $request) {

        $story_id = $request->input('story_id');
        $title = $request->title;
        $content = $request->content;

        if($_FILES["image"]["name"]) {
            $postImage = time().'_'.$_FILES["image"]["name"];
            $posturl="public/assets/img/".time().'_'.$postImage;
            $name = $_FILES["image"]["name"];
            move_uploaded_file( $_FILES["image"]["tmp_name"], "public/assets/img/" .time().'_'.$_FILES['image']['name']);
        }else{
            $data['story_info'] = DB::table('success_story')->where('id',$story_id)->where('status', 1)->first();
            $postImage=$data['story_info']->image;
        }

        $data = array(
            'title' => $title,
            'image' => $postImage,
            'content' => $content,
            );

        $updateRow = DB::table('success_story')->where('id', $story_id)->update($data);  
        if ($updateRow) {
            session::flash('message', 'Success story records updated succesfully.');
            return redirect('admin/story_list');
        } else {
            session::flash('error', 'Somthing went wrong.');
            return redirect('admin/story_list');
        }
        
    }


    public function change_story_status(Request $request) {
 
        $updateRow = DB::table('success_story')->where('id', $request->story_id)->update(array('status'=>$request->status)); 

        return response()->json(['success' => 'Story status change successfully.']);
    }

    public function UserImportData(Request $request) {

       $rules = array('import_file' => 'required',);

        $messages = array('import_file.required' => 'Please select file to be upload',);

        $validator = Validator::make($request->all(), $rules, $messages);

        

        if ($validator->fails()) {

                return redirect()->back()->withErrors($validator)->withInput();

        }else{

            

            if ($request->file('import_file')) {

               // $path = $request->file('import_file')->getRealPath();

               $path1 = $request->file('import_file')->store('temp');

               $path = storage_path('app') . '/' . $path1;

               $data = \Excel::import(new StudentImport, $path);

               if ($data) {

                   session::flash('message', 'File import succesfully.');

                   return redirect('admin/user_list');

               } else {

                   session::flash('error', 'Something went Wrong.');

                   return redirect('admin/user_list');

               }

           } else {

               session::flash('error', 'Please select file to upload.');

               return redirect('admin/user_list');

            }

        }

    }



    public function UserExportData() {



        return Excel::download(new StudentExport, 'user.xlsx');

    }

    public function delete_story(Request $request) {

        $story_id = $request->story_id;

        $student_info = DB::table('success_story')->where('id', '=', $story_id)->first();

        $res = DB::table('success_story')->where('id', '=', $story_id)->delete();

        if ($res) {

            return json_encode(array('status' => 'success', 'msg' => 'Data has been deleted successfully!'));

        } else {

            return json_encode(array('status' => 'error', 'msg' => 'Some internal issue occured.'));

        }

    }

}



?>