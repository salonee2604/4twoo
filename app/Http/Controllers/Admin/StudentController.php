<?php



namespace App\Http\Controllers\Admin;



use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\User;

use Illuminate\Support\Facades\Auth;

use Validator, DB;

use Illuminate\Validation\Rule;

use Twilio\Rest\Client;

use Session;

use App\Imports\StudentImport;

use App\Exports\StudentExport;

use Maatwebsite\Excel\Facades\Excel;

use App\Helpers\Helper;

use Mail;



class StudentController extends Controller {

    public function __construct()
    {
        if (empty(Auth::user())) {
            return redirect('/admin');
        }
    }

    public function student_list(Request $request) {

        if (empty(Auth::user())) {
            return redirect('/admin');
        }else{
                  $uri = $request->path();

        // if(Auth::user()->role_id==4){

        //   $accessPermission = Helper::getAccessPath($uri,Auth::user()->id);

        //  if(!$accessPermission){

        //     return redirect('/admin/error-access-permission'); 

        //  }

        // }    

        $data['student_list'] = DB::table('users')

                        ->leftjoin('member_grade', 'member_grade.user_id', '=', 'users.id')

                        ->leftJoin('grades', 'grades.id', '=', 'member_grade.grade_id')

                        ->select('users.*', 'grades.grade')

                        ->where('role_id', 2)->get();

        $data['language_list'] = DB::table('languages')->where(['isenabled' => 1])->get()->all();

        return view('admin/student/student_list')->with($data); 
        }



    }

	public function getStudentList(Request $request){

		

        $columns = array(0 =>'id', 

                         1 =>'profile_pic',

						 2 =>'full_name',

						 3=> 'username',

						 4=> 'email',

					     5=> 'grade',

						 6=> 'status',

						 7=> 'created_at',

						 8=> 'action',

                        );

						

						

	   $totalData = DB::table('users')

	     			->where('role_id',2)

					->count();

       $totalFiltered = $totalData; 



        $limit = $request->input('length');

        $start = $request->input('start');

        $order = $columns[$request->input('order.0.column')];

        $dir = $request->input('order.0.dir');

            

        if(empty($request->input('search.value'))){    



		  $studentList = DB::table('users')


						->where('role_id',2)

						->offset($start)

						->limit($limit)

						->orderBy($order,$dir)

						->get();

        }else {

	        $search = $request->input('search.value'); 

		    $studentList =DB::table('users','')

						 ->where('role_id',2)

						 ->where(function ($query) use ($search) {

							    $query->where('first_name', 'LIKE',"%{$search}%")

								->orWhere('last_name', 'LIKE',"%{$search}%")

								->orWhere('username', 'LIKE',"%{$search}%")

								->orWhere('email', 'LIKE',"%{$search}%")

                                ->orWhere('contact_number', 'LIKE',"%{$search}%");

								// ->orWhere('grades.grade', 'LIKE',"%{$search}%");

							 })

						 ->offset($start)

						 ->limit($limit)

						 ->orderBy($order,$dir)

						 ->get();



            $totalFiltered = DB::table('users')

							->where('role_id',2)

							->where(function ($query) use ($search) {

							    $query->where('first_name', 'LIKE',"%{$search}%")

								->orWhere('last_name', 'LIKE',"%{$search}%")

								->orWhere('username', 'LIKE',"%{$search}%")

								->orWhere('email', 'LIKE',"%{$search}%")

                                ->orWhere('contact_number', 'LIKE',"%{$search}%");

								// ->orWhere('grades.grade', 'LIKE',"%{$search}%");

							 })

							->count();

	    }					

	 	$data = array();

		if (!empty($studentList)) {

		   $i = $start+1;

            foreach ($studentList as $key => $value) {

			$checked =	($value->status) ? 'checked' : '';

				

				$nestedData['id'] = $i;

                if(!empty($value->profile_pic)){

                   $nestedData['profile_pic'] = '<img src="'.url('/').'/public/uploads/profile_img/'. $value->profile_pic.'" style="width:100px;height:100px;">'; 

               }else{

                $nestedData['profile_pic'] = '<img src="'.url('/').'/public/uploads/profile_img/default.png" style="width:100px;height:100px;">';

               }

                



                $nestedData['full_name'] = $value->first_name." ".$value->last_name;

                $nestedData['username'] = $value->first_name." ".$value->last_name;

                $nestedData['email'] = !empty($value->email) ? $value->email : '' ;

                $nestedData['contact_number'] = !empty($value->contact_number) ? $value->contact_number : '' ;

                $nestedData['grade'] = !empty($value->grade) ? $value->grade : '' ;

                $nestedData['status'] = '<input type="checkbox" data-id="'.$value->id.'" class="toggle-class"  data-onstyle="success" data-offstyle="danger" data-on="Active" data-off="InActive" '.$checked.'>';

                $nestedData['created_at'] = !empty($value->created_at) ? date('d-m-Y H:i A',strtotime($value->created_at)) : '' ;

                //$nestedData['action'] ="<a href='".url('/admin/change_student_password/'.base64_encode($value->id))."'>Update Password</a> |<a href='".url('/admin/edit_student/'.base64_encode($value->id))."'>Edit</a> | <a href='javascript:void(0)' onclick='delete_student(".$value->id.");'>Delete</a>|<a href='javascript:void(0)' data-email='".$value->email."' id='track_email_".$value->id."' onclick='sendemail(".$value->id.")';>Send Email</a>";

                $nestedData['action'] ="<a href='".url('/admin/view_quation_answer/'.base64_encode($value->id))."'>View Quation Answer</a> |<a href='".url('/admin/matchProfiles/'.base64_encode($value->id))."'>View Match</a> |<a href='".url('/admin/edit_user/'.base64_encode($value->id))."'>Edit</a> | <a href='javascript:void(0)' onclick='delete_student(".$value->id.");'>Delete</a>";

			 	$i++;

				$data[] = $nestedData;

			}

		 

        }

		

		 $json_data = array(

                    "draw"            => intval($request->input('draw')),  

                    "recordsTotal"    => intval($totalData),  

                    "recordsFiltered" => intval($totalFiltered), 

                    "data"            => $data,  

                    );

            

        echo json_encode($json_data); 			

	    die;   

		

		

	}

	

    public function add_student() {

        $data['grade_list'] = DB::table('grades')->where('status', 1)->get();

        $data['curriculum_list'] = DB::table('curriculums')->where('status', 1)->get();

        return view('admin/student/add_student')->with($data);

    }



    public function submit_student(Request $request) {



       $validator = Validator::make($request->all(), [

                    'name' => 'required',

                    'username' => 'required|unique:users',

                    'email' => 'required|email|max:255|unique:users',

                    'password' => 'required|min:6',

                    'confirm_password' => 'required|same:password',

                    //'grade' => 'required',

                    //'curriculum' => 'required',

        ]);



        if ($validator->fails()) {

            return redirect('/admin/add_student')->withErrors($validator)->withInput();

        } else {

			

            $name = $request->name;

            $first_name = $request->name;

            $email = $request->email;

            $password = $request->password;

            $username = $request->username;

            $last_name = $request->username;

            $curriculum = $request->curriculum;

            $grade      = $request->grade;

            // $name_arr = explode(' ', $name);

            $obj = new User;

            $obj->first_name = $first_name;

            $obj->last_name = $last_name;

            $obj->username = $first_name.$last_name;

            $obj->email = $email;

            $obj->role_id = 3;

            $obj->grade = $grade;

            $obj->curriculum = $curriculum;

            $obj->password = bcrypt($password);

            $obj->temp_password = $password;

            $obj->status = 1;

            $obj->created_at = date('Y-m-d H:i:s');

            $res = $obj->save();
            if ($res) {

                session::flash('message', 'Student Added Succesfully.');

                return redirect('admin/student_list');

            } else {

                session::flash('error', 'Student records not inserted.');

                return redirect('admin/student_list');

            }

        }

    }



    public function edit_student(Request $request) {

        $student_id = base64_decode($request->id);

        $data['student_info'] = User::find($student_id);

        $data['grade_list'] = DB::table('grades')->where('status', 1)->get();

        $data['curriculum_list'] = DB::table('curriculums')->where('status', 1)->get();

        return view('admin/student/edit_student')->with($data);

    }



    public function update_student(Request $request) {

        $student_id = $request->input('student_id');

        $name = $request->name;

        $email = $request->email;

        $password = $request->password;

        $username = $request->username;

        $curriculum = $request->curriculum;

       // $grade = $request->grade;

        

        $where=array(['username', '=',$username]);

        if(!empty($student_id)){

                $where[]=['id','!=',$student_id];

        }

        $records = DB::table('users')->where($where)->get()->all();

        if(!empty($records)){

            session::flash('error', 'UserName already exist.');

            return redirect('admin/edit_student/'. base64_encode($student_id));

        }else{

            $name_arr = explode(' ', $name);

            $studentData = User::where('id', $student_id)->first();

            $studentData->first_name = (!empty($name_arr[0]) ? $name_arr[0] : '');

            $studentData->last_name = (!empty($name_arr[1]) ? $name_arr[1] : '');



            $studentData->username = $username;

           // $studentData->curriculum = $curriculum;

           // $studentData->grade = $grade;

            $studentData->updated_at = date('Y-m-d H:i:s');

            $res = $studentData->save();

            if ($res) {

                session::flash('message', 'User records updated succesfully.');

                return redirect('admin/user_list');

            } else {

                session::flash('error', 'Somthing went wrong.');

                return redirect('admin/user_list');

            }

        }

      

        

    }



    public function change_student_status(Request $request) {



        $student_info = User::find($request->student_id);



        $student_info->status = $request->status;



        $student_info->save();



        return response()->json(['success' => 'User status change successfully.']);

    }



    public function delete_student(Request $request) {



        $student_id = $request->student_id;

        $student_info = DB::table('users')->where('id', '=', $student_id)->first();

        $res = DB::table('users')->where('id', '=', $student_id)->delete();

        if ($res) {



            return json_encode(array('status' => 'success', 'msg' => 'Data has been deleted successfully!'));

        } else {



            return json_encode(array('status' => 'error', 'msg' => 'Some internal issue occured.'));

        }

    }



    public function change_password(Request $request) {







        $student_id = base64_decode($request->id);



        $data['student_info'] = User::find($student_id);



        return view('admin/student/change_password')->with($data);

    }



    public function update_password(Request $request) {

        $user_id = $request->student_id;

        $user_info = User::find($user_id);

        $validatedData = $request->validate([

            'new-password' => 'required|string|min:6|confirmed',

        ]);

      $password = bcrypt($request->input('new-password'));

      $temp_password = $request->input('new-password');

        //Change Password

       DB::table('users')

                ->where('id', $user_id)

                ->update(['password' => $password,'temp_password' =>$temp_password,'updated_at' => date('Y-m-d H:i:s')]);

           

      // DB::table('users')->insert(['password' => $password,'temp_password' =>$temp_password]);

       return redirect()->back()->with("message", "Password changed successfully !");

    }



    public function StudentImportData(Request $request) {

        

        $rules = array('import_file' => 'required',);

	    $messages = array('import_file.required' => 'Please select file to be upload',);

	    $validator = Validator::make($request->all(), $rules, $messages);

        

        if ($validator->fails()) {

                return redirect()->back()->withErrors($validator)->withInput();

        }else{

            

            if ($request->file('import_file')) {

               $path1 = $request->file('import_file')->store('temp');

               $path = storage_path('app') . '/' . $path1;

               $data = \Excel::import(new StudentImport, $path);

			  

               if ($data) {

                   session::flash('message', 'File import succesfully.');

                   return redirect('admin/student_list');

               } else {

                   session::flash('error', 'Something went Wrong.');

                   return redirect('admin/student_list');

               }

           } else {

               session::flash('error', 'Please select file to upload.');

               return redirect('admin/student_list');

            }

        }

    }



    public function StudentExportData() {



        return Excel::download(new StudentExport, 'student.xlsx');

    }

    public function sendEmail(Request $req) {

     

     $student_id= $req->student_id;

     $email= $req->email;

     $languageCode= $req->language;

     $emailFormate = DB::table('email_formate')

                    ->where('email_type', '=', 'RegistrationConfirmEmail')

                    ->where('language', '=', $languageCode)

                    ->first();

        $logoImg = DB::table('logo_file')->first();

        $studentInfo = DB::table('users')->where(['id'=>$student_id,'email'=>$email])->get()->first();

   

        $subject = $emailFormate->subject;

        $logo_img = url('/').'/public/uploads/logo/'.$logoImg->logo_img;

        $loginurl = url('/');

        $email_content = str_replace("{logo}", $logo_img, $emailFormate->body);

        $email_content = str_replace("{url}", $loginurl, $email_content);

        $email_content = str_replace("{0}",ucfirst($studentInfo->first_name), $email_content);

        $email_content = str_replace("{1}", $studentInfo->username, $email_content);

        $email_content = str_replace("{2}",$studentInfo->temp_password, $email_content);

      



        // $email = 'votivephp.dharmaraj@gmail.com';

        //      $content = "<h3>Dear {name},</h3><p>Email :{email} and your Password Is {password}<p>Regards,<br>Virtual Class</p>";

        //      $content = str_replace("{0}",ucfirst($studentInfo->first_name),$content); 

        //      $content = str_replace("{password}",ucfirst($studentInfo->temp_password),$content); 

        //      $content = str_replace("{email}",ucfirst($studentInfo->email),$content);

      

      $data = [

            'name' => $studentInfo->first_name,

            'email' => $email,

            'subject' =>$subject,

            'content' =>$email_content,

        ];

      $res=  Helper::send_mail($data);

      return response()->json(['status' => true, 'message' =>'Email sent successfully']); 

    }



    public function user_list(Request $request) {
        if (empty(Auth::user())) {
            return redirect('/admin');
        }else{
                    $url  = url('/');

            $uri = $request->path();

            //echo URL::to('/');die;

            if(Auth::user()->role_id==4){

                 $accessPermission = Helper::getAccessPath($uri,Auth::user()->id);

                if(!$accessPermission){

                   return redirect('/admin/error-access-permission'); 

                }

            }   

            $data['student_list'] = DB::table('users')

                            ->where('role_id', 2)->get();

            $data['language_list'] = DB::table('languages')->where(['isenabled' => 1])->get()->all();

            return view('admin/student/user_list')->with($data);
        }




    }



    public function add_user() {
        if (empty(Auth::user())) {
            return redirect('/admin');
        }else{
             return view('admin/student/add_user'); 
        }
        //$data['grade_list'] = DB::table('grades')->where('status', 1)->get();

        //$data['curriculum_list'] = DB::table('curriculums')->where('status', 1)->get();

        //return view('admin/student/add_user')->with($data);
      

    }



    public function submit_user(Request $request) {



       $validator = Validator::make($request->all(), [

                    'name' => 'required',

                    'username' => 'required',

                    'email' => 'required|email|max:255|unique:users',

                    'password' => 'required|min:6',

                    'confirm_password' => 'required|same:password',

                    'image' => 'required',

                    'contact_number' => 'required|digits:10'

        ]);



        if ($validator->fails()) {

            return redirect('/admin/add_user')->withErrors($validator)->withInput();

        } else {

            
            //echo "<pre>"; print_r($request->all());die;

            $fullname   = $request->name;
            $first_name = $request->name;

            $email = $request->email;

            $password = $request->password;

            $username = $request->username;

            $last_name = $request->username;

            $contact_number = $request->contact_number;

            if ($_FILES["image"]["name"]) {

            $userImage = time().'_'.$_FILES["image"]["name"];

            $Imgurl="public/uploads/profile_img/".time().'_'.$userImage;

            $name = $_FILES["image"]["name"];

            move_uploaded_file( $_FILES["image"]["tmp_name"], "public/uploads/profile_img/" .time().'_'.$_FILES['image']['name']);

            }else{

                $userImage='';

            }

            $name_arr = explode(' ', $fullname);

            $obj = new User;

            $obj->first_name = $first_name;

            $obj->last_name = $last_name;

            $obj->username = $first_name.$last_name;

            $obj->email = $email;

            $obj->profile_pic = $userImage;

            $obj->contact_number = $contact_number;

            $obj->role_id = 2;

            $obj->password = bcrypt($password);

            $obj->temp_password = $password;

            $obj->status = 1;

            $obj->created_at = date('Y-m-d H:i:s');

            $res = $obj->save();
            $data=array('user_id'=>$obj->id);
            DB::table('users_details')->insert($data);

            
            if ($res) {

                session::flash('message', 'User Added Succesfully.');

                return redirect('admin/user_list');

            } else {

                session::flash('error', 'User records not inserted.');

                return redirect('admin/user_list');

            }

        }

    }



    public function edit_user(Request $request) {

        $user_id = base64_decode($request->id);

        $data['student_info'] = User::find($user_id);

        //$data['grade_list'] = DB::table('grades')->where('status', 1)->get();

        //$data['curriculum_list'] = DB::table('curriculums')->where('status', 1)->get();

        return view('admin/student/edit_user')->with($data);

    }



    public function update_user(Request $request) {

        //print_r($request->all());die;

        $user_id = $request->input('user_id');

        $name = $request->name;

        $first_name = $request->name;

        $contact_number = $request->contact_number;

        $email = $request->email;

        $password = $request->password;

        $temp_password = $request->password;

        

        $last_name = $request->username;

        $username = $first_name.$last_name;

        $curriculum = $request->curriculum;        

        $where=array(['username', '=',$username]);

        if(!empty($user_id)){

                $where[]=['id','!=',$user_id];

        }

        $records = DB::table('users')->where($where)->get()->all();

        if(!empty($records)){

            session::flash('error', 'UserName already exist.');

            return redirect('admin/edit_user/'. base64_encode($user_id));

        }else{

            $name_arr = explode(' ', $name);

            $studentData = User::where('id', $user_id)->first();

            $studentData->first_name = $first_name;

            $studentData->last_name = $last_name;



            $studentData->username = $first_name.$last_name;

            if(!empty($password)){

              $studentData->password = bcrypt($password);

              $studentData->temp_password = $temp_password;  

            }

            if($_FILES["image"]["name"]) {

            $userImage = time().'_'.$_FILES["image"]["name"];

            $Imgurl="public/uploads/profile_img/".time().'_'.$userImage;

            $name = $_FILES["image"]["name"];

            move_uploaded_file( $_FILES["image"]["tmp_name"], "public/uploads/profile_img/" .time().'_'.$_FILES['image']['name']);

            $studentData->profile_pic =$userImage;

            }



            $studentData->contact_number = $contact_number;

            $studentData->updated_at = date('Y-m-d H:i:s');

            $res = $studentData->save();

            if ($res) {

                session::flash('message', 'User records updated succesfully.');

                return redirect('admin/user_list');

            } else {

                session::flash('error', 'Somthing went wrong.');

                return redirect('admin/user_list');

            }

        } 

    }



    public function UserImportData(Request $request) {

       $rules = array('import_file' => 'required',);

        $messages = array('import_file.required' => 'Please select file to be upload',);

        $validator = Validator::make($request->all(), $rules, $messages);

        

        if ($validator->fails()) {

                return redirect()->back()->withErrors($validator)->withInput();

        }else{

            

            if ($request->file('import_file')) {

               // $path = $request->file('import_file')->getRealPath();

               $path1 = $request->file('import_file')->store('temp');

               $path = storage_path('app') . '/' . $path1;

               $data = \Excel::import(new StudentImport, $path);

               if ($data) {

                   session::flash('message', 'File import succesfully.');

                   return redirect('admin/user_list');

               } else {

                   session::flash('error', 'Something went Wrong.');

                   return redirect('admin/user_list');

               }

           } else {

               session::flash('error', 'Please select file to upload.');

               return redirect('admin/user_list');

            }

        }

    }



    public function UserExportData() {



        return Excel::download(new StudentExport, 'user.xlsx');

    }



    public function delete_user(Request $request) {



        //print_r($request->all());die;

        $user_id = $request->user_id;

        $student_info = DB::table('users')->where('id', '=', $user_id)->first();

        $res = DB::table('users')->where('id', '=', $user_id)->delete();

        if ($res) {



            return json_encode(array('status' => 'success', 'msg' => 'Data has been deleted successfully!'));

        } else {



            return json_encode(array('status' => 'error', 'msg' => 'Some internal issue occured.'));

        }

    }



     public function InstaUser(Request $request) {

        return view('admin/insta_user_list');

    }

    public function view_quation_answer(Request $request)
    {
            $user_id = base64_decode($request->id);

            $data['question_answer'] = DB::table('questionnaire')->where('status', '=', 1)->get();
            $data['edit_question_answer'] = DB::table('user_answer')->where('user_id', '=', $user_id)->get();
            return view('admin/student/view_quation_answer')->with($data);
    }

    public function matchProfiles(Request $request)
    {



        if (empty(Auth::user())) 
        {
            return redirect('/');
        }else{
            
            $user_id                =  $user_id = base64_decode($request->id);
            $answer_details         = DB::table('user_answer')->where('user_id', '=', $user_id)->get();
            $matche_user_id_arr     = array();
            foreach ($answer_details as $value) 
            {
                
                $question_id    =   $value->question_id;
                $answer         =   $value->answer;
                $mandatory_status =  $value->mandatory_status;
                if($mandatory_status == 1)
                {
                   
                   $user_details = DB::table('user_answer')->where('question_id', '=', $question_id)->where('answer', '=', $answer)->where('user_id', '!=' , $user_id)->get();
                   
                }
                else
                {

                   $user_details_arr  =array();
                   $answer_array = explode(",", $answer);
                   $i= 0;
                   foreach ($answer_array as $value) 
                   {
                        $user_details_all_user = DB::table('user_answer')->where('question_id', '=', $question_id)->where('user_id', '!=' , $user_id)->whereRaw("find_in_set('".$value."',answer)")->get();

                        foreach ($user_details_all_user as $value) 
                        {


                             
                             $user_details = DB::table('user_answer')->where('question_id', '=', $question_id)->where('user_id', '!=' , $user_id)->where('user_id','=',$value->user_id)->get();
                        }
                    $i++;
                   }

                   
                  
                }
               
                foreach ($user_details as $value_match) 
                {
                    $matche_user_id_arr[] = $value_match->user_id;

                }
             
                
            }
     

            $matche_arrycount                        = array_count_values($matche_user_id_arr);
            arsort($matche_arrycount );
            $arry_main = $matche_arrycount;
            $userinfo  = array();
            $matched_quation = array();
            foreach($arry_main as $x => $x_value) 
            {
               
               $data['user_id']  =  $x;
               $userdetails['data']       = DB::table('users')->select('users.*')->where('users.id', '=', $x)->orderBy('users.id', 'DESC')->first();
               $count['count']             = $x_value;
               $userinfo[] = array_merge( $userdetails,$count);
               
              

            }
            
            $data['usersinfo']       = $userinfo;
            return view('admin/student/matching_profiles')->with($data);
        }
    }

    public function update_quation_answer_admin(Request $request)
    {
      

       
       
        $forminput  = $request->all();
        $uid        = $request->user_id;
        $forminput  = $request->all();
       if(!empty($forminput))
        {
            
            DB::table('user_answer')->where('user_id', '=', $uid)->delete();
            $datam = $forminput['question_id'];
            foreach ($datam as $key => $value) 
            {
                  if(!empty($forminput['answer'.$value]))
                  {
                    $answer_array     =  implode(",", $forminput['answer'.$value]);
                  if(!empty($answer_array))
                  {
                    if(!empty($forminput['mandatory_status'.$value]))  
                    {
                        $mandatory_status_arry =$forminput['mandatory_status'.$value];
                        $mandatory_status = $mandatory_status_arry[0];

                    }
                    else
                    {
                        $mandatory_status = 0;
                    }  
                    $applyjob = DB::table('user_answer')->insertGetId(
                    array('user_id' => $uid,            
                     'question_id' =>  $value,
                     'answer' => $answer_array,
                     'mandatory_status' =>$mandatory_status,
                     'created_at' =>  date('Y-m-d H:i:s'),
                     ));
                  }
                  else 
                  {
                       $applyjob = DB::table('user_answer')->insertGetId(
                    array('user_id' => $uid,            
                     'question_id' =>  $value,
                     'answer' =>"",
                     'mandatory_status' =>0,
                     'created_at' =>  date('Y-m-d H:i:s'),
                     ));
                  }
                }
                else
                {
                  $applyjob = DB::table('user_answer')->insertGetId(
                    array('user_id' => $uid,            
                     'question_id' =>  $value,
                     'answer' =>"",
                     'mandatory_status' =>0,
                     'created_at' =>  date('Y-m-d H:i:s'),
                     ));
                }
                   
            }
            session::flash('message', 'Answer updated Successfully');
             return redirect('admin/view_quation_answer/'.base64_encode($uid));        }  

    }


}



?>