<?php 



namespace App\Http\Controllers\Admin;



use Illuminate\Http\Request;

use App\Http\Controllers\Controller; 

use App\LanguageModel;

use Illuminate\Support\Facades\Auth; 

use Validator, DB;

use Illuminate\Validation\Rule;

use Twilio\Rest\Client;

use File;

use Illuminate\Support\Facades\Storage;

use Session;

use App;

use App\Helpers\Helper;







class LanguageController extends Controller 



{



	public function language_list(Request $request) {

		  $uri = $request->path();

		if(Auth::user()->role_id==4){

			 $accessPermission = Helper::getAccessPath($uri,Auth::user()->id);

			if(!$accessPermission){

			   return redirect('/admin/error-access-permission'); 

			}

		} 	



		$data['language_list'] = DB::table('languages')->get();

		return view('admin/languages/language_list')->with($data);

	}

	public function add_language(){

		return view('admin/languages/add_language'); 

	}







	public function submit_language(Request $request){

		$validator = Validator::make($request->all(), [

			'name' => 'required',

			'code' => 'required',

		]);



		if ($validator->fails()) {

			session::flash('error', 'Validation error.');

			return redirect('/admin/add_language')->withErrors($validator)->withInput(); 

		} else {



			$name = $request->name;

			$code = $request->code;

			$isenabled = $request->isenabled;

			$isdefault = $request->isdefault;

			if ($request->hasFile('icon')){

				$iconImage = $request->file('icon');

				$extension  = $iconImage->getClientOriginalName();

				$imageName = time().'_'.$extension;

				$destinationPath = public_path('/uploads/language/icons');

				$iconImage->move($destinationPath, $imageName);

			}



        	$obj = new LanguageModel;

			$obj->name = $name;

			$obj->code = strtolower($code);

			$obj->icon = $imageName;

			$obj->isenabled = $isenabled;

			$obj->isdefault = $isdefault;

			$obj->status = 1;

			$obj->created_at = date('Y-m-d H:i:s');

			$res = $obj->save();

			$lastid = $obj->id;

			if($res){

				if($isdefault == "Yes"){

					$updateData = LanguageModel::where('id', '!=', $lastid)->update(array('isdefault' => 'No'));

				}

				session::flash('message', 'Record Addeed Succesfully.');

				return redirect('admin/language_list');

			}else{

				session::flash('error', 'Record not inserted.');

				return redirect('admin/language_list');

			}

		}



	}



	public function edit_language(Request $request){

		$lang_id = base64_decode($request->id);

		$data['lang_info'] = LanguageModel::find($lang_id);

		return view('admin/languages/edit_language')->with($data) ;



	}

	public function update_language(Request $request){

		$lang_id = $request->input('lang_id') ;

		$name = $request->name;

		$code = $request->code;

		$isenabled = $request->isenabled;

		$isdefault = $request->isdefault;

		$langData = LanguageModel::where('id', $lang_id)->first();

		$langData->name = $name;

		$langData->code = strtolower($code);

		$langData->isenabled = $isenabled;

		$langData->isdefault = $isdefault;

		$langData->updated_at = date('Y-m-d H:i:s');

		$res = $langData->save();

		$lastid = $langData->id;

		if($res){



			if($isdefault == "Yes"){

				$updateData = LanguageModel::where('id', '!=', $lastid)->update(array('isdefault' => 'No'));

			}

			session::flash('message', 'Record updated succesfully.');

			return redirect('admin/language_list');



		}else{



			session::flash('error', 'Somthing went wrong.');



			return redirect('admin/language_list');



		} 



	} 

	

	public function setDefaultLanguage($language_id=""){

		$lang = LanguageModel::find($language_id);

		if(!empty($lang)){

		   DB::table('languages')->update(['isdefault' => 'No','updated_at' => date("Y-m-d H:i:s")]);

		 $result = DB::table('languages')->where(['id' => $language_id])->update(['isdefault' => 'Yes']);

		  if($result){

			 //  $Defaultlang = DB::table('languages')->where('isdefault','=','Yes')->first();

			  // App::setLocale($Defaultlang->code);

			  //session()->put('locale', $Defaultlang->code);

		      session::flash('message', 'Languages set Default succesfully.');

		  }else{

			 session::flash('error', 'Somthing went wrong.');

	    	}

	    }else{

		  session::flash('error', 'Record not found.please try agian');

		}

	    return redirect('admin/language_list');	

	}

	

	public function change_language_status(Request $request){

        $lang_info = LanguageModel::find($request->lang_id);



        $lang_info->status = $request->status;



        $lang_info->save();

        return response()->json(['success'=>'Language status change successfully.']);



    }



	public function delete_language(Request $request) {



		$lang_id = $request->lang_id;

		$lang_info = DB::table('languages')->where('id','=',$lang_id)->first();

		$res = DB::table('languages')->where('id', '=', $lang_id)->delete();	

		if ($res) {

			return json_encode(array('status' => 'success','msg' => 'Data has been deleted successfully!'));

		} else {

			return json_encode(array('status' => 'error','msg' => 'Some internal issue occured.'));

		}



	}

	public function updatestring(request $request){

		$lang_id = base64_decode($request->id);

		$data['langinfo'] = LanguageModel::find($lang_id);

		return view('admin/languages/update_string_language')->with($data) ;

		

	}

	public function saveUpdateString(request $request){

		$lang_id = $request->input('language_id') ;

		if(!empty($lang_id)){

		    $arr = $_POST;

		   	unset($arr['_token']);

			unset($arr['language_id']);

		    $result = DB::table('languages')->where(['id' => $lang_id])->update(['lang_content'=>json_encode($arr),'updated_at' => date("Y-m-d H:i:s")]);

			if($result){

				$this->updateLanuageFile($lang_id);

			}

			session::flash('message', 'Record updated succesfully.');

		}else{

			session::flash('error', 'Somthing went wrong.');

		}

		return redirect('admin/language_list');

	}

	public function updateLanuageFile($language_id=null){

		

		$languageInfo = LanguageModel::find($language_id);

		$language_code = ($languageInfo->code);


		$file_dir = base_path().'/resources/lang/'.strtolower($languageInfo->code);

		$filepath = $file_dir.'/messages.php';

		

		  if (!file_exists($file_dir)) {

			  mkdir($file_dir, 0777, true);

		   }

		   if (!file_exists($filepath)) {

		   }else{

			  $lang_content = json_decode($languageInfo->lang_content,JSON_UNESCAPED_UNICODE);

		      $putContent  = '<?php  return [';

			   foreach( $lang_content as $key=>$content){

				  $putContent .= '"'.$key.'"=>"'.$content.'",';

			   }

		    $putContent.='];';

		    $fh = fopen($filepath, 'w');

			file_put_contents($filepath, $putContent );

			fclose($fh);

		   }

	}
 
}



?>