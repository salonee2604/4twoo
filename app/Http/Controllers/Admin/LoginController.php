<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use App\User; 
use Redirect,Response,Session,Hash; 
use Auth,Helper ;

class LoginController extends Controller{

  public function index(){
    return view('admin/admin_login');
  }

  public function loginCheck(Request $request){
    $email = Input::get('email') ; 
    $password = Input::get('password') ; 

    $loginData = array(
     'email' => $email,
     'password' => $password,
     'usertype_id'=>'1'
   );

    if(Auth::attempt($loginData)){
      if(Auth::user()->usertype_id == '1'){
        $data = array('success'=>true,'message'=>'Erfolgreich eingeloggt');
      }else{
       $data = array('success'=>false,'message'=>'Hoppla. Etwas ist schief gelaufen. Bitte versuchen Sie es erneut!');
     }
   } else {
    $data = array('success'=>false,'message'=>'E-Mail-Adresse oder Passwort ist falsch');
  }

  return Response()->json($data);
}

public function forgot_password(){
  return view('admin/forgot_password');
}

public function forgot_password_function(Request $request){

  $email = $request->input('admin_email') ; 

  $checkUser = User::where('email','=',$email)->first() ;

//print_r($checkUser);die;
  if($checkUser){
    if($checkUser->status == 1){
      $user_id = Helper::encode($checkUser->id) ;
      $fullname = $checkUser->fullname ;

      $reset_link = url('admin/reset-password/'.$user_id) ;
      $template_id = '2' ;
      // //$templateData = Helper::get_mail_template($template_id) ;

      // if($templateData){
      //   $subject = $templateData->subject ; 
      //   $description= str_replace("%name%",$fullname,$templateData->description);
      //   $content= str_replace("%link%",$reset_link,$description);
      //   $mailBody= $content ;
      // }else{
      //   $subject = 'Forgot Password';
      //   $mailBody = '<html><body><b>Dear, '.$fullname .'</b><br><br>Please click below link to reset your password :- <br><br><a href="'.$reset_link.'">Reset Password</a><br><br><br>Thanks<br><b>fetLancer Team</b></body></html>' ;
      // }

      $subject = 'Forgot Password';
      $mailBody = '<html><body><b>Dear, '.$fullname .'</b><br><br>Please click below link to reset your password :- <br><br><a href="'.$reset_link.'">Reset Password</a><br><br><br>Thanks<br><b>4Twoo Team</b></body></html>' ;

      $mailData = array(
        'email'=>$email,
        'subject'=>$subject,
        'mailBody'=>$mailBody,
        'content' =>$mailBody,
      ) ;

      $output = Helper::send_mail($mailData); 

        //if($output == 1){
      $data = array('success'=>true,'message'=>'The link to reset your password has been sent to your email address.');

    }else{
     $data = array('success'=>false,'message'=>'Your account is deactivated. Please contact the support team.') ;
   }
 }else{
  $data = array('success'=>false,'message'=>'Email address was not found') ;
}

return Response()->json($data) ;

}

public function reset_password($param = ''){
  $data['user_id'] = $param ;
  return view('admin/reset_password',$data) ;
}


public function reset_password_function(Request $request){
  $password = $request->input('password') ;

  $user_id = Helper::decode($request->input('admin_id')) ;

  $login_ip = $_SERVER['REMOTE_ADDR'];

  $arrayData = array(
    'password'=>bcrypt($password),
    //'salt'=>$password,
    //'login_ip'=>$login_ip,
    'updated_at'=>date('Y-m-d H:i:s')
  );

  $update = User::where('id','=',$user_id)->update($arrayData);
  $message = 'The password was changed successfully' ; 

  if($update){ 
    $arr = array('success'=>true,'message'=>$message);
  } else {
    $arr = array('success'=>false,'message'=>'Something went wrong. Please try again');
  }
  return Response()->json($arr);

}

public function logout(){
  Auth::logout();
  Session::flush();
  return redirect('/admin') ;

}

}
