<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\FormateModel;
use Illuminate\Support\Facades\Auth;
use Validator,
    DB;
use Illuminate\Validation\Rule;
use Twilio\Rest\Client;
use Session;
use App\Helpers\Helper;

class NotificationFormateController extends Controller {

    public function formate_listing(Request $request) {
		  $uri = $request->path();
		if(Auth::user()->role_id==4){
			 $accessPermission = Helper::getAccessPath($uri,Auth::user()->id);
			if(!$accessPermission){
			   return redirect('/admin/error-access-permission'); 
			}
		} 
        $data['formate_list'] = DB::table('notification_format')->get();
        return view('admin/notification_formate/listing')->with($data);
    }

    public function add_format() {
        $data['language_list'] = DB::table('languages')->where(['isenabled' => 1])->get()->all();
        return view('admin/notification_formate/add_format')->with($data);
        ;
    }

    public function submit_formate(Request $request) {

        $validator = Validator::make($request->all(), [
                    'message' => 'required',
                    'notification_type' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect('/admin/add_format')->withErrors($validator)->withInput();
        } else {
            $message = $request->message;
            $notification_type = $request->notification_type;
            $for_student = $request->for_student;
            $language = $request->language;

            $where = array(
                [
                    'notification_type', '=', $notification_type
                ],
                [
                    'language', '=', $language
                ],
                [
                    'for_student', '=', (!empty($for_student) ? $for_student : 0)
                ]
            );
            $records = DB::table('notification_format')->where($where)->get()->all();
            if (!empty($records)) {
                return redirect()->back()->withErrors(['error' => 'Notification type with "' . $language . '" language already exist.'])->withInput();
            } else {
                $image = $request->file('icon_file');
                $imagename = time() . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('uploads/notification_icon');
                $image->move($destinationPath, $imagename);

                $obj = new FormateModel;
                $obj->notification_type = $notification_type;
                $obj->notification_msg = $message;
                $obj->notification_icon = (!empty($imagename) ? $imagename : NULL);
                $obj->for_student = (!empty($for_student) ? $for_student : 0);
                $obj->language = $language;
                $obj->status = 1;
                $obj->created_at = date('Y-m-d H:i:s');
                $obj->updated_at = date('Y-m-d H:i:s');
                $res = $obj->save();
                if ($res) {
                    session::flash('message', 'Notification formate addeed Succesfully.');
                    return redirect('admin/formate_list');
                } else {
                    return redirect()->back()->withErrors(['error' => 'Notification formate not added pleas try agian.'])->withInput();
                }
            }
        }
    }

    public function change_format_status(Request $request) {
        $grade_info = FormateModel::find($request->format_id);
        $grade_info->status = $request->status;
        $grade_info->save();
        return response()->json(['success' => 'Notification format status change successfully.']);
    }

    public function delete_format(Request $request) {
        $format_id = $request->format_id;

        $grade_info = DB::table('notification_format')->where('id', '=', $format_id)->first();

        $res = DB::table('notification_format')->where('id', '=', $format_id)->delete();

        if ($res) {
            return json_encode(array('status' => 'success', 'msg' => 'Data has been deleted successfully!'));
        } else {
            return json_encode(array('status' => 'error', 'msg' => 'Some internal issue occured.'));
        }
    }

    public function edit_format(Request $request) {
        $format_id = base64_decode($request->id);
        $data['language_list'] = DB::table('languages')->where(['isenabled' => 1])->get()->all();
        $data['format_info'] = FormateModel::find($format_id);
        return view('admin/notification_formate/edit_format')->with($data);
    }

    public function update_format(Request $request) {
        $message = $request->message;
        $notification_type = $request->notification_type;
        $for_student = $request->for_student;
        $language = $request->language;
        $validator = Validator::make($request->all(), [
                    'message' => 'required',
                    'notification_type' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $format_id = $request->input('format_id');
            $where = array(['notification_type', '=', $notification_type]);
            if (!empty($format_id)) {
                $where[] = ['id', '!=', $format_id];
            }
            $where[] = ['language', '=', $language];
            $where[] = ['for_student', '=', (!empty($for_student) ? $for_student : 0)];

            $records = DB::table('notification_format')->where($where)->get()->all();
            if (!empty($records)) {
                return redirect()->back()->withErrors(['error' => 'Notification type with "' . $language . '" language already exist.'])->withInput();
            } else {
                if ($request->hasFile('icon_file')) {
                    $image = $request->file('icon_file');
                    $imagename = time() . '.' . $image->getClientOriginalExtension();
                    $destinationPath = public_path('uploads/notification_icon');
                    $image->move($destinationPath, $imagename);
                }


                $format_id = $request->input('format_id');
                $objData = FormateModel::where('id', $format_id)->first();
                $objData->notification_type = $notification_type;
                $objData->notification_msg = $message;

                if (!empty($imagename)) {
                    $objData->notification_icon = $imagename;
                }

                $objData->for_student = (!empty($for_student) ? $for_student : 0);
                $objData->language = $language;
                $objData->updated_at = date('Y-m-d H:i:s');
                $res = $objData->save();
                if ($res) {
                    session::flash('message', 'Notification formate updated succesfully.');
                    return redirect('admin/formate_list');
                } else {
                    session::flash('error', 'Somthing went wrong.please try agian');
                    return redirect('admin/formate_list');
                }
            }
        }
    }

}
