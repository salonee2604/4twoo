<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 
use Illuminate\Support\Facades\Auth; 
use Validator, DB;
use App\User;
use Illuminate\Validation\Rule;
use Twilio\Rest\Client;
use Session;
use App\Helpers\Helper;

class CMSController extends Controller
{
	public function update_logo(Request $request)
	{
		  $uri = $request->path();
		if(Auth::user()->role_id==4){
			 $accessPermission = Helper::getAccessPath($uri,Auth::user()->id);
			if(!$accessPermission){
			   return redirect('/admin/error-access-permission'); 
			}
		} 
		$data['logo_info'] = DB::table('logo_file')->first();
		return view('admin/cms/update_logo')->with($data);
	}

	public function edit_video(Request $request)
	{    $uri = $request->path();
		if(Auth::user()->role_id==4){
			 $accessPermission = Helper::getAccessPath($uri,Auth::user()->id);
			if(!$accessPermission){
			   return redirect('/admin/error-access-permission'); 
			}
		} 
		$data['video_info'] = DB::table('video_link')->first();
		return view('admin/cms/edit_video')->with($data);
	}

	public function update_video(Request $request)
	{
		$link_info = DB::table('video_link')->first();

        
        $video_link = str_replace("https://www.youtube.com/watch?v=","http://www.youtube.com/embed/",$request->video_link);
        $video_link = $video_link.'?wmode=opaque';
        
	     if (!empty($link_info->id)) {
	     		
	     		$resp = DB::table('video_link')
			            ->where('id', $link_info->id)
			            ->update([
			            	'video_link' => (!empty($video_link) ? $video_link : null),
		        				'updated_at' => date('Y-m-d H:i:s')
			            ]);
	     } else {
	     		$resp = DB::table('video_link')->insertGetId([
			        			'title' => (!empty($request->title) ? $request->title : null), 
			        			'video_link' => (!empty($video_link) ? $video_link : null),
			        			'status' => 1,
			        			'created_at' => date('Y-m-d H:i:s'),
			        			'updated_at' => date('Y-m-d H:i:s')
		        		  ]);
	     }
	     if ($resp) {
	     		Session::flash('success', 'Video link has been updated successfully!'); 
	     } else {
	     		Session::flash('error', 'Some internal issue occured!'); 
	     }
	     return redirect('/admin/edit_video');
	}
	public function update_video_student(Request $request){
		
		$link_info = DB::table('video_link')->first();
        $video_link = str_replace("https://www.youtube.com/watch?v=","http://www.youtube.com/embed/",$request->video_link_student);
        $video_link = $video_link.'?wmode=opaque';
        
	     if (!empty($link_info->id)) {
	     		
	     		$resp = DB::table('video_link')
			            ->where('id', $link_info->id)
			            ->update(['video_link_student' => (!empty($video_link) ? $video_link : null),
		        				'updated_at' => date('Y-m-d H:i:s')
			              ]);
	     } else {
	     		$resp = DB::table('video_link')->insertGetId([
			        			'title' => (!empty($request->title) ? $request->title : null), 
			        			'video_link_student' => (!empty($video_link) ? $video_link : null),
			        			'status' => 1,
			        			'created_at' => date('Y-m-d H:i:s'),
			        			'updated_at' => date('Y-m-d H:i:s')
		        		  ]);
	     }
	     if ($resp) {
	     		Session::flash('success', 'Video link has been updated successfully!'); 
	     } else {
	     		Session::flash('error', 'Some internal issue occured!'); 
	     }
	     return redirect('/admin/edit_video');
	}

	public function edit_logo(Request $request){
      
        $validator = Validator::make($request->all(), [
                    'title' => 'required',
                    'logo_file' => 'required_without:id|mimes:jpeg,png,jpg,gif,svg|max:2048',
                    'favicon_file' => 'required_without:id'
                    ]);
        if ($validator->fails()) {
            return redirect('/admin/update_logo')->withErrors($validator)->withInput();
        } else {

              $title = $request->get('title');
              $media_id = $request->get('media_id');
			
			   $data = array();
			   $data['title']= $title;
			   $data['status']= 1;
			   $upload_path = public_path().'/uploads/logo/';
			   $logo_img="";
			   $homelogo_img="";
			   $favicon_img="";
		   
		   
           if ($request->hasFile('logo_file')) {
                if (!file_exists($upload_path)) {
                    mkdir($upload_path, 0777, true);
               }
		        $image = $request->file('logo_file');
		        $logo_img = 'logo_'.time().'.'.$image->getClientOriginalExtension();
		        $image->move($upload_path, $logo_img);
                $data['logo_img'] = $logo_img;
             }
			 if ($request->hasFile('logo_home_page')) {
                if (!file_exists($upload_path)) {
                    mkdir($upload_path, 0777, true);
               }
		        $image = $request->file('logo_home_page');
		        $homelogo_img = 'home_logo_'.time().'.'.$image->getClientOriginalExtension();
		        $image->move($upload_path, $homelogo_img);
                $data['homelogo_img'] = $homelogo_img;
             }
			 
			 
			 if ($request->hasFile('favicon_file')) {
               
		        $favicon = $request->file('favicon_file');
		        $favicon_img = 'favicon'.'.'.$favicon->getClientOriginalExtension();
		        $favicon->move( base_path(), $favicon_img);
                $data['favicon'] = $favicon_img;
             }
			 
			 if ($request->hasFile('admin_background_img')) {
               
		        $admin_background_img = $request->file('admin_background_img');
		        $adminBackgroundImg = 'admin_background_img_'.time().'.'.$admin_background_img->getClientOriginalExtension();
		        $admin_background_img->move($upload_path, $adminBackgroundImg);
                $data['admin_background_img'] = $adminBackgroundImg;
             }
			 
			 
			 
			 
			 
			  $logoInfo = DB::table('logo_file')->first();
			 
                if (!empty($logoInfo)) {
                    $data['updated_at'] = date('Y-m-d H:i:s');
                 $resp =    DB::table('logo_file')->where(['id' =>$logoInfo->id])->update($data);
                    $msg = 'data updated successfully.';
                } else {
                    $data['created_at'] = date('Y-m-d H:i:s');
                  $resp =   DB::table('logo_file')->insert($data);
                    $msg = 'data add successfully.';
                }
		   if ($resp) {
					Session::flash('success', 'data updated successfully!'); 
			} else {
					Session::flash('error', 'Some internal issue occurred!'); 
			}
			return redirect('/admin/update_logo');
            }
   }
        public function manageMediaList(Request $request) {
			  $uri = $request->path();
			if(Auth::user()->role_id==4){
				 $accessPermission = Helper::getAccessPath($uri,Auth::user()->id);
				if(!$accessPermission){
				   return redirect('/admin/error-access-permission'); 
				}
			} 
          $data['manage_media'] = DB::table('manage_media')->get();
          return view('admin/cms/media_list')->with($data);   
        }
        public function addManageMedia($id=0,Request $request) {
            $data['manage_media'] ="";
            if (!empty($id)) {
             $data['manage_media'] = DB::table('manage_media')->where('id', '=',base64_decode($id))->first();
            }
           return view('admin/cms/manage_media')->with($data);
        }
        public function savemedia(Request $request) {
      
        $validator = Validator::make($request->all(), [
                    'title' => 'required',
                    'files' => 'required_without:media_id|mimes:jpeg,png,jpg,gif,svg|max:2048'
                    ]);
        if ($validator->fails()) {
            return redirect('/admin/manage-media')->withErrors($validator)->withInput();
        } else {

            $title = $request->get('title');
            $media_id = $request->get('media_id');
            $data = array();
           $data['title']= $title;
           if ($request->hasFile('files')) {
                $upload_path = public_path() . '/uploads/media_icon/';
                if (!file_exists($upload_path)) {
                    mkdir($upload_path, 0777, true);
                }
                $file = $request->file('files');
                $extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
                $fileformat = $file->getClientMimeType();
                $iconName = 'icon_'.time().'.'. $extension;
                $file->move($upload_path, $iconName);
                $data['icon']=$iconName;
            }
              
         
                if (!empty($media_id)) {
                    $data['updated_at'] = date('Y-m-d H:i:s');
                    DB::table('manage_media')->where(['id' => $media_id])->update($data);
                    $msg = 'data updated successfully.';
                } else {
                    $data['created_at'] = date('Y-m-d H:i:s');
                     $data['url']= 'manage-default-curriculum';
                     $data['admin_url']= 'curriculum-default';
                    DB::table('manage_media')->insert($data);
                    $msg = 'data add successfully.';
                }
                return redirect()->to('admin/manage-media-list')->with('message', $msg);
            //}
        }
            
        }
    public function changeManageMediaStatus(Request $request) {
       $media_id = $request->media_id;
        DB::table('manage_media')->where(['id' => $media_id])->update(['status'=>$request->status]);
        return response()->json(['success' => 'Status change successfully.']);
    }
	public function ChangeBackgroundAdmin(Request $request){
		$data['colorCodeCss'] = DB::table('logo_file')->first();
		return view('admin/cms/change_background_css')->with($data);
	}
	
	public function SaveChangeBackgroundAdmin(Request $request){
	    $validator = Validator::make($request->all(), [
                       'color_code' => 'required',
                    ]);
        if ($validator->fails()) {
            return redirect('/admin/change-background-admin')->withErrors($validator)->withInput();
        } else {

               $color_code = $request->get('color_code');
               $logoInfo = DB::table('logo_file')->first();
			 
                if (!empty($logoInfo)) {
                    $resp = DB::table('logo_file')->where(['id' =>$logoInfo->id])->update(['color_code'=>$color_code]);
                    $msg = 'data updated successfully.';
                } else {
                     $resp = DB::table('logo_file')->insert(['color_code'=>$color_code]);
                     $msg = 'data add successfully.';
                }
		   if ($resp) {
					Session::flash('success',$msg); 
			} else {
					Session::flash('error', 'Some internal issue occurred!'); 
			}
			return redirect('/admin/change-background-admin');
        }
	}
        
        
    public function emailFormateList(Request $request) {
		  $uri = $request->path();
		if(Auth::user()->role_id==4){
			 $accessPermission = Helper::getAccessPath($uri,Auth::user()->id);
			if(!$accessPermission){
			   return redirect('/admin/error-access-permission'); 
			}
		} 
        $data['formate_list'] = DB::table('email_formate')->get();
        return view('admin/cms/email_listing')->with($data);
    }

    public function addEmailFormate($emailformate_id="") {
        $data['emailformate']="";
        $emailformate_id = base64_decode($emailformate_id);
        if(!empty($emailformate_id)){
          $data['emailformate']= DB::table('email_formate')->where(['id' =>$emailformate_id])->get()->first();   
        }
        $data['language_list'] = DB::table('languages')->where(['isenabled' => 1])->get()->all();
        return view('admin/cms/add_email_format')->with($data);
        ;
    }
    public function saveEmailFormate(Request $request) {

        $validator = Validator::make($request->all(), [
                    'body' => 'required',
                    'subject' => 'required',
                    'email_type' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect('/admin/addemailformate')->withErrors($validator)->withInput();
        } else {
            $email_formate_id = $request->id;
            $body = $request->body;
            $subject = $request->subject;
            $email_type = $request->email_type;
            $language = $request->language;

            $where = array(['email_type', '=', $email_type],['language', '=',$language],);
             if(!empty($student_id)){
                $where[]=['id','!=',$email_formate_id];
             }
            
            $records = DB::table('email_formate')->where($where)->get()->all();
            if (!empty($records)) {
                return redirect()->back()->withErrors(['error' => 'Email type with "' . $language . '" language already exist.'])->withInput();
            } else {
               $data= array();
                $data['body'] = $body;
                $data['subject'] = $subject;
                $data['email_type'] = $email_type;
                $data['language'] = $language;
               if (!empty($email_formate_id)) {
                    $data['created_at'] = date('Y-m-d H:i:s');
                    $res = DB::table('email_formate')->where(['id' =>$email_formate_id])->update($data);
                    $msg = 'data updated successfully.';
                } else {
                     $data['updated_at'] = date('Y-m-d H:i:s');
                     $res = DB::table('email_formate')->insert($data);
                     $msg = 'data add successfully.';
                }
                if ($res) {
                    session::flash('message', 'Email formate addeed Succesfully.');
                    return redirect('admin/email-system-list');
                } else {
                    return redirect()->back()->withErrors(['error' => 'Email formate not added pleas try agian.'])->withInput();
                }
            }
        }
    }
    public function deleteformat($format_id="",Request $request) {
            $format_id = $request->format_id;
            $res = DB::table('email_formate')->where('id','=',$format_id)->first();
            if ($res) {
                 $res = DB::table('email_formate')->where('id', '=', $format_id)->delete();
                session::flash('message', 'Email deleted successfully!.');
                return redirect('admin/email-system-list');
            } else {
               session::flash('error', 'Email not deleted.please try agian!.');
               return redirect('admin/email-system-list');
            }
    }
    
     public function subuserlist(Request $request) {
		   $uri = $request->path();
		if(Auth::user()->role_id==4){
			 $accessPermission = Helper::getAccessPath($uri,Auth::user()->id);
			if(!$accessPermission){
			   return redirect('/admin/error-access-permission'); 
			}
		} 
        $data['subuserlist'] = DB::table('users')
                                  ->select('users.*')
                                  ->where('role_id',1)
								  ->orWhere('role_id',4)
								  ->get();
        return view('admin/admin_subuser/subsuer_list')->with($data);
    }
    public function addsubuser() {
        $data['grade_list'] = DB::table('grades')->where('status', 1)->get();
        $data['curriculum_list'] = DB::table('curriculums')->where('status', 1)->get();
        return view('admin/admin_subuser/add_subsuer')->with($data);
    }
    public function updateuser(Request $request) {
        $student_id = base64_decode($request->id);
        $data['userinfo'] = User::find($student_id);
        return view('admin/admin_subuser/add_subsuer')->with($data);
    }
    public function savesubsuer(Request $request) {

		$messages = [
					'role_id.required' => 'Select user role',
				];
       $validator = Validator::make($request->all(), [
                    'role_id' => 'required',
                    'name' => 'required',
                    'username' => 'required|unique:users',
                    'email' => 'required|email|max:255|unique:users',
                    'password' => 'required|min:6',
                    'confirm_password' => 'required|same:password',
                    ],$messages);

        if ($validator->fails()) {
            return redirect('/admin/addsubuser')->withErrors($validator)->withInput();
        } else {
			
            $role_id = $request->role_id;
            $name = $request->name;
            $email = $request->email;
            $password = $request->password;
            $username = $request->username;
            $curriculum = $request->curriculum;
            $grade = $request->grade;
            $name_arr = explode(' ', $name);
            $obj = new User;
            $obj->first_name = (!empty($name_arr[0]) ? $name_arr[0] : '');
            $obj->last_name = (!empty($name_arr[1]) ? $name_arr[1] : '');
            $obj->username = $username;
            $obj->email = $email;
            $obj->role_id = $role_id;
            $obj->password = bcrypt($password);
            $obj->temp_password = $password;
            $obj->status = 1;
            $obj->created_at = date('Y-m-d H:i:s');
            $res = $obj->save();
            if ($res) {
                session::flash('message', 'User Addeed Succesfully.');
                return redirect('admin/subuserlist');
            } else {
                session::flash('error', 'User records not inserted.');
                return redirect('admin/subuserlist');
            }
        }
    }
    public function changeuserpassword(Request $request) {
        $user_id = base64_decode($request->id);
        $data['userInfo'] = User::find($user_id);
        return view('admin/admin_subuser/change_password')->with($data);
    }
    public function updateuserpassword(Request $request) {
        $user_id = $request->user_id;
        $user_info = User::find($user_id);
        $validatedData = $request->validate([
            'new-password' => 'required|string|min:6|confirmed',
        ]);
        
         $password = bcrypt($request->input('new-password'));
         $temp_password = $request->input('new-password');
        DB::table('users')
        ->where('id', $user_id)
        ->update(['password' => $password,'temp_password' =>$temp_password,'updated_at' => date('Y-m-d H:i:s')]);
        return redirect()->back()->with("message", "Password changed successfully !");
    }
    public function deleteUser($user_id="",Request $request) {
        $userinfo = DB::table('users')->where('id', '=', $user_id)->first();
        if ($userinfo) {
            $res = DB::table('users')->where('id', '=', $user_id)->delete();
             session::flash('message', 'User deleted successfully!.');
             return redirect('admin/subuserlist');
        } else {
            session::flash('message', 'Invalid user');
            return redirect('admin/subuserlist');
        }
    }
    public function emailsetting(Request $request) {
		  $uri = $request->path();
		if(Auth::user()->role_id==4){
			 $accessPermission = Helper::getAccessPath($uri,Auth::user()->id);
			if(!$accessPermission){
			   return redirect('/admin/error-access-permission'); 
			}
		} 
        $data['emailsetting'] = DB::table('email_setting')->where(['id' => 1])->get()->first();
        return view('admin/cms/email_setting')->with($data);
    }

    public function saveemailsetting(Request $request) {
        
        $validator = Validator::make($request->all(), [
                    'email_name' => 'required',
                    'email_user' => 'required',
                    'email_password' => 'required',
                    'email_port' => 'required',
                    'email_smtp' => 'required',
                ]);
        if ($validator->fails()) {
            return redirect('/admin/email_setting')->withErrors($validator)->withInput();
        } else {
            $email_name = $request->get('email_name');
            $email_user = $request->get('email_user');
            $email_password = $request->get('email_password');
            $email_port = $request->get('email_port');
            $email_smtp = $request->get('email_smtp');
            $is_email_ssl = $request->get('is_email_ssl');
            
            $emailsettingData = array(
                                    'email_name'=>$email_name,
                                    'email_user'=>$email_user,
                                    'email_password'=>$email_password,
                                    'email_port'=>$email_port,
                                    'email_smtp'=>$email_smtp,
                                    'is_email_ssl'=>$is_email_ssl,
                                    );
              
            $email_setting = DB::table('email_setting')->first();
            if (!empty($email_setting)) {
                $resp = DB::table('email_setting')->where(['id' => $email_setting->id])->update($emailsettingData);
                $msg = 'Email setting updated successfully.';
            } else {
                $resp = DB::table('email_setting')->insert($emailsettingData);
                $msg = 'Email setting added successfully.';
            }
            if ($resp) {
                Session::flash('success', $msg);
            } else {
                Session::flash('error', 'Some internal issue occurred!');
            }
            return redirect('/admin/email-setting');
        }
    }
	
	 public function UpdatePermission($user_id="") {

        $menu = DB::table('menu')->where('is_parent',0)->get();
        //print_r($menu);die;
		if(!empty($menu)){
			foreach($menu as $key=>$menuVal){
			
			   $childMenu = DB::table('menu')->where('is_parent',$menuVal->id)->get()->all();
			   if(!empty($childMenu)){
				   $menu[$key]->is_child = $childMenu; 
			   }else{
				   $menu[$key]->is_child = ""; 
			   }
			}
		}
	    $data['menulist'] = $menu;
	    $data['user_id'] = $user_id = base64_decode($user_id);
		//$data['permission'] = DB::table('permission')->select('menu_id')->where('user_id', '=',$user_id)->get()->all();
        $collection = collect(['name' => 'Desk', 'price' => 200]);
        $collection->toArray();       

	   return view('admin/admin_subuser/update_permission')->with($data);
    }
	public function saveupdatepermission(Request $request){
		 $data = $request->all();
		 $user_id = $request->get('user_id'); 
		 $permissionData = array();
		    foreach ($data as $key => $value) {
		
			    if($key != '_token' && $key != 'user_id'){
				
				  $permissionData[] = array('user_id'=>$user_id,
											'menu_id'=>$value
											);
				
				}
		    }
		
		   DB::table('permission')->where('user_id', '=',$user_id)->delete();
		   DB::table('permission')->insert($permissionData);
		Session::flash('message','User permission update successfully');
		return redirect('/admin/subuserlist');
    }
	

}
?>