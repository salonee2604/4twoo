<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller; 

use Illuminate\Support\Facades\Auth; 

use Validator, DB;

use App\User;

use Illuminate\Validation\Rule;

use Twilio\Rest\Client;

use Session;

use Route;

use App\Helpers\Helper;



class TableBookingController extends Controller

{

        /**

     * Show the application dashboard.

     *

     * @return \Illuminate\Contracts\Support\Renderable

     */



    public function index(Request $request)

    {
        if (empty(Auth::user())) {
            return redirect('/admin');
        }else{
            $uri = $request->path();

        if(Auth::user()->role_id==4){

             $accessPermission = Helper::getAccessPath($uri,Auth::user()->id);

            if(!$accessPermission){

               return redirect('/admin/error-access-permission'); 

            }

        } 

        $now_date = date('Y-m-d H:i:s');

        $current_date = date('Y-m-d');

        $current_time = date('H:i:s');

        $data['total_users'] = DB::table('users')->where('role_id','=',2)->count();

        $data['total_stories'] = DB::table('success_story')->where('status','=',1)->count();

        $data['total_testimonial'] = DB::table('testimonial')->where('status','=',1)->count();

        return view('admin/dashboard')->with($data);

        }

    }

    public function listOfSection(Request $request)
    {
        
        if (empty(Auth::user())) 
        {
            return redirect('/admin');
        }
        else
        {
	        $sql= DB::table('table_section');
	        $sql->join('restaurants_detail', 'restaurants_detail.rest_id', '=', 'table_section.rest_id');
	        $sql->select('table_section.*', 'restaurants_detail.*');
	        $sql->orderBY('section_id','desc');
	        $tablesections=$sql->get();
	        return view('admin.listSection',['tablesections'=>$tablesections]);
              
        }
    }

    public function addSection()
    {
        return view('admin.addSection');
    }

    public function submitsection(Request $request) 
    {

        $validator = Validator::make($request->all(), [

                    'table_type' => 'required',
        ]);

        if ($validator->fails()) {
        return redirect('/admin/addSection')->withErrors($validator)->withInput();

        }else {

            $section_name = $request->table_type;
            $status = 1;
            $created_at = date('Y-m-d H:i:s');
            $data = array(
                'section_name' => $section_name,
                'status'       => $status,
                'created_at'   => $created_at,
                'rest_id'      => 1,
            );
           
            $updateRow = DB::table('table_section')->insert($data);
           
            $id = DB::getPdo()->lastInsertId();

            if ($updateRow) {

                session::flash('message', 'Table Type added succesfully.');

                return redirect('admin/TableBooking');

            } else {

                session::flash('error', 'Table Type  not inserted.');

                return redirect('admin/TableBooking');

            }

        }

    }



    public function viewsection(Request $request)
    {
        $section_id = base64_decode(request()->segment(3));
        $sql= DB::table('table_section');
        $sql->join('restaurants_detail', 'restaurants_detail.rest_id', '=', 'table_section.rest_id');
        $sql->select('table_section.*', 'restaurants_detail.*');
        $sql->where('table_section.section_id', $section_id);
        $sql->orderBY('section_id','desc');
        $tablesections=$sql->first();
        $data['section'] =  $tablesections;
        return view('admin/view_section')->with($data);
      
    }
    public function editsection(Request $request)
    {
        $section_id    = $request->id;
        $sql= DB::table('table_section');
        $sql->join('restaurants_detail', 'restaurants_detail.rest_id', '=', 'table_section.rest_id');
        $sql->select('table_section.*', 'restaurants_detail.*');
        $sql->where('table_section.section_id', $section_id);
        $sql->orderBY('section_id','desc');
        $tablesections=$sql->first();
        $data['section'] =  $tablesections;
        return view('admin/edit_section')->with($data);
      
    } 

    public function updateSection(Request $request)
    {
        $section_id        = $request->section_id;
        $section_name      = $request->table_type;
        $data              = array(
            'section_name' => $section_name,
            );

        $updateRow = DB::table('table_section')->where('section_id', $section_id)->update($data);  
        if ($updateRow) {
            session::flash('message', 'Table Type updated succesfully.');
            return redirect('admin/TableBooking');
        } else {
            session::flash('error', 'Somthing went wrong.');
            return redirect('admin/TableBooking');
        }
      
    } 

    public function delete_section(Request $request) 
    {

        $section_id = $request->section_id;
        $student_info = DB::table('table_section')->where('section_id', '=', $section_id )->first();

        $res = DB::table('table_section')->where('section_id', '=', $section_id )->delete();

        if ($res) {

            return json_encode(array('status' => 'success', 'msg' => 'Data has been deleted successfully!'));

        } else {

            return json_encode(array('status' => 'error', 'msg' => 'Some internal issue occured.'));

        }
    }

    
    public function tableList(Request $request) 
    {
        if (empty(Auth::user())){
            return redirect('/admin');
        }else{
           

            $sql= DB::table('table_list');
            $sql->join('restaurants_detail', 'table_list.rest_id', '=', 'restaurants_detail.rest_id','inner');
            $sql->join('table_section', 'table_list.section_id', '=', 'table_section.section_id','inner');
            $sql->select('table_section.*', 'restaurants_detail.*','table_list.*');
            $sql->orderBY('table_id','desc');
            $sql->groupBy('table_id');
            $tablesections=$sql->get();
            $data['table_list'] = $tablesections;
            return view('admin/tablebooking/table_booking')->with($data);
        }
    }

    public function addTableList()
    {

        $data['all_section_list'] = DB::table('table_section')->where('status',1)->get();
        return view('admin/tablebooking/addTableList')->with($data);
    }
    public function submit_tablelist(Request $request) 
    {



       $validator = Validator::make($request->all(), [

                    'table_type' => 'required',

                    'table_number' => 'required',

                    'table_seet_capacity' => 'required'

        ]);

        if ($validator->fails()) {
            

            return redirect('/admin/addTableList')->withErrors($validator)->withInput();

        } else {

            $section_id         = $request->table_type;
           
            $table_number         = $request->table_number;

            $table_seet_capacity  = $request->table_seet_capacity;
           
            $status = 1;
           
            $created_at = date('Y-m-d H:i:s');
            $data = array(
                
                'rest_id' => 1,
                'section_id' => $section_id ,
                'table_no' => $table_number,
                'table_seet_capacity' =>$table_seet_capacity,
                'status'=> 1,
                'created_at' => $created_at
            );
           
            $updateRow = DB::table('table_list')->insert($data);
           
            $id = DB::getPdo()->lastInsertId();

            if ($updateRow) {

                session::flash('message', 'Table Management addeed succesfully.');

                return redirect('admin/tableList');

            } else {

                session::flash('error', 'Table Management records not inserted.');

                return redirect('admin/tableList');

            }

        }

    }

    public function view_table_list(Request $request)
    {

        $table_id = base64_decode(request()->segment(3));
        $sql= DB::table('table_list');
        $sql->join('restaurants_detail', 'table_list.rest_id', '=', 'restaurants_detail.rest_id','inner');
        $sql->join('table_section', 'table_list.section_id', '=', 'table_section.section_id','inner');
        $sql->select('table_section.*', 'restaurants_detail.*','table_list.*');
        $sql->where('table_id',$table_id);
        $sql->orderBY('table_id','desc');
        $sql->groupBy('table_id');
        $tablesections=$sql->first();
        $data['view_table_list'] = $tablesections;
        return view('admin/tablebooking/view_TableList')->with($data);

    }

    public function edittablelist()
    {


        $table_id = base64_decode(request()->segment(3));
        $sql= DB::table('table_list');
        $sql->join('restaurants_detail', 'table_list.rest_id', '=', 'restaurants_detail.rest_id','inner');
        $sql->join('table_section', 'table_list.section_id', '=', 'table_section.section_id','inner');
        $sql->select('table_section.*', 'restaurants_detail.*','table_list.*');
        $sql->where('table_id',$table_id);
        $sql->orderBY('table_id','desc');
        $sql->groupBy('table_id');
        $tablesections=$sql->first();
        $data['editdata'] = $tablesections;
        $data['all_section_list'] = DB::table('table_section')->where('status',1)->get();
        return view('admin/tablebooking/editTableList')->with($data); 
    }

    public function update_tablelist(Request $request)
    {
        $table_id                 = $request->table_id;
        $section_id               = $request->section_name;
        $table_number             = $request->table_number;
        $table_seet_capacity      = $request->table_seet_capacity;
        $status                   = $request->status;
        $created_at               = date('Y-m-d H:i:s');
        $data = array(
                
                'rest_id' => 1,
                'section_id' => $section_id ,
                'table_no' => $table_number,
                'table_seet_capacity' =>$table_seet_capacity,
                'status'=> $status,
                'created_at' => $created_at
        );
        $updateRow = DB::table('table_list')->where('table_id', $table_id)->update($data);  
        if ($updateRow) {
            session::flash('message', 'Table List updated succesfully.');
            return redirect('admin/tableList');
        } else {
            session::flash('error', 'Somthing went wrong.');
            return redirect('admin/tableList');
        }  

    }




	public function errorAccessPermission(){

	    $data['error_message']= "you don't have permission to access this page";

        return view('admin/access_permission_error')->with($data);

    }

	

	public function logout(){

        Auth::logout();
        return redirect('/admin');

    }

     public function delete_table_list(Request $request) 
    {

        $table_id    = $request->table_id;

        $table_list = DB::table('table_list')->where('table_id', '=', $table_id)->first();

        $res = DB::table('table_list')->where('table_id', '=', $table_id)->delete();

        if ($res) {

            return json_encode(array('status' => 'success', 'msg' => 'Table Management has been deleted successfully!'));

        } else {

            return json_encode(array('status' => 'error', 'msg' => 'Some internal issue occured.'));

        }

    }
    
    public function timeSlotList(Request $request)
    {
        
        if (empty(Auth::user())) 
        {
            return redirect('/admin');
        }
        else
        {

            $day = request()->segment(3);
            $sql= DB::table('table_time_slot');
	        $sql->join('restaurants_detail', 'table_time_slot.rest_id', '=', 'restaurants_detail.rest_id');
	        $sql->select('table_time_slot.*', 'restaurants_detail.*');
            if($day != 'day')
            {
               $sql->where('day',$day);  
            }
            $sql->orderBY('time_slot_id','desc');
	        $tablesections=$sql->get();
	        $data['timeSlotList']  = $tablesections;
            return view('admin/tablebooking/time_slot_list')->with($data);
              
        }
    }


    public function add_timeslot()
    {
       
       return view('admin/tablebooking/addTimeSlot');

    }

     public function addslot_post(Request $request)
     {
            $validator = Validator::make($request->all(), [
               
                'day' => 'required',
                'start_time' => 'required|array',
                'end_time' => 'required|array',

                

            ]);

            if ($validator->fails()) {


            return redirect('/admin/add_timeslot')->withErrors($validator)->withInput();

            } else {
           $resturant_name = 1;
           $day = $request->day;
           $start_time = $request->start_time;
           $end_time = $request->end_time;
           $status = $request->status;
           $created_at               = date('Y-m-d H:i:s');
            for($count = 0; $count < count($start_time); $count++)
            {
               
              $starttimemain = $start_time[$count];
              if(!empty($starttimemain))
              {
                  
                $data = array(
                'rest_id' => $resturant_name,
                'day' => $day,
                'start_time'  => $start_time[$count],
                'end_time'  => $end_time[$count],
                'status'  => $status,
                'created_at' => $created_at
                    );
                $insert_data[] = $data; 

              }
               
            }

            $query=DB::table('table_time_slot')->insert($insert_data);

            if($query)
            {


                session::flash('message', 'Time Slot Add successfully');
                return redirect('admin/timeSlotList/day');
            }
            else
            {
                session::flash('error', 'Something went wrong');
                return redirect('admin/timeSlotList');
            }
            return redirect('admin/timeSlotList');
        }        
     }

    public function viewTimeSlot(Request $request)
    {


        $time_slot_id = base64_decode(request()->segment(3));
        $sql= DB::table('table_time_slot');
        $sql->join('restaurants_detail', 'table_time_slot.rest_id', '=', 'restaurants_detail.rest_id');
        $sql->select('table_time_slot.*', 'restaurants_detail.*');
        $sql->where('table_time_slot.time_slot_id', $time_slot_id);
        $sql->orderBY('time_slot_id','desc');
        $tablesections=$sql->get();
        $data['viewtabl_data'] =  $tablesections;
        return view('admin/tablebooking/view_timeslot')->with($data);
      
    }

    public function editTimeSlot(Request $request)
    {
        

        $time_slot_id = base64_decode(request()->segment(3));
        $sql= DB::table('table_time_slot');
        $sql->join('restaurants_detail', 'table_time_slot.rest_id', '=', 'restaurants_detail.rest_id');
        $sql->select('table_time_slot.*', 'restaurants_detail.*');
        $sql->where('table_time_slot.time_slot_id', $time_slot_id);
        $sql->orderBY('time_slot_id','desc');
        $tablesections=$sql->first();
        $data['Tableslot'] =  $tablesections;
        return view('admin/tablebooking/edit_timeslot')->with($data);
    }

    public function update_timeslot(Request $request)
    {

        $resturant_name = 1;
        $day            = $request->day;
	    $start_time     = $request->start_time;
	    $end_time       = $request->end_time;
	    $status         = $request->status;
	    $time_slot_id   = $request->time_slot_id;
	    $created_at     = date('Y-m-d H:i:s');
        $data = array(
                
                'rest_id' => 1,
                'day'     =>$day,
                'start_time' => $start_time  ,
                'end_time' => $end_time,
                'status'=> $status,
                'created_at' => $created_at
        );
        $updateRow = DB::table('table_time_slot')->where('time_slot_id', $time_slot_id)->update($data);  
        if ($updateRow) {
            session::flash('message', 'Time Slot updated succesfully.');
            return redirect('admin/timeSlotList/day');
        } else {
            session::flash('error', 'Somthing went wrong.');
            return redirect('admin/timeSlotList/day');
        }  
    }

    public function delete_timeslot(Request $request) 
    {

        $time_slot_id = $request->time_slot_id;
        $res = DB::table('table_time_slot')->where('time_slot_id', '=', $time_slot_id )->delete();

        if ($res) {

            return json_encode(array('status' => 'success', 'msg' => 'Data has been deleted successfully!'));

        } else {

            return json_encode(array('status' => 'error', 'msg' => 'Some internal issue occured.'));

        }
    }

    public function booktable_list()
    {

        if (empty(Auth::user())) 
        {
            return redirect('/admin');
        }
        else
        {
	        $sql= DB::table('table_booking');
	        $sql->join('restaurants_detail', 'table_booking.rest_id', '=', 'restaurants_detail.rest_id');
            $sql->join('users', 'table_booking.user_id', '=', 'users.id');
            $sql->join('table_time_slot', 'table_booking.time_slot_id', '=', 'table_time_slot.time_slot_id');
            $sql->select('table_booking.*', 'restaurants_detail.*','users.*','table_time_slot.*','table_booking.status as booking_status');
	        $sql->orderBY('table_booking_id','desc');
	        $tablesections=$sql->get();
            $data['booking_list']  = $tablesections;
            return view('admin/tablebooking/booking_list')->with($data);
              
        }

    }

    public function add_table_booking()
    {
      
        $data['userlist'] = DB::table('users')->where('status', '=', 1)->where('role_id', '!=', 1)->orderBy('id', 'DESC')->get();
        return view('admin/tablebooking/add_table_booking')->with($data);

    }

    public function ajax_timeslot(Request $request)
    {
        $rest_id  =$request->rest_id;
        $day      =$request->day;
        $timeslot = DB::table('table_time_slot')->where('day', '=', $day)->where('rest_id', '=', $rest_id)->get();
        if(!empty($timeslot))
        {    
            $response = '';
            foreach($timeslot as $row)
            {
                $time_slot_id=$row->time_slot_id;
                $start_time=$row->start_time;
                $end_time=$row->end_time;
                echo $response = "<option value='". $time_slot_id ."'>" . $start_time . ' - '.$end_time."</option>";
            }
        }
        else{
        echo "<option> Time Slot Not Available</option>";
        }
    }

   public function TimeSlot(Request $request){  
    $validator = Validator::make($request->all(), 
            [ 
              'rest_id'  => 'required',
              'date'  => 'required',
              'localization'  => 'required',
            ],
            [          
              'rest_id.required' => "required rest_id",          
              'date.required' => "required date",          
              'localization.required' => "required localization",          
            ]);
        if ($validator->fails()) {
              $messages = $validator->messages();
              foreach ($messages->all() as $message)
              {   
                  return response()->json(['status'=>$this->failureStatus,'msg'=>$message]);            
              }
            }
            $forminput = $request->all();
            $rest_id=$forminput['rest_id'];
            $date=$forminput['date'];
            $localization=$forminput['localization'];
            date_default_timezone_set('Asia/Riyadh');
            $currentDate =Date('Y-m-d');
            $currentTime =Date('H:i:s');
           
            $currentTime30Plus=date("H:i:s", strtotime("+30 minutes"));
         
       //// Check item in cart another restaurent item availble or not ///
            $time_slot = DB::table("table_time_slot");
            $time_slot->where("rest_id",$rest_id);
            $time_slot->where("status",1);
            $time_slot->orderby("start_time",'ASC');
            if($currentDate == $date){
             $time_slot->wheretime("start_time",">=", $currentTime30Plus);
            }
            $getdata=$time_slot->get();
    
        if(!empty(count($getdata))){
              return response()->json(array('status' => $this->successStatus, 'msg' => trans('messages.label_128') , 'response' =>$getdata,'child_limit'=>100,'adult_limit'=>100));  
        }else{
           return response()->json(['status'=>$this->failureStatus,'msg'=> trans('messages.label_32'),'child_limit'=>100,'adult_limit'=>100]);   
        } 
}

       public function table_booking_post(Request $request)
       {
        
       
        $rest_id = 1;
        $day = $request->input('day');
        $booking_date = $request->input('booking_date');
        $time_slot_id = $request->input('time_slot_id');
        $people = $request->input('number_of_memebers');
        $message = $request->input('message');
        $fullname = $request->input('fullname');
        $email = $request->input('email');
        $mobile_no = $request->input('mobile');
        $booking_unique_id=strtoupper(substr(md5(time()), 0, 8));           
        $exist_username = $request->input('exist_username');
        $customer_type = $request->input('customer_type');
        if(!empty($fullname))
        {

         $email_details = DB::table('users')->where('email', '=', $email)->first();
         if(!empty($email_details->email))
         {
           
            session::flash('error', 'Email Already Exist');
            return redirect('admin/booktable_list');  

         }
         else
         {
         $data = array('username'=>$fullname,"email"=>$email,"contact_number"=>$mobile_no);
         $query=DB::table('users')->insert($data);
         if($query){
            $id = DB::getPdo()->lastInsertId();
          $data1=array('rest_id'=>$rest_id,"booking_date"=>$booking_date,"booking_day"=>$day,"time_slot_id"=>$time_slot_id,"people"=>$people,"message"=>$message,"user_id"=>$id,"booking_unique_id"=>$booking_unique_id);
          $query1=DB::table('table_booking')->insert($data1);
           session::flash('message', 'Customer Table Booking Add Successfully');

       }
      else{
           session::flash('error', 'Something went wrong');
          }
          return redirect('admin/booktable_list');  

      }
  }
      else
      {
        $data_insert=array('rest_id'=>$rest_id,"booking_date"=>$booking_date,"booking_day"=>$day,"time_slot_id"=>$time_slot_id,"people"=>$people,"message"=>$message,"user_id"=>$exist_username,"booking_unique_id"=>$booking_unique_id);
       $query_insert=DB::table('table_booking')->insert($data_insert);
      if($query_insert)
      {
        session::flash('message', 'Customer Table Booking Add Successfully');
      }
      else
      {
        session::flash('error', 'Something went wrong');
      }
      return redirect('admin/booktable_list');  

    }
  }

  // email check function//
  public function uniqueemail(Request $request){
    echo $request->input('email');
    die;
        if ($request->input('email') !== '') {
            $user_id = '';
            if($request->input('user_id')){
                $user_id = $request->input('user_id');
            }
            //$validator = '';
            if ($request->input('email')) {
                $validator = Validator::make($request->all(), [             
                    'email' => [
                            'required',
                            Rule::unique('users')->ignore($user_id)->where(function ($query) {
                                return $query->where('user_status','!=', 2);
                            }),
                        ],        
                ]);

            }
            if (!$validator->fails()) {
                die('true');
            }
        }
        die('false');       
    }

    public function uniquenumber(Request $request)
    {
        if ($request->input('mobile') !== '') {
            $user_id = '';
            if($request->input('user_id')){
                $user_id = $request->input('user_id');
            }
            //$validator = '';
            if ($request->input('mobile')) {
                $validator = Validator::make($request->all(), [             
                    'mobile' => [
                            'required',
                            Rule::unique('users')->ignore($user_id)->where(function ($query) {
                                return $query->where('user_status','!=', 2);
                            }),
                        ],        
                ]);

            }
            if (!$validator->fails()) {
                die('true');
            }
        }
        die('false');       
    }

     public function view_table_booking_list(Request $request)
     {

            $id = base64_decode(request()->segment(3));
            $data['viewtabl_datas'] = DB::select('SELECT table_booking.*,users.*,restaurants_detail.*,table_time_slot.*,table_section.*,table_booking.status,table_list.table_no from table_booking INNER join restaurants_detail on restaurants_detail.rest_id=table_booking.rest_id left join users on users.id=table_booking.user_id left join table_time_slot on table_time_slot.time_slot_id=table_booking.time_slot_id left join table_section on table_section.section_id=table_booking.table_type left join table_list on table_list.table_id=table_booking.table_id where table_booking.table_booking_id = ?',[$id]);
            return view('admin/tablebooking/viewtable_booking')->with($data);
     }

    public function notification($user_id,$order_id,$no_id,$rest_id)
    {
      
      $notification = DB::table('notification')->insert([
        'user_id' => $user_id, 'order_id' => $order_id, 'no_id' => $no_id, 'tag' => 'table'
      ]);
        // return 1;
      $noti_status = DB::table('notification_list')->where('status',$no_id)->where('noti_cat','table')->first();
      $User = DB::table('users')->where('id',$user_id)->first();
       $Order = DB::table('table_booking')->where('booking_unique_id',$order_id)->first();
      if($Order->status==1){
              $status="Pending";
      }
      if($Order->status==2){
              $status="Cancelled";
      }
      $noti_status = DB::table('notification_list')->where('status',$no_id)->where('noti_cat','table')->first();
      $User = DB::table('users')->where('id',$user_id)->first();
      $localization='en';
        $mobile=$User->country_code.''.$User->mobile;
        $mobile=str_replace('+', '', $mobile); 
        smsorder($mobile,$Order->booking_unique_id,$status,$localization);
        $data1=array();
        

        if($User->setting_language == 'en'){
          $title_1 = $noti_status->noti_title_en;
        }else{
          $title_1 = $noti_status->noti_title_ar;
        }

        $data1["device_type"] = $User->device_type;
        $data1["title"] =  "Ocean Restaurant";

        if($User->setting_language == 'en'){
          $data1["body"] =  'Hey! '.$User->fullname.' '.$title_1;
        }else{
          $data1["body"] =  'هلا! '.$User->fullname.' '.$title_1;
        }

        if($no_id==2){
          $data1["tag"] =  'Cancel Table Status';
        }
        elseif($no_id==4){
          $data1["tag"] =  'Customer arrived';
        }
        else{
           $data1["tag"] =  'Table Status';
        }
       
        $data1["id"] = $order_id;
        $data1["rest_id"] = $rest_id;
        $data1["image_url"] = "resources/assets/images";
        $data1["link"] = "";
        $data1["sound_notification"] = 1; 
        $device_token = $User->firebase_token;
        /* $device_token='dI83PsAAJbM:APA91bH4ydkteQYs5fouNIiSuYTQoemyXIr0AOY3THwj9N6-20YqTsJnvb5IP0YJp9bJpkNknE94GO9IwpHDE-M-iqIs9iHCEWwfCCYyZWSc07vqIcgf7aGWvvqGJ4yoR2SSrvtmw6aS';  */

        $res=$this->sendPushNotification($data1, $device_token); 
          // print_r($res);die;
          
        return 1;
     }

    public function assign_table(Request $request)
    {


        $book_id = $request->input('book_id');
        $tabl_no = $request->input('tabl_no');
        if(!empty($tabl_no))
        {
            $tableImplode=implode(',', $tabl_no);
            $Tbl = DB::table('table_booking')->where('table_booking_id','=',$book_id)->first();
            $ii=0;
            foreach ($tabl_no as $key) 
            {         
             $checkTbl = DB::table('table_booking')->whereRaw("FIND_IN_SET($key,table_id)")
            ->where('booking_date','=',$Tbl->booking_date)->where('time_slot_id','=',$Tbl->time_slot_id)->count();
             $ii +=1;
            }
            if(count($tabl_no) > $ii){
            session::flash('error', 'Table already assign.');
            }
            else
            {
                $query=DB::update('update table_booking set table_id = ?, status = ?,table_status = ? where table_booking_id = ?',[$tableImplode,'3','3',$book_id]);
                // $noti = $this->notification($Tbl->user_id,$Tbl->booking_unique_id,'3',$Tbl->rest_id);
                $user = DB::table('users')->where('id',$Tbl->user_id)->first();
                $setting_language = "en";
                $email_content = DB::Table('email_formate')->where('id', 46)->select('email_type','subject','body')->first();
                    $searchArray = array("{user_name}", "{user_email}", "{order_id}","{site_url}","{table_number}");
                    $replaceArray = array($user->username, $user->email, $Tbl->booking_unique_id, url('/'),$tableImplode);
                    $content = str_replace($searchArray, $replaceArray, $email_content->body);
                    $data = [
                        'name'      => $user->username,
                        'email'     => $user->email,                    
                        'vrfn_code' => $user->vrfn_code,         
                        'subject'   => $email_content->subject,
                        'content'   => $content,
                    ];
                  Helper::send_mail($data);
                  session::flash('success', 'Assign Table successfully');
                 
              
                }
              }else{
                 session::flash('error', 'Something Went Wrong.');
              }
    }

     public function ajax_table_cancel(Request $request){
               $id = base64_decode($request->input('table_id'));
               @$reasonMessage = $request->input('reasonMessage');
               $tablestatus = $request->input('tablestatus');
               $query=DB::update('update table_booking set status = ?, table_status = ? where table_booking_id = ?',[$tablestatus,$tablestatus,$id]);
               $Tbl = DB::table('table_booking')->where('table_booking_id','=',$id)->first();
               // $noti = $this->notification($Tbl->user_id,$Tbl->booking_unique_id,'2',$Tbl->rest_id);
               $user = DB::table('users')->where('id',$Tbl->user_id)->first();
               $email_content = DB::Table('email_formate')->where('id', 47)->select('email_type','subject','body')->first();
               $searchArray = array("{user_name}", "{user_email}", "{comment}","{booking_id}","{site_url}");
               $replaceArray = array($user->username, $user->email, $reasonMessage, $Tbl->booking_unique_id, url('/'));
                $content = str_replace($searchArray, $replaceArray, $email_content->body);
                $data = [
                'name'      => $user->username,
                'email'     => $user->email,                    
                'vrfn_code' => $user->vrfn_code,         
                'subject'   => $email_content->subject,
                'content'   => $content,
                ];
                
                Helper::send_mail($data);
                session::flash('success', 'Status updated successfully');
          }

    public function table_filter_status()
    {
                
            $data['booktbl'] = DB::table('table_list')
            ->join('table_booking',function($join){
            $join->on('table_list.table_id', '=', 'table_booking.table_id');
            })
            ->where('table_booking.status', '=', '3')->get();
            return view('admin/tablebooking/table_filter_status')->with($data);

    }

     public function ajaxval()
    {

        $date          = (!empty($_GET["booking_date"])) ? ($_GET["booking_date"]) : ('');
        $day           =  (!empty($_GET["day"])) ? ($_GET["day"]) : (''); 
        $time_slot     = (!empty($_GET["timeslot"])) ? ($_GET["timeslot"]) : ('');
        $search_status = DB::table('table_list');
        $filterval     = $search_status->get();
        if($filterval)
            {
                $output=array();

                  foreach($filterval as $row){
                   
                    if($row->table_id){
                        $Rows1 = DB::table('table_booking')->where('table_id',$row->table_id);

                        if(!empty($time_slot)){
                          $Rows1->where('table_booking.time_slot_id',$time_slot);
                        }
                        if(!empty($date)){
                          $Rows1->where('table_booking.booking_date',$date);
                        }
                        if(!empty($day))
                        {

                          $Rows1->where('table_booking.booking_day',$day);  
                        }
                        $Rows2 = $Rows1->first();
                        
                        $people=@$Rows2->people;
                        $table_no=$row->table_no;
                        $person=$people;
                      
                       

                        $GetStatus = @$Rows2->status; 
                    }

                    $Status='';

                  if ($GetStatus==1) 
                    {
                      $Status ='Pending';
                      $DivCalss = 'statusOne';
                    }
                  elseif ($GetStatus==2) 
                    {
                      $Status ='Cancel';
                      $DivCalss = 'statusOne';
                    }
                    elseif ($GetStatus==3) 
                    {
                      $Status ='Accept';
                      $DivCalss = 'statusTwo';
                    }
                    elseif ($GetStatus==4) 
                    {
                      $Status ='Complete';
                      $DivCalss = 'statusOne';
                    }
                    else{
                      $Status ='Pending';
                      $DivCalss = 'statusOne';
                    }

        
                      $output[]= "<div class='tableStatus'>
                      <strong>Table $table_no</strong>
                      <div class='tblStatus $DivCalss'>Table Status $Status</div>
                      <div class='tblPrson'>$person Persons</div>
                    </div>";
               }

               return Response($output);
                }

    }

    public function ajax_booktbl_filter(Request $request)
    {
        //get restid in session
        $status = (!empty($_GET["status"])) ? ($_GET["status"]) : ('');
        $to_date = (!empty($_GET["booking_date"])) ? ($_GET["booking_date"]) : ('');
        $day =  (!empty($_GET["day"])) ? ($_GET["day"]) : ('');
        $filter_query="SELECT table_booking.*,restaurants_detail.resturent_name,users.username,users.contact_number,table_time_slot.start_time,table_time_slot.end_time,table_list.table_no from table_booking INNER join restaurants_detail on restaurants_detail.rest_id=table_booking.rest_id left join users on users.id=table_booking.user_id left join table_time_slot on table_time_slot.time_slot_id=table_booking.time_slot_id left join table_list on table_list.table_id=table_booking.table_id WHERE";
        $filter_query .=" table_booking.status !=0 ";
    if(!empty($status)){
    $filter_query .="  AND table_booking.status =".$status;
     }
     if(!empty($to_date)){
    $filter_query .="  AND table_booking.booking_date ='$to_date'";
    }
    if(!empty($day)){
    $filter_query .="  AND table_booking.booking_day ='$day'";
    }
    $querys=DB::select( $filter_query);
    if($querys){
      $i=0;
      $output="";
      $action="";
                  foreach($querys as $row){
                  $i++;
           $Status='';
                  if ($row->status==1) 
                      {
                       $Status ='Pending';
                      }
                      if ($row->status==2) 
                      {
                       $Status ='Cancel';
                      }
                      if ($row->status==3) 
                      {
                       $Status ='Accept';
                      }
                      if ($row->status==4) 
                      {
                       $Status ='Complete';
                      }
        
              if($row){
              $output.='<tr>'.

                    '<td>'.$i.'</td>'.

                    '<td>'.$row->booking_unique_id.'</td>'.

                     '<td>'.$row->username.'</td>'.

                    '<td>'.$row->contact_number.'</td>'.

                    '<td>'.$row->people.'</td>'.

                    '<td>'.$row->booking_day.'</td>'.

                    '<td>'.$row->booking_date.'</td>'.

                     '<td>'.$row->start_time.'-'.$row->end_time.'</td>'.

                     '<td>'.$row->table_no.'</td>'.

                    '<td>'.$row->message.'</td>'.


                    '<td>'.$Status.'</td>'.

'<td>'
                    .$action="<div class='link-del-view link_one'>
         <a href=".url('/admin/view_table_booking_list', base64_encode($row->table_booking_id))." class='btn btn-outline-primary'data-toggle='tooltip' title='View'>View</a>
        </div>"
.'</td>'.
              

                    '</tr>';

              }

               
            }
        return Response($output);
           }else{
       echo "No Record Found";
            }
         
            } 


         
    







}

