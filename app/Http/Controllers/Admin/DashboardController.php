<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller; 

use Illuminate\Support\Facades\Auth; 

use Validator, DB;

use App\User;

use Illuminate\Validation\Rule;

use Twilio\Rest\Client;

use Session;

use Route;

use App\Helpers\Helper;



class DashboardController extends Controller

{

        /**

     * Show the application dashboard.

     *

     * @return \Illuminate\Contracts\Support\Renderable

     */



    public function index(Request $request)

    {
        if (empty(Auth::user())) {
            return redirect('/admin');
        }else{
            $uri = $request->path();

        if(Auth::user()->role_id==4){

             $accessPermission = Helper::getAccessPath($uri,Auth::user()->id);

            if(!$accessPermission){

               return redirect('/admin/error-access-permission'); 

            }

        } 

        $now_date = date('Y-m-d H:i:s');

        $current_date = date('Y-m-d');

        $current_time = date('H:i:s');

        $data['total_users'] = DB::table('users')->where('role_id','=',2)->count();

        $data['total_stories'] = DB::table('success_story')->where('status','=',1)->count();

        $data['total_testimonial'] = DB::table('testimonial')->where('status','=',1)->count();

         $data['total_questionnaire'] = DB::table('questionnaire')->where('status','=',1)->count();



        return view('admin/dashboard')->with($data);

        }

    }

	public function errorAccessPermission(){

	    $data['error_message']= "you don't have permission to access this page";

        return view('admin/access_permission_error')->with($data);

    }

	

	public function logout(){

        Auth::logout();

        return redirect('/admin');

    }



}

