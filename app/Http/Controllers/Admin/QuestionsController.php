<?php



namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Story;

use Illuminate\Support\Facades\Auth;

use Validator, DB;

use Illuminate\Validation\Rule;

use Twilio\Rest\Client;

use Session;

use Maatwebsite\Excel\Facades\Excel;

use App\Helpers\Helper;

use Mail;



class QuestionsController extends Controller {

    public function __construct()
    {
        if (empty(Auth::user())) {
            return redirect('/admin');
        }
    }


    public function question_list(Request $request) {
        if (empty(Auth::user())) {
            return redirect('/admin');
        }else{

            $data['question_list'] = DB::table('questionnaire')->where('status', 1)->orderBy('id','desc')->get();

            return view('admin/question/question_list')->with($data);
        }

    }



    public function add_question() {
        if (empty(Auth::user())) {
            return redirect('/admin');
        }else{
             return view('admin/question/add_question'); 
        }

    }



    public function submit_question(Request $request) {

      
       $answer = $request->answer;
       $quation = $request->question;
       $answer_main        = array_filter($answer);
       $count   = count(array_filter($answer));
       $validator = Validator::make($request->all(), [

                    'question' => 'required',
                    "answer" => ["required","array","min:2"],

                    
                   

        ]);

        if ($validator->fails()) {

            return redirect('/admin/add_question')->withErrors($validator)->withInput();

        } 
        else {

            $question   = $request->question;
            $quns_type  = $request->quns_type;
            $answer     = $answer_main;
            $answer_arr = implode(",", $answer);
            $status = 1;
            $created_at = date('Y-m-d H:i:s');
            $data = array(
                'question' => $question,
                'answer'   => $answer_arr,
                'type'     => $quns_type,
                'status' => $status,
                'created_at' => $created_at
            );
           
            $updateRow = DB::table('questionnaire')->insert($data);
           
            $id = DB::getPdo()->lastInsertId();

            if ($updateRow) {

                session::flash('message', 'Question addeed succesfully.');

                return redirect('admin/question_list');

            } else {

                session::flash('error', 'Question records not inserted.');

                return redirect('admin/question_list');

            }

        }

    } 


    public function edit_question(Request $request) {

        $question_id = base64_decode($request->id);

        $data['question_info'] = DB::table('questionnaire')->where('id',$question_id)->where('status', 1)->first();

        return view('admin/question/edit_question')->with($data);

    }
 
    public function update_question(Request $request) {

        $question_id = $request->input('question_id');
        $title = $request->title;
        $answer = $request->answer;
        $answer_main  = array_filter($answer);
        $answer_arr = implode(",", $answer_main);
        $data = array(
            'question' => $title,
            'answer' => $answer_arr,
            
            );
       

        $updateRow = DB::table('questionnaire')->where('id', $question_id)->update($data);  
        if ($updateRow) {
            session::flash('message', 'Question records updated succesfully.');
            return redirect('admin/question_list');
        } else {
             session::flash('message', 'Question records updated succesfully.');
            return redirect('admin/question_list');
        }
        
    }


    public function change_question_status(Request $request) {
 
        $updateRow = DB::table('questionnaire')->where('id', $request->question_id)->update(array('status'=>$request->status)); 

        return response()->json(['success' => 'Question status change successfully.']);
    }


    public function addquestiontruefalse() {
        if (empty(Auth::user())) {
            return redirect('/admin');
        }else{
             return view('admin/question/add_question_truefalse'); 
        }

    }

    public function submit_questiontruefalse(Request $request) {

       $validator = Validator::make($request->all(), [

                    'question' => 'required',

        ]);

        if ($validator->fails()) {

            return redirect('/admin/addquestiontruefalse')->withErrors($validator)->withInput();

        } else {

            $question = $request->question;

            $quns_type= $request->quns_type;
           
            $status = 1;
           
            $created_at = date('Y-m-d H:i:s');

            $data = array(
                'question' => $question,
                'type'     => $quns_type,
                'status' => $status,
                'created_at' => $created_at
            );
           
            $updateRow = DB::table('questionnaire')->insert($data);
           
            $id = DB::getPdo()->lastInsertId();

            if ($updateRow) {

                session::flash('message', 'Question addeed succesfully.');

                return redirect('admin/question_list');

            } else {

                session::flash('error', 'Question records not inserted.');

                return redirect('admin/question_list');

            }

        }

    }

    public function edit_questiontruefalse(Request $request) {

        $question_id = base64_decode($request->id);

        $data['question_info'] = DB::table('questionnaire')->where('id',$question_id)->where('status', 1)->first();

        return view('admin/question/edit_question_truefalse')->with($data);

    }


    public function update_questiontruefalse(Request $request) {

        $question_id = $request->input('question_id');
        $title = $request->title;

        $data = array(
            'question' => $title,
            );

        $updateRow = DB::table('questionnaire')->where('id', $question_id)->update($data);  
        if ($updateRow) {
            session::flash('message', 'Question records updated succesfully.');
            return redirect('admin/question_list');
        } else {
            session::flash('error', 'Somthing went wrong.');
            return redirect('admin/question_list');
        }
        
    }

    public function addquestionInsertText() {
        if (empty(Auth::user())) {
            return redirect('/admin');
        }else{
             return view('admin/question/add_question_input'); 
        }

    }

    public function submit_questionInsertText(Request $request) {

       $validator = Validator::make($request->all(), [

                    'question' => 'required',

        ]);

        if ($validator->fails()) {

            return redirect('/admin/addquestionInsertText')->withErrors($validator)->withInput();

        } else {

            $question = $request->question;

            $quns_type= $request->quns_type;
           
            $status = 1;
           
            $created_at = date('Y-m-d H:i:s');

            $data = array(
                'question' => $question,
                'type'     => $quns_type,
                'status' => $status,
                'created_at' => $created_at
            );
           
            $updateRow = DB::table('questionnaire')->insert($data);
           
            $id = DB::getPdo()->lastInsertId();

            if ($updateRow) {

                session::flash('message', 'Question addeed succesfully.');

                return redirect('admin/question_list');

            } else {

                session::flash('error', 'Question records not inserted.');

                return redirect('admin/question_list');

            }

        }

    }

    public function edit_questionInsertText(Request $request) {

        $question_id = base64_decode($request->id);

        $data['question_info'] = DB::table('questionnaire')->where('id',$question_id)->where('status', 1)->first();

        return view('admin/question/edit_question')->with($data);

    }


    public function update_questionInsertText(Request $request) {

        $question_id = $request->input('question_id');
        $title = $request->title;

        $data = array(
            'question' => $title,
            );

        $updateRow = DB::table('questionnaire')->where('id', $question_id)->update($data);  
        if ($updateRow) {
            session::flash('message', 'Question records updated succesfully.');
            return redirect('admin/question_list');
        } else {
            session::flash('error', 'Somthing went wrong.');
            return redirect('admin/question_list');
        }
        
    }



    public function UserImportData(Request $request) {

       $rules = array('import_file' => 'required',);

        $messages = array('import_file.required' => 'Please select file to be upload',);

        $validator = Validator::make($request->all(), $rules, $messages);

        

        if ($validator->fails()) {

                return redirect()->back()->withErrors($validator)->withInput();

        }else{

            

            if ($request->file('import_file')) {

               // $path = $request->file('import_file')->getRealPath();

               $path1 = $request->file('import_file')->store('temp');

               $path = storage_path('app') . '/' . $path1;

               $data = \Excel::import(new StudentImport, $path);

               if ($data) {

                   session::flash('message', 'File import succesfully.');

                   return redirect('admin/user_list');

               } else {

                   session::flash('error', 'Something went Wrong.');

                   return redirect('admin/user_list');

               }

           } else {

               session::flash('error', 'Please select file to upload.');

               return redirect('admin/user_list');

            }

        }

    }



    public function UserExportData() {



        return Excel::download(new StudentExport, 'user.xlsx');

    }

    public function delete_story(Request $request) 
    {

        $story_id = $request->story_id;

        $student_info = DB::table('success_story')->where('id', '=', $story_id)->first();

        $res = DB::table('success_story')->where('id', '=', $story_id)->delete();

        if ($res) {

            return json_encode(array('status' => 'success', 'msg' => 'Data has been deleted successfully!'));

        } else {

            return json_encode(array('status' => 'error', 'msg' => 'Some internal issue occured.'));

        }




    }

     public function delete_question(Request $request) 
    {
    
         


        $question_id = $request->question_id;

        $student_info = DB::table('questionnaire')->where('id', '=', $question_id)->first();

        $res = DB::table('questionnaire')->where('id', '=', $question_id)->delete();

        if ($res) {

            return json_encode(array('status' => 'success', 'msg' => 'Question has been deleted successfully!'));

        } else {

            return json_encode(array('status' => 'error', 'msg' => 'Some internal issue occured.'));

        }




    }

}



?>