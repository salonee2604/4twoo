<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 
use Illuminate\Support\Facades\Auth; 
use Validator, DB;
use App\User;
use Illuminate\Validation\Rule;
use Twilio\Rest\Client;
use Session;
use App\Helpers\Helper;

class NotificationController extends Controller
{
	public function notification_list(Request $request)
	{
	  $uri = $request->path();
		if(Auth::user()->role_id==4){
			 $accessPermission = Helper::getAccessPath($uri,Auth::user()->id);
			if(!$accessPermission){
			   return redirect('/admin/error-access-permission'); 
			}
		} 	
		
		$data['notification_list'] = DB::table('notification')
																	->select('notification.*','users.first_name','users.last_name','schedule.lesson_date','schedule.time_from','schedule.time_until','schedule.description','grades.grade as grade_name')
																	->join('schedule', 'schedule.id', '=', 'notification.schedule_id')
																	->join('users', 'users.id', '=', 'notification.user_id')
																	->join('grades', 'grades.id', '=', 'schedule.grade')
																	->get();
		return view('admin/notification/notification_list')->with($data);
	}
}
?>