<?php



namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Story;

use Illuminate\Support\Facades\Auth;

use Validator, DB;

use Illuminate\Validation\Rule;

use Twilio\Rest\Client;

use Session;

use Maatwebsite\Excel\Facades\Excel;

use App\Helpers\Helper;

use Mail;



class TestimonialController extends Controller {

    public function __construct()
    {
        if (empty(Auth::user())) {
            return redirect('/admin');
        }
    }


    public function testimonial_list(Request $request) {
        if (empty(Auth::user())) {
            return redirect('/admin');
        }else{

            $data['testimonial_list'] = DB::table('testimonial')->where('status', 1)->get();
            
            return view('admin/testimonial/testimonial_list')->with($data);
        }

    }



    public function add_testimonial() {
        if (empty(Auth::user())) {
            return redirect('/admin');
        }else{
             return view('admin/testimonial/add_testimonial'); 
        }

    }



    public function submit_testimonial(Request $request) {

       $validator = Validator::make($request->all(), [

                    'title' => 'required',

                    'content' => 'required',

                    'image' => 'required'

        ]);

        if ($validator->fails()) {

            return redirect('/admin/add_testimonial')->withErrors($validator)->withInput();

        } else {

            $title = $request->title;
           
            $content = $request->content;
           
            $status = 1;
           
            $created_at = date('Y-m-d H:i:s');
           
            if ($_FILES["image"]["name"]) {
           
            $postImage = time().'_'.$_FILES["image"]["name"];
           
            $posturl="public/assets/img/".time().'_'.$postImage;
           
            $name = $_FILES["image"]["name"];
           
            move_uploaded_file( $_FILES["image"]["tmp_name"], "public/assets/img/" .time().'_'.$_FILES['image']['name']);
           
            }

            $data = array(
                'title' => $title,
                'image' => $postImage,
                'content' => $content,
                'status' => $status,
                'created_at' => $created_at
            );
           
            $updateRow = DB::table('testimonial')->insert($data);
           
            $id = DB::getPdo()->lastInsertId();

            if ($updateRow) {

                session::flash('message', 'Testimonial addeed succesfully.');

                return redirect('admin/testimonial_list');

            } else {

                session::flash('error', 'Testimonial records not inserted.');

                return redirect('admin/testimonial_list');

            }

        }

    }



    public function edit_testimonial(Request $request) {

        $story_id = base64_decode($request->id);

        $data['testimonial_info'] = DB::table('testimonial')->where('id',$story_id)->where('status', 1)->first();
        return view('admin/testimonial/edit_testimonial')->with($data);

    }



    public function update_testimonial(Request $request) {

        $testimonial_id = $request->input('testimonial_id');
        $title = $request->title;
        $content = $request->content;

        if($_FILES["image"]["name"]) {
            $postImage = time().'_'.$_FILES["image"]["name"];
            $posturl="public/assets/img/".time().'_'.$postImage;
            $name = $_FILES["image"]["name"];
            move_uploaded_file( $_FILES["image"]["tmp_name"], "public/assets/img/" .time().'_'.$_FILES['image']['name']);
        }else{
            $data['testimonial_info'] = DB::table('testimonial')->where('id',$testimonial_id)->first();
            $postImage=$data['testimonial_info']->image;
        }

        $data = array(
            'title' => $title,
            'image' => $postImage,
            'content' => $content,
            );

        $updateRow = DB::table('testimonial')->where('id', $testimonial_id)->update($data);  
        if ($updateRow) {
            session::flash('message', 'Testimonial records updated succesfully.');
            return redirect('admin/testimonial_list');
        } else {
            session::flash('error', 'Somthing went wrong.');
            return redirect('admin/testimonial_list');
        }
        
    }


    public function change_testimonial_status(Request $request) {
 
        $updateRow = DB::table('testimonial')->where('id', $request->testimonial_id)->update(array('status'=>$request->status)); 

        return response()->json(['success' => 'Testimonial status change successfully.']);
    }

    public function UserImportData(Request $request) {

       $rules = array('import_file' => 'required',);

        $messages = array('import_file.required' => 'Please select file to be upload',);

        $validator = Validator::make($request->all(), $rules, $messages);

        

        if ($validator->fails()) {

                return redirect()->back()->withErrors($validator)->withInput();

        }else{

            

            if ($request->file('import_file')) {

               // $path = $request->file('import_file')->getRealPath();

               $path1 = $request->file('import_file')->store('temp');

               $path = storage_path('app') . '/' . $path1;

               $data = \Excel::import(new StudentImport, $path);

               if ($data) {

                   session::flash('message', 'File import succesfully.');

                   return redirect('admin/user_list');

               } else {

                   session::flash('error', 'Something went Wrong.');

                   return redirect('admin/user_list');

               }

           } else {

               session::flash('error', 'Please select file to upload.');

               return redirect('admin/user_list');

            }

        }

    }



    public function UserExportData() {



        return Excel::download(new StudentExport, 'user.xlsx');

    }

    public function delete_testimonial(Request $request) 
    {

        $testimonial_id = $request->testimonial_id;

        $student_info = DB::table('testimonial')->where('id', '=', $testimonial_id)->first();

        $res = DB::table('testimonial')->where('id', '=', $testimonial_id)->delete();

        if ($res) {

            return json_encode(array('status' => 'success', 'msg' => 'Data has been deleted successfully!'));

        } else {

            return json_encode(array('status' => 'error', 'msg' => 'Some internal issue occured.'));

        }

    }

}



?>