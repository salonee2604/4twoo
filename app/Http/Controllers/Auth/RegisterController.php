<?php



namespace App\Http\Controllers\Auth;



use App\User;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Facades\Validator;

use Illuminate\Foundation\Auth\RegistersUsers;



class RegisterController extends Controller

{

    /*

    |--------------------------------------------------------------------------

    | Register Controller

    |--------------------------------------------------------------------------

    |

    | This controller handles the registration of new users as well as their

    | validation and creation. By default this controller uses a trait to

    | provide this functionality without requiring any additional code.

    |

    */



    use RegistersUsers;



    /**

     * Where to redirect users after registration.

     *

     * @var string

     */

    protected $redirectTo = '/user/dashboard';



    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function __construct()

    {

        $this->middleware('guest');

    }



    /**

     * Get a validator for an incoming registration request.

     *

     * @param  array  $data

     * @return \Illuminate\Contracts\Validation\Validator

     */

    protected function validator(array $data)

    {

        return Validator::make($data, [

            'name' => ['required', 'string', 'max:255'],

            'last_name' => ['required', 'string', 'max:255'],

            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],

            'password' => ['required', 'string', 'min:8', 'confirmed'],

        ]);

    }



    /**

     * Create a new user instance after a valid registration.

     *

     * @param  array  $data

     * @return \App\User

     */

    protected function create(array $data)

    {
            //echo "<pre>"; print_r($data);die;
            //$user = new User; 
        return User::create([

            'first_name' => $data['name'],

            'last_name' => $data['last_name'],

            'email' => $data['email'],

            'dob'  =>  date("Y-m-d", strtotime($data['dob'])),

            'address' => $data['address'],

            'contact_number' => $data['contact_number'],

            'role_id' => 2,

            'password' => Hash::make($data['password']),

            'gender' => $data['gender'],

        ]);

        session()->flash('success', 'You have Successfully Registered if you want to Login click button'); 
    }


    public function user_save(Request $request) 
    {   

        print_r($request->all());die;
        // $validator = Validator::make($request->all(), [ 
        //     'first_name'  => 'required',
        //     'last_name'  => 'required',
          
        //     'email' => [
        //             'required',
        //             'email',
        //             Rule::unique('users')->where(function ($query) {
        //                 return $query->where('status','=', 1);
        //             }),
        //         ],
        //     'contact_number' => [
        //         'required',
        //         'min:8',
        //         'max:16',
        //         Rule::unique('users')->where(function ($query) {
        //             return $query->where('status','=', 1);
        //         }),
        //     ], 
           
        //     'password'  => [
        //         'required', 
        //         // 'min:8', 
        //         // 'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*(_|[^\w])).+$/'
                
        //    ],
          
        // ],
        // [   
        //     'first_name.required'       =>"First name is required",
        //     'last_name.required'        =>"last name is required",
        //     'email.email'               =>"Please enter vailid email id",
        //     'email.unique'              =>"Email id already exists",
        //     'contact_number.required'   =>"Contact number is required",
        //     'contact_number.min'        => "Please enter vailid contact number",
        //     'contact_number.max'        => "Please enter vailid contact number",
        //     'contact_number.unique'     => "Contact number already exists",
        //     'password.required'         => "Password is required",
        //     // 'password.min'              => "Please password 8 digits or character",
        //     // 'password.regex'            => "Password should be contain one capital latter, numbar and special character",
        // ]);
        // if ($validator->fails()) {
        //     $messages = $validator->messages();
        //     foreach ($messages->all() as $message)
        //     {   
        //         return response()->json(['status'=>$this->failureStatus,'msg'=>$message]);            
        //     }
        // }
        // $forminput  = $request->all();
    

        //     // $name = '';
        //     // if ($request->hasFile('profile_img')) {
        //     //     $image = $request->file('profile_img');
        //     //     $name = time().'.'.$image->getClientOriginalExtension();
        //     //     $destinationPath = public_path('/uploads/profile_img');         
        //     //     $imagePath = $destinationPath. '/'.  $name;
        //     //     $image->move($destinationPath, $name);
        //     // }

        //     $digits                 = 6;
        //     $otp                    = rand(pow(10, $digits-1), pow(10, $digits)-1);
        //     $forminput              = $request->all();
        //     $user                   = new User; 
        //     $user->first_name         = $forminput['first_name'];
        //     $user->last_name         = $forminput['last_name'];
        //     $user->username         = $forminput['first_name'];
        //     $user->password         = Hash::make($forminput['password']);
        //     $user->status      = '0';
        //     $user->role_id     = '3';
        //     $user->vrfn_code       = $otp;
        //     $user->is_verify_email  = '0';
        //     $user->email            = $forminput['email'];
        //     $user->contact_number   = $forminput['contact_number']; 
        //     $user->code   = $forminput['code'];          
        //     $user->created_at       = Date('Y-m-d H:i:s');
           
        //     //$user->profile_img      = $name;
        //     $access_key = 'a4cb1b1ea5acd891f759a5d0b29de70c';

        //     // set phone number
        //     $phone_number = $forminput['code'].''.$forminput['contact_number'];

        //     // Initialize CURL:
        //     $ch = curl_init('http://apilayer.net/api/validate?access_key='.$access_key.'&number='.$phone_number.'');  
        //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //     $json = curl_exec($ch);
        //     curl_close($ch);
        //     $validationResult = json_decode($json, true);
        //     $valid=$validationResult['valid'];  

        //     //$user->profile_img      = $name;
        //     if($valid=="1"){
               
        //     if($user->save()){
        //         $searchArray = array("{user_fname}", "{user_lname}","{user_email}", "{user_otp}");

        //         $replaceArray = array($user->first_name, $user->last_name, $user->email, $otp);

        //         $email_content = "<p>Hello, {user_fname} {user_lname} <br>
        //         Welcome to Elearning<br>
        //         your verfication otp is : {user_otp}
        //         </p>";

        //         $content = str_replace($searchArray, $replaceArray, $email_content);

        //         $data = [
        //         'name'      => $user->first_name,
        //         'email'     => $user->email,                 
        //         'subject'   => "Elearning",
        //         'content'   => $content,
        //         ];

        //         Helper::send_mail($data);

        //         return response()->json(          
        //             [
        //                 'status'=>$this->successStatus, 
        //                 'msg' => "You have registered successfully",
        //             ]
        //         ); 
        //     }else{
        //         return response()->json(['status'=>$this->failureStatus, 'msg' => "some thing is wrong please try again later."]);
        //     }
        //     }else{

        //     return response()->json(['status'=>$this->failureStatus, 'msg' => "Please enter vailid contact number"]);
        // }
    
    }

}

