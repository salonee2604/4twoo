<?php



namespace App\Http\Controllers; 

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use Validator,DB;

use App\User;

use Illuminate\Validation\Rule;

use Twilio\Rest\Client;

use Session;

use Exception;

use File;

use App\Libraries\BigBlueButton;

use ZipArchive;

use Hash;

use Artisan;

//use Carbon\Carbon;

use App\Helpers\Helper; 


class DashboardController extends Controller { 

    /**

     * Show the application dashboard.

     *

     * @return \Illuminate\Contracts\Support\Renderable

     */
    public function __construct()
    {
        if (empty(Auth::user())) {
            return redirect('/');
        }
    }

    public function index($name = null) {
        
        if (empty(Auth::user())) {
            return redirect('/');
        }else{
            $user_id = Auth::user()->id;

            $role_id = Auth::user()->role_id;

            $now_date = date('Y-m-d H:i:s');

            $data['tabname'] = $name;

            $data['userinfo'] = $userinfo = DB::table('users')

            ->join('users_details', 'users.id', '=', 'users_details.user_id')

            ->where('users.id', '=', $user_id)

            ->where('users.role_id', '=', 2)

            ->first();
           
            if($role_id == 1){
                return view('admin/dashboard');
            }else{
                return view('user/user')->with($data);
            }
            
        }
       

    } 

    public function editUserProfile($name = null)
    {
       if (empty(Auth::user())) {
            return redirect('/');
        }else{
             $user_id = Auth::user()->id;

            $now_date = date('Y-m-d H:i:s');

            $data['tabname'] = $name;

            $data['userinfo'] = $userinfo = DB::table('users')

                    ->join('users_details', 'users.id', '=', 'users_details.user_id')

                    ->where('users.id', '=', $user_id)

                    ->where('users.role_id', '=', 2)

                    ->first();
            return view('user/edit_user_profile')->with($data);
        }
    }

    public function changepassword()
    {
       if (empty(Auth::user())) {
            return redirect('/');
        }else{
             $user_id = Auth::user()->id;
             $data['user_id'] = $user_id;


            return view('user/changepassword')->with($data);;
        }
    }
    
    public function update_password_action(Request $request)
    {   

        $user_id = $request->input('user_id') ;

        $old_password = $request->input('old_password');
        
        $password = $request->input('password') ;

        $login_ip = $_SERVER['REMOTE_ADDR'];
        
        if(!\Hash::check($old_password, auth()->user()->password)){

            $arr = array('success'=>false,'message'=>'Your old password does not matches please try again');
             return Response()->json($arr);

        }else{
            $arrayData = array(
                'password'=>bcrypt($password),
                'updated_at'=>date('Y-m-d H:i:s')
            );

            $update = User::where('user_id','=',$user_id)->update($arrayData);
            $message = 'Password changed successfully' ; 

            if($update){ 
                $arr = array('success'=>true,'message'=>$message);
            } else {
                $arr = array('success'=>false,'message'=>'Something went wrong. Please try again');
            }
            return Response()->json($arr);
        }

    }

    public function UploadProfilePicture(Request $request) {


        $validator = Validator::make($request->all(), [

            'user_pic' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',

        ]);
 
        if ($validator->fails()) {

            $Arr = array();

            $error = $validator->errors()->toArray();

            return response()->json(['status' => false, 'msg' => $validator->errors()->first('user_pic')]);

        } else {

            $user_id = Auth::user()->id;

            $fileName = "";

            if ($request->hasFile('user_pic')) {

                $upload_path = public_path() . '/uploads/profile_img/';

                if (!file_exists($upload_path)) {

                    mkdir($upload_path, 0777, true);

                }

                $file = $request->file('user_pic');

                $fileName = $file->getClientOriginalName();

                $file->move($upload_path, $fileName);

                $data['profile_pic'] = $fileName;

            } 

            $userData = DB::table('users')->where('id', $user_id)->first();

            DB::table('users')->where(['id' => $user_id])->update($data);

            if (!empty($userData->profile_pic)) {

                $image_path = public_path() . '/uploads/profile_img/' . $userData->profile_pic;

                if (File::exists($image_path)) {

                    File::delete($image_path);

                }

            }

            return response()->json(['status' => true, 'msg' => 'Profile image update successfully !', 'filename' => $fileName]);

        }

    }

    public function user_update(Request $request) 
    { 
        //echo "<pre>";print_r($request->all());die;
        //$name = '';
        if ($request->hasFile('user_image')) {
            $image = $request->file('user_image');
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/profile_img');         
            $imagePath = $destinationPath. '/'.  $name;
            $image->move($destinationPath, $name);
        }
        else {
            $name=$request->oldimage;
        }  
        $update =  DB::table('users')
         ->where('id',Auth::user()->id)
         ->update(['first_name'=>$request->first_name, 'last_name'=>$request->last_name,'contact_number'=>$request->contact_number,'profile_pic'=>$name]);

        $user_id = DB::table('users_details')->where('user_id', Auth::user()->id)->get()->count();
        if($user_id > 0){
            $update =  DB::table('users_details')
         ->where('user_id',Auth::user()->id)
         ->update(['i_am_looking'=>$request->looking,'skin_type'=>$request->skin_type,'height'=>$request->height,'figure'=>$request->figure,'hair_color'=>$request->hair_color,
            'hair_length'=>$request->hair_length,'beard'=>$request->beard,'tattoos'=>$request->tattoos,'piercings'=>$request->piercings,'hobbies'=>$request->hobbies,'pets'=>$request->pets,'travel'=>$request->travel,'occupation'=>$request->occupation,'food'=>$request->food,'religion'=>$request->religion,'outing'=>$request->outing,'macho'=>$request->macho,'smoker'=>$request->smoker,'residence'=>$request->residence,'drinking'=>$request->drinking,'sex'=>$request->sex]);
        }else{
            $update = DB::table('users_details')->insert(['user_id'=>Auth::user()->id,'i_am_looking'=>$request->looking, 'skin_type'=>$request->skin_type,'height'=>$request->height,'figure'=>$request->figure,'hair_color'=>$request->hair_color,'hair_length'=>$request->hair_length,'beard'=>$request->beard,'tattoos'=>$request->tattoos,'piercings'=>$request->piercings,'hobbies'=>$request->hobbies,'pets'=>$request->pets,'travel'=>$request->travel,'occupation'=>$request->occupation,'food'=>$request->food,'religion'=>$request->religion,'outing'=>$request->outing,'macho'=>$request->macho,'smoker'=>$request->smoker,'residence'=>$request->residence,'drinking'=>$request->drinking,'sex'=>$request->sex,'created_at'=>date('Y-m-d H:i:s')]);
        }
        if($update) {
          return redirect('user/dashboard')->with('success','update Successfully!'); 
           }else {
            return redirect('user/dashboard')->with('success','update Successfully!');  
        } 
    }


    public function matchProfiles()
    {



        if (empty(Auth::user())) 
        {
            return redirect('/');
        }else{
            $user_id                = Auth::user()->id;
            $answer_details         = DB::table('user_answer')->where('user_id', '=', $user_id)->get();
            $matche_user_id_arr     = array();
            foreach ($answer_details as $value) 
            {
                
                $question_id  =   $value->question_id;
                $answer       =   $value->answer;
                
                $user_details = DB::table('user_answer')->where('question_id', '=', $question_id)->where('answer', '=', $answer)->where('user_id', '!=' , $user_id)->get();
                foreach ($user_details as $value_match) 
                {
                    $matche_user_id_arr[] = $value_match->user_id;

                }
            }

            $matche_arrycount                        = array_count_values($matche_user_id_arr);
            arsort($matche_arrycount );
            $arry_main = $matche_arrycount;
            $userinfo  = array();
            $matched_quation = array();
            foreach($arry_main as $x => $x_value) 
            {
               
               $data['user_id']  =  $x;
               $userdetails['data']       = DB::table('users')->select('users.*')->where('users.id', '=', $x)->orderBy('users.id', 'DESC')->first();
               $count['count']             = $x_value;
               $userinfo[] = array_merge( $userdetails,$count);
               
              

            }
            
            $data['usersinfo']       = $userinfo;
            return view('user/matching_profiles')->with($data);
        }
    }
    public function viewmatchProfileold($id = null)
    {
       $uid = base64_decode($id);

        if (empty(Auth::user())) {
            return redirect('/');
        }else{
            $user_id = Auth::user()->id;

            $data['user_id'] = $user_id;

            $data['userinfo'] = $userinfo = DB::table('users')

            ->join('users_details', 'users.id', '=', 'users_details.user_id')

            ->where('users.id', '=', $uid)

            ->where('users.role_id', '=', 2)


            ->first();
            //echo "<pre>"; print_r($data);die;
            return view('user/view_match_profile')->with($data);
        }
    }

     public function viewmatchProfile($id = null)
    {


        $uid = base64_decode($id);
        if (empty(Auth::user())) {
            return redirect('/');
        }
        else{
            
            $matche_user_id_arr = array();
            $user_id            = Auth::user()->id;
            $answer_details     = DB::table('user_answer')->where('user_id', '=', $user_id)->get();
            foreach ($answer_details as $value) 
            {
                $question_id  =   $value->question_id;
                $answer       =   $value->answer;
                $user_details = DB::table('user_answer')->where('question_id', '=', $question_id)->where('answer', '=', $answer)->where('user_id', '=', $uid)->where('user_id', '!=' , $user_id)->get();
                foreach ($user_details as $value_match) 
                {
                    $matche_user_id_arr[] = $value_match->user_id;

                }

            }

            $matche_arrycount                        = array_count_values($matche_user_id_arr);
            arsort($matche_arrycount );
            $arry_main = $matche_arrycount;
            $count ='';
            foreach($arry_main as $key => $value){
             $count = $value;
            }
            

                       $matchedcount = $count;
                       $allquation_count        = DB::table('questionnaire')->count();
                       $matchedpercentage        = ($matchedcount/$allquation_count)*100;

                      if(!empty($matchedpercentage))
                      {
                        $matchedpercentagevalue = round($matchedpercentage,2); 
                      }
                      else
                      {
                        $matchedpercentagevalue = "";
                      }
            $data['matchpercentage']  =  $matchedpercentagevalue;
            $data['user_id'] = $user_id;

            $data['userinfo'] = $userinfo = DB::table('users')

            ->join('users_details', 'users.id', '=', 'users_details.user_id')

            ->where('users.id', '=', $uid)

            ->where('users.role_id', '=', 2)


            ->first();
           
            //echo "<pre>"; print_r($data);die;
            return view('user/view_match_profile')->with($data);
        }
    }

    public function send_request(Request $request)
    {   
        $user_id = Auth::user()->id;
        $exists = DB::table('request')->where('form_id', Auth::user()->id)->where('to_id', $request->to_id)->orWhere('form_id',$request->to_id)->orWhere('to_id',Auth::user()->id)->get();
        if(!$exists->isEmpty()){
            return response()->json(['success' => false, 'message' => 'You have already sent request']);
        }
        $send_request = DB::table('request')->insert(['form_id'=>Auth::user()->id,'to_id'=>$request->to_id, 'status'=>0,'created_at'=>date('Y-m-d H:i:s')]);
        if($send_request) {
          return response()->json(['success' => true, 'message' => 'Request sent successfully.']);
        }else {
           return response()->json(['success' => false,'message' => 'Some occure problem.']);  
        } 

    }

    public function logout() {

        Auth::logout();

        return redirect('/');

    }


    

    /* End Profile section */ 

    /* Notification Section */


    public function getNotification() {

        $user_id = Auth::user()->id;

        if (empty($user_id)) {

            return response()->json(['status' => false, 'login' => false, 'msg' => 'student not login.']);

            die;

        }



        $totalnotification = DB::table('notification')

                        ->where('notification.user_id', $user_id)

                        ->where('notification.notification_status', 0)

                        ->get()->count();



        $notification = DB::table('notification')

                ->Join('notification_format', 'notification_format.id', '=', 'notification.format_id')

                ->select('notification.id', 'notification.schedule_id', 'notification.user_id', 'notification.notification_type', 'notification.notification_msg', 'notification_format.notification_icon', 'notification.format_id')

                ->where('notification.user_id', $user_id)

                ->where('notification.notification_status', 0)

                ->limit(5)

                ->orderBy('notification.id', 'DESC')

                ->get();



        if (!empty($notification)) {

            foreach ($notification as $key => $value) {



                if (empty($value->notification_icon)) {

                    $notification_icon = url('/') . "/resources/assets/images/avatar5.png";

                } else {

                    $notification_icon = url('/') . "/public/uploads/notification_icon/" . $value->notification_icon;

                }

                $icon_img = "<img style='width: 50px;' src='$notification_icon' />";

                $msg = $icon_img . $value->notification_msg;

                $notification[$key]->ready_msg = $msg;

            }

        }

        return response()->json(['status' => true, 'totalnotification' => $totalnotification, 'notification' => $notification,]);

        die;

    }

    public function readNotification(Request $request) {

        $user_id = Auth::user()->id;

        if (empty($user_id)) {

            return response()->json(['status' => false, 'login' => false, 'msg' => 'student not login.']);

            die;

        }

        $notificationID = $request->notificationID;

        $notification = DB::table('notification')->where(['id' => $notificationID, 'notification.notification_status' => 0])->orderBy('notification.id', 'desc')->get()->first();

        if (!empty($notification)) {



            $data['updated_at'] = date('Y-m-d H:i:s');

            $data['notification_status'] = 1;

            DB::table('notification')->where(['id' => $notificationID])->update($data);

        }
 
        $totalnotification = DB::table('notification')->where('notification.user_id', $user_id)

                        ->where('notification.notification_status', 0)

                        ->get()->count();
 
        return response()->json(['status' => true, 'notificationID' => base64_encode($notificationID), 'totalnotification' => $totalnotification, 'notification_type' => $notification->notification_type]);

    }

    public function notificationDetails($notification_id = null, Request $request) {

        $user_id = Auth::user()->id;

        $notificationID = base64_decode($notification_id);

        $data['userinfo'] = $userinfo = DB::table('users')

                ->leftJoin('member_grade as mg', 'mg.user_id', '=', 'users.id')

                ->leftJoin('grades as g', 'g.id', '=', 'mg.grade_id')

                ->select('users.id', DB::raw("CONCAT(users.first_name,' ',users.last_name) as name"), 'users.first_name', 'users.last_name', 'users.email', 'users.username', 'users.profile_pic', 'mg.grade_name', 'mg.grade_id')

                ->where('users.id', '=', $user_id)

                ->where('users.role_id', '=', 3)

                ->first();


        $data['notificationDetals'] = DB::table('notification')

                        ->leftJoin('schedule', 'schedule.id', '=', 'notification.schedule_id')

                        ->leftJoin('grades', 'grades.id', '=', 'schedule.grade')

                        ->leftJoin('users', 'users.id', '=', 'schedule.tutor_id')

                        ->leftJoin('notification_format', 'notification_format.id', '=', 'notification.format_id')

                        ->select('notification.*', 'schedule.lesson_date', 'schedule.time_from', 'schedule.time_until', 'schedule.description', 'schedule.status as schedule_type', DB::raw("CONCAT(users.first_name,' ',users.last_name) as tutor_name"), 'users.role_id', 'users.email', 'users.profile_pic', 'users.status', 'grades.grade', 'notification_format.notification_icon')

                        ->where('notification.id', '=', $notificationID)

                        ->where('notification.user_id', '=', $user_id)

                        ->orderBy('notification.id', 'DESC')

                        ->get()->first();

        return view('student/notification_details')->with($data);

    }

    public function notification_reply() {

        

        if(Session::has('locale')){

           $langCode= Session::get('locale');

        }else{

            $language=  Helper::getDefaultLanguage();

            $langCode = $language->code; 

            Session::put('locale',$language->code);

        } 

        $notification_id = $_POST['notification_id'];

        $is_accept = $_POST['is_accept'];

        $reason = (!empty($_POST['reason']) ? $_POST['reason'] : '');

        $logoImg = DB::table('logo_file')->first();

        $resp = DB::table('notification')

                ->where('id', $notification_id)

                ->update(['notification_status' => 1, 'is_panding' => 1, 'updated_at' => date('Y-m-d H:i:s')]);



        $notification_info = DB::table('notification')

                ->where('id', '=', $notification_id)

                ->first();

        $studentstatus_info = DB::table('lesson_student_status')

                ->where('lesson_id', '=', $notification_info->schedule_id)

                ->where('student_id', '=', $notification_info->user_id)

                ->first();



        if (!empty($studentstatus_info->id)) {

            // update

            if ($is_accept == 1) {

                DB::table('lesson_student_status')

                        ->where('id', $studentstatus_info->id)

                        ->update(['reason' => null, 'status' => 1, 'updated_at' => date('Y-m-d H:i:s')

                ]);

            } else {

                DB::table('lesson_student_status')

                        ->where('id', $studentstatus_info->id)

                        ->update(['reason' => (!empty($reason) ? $reason : null),

                            'status' => 2, 'updated_at' => date('Y-m-d H:i:s')

                ]);

            }

        } else {

            // insert

            $post_arr = array(

                'lesson_id' => $notification_info->schedule_id,

                'student_id' => $notification_info->user_id,

                'reason' => (!empty($reason) ? $reason : null),

                'meeting_id' => null,

                'meeting_password' => null,

                'status' => ($is_accept == 1 ? 1 : 2),

                'created_at' => date('Y-m-d H:i:s'),

                'updated_at' => date('Y-m-d H:i:s')

            );

            DB::table('lesson_student_status')->insert($post_arr);

        }



        $student_info = DB::table('users')->where('id', '=', $notification_info->user_id)->first();

        $schedule_info = DB::table('schedule')

                        ->Join('grades as g', 'g.id', '=', 'schedule.grade')

                        ->select('schedule.id', 'schedule.tutor_id', 'schedule.lesson_date', 'schedule.time_from', 'schedule.time_until', 'schedule.description', 'schedule.grade', 'g.grade as grade_name')

                        ->where('schedule.id', '=', $notification_info->schedule_id)->get()->first();

        $student_name = $student_info->first_name . " " . $student_info->last_name;

       

        $userInfo = Helper::getUserEmail($schedule_info->tutor_id);

        

        $grade = $schedule_info->grade_name;

        $lession_date = date('d-m-Y', strtotime($schedule_info->lesson_date));

        $time_from = date('H:i A', strtotime($schedule_info->time_from));

        $time_until = date('H:i A', strtotime($schedule_info->time_until));



        if ($is_accept == 1) {

            $format_info = DB::table('notification_format')

                    ->where('notification_type', '=', 'StudentAttendingConfirmation')

                    ->where('language','=',$langCode)

                    ->first();

            $notification_type = 'StudentAttendingConfirmation';

           

            $format_id = $format_info->id;

            $noti_msg = str_replace("{0}", $student_name, $format_info->notification_msg);

            $noti_msg = str_replace("{1}", $grade . ' grade ', $noti_msg);

            $noti_msg = str_replace("{2}", $lession_date, $noti_msg);

            $noti_msg = str_replace("{3}", $time_from, $noti_msg);

            $noti_msg = str_replace("{4}", $time_until, $noti_msg);

            $noti_msg = str_replace("{5}", $reason, $noti_msg);

            

            $emailFormate = DB::table('email_formate')

                  ->where('email_type', '=', 'StudentAttendingConfirmation')

                  ->where('language','=',$langCode)

                  ->first();

                $subject = $emailFormate->subject;

                $logo_img = url('/').'/public/uploads/logo/'.$logoImg->logo_img;

                

                $email_content = str_replace("{logo}", $logo_img, $emailFormate->body);

                $email_content = str_replace("{0}", $userInfo->first_name, $email_content);

                $email_content = str_replace("{1}", $student_name, $email_content);

                $email_content = str_replace("{2}", $grade, $email_content);

                $email_content = str_replace("{3}", $lession_date, $email_content);

                $email_content = str_replace("{4}", $time_from, $email_content);

                $email_content = str_replace("{5}", $time_until, $email_content);

                

                 $data = [

                     'name' => $userInfo->first_name,

                     'email' => $userInfo->email,

                     'subject' => $subject,

                     'content' => $email_content,

                 ];

       } else {

            $format_info = DB::table('notification_format')

                    ->where('notification_type', '=', 'StudentNotAttending')

                    ->where('language','=',$langCode)

                    ->first();

            $notification_type = 'StudentNotAttending';

            

            $format_id = $format_info->id;

            $noti_msg = str_replace("{0}", $student_name, $format_info->notification_msg);

            $noti_msg = str_replace("{1}", $grade, $noti_msg);

            $noti_msg = str_replace("{2}", $lession_date, $noti_msg);

            $noti_msg = str_replace("{3}", $time_from, $noti_msg);

            $noti_msg = str_replace("{4}", $time_until, $noti_msg);

            $noti_msg = str_replace("{5}", $reason, $noti_msg);

            

             $emailFormate = DB::table('email_formate')

                  ->where('email_type', '=', 'StudentNotAttending')

                  ->where('language','=',$langCode)

                  ->first();

                $subject = $emailFormate->subject;

                $logo_img = url('/').'/public/uploads/logo/'.$logoImg->logo_img;

                

                $email_content = str_replace("{logo}", $logo_img, $emailFormate->body);

                $email_content = str_replace("{0}", $userInfo->first_name, $email_content);

                $email_content = str_replace("{1}", $student_name, $email_content);

                $email_content = str_replace("{2}", $grade, $email_content);

                $email_content = str_replace("{3}", $lession_date, $email_content);

                $email_content = str_replace("{4}", $time_from, $email_content);

                $email_content = str_replace("{5}", $time_until, $email_content);

                $email_content = str_replace("{6}", $reason, $email_content);

                

                 $data = [

                     'name' => $userInfo->first_name,

                     'email' => $userInfo->email,

                     'subject' => $subject,

                     'content' => $email_content,

                 ];

          }



        $noti_arr = array(

            'schedule_id' => $notification_info->schedule_id,

            'user_id' => $schedule_info->tutor_id,

            'notification_type' => $notification_type,

            'notification_msg' => $noti_msg,

            'notification_status' => 0,

            'format_id' => $format_id,

            'created_at' => date('Y-m-d H:i:s'),

            'updated_at' => date('Y-m-d H:i:s')

        );

        DB::table('notification')->insert($noti_arr);

        

       /*  if ($_SERVER['HTTP_HOST'] != 'localhost') {

            try {

                 Helper::send_mail($data);

            } catch (Exception $e) {

                echo 'Message: ' . $e->getMessage();

            }

        } */

        



        if ($resp) {

            return response()->json(['status' => true, 'is_accept' => $is_accept,]);

        } else {

            return response()->json(['status' => false, 'is_accept' => 0]);

        }

    }

    public function notificationlist(Request $request) {

        $user_id = Auth::user()->id;



        DB::table('notification')->where(['user_id' => $user_id])->update(['notification_status' => 1, 'updated_at' => date('Y-m-d H:i:s')]);



        $data['userinfo'] = $userinfo = DB::table('users')

                ->leftJoin('member_grade as mg', 'mg.user_id', '=', 'users.id')

                ->leftJoin('grades as g', 'g.id', '=', 'mg.grade_id')

                ->select('users.id', DB::raw("CONCAT(users.first_name,' ',users.last_name) as name"), 'users.first_name', 'users.last_name', 'users.email', 'users.username', 'users.profile_pic', 'mg.grade_name', 'mg.grade_id')

                ->where('users.id', '=', $user_id)

                ->where('users.role_id', '=', 3)

                ->first();

        $data['notificationlist'] = DB::table('notification')

                        ->leftJoin('schedule', 'schedule.id', '=', 'notification.schedule_id')

                        ->leftJoin('grades', 'grades.id', '=', 'schedule.grade')

                        ->leftJoin('users', 'users.id', '=', 'notification.user_id')

                        ->leftJoin('notification_format', 'notification_format.id', '=', 'notification.format_id')

                        ->select('notification.*', 'schedule.lesson_date', 'schedule.time_from', 'schedule.time_until', 'schedule.description', 'schedule.status as schedule_type', DB::raw("CONCAT(users.first_name,' ',users.last_name) as tutor_name"), 'users.role_id', 'users.email', 'users.profile_pic', 'users.status', 'grades.grade', 'notification_format.notification_icon')

                        ->where('notification.user_id', $user_id)

                        ->orderBy('notification.id', 'DESC')

                        ->get()->all();



        return view('student/notification_list')->with($data);

    }

    public function acceptRerequest(Request $request) 
    {



        $notification_id = $request->notification_id;



        $notification_info = DB::table('notification')

                ->where('id', '=', $notification_id)

                ->first();



        $studentstatus_info = DB::table('lesson_student_status')

                ->where('lesson_id', '=', $notification_info->schedule_id)

                ->where('student_id', '=', $notification_info->user_id)

                ->first();

        if (!empty($studentstatus_info->id)) {

            DB::table('lesson_student_status')

                    ->where('id', $studentstatus_info->id)

                    ->update(['reason' => null, 'status' => 1, 'updated_at' => date('Y-m-d H:i:s')

            ]);

        } else {



            $post_arr = array(

                'lesson_id' => $notification_info->schedule_id,

                'student_id' => $notification_info->user_id,

                'reason' => null,

                'meeting_id' => null,

                'meeting_password' => null,

                'status' => 1,

                'created_at' => date('Y-m-d H:i:s'),

                'updated_at' => date('Y-m-d H:i:s')

            );

            DB::table('lesson_student_status')->insert($post_arr);

        }



        $student_info = DB::table('users')->where('id', '=', $notification_info->user_id)->first();

        $schedule_info = DB::table('schedule')

                        ->Join('grades as g', 'g.id', '=', 'schedule.grade')

                        ->where('schedule.id', '=', $notification_info->schedule_id)->get()->first();



        $student_name = $student_info->first_name . " " . $student_info->last_name;

        $grade = $schedule_info->grade;

        $lession_date = date('d-m-Y', strtotime($schedule_info->lesson_date));

        $time_from = $schedule_info->time_from;

        $time_until = $schedule_info->time_until;



        $format_info = DB::table('notification_format')->where('notification_type', '=', 'StudentAttendingConfirmation')->first();

        $notification_type = 'StudentAttendingConfirmation';

        $noti_msg = str_replace("{0}", $student_name, $format_info->notification_msg);

        $noti_msg = str_replace("{1}", $grade . ' grade ', $noti_msg);

        $noti_msg = str_replace("{2}", $lession_date, $noti_msg);

        $noti_msg = str_replace("{3}", $time_from, $noti_msg);

        $noti_msg = str_replace("{4}", $time_until, $noti_msg);



        $noti_arr = array(

            'schedule_id' => $notification_info->schedule_id,

            'user_id' => $schedule_info->tutor_id,

            'notification_type' => $notification_type,

            'notification_msg' => $noti_msg,

            'notification_status' => 0,

            'created_at' => date('Y-m-d H:i:s'),

            'updated_at' => date('Y-m-d H:i:s')

        );

        $resp = DB::table('notification')->insert($noti_arr);



        if ($resp) {

            echo 1;

        } else {

            echo 0;

        }

        die;

    } 

    public function delete_profile()
    {


        $user_id  = Auth::user()->id;
        $status   = 0;
        $update = User::where('id','=',$user_id)->update(array('status'=>0));
        session::flash('message', 'your profile delete successfully');
        return redirect('user/dashboard');

       
    }

    public function logout_profile()
    {

        Auth::logout();

        return redirect('/');

    }
    
    /* End Notification Section */	

	

}