<?php



namespace App;



use Illuminate\Notifications\Notifiable;

use Illuminate\Contracts\Auth\MustVerifyEmail;

use Illuminate\Foundation\Auth\User as Authenticatable;

use HasApiTokens;







class User extends Authenticatable

{

    

        use Notifiable;





    const USER_FIRST_NAME_FIELD        = "first_name";

    const USER_LAST_NAME_FIELD         = "last_name";

    const USER_PREFERRED_NAME_FIELD    = "preferred_name";

    const USER_EMAIL_FIELD             = "email";

    const USER_CONTACT_NUMBER_FIELD    = "contact_number";

    const USER_DOB_FIELD               = "dob";
                                       
    const USER_GERNDER_FIELD           = "gender";
    
    const USER_EMAIL_VERIFIED_AT_FIELD = "email_verified_at";

    const USER_PASSWORD_FIELD          = "password";

    const USER_REMEMBER_TOKEN_FIELD    = "remember_token";

    const USER_RECEIVE_NEWSLETTER_FIELD= "receive_newsletter";

    const USER_ACTIVE_FIELD            = "active";



  

    protected $fillable = [

        'first_name', 'last_name', 'username', 'email', 'password', 'grade', 'curriculum', 'role_id','temp_password','contact_number','gender','dob'

    ];



    /**

     * The attributes that should be hidden for arrays.

     *

     * @var array

     */

    protected $hidden = [

        'password', 'remember_token',

    ];



    /**

     * The attributes that should be cast to native types.

     *

     * @var array

     */

    protected $casts = [

        'email_verified_at' => 'datetime',

    ];

}

