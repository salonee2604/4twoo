<?php

namespace App\Imports;

use App\Post;
use Illuminate\Support\Str;
use Validator, DB;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Session;

class PostImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row) {	
            
            if (!isset($row[0]) && empty($row[0])) {
                return null;
            }
           
            $title = $row[0];
            $text = $row[1];
            $tag = $row[2];
            $category = $row[3];
         
            $data = array(
                'title'    => $title,
                'text'     => $text,
                'tags'      => $tag,
                'categories' => $category,
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s')
            );

            DB::table('posts_list')->insert($data);
            return;
    }
     /**
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }

}
