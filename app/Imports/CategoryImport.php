<?php

namespace App\Imports;

use App\Post;
use Illuminate\Support\Str;
use Validator, DB;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Session;

class CategoryImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row) {	
            
        if (!isset($row[0]) && empty($row[0])) {
            return null;
        }
       
        $category = $row[0];
        $data = array(
            'category'    => $category,
            'status' => 1,
            'created_at' => date('Y-m-d H:i:s')
        );

        DB::table('post_category')->insert($data);
        return;
    }
     /**
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }

}
