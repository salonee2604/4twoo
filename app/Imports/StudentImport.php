<?php

namespace App\Imports;

use App\User;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Session;

class StudentImport implements ToModel, WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row) {	

            if (!isset($row[0]) && empty($row[0])) {
                return null;
            }

            if (!empty($row[2]) && User::where('username',$row[2])->exists()) {

                
                if (!empty(Session::get('error_arr'))) {
                    $parr = Session::get('error_arr');
                    $error_msg[] = 'username "'.$row[2].'" already registered.';
                    $error_arr = array_merge($parr,$error_msg);
                } else {
                    $error_arr[] = 'username "'.$row[2].'" already registered.';
                }
                Session::put('error_arr', $error_arr);
                return null;

            }

            if (!empty($row[3]) && User::where('email',$row[3])->exists()) {
                
                if (!empty(Session::get('error_arr'))) {
                    $parr = Session::get('error_arr');
                    $error_msg[] = 'email "'.$row[3].'" already registered.';
                    $error_arr = array_merge($parr,$error_msg);
                } else {
                    $error_arr[] = 'email "'.$row[3].'" already registered.';
                }
                Session::put('error_arr', $error_arr);
                return null;

            }

            if(!User::where('email',$row[2])->orWhere('username', $row[1])->exists()) {
                $name = $row[0];
                $name_arr = explode(' ', $name);
                $first_name = !empty($name_arr[0]) ? $name_arr[0] : '';
                $last_name = !empty($name_arr[1]) ? $name_arr[1] : '';
                $strpassword = Str::random(6);
                return new User([
                    'first_name'     => $first_name,
                    'last_name'     => $last_name,
                    'username'    => $row[2],
                    'email'    => $row[3], 
                    'password' => (!empty($row[4]) ? bcrypt($row[4]) : bcrypt($strpassword)),
                    'grade' => $row[5], 
                    'curriculum' => $row[6], 
                    'role_id' =>3,
                    'temp_password' => (!empty($row[4]) ? $row[4] : $strpassword),
                ]);
            }
    }
    public function startRow(): int
    {
        return 2;
    }

}
