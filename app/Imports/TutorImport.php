<?php

namespace App\Imports;

use App\User;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class TutorImport implements ToModel,WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row){
        
        if (!isset($row[0]) && empty($row[0])) {
              return null;
          }

         if (!empty($row[1]) && User::where('username',$row[1])->exists()) {
                
             if (!empty(Session::get('error_arr'))) {
                 $parr = Session::get('error_arr');
                 $error_msg[] = 'username "'.$row[1].'" already registered.';
                 $error_arr = array_merge($parr,$error_msg);
             } else {
                 $error_arr[] = 'username "'.$row[1].'" already registered.';
             }
             Session::put('error_arr', $error_arr);
             return null;

         }

         if (!empty($row[2]) && User::where('email',$row[2])->exists()) {
             
             if (!empty(Session::get('error_arr'))) {
                 $parr = Session::get('error_arr');
                 $error_msg[] = 'email "'.$row[2].'" already registered.';
                 $error_arr = array_merge($parr,$error_msg);
             } else {
                 $error_arr[] = 'email "'.$row[2].'" already registered.';
             }
             Session::put('error_arr', $error_arr);
             return null;

         } 
           
        if(!User::where('email',$row[2])->orWhere('username', $row[1])->exists()) {
            
            $name = $row[0];
            $name_arr = explode(' ', $name);
            $first_name = !empty($name_arr[0]) ? $name_arr[0] : '';
            $last_name = !empty($name_arr[1]) ? $name_arr[1] : '';
            $strpassword = str_random(6);
            return new User([
                'first_name'=> $first_name,
                'last_name' => $last_name,
                'username' => $row[2],
                'email'    => $row[3], 
                'password' => (!empty($row[4]) ? bcrypt($row[4]) : bcrypt($strpassword)),
                'temp_password' => (!empty($row[4]) ? $row[4] : $strpassword),
                'grade' => $row[5], 
                'curriculum' => $row[6], 
                'role_id' =>2
            ]);
        }
    }
     public function startRow(): int
    {
        return 2;
    }
}
